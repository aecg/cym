<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<body>
<div class="contenido">

    <h2 style="font-family:Avenir Black; text-align:center;">RESTABLECER CONTRASEÑA DE USUARIO</h2>
    <p style="font-family:Helvetica;">
        Hola <strong> ${nombre} ${apellido} </strong>,<br><br>

        Has solicitado recuperar tu contraseña del BackOffice de CYM.<br><br>

        Si quieres cambiar tu contraseña accede al siguiente enlace:<br><br>

        ${url}<br><br>

        En caso contrario, simplemente elimina este correo de tu bandeja de entrada y no se realizará ningún cambio.<br><br>


        Saludos,<br>
        El equipo de CYM

    </p>

</div>
</body>
</html>