<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<body>
<div class="contenido">

    <h2 style="font-family:Avenir Black; text-align:center;">CYM-AR Credenciales de Seguridad</h2>
    <p style="font-family:Helvetica;">
        <#if (nombre)??>
            Hola <strong>${nombre} ${apellido}</strong>,<br><br>
        <#else>
            Hola,<br><br>
        </#if>

        Ha solicitado recuperar sus credenciales de seguridad de la aplicación de CYM-AR Servicios Turísticos.<br><br>

        <#if (password)??>
            Su Contraseña es: <h1>${password}</h1><br>
        <#else>
            Usted inició sesión usando su usuario de Google<br>
        </#if>

        <br>En caso contrario, ignore y elimine este correo de su bandeja de entrada y no se realizará ningún cambio.<br><br>


        Saludos,<br>
        El equipo de CYM-AR
    </p>

</div>
</body>
</html>