package net.softclear.cym.backend.web.rest;



import net.softclear.cym.backend.model.jpa.Tracking;
import net.softclear.cym.backend.model.jpa.User;
import net.softclear.cym.backend.service.TrackingService;
import net.softclear.cym.backend.service.UserService;
import net.softclear.cym.backend.web.rest.response.ListTrackingResponse;
import net.softclear.cym.backend.web.rest.response.TrackingResponse;
import net.softclear.cym.backend.web.rest.response.UserBookRequest;
import net.softclear.cym.backend.web.rest.response.UserResponse;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.List;



@RestController
@RequestMapping("/trackings*")
public class TrackingRestController
{
    //region Atributos

    @Autowired
    private ModelMapper     modelMapper;
    @Autowired
    private TrackingService service;

    //endregion

    //region Servicios


    /**
     * Nombre:                  getUserByUserName
     * Descripcion:             Servicio que consulta los datos del usuario dado su nombre de usuario
     * @version 1.0
     * @author guzmle
     * @since 07/03/16
     *
     * @param username nombre de usuario
     * @return datos del usuario
     */
    @RequestMapping( value = "/{username}", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    ListTrackingResponse getTrackingByUserName( @PathVariable String username)
    {
        ListTrackingResponse response = new ListTrackingResponse();
        List<Tracking> trackings = service.getTrackingByUserName(username);

        Type listDTOType = new TypeToken<List<TrackingResponse>>(){}.getType();
        List<TrackingResponse> list = modelMapper.map(trackings, listDTOType);

        response.setUsername( username );
        response.setTrackings( list );
        return response;

    }



    /**
     * Nombre:                  saveUserBook
     * Descripcion:             Metodo que almacena la relacion del usuario con el libro
     * @version                 1.0
     * @author                  guzmle
     * @since                   29/03/16
     *
     * @param request datos del usuario y del libro
     */
    @RequestMapping ( method = RequestMethod.PUT, consumes = "application/json")
    @ResponseStatus( HttpStatus.OK )
    public void saveTrackingUser (@RequestBody ListTrackingResponse request)
    {
        Type listDTOType = new TypeToken<List<Tracking>>(){}.getType();
        List<Tracking> list = modelMapper.map(request.getTrackings(), listDTOType);
        service.saveTrackingUser( request.getUsername(), list );
    }
    //endregion
}
