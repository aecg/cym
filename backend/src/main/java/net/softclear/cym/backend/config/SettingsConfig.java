package net.softclear.cym.backend.config;



import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SettingsConfig
{
    /**
     * Ruta de las imagenes
     */
    @Value ( "${config.pathImages}")
    public String pathImageAbsolute;

    @Value ( "${config.booksFolder}")
    public String booksFolder;

    @Value ( "${config.msadsFolder}")
    public String msadsFolder;

    @Value ( "${config.convertedFolder}")
    public String convertedFolder;

    @Value ( "${config.adminUser}")
    public String adminUser;
}
