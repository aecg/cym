package net.softclear.cym.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "book_mustseeanddo")
@Setter
@Getter
public class BookMustSeeAndDo implements Serializable
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;


    @ManyToOne()
    @JoinColumn( name = "book_id" )
    private Book book;


    @ManyToOne()
    @JoinColumn( name = "mustseeanddo_id" )
    private MustSeeAndDo mustSeeAndDo;
}
