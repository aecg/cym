package net.softclear.cym.backend.model.jpa;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.CascadeType;
import java.io.Serializable;
import java.util.List;


/**
 * Sistema:                 CyM.
 *
 * Nombre:                  State
 * Descripcion:             Clase que emula la entidad state dentro del sistema
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/9/2015
 *
 */
@Entity
@Table(name="state")
@Setter
@Getter
public class State implements Serializable
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;

    @Column( name = "name", nullable = false )
    private String name;

    @ManyToOne()
    @JoinColumn( name = "country_id" )
    private Country country;

    @OneToMany( mappedBy = "state" , cascade = CascadeType.ALL)
    private List<StateMustSeeAndDoRecommended> recommendeds;

}
