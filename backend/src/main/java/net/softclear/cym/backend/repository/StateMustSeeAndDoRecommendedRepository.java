package net.softclear.cym.backend.repository;

import net.softclear.cym.backend.model.jpa.StateMustSeeAndDoRecommended;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


/**
 * Sistema:                 CyM.
 *
 * Nombre:                  BusinessTypeRepository
 * Descripcion:             Contrato para todas las operaciones de la base de datos que involucran la entidad
 * businessType
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public interface StateMustSeeAndDoRecommendedRepository extends JpaRepository<StateMustSeeAndDoRecommended, Long>
{
    List<StateMustSeeAndDoRecommended> findByStateId(long id);
}
