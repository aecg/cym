package net.softclear.cym.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  Authority
 * Descripcion:             Clase que representa la entidad autoridad que sirve para almacenar la informacion de los
 * privilegios de los usuarios.
 *
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/21/2015
 *
 */
@Entity
@Table(name = "book")
@Setter
@Getter
public class Book implements Serializable
{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;

    @Column( nullable = false )
    private String name;


    @Column( nullable = false, length = 1000 )
    private String description;


    @Temporal( TemporalType.TIMESTAMP )
    @Column( name = "last_update", nullable = false )
    private Date lastUpdate;

    @Lob
    @Column( name = "image", nullable = false, columnDefinition="LONGBLOB" )
    private byte[] image;

    @Column( nullable = true )
    private String imagePath;
}
