package net.softclear.cym.backend.repository;

import net.softclear.cym.backend.model.jpa.BookMustSeeAndDo;
import net.softclear.cym.backend.model.jpa.LocationBook;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookMustSeeAndDoRepository extends JpaRepository<BookMustSeeAndDo, Long> {
    List<BookMustSeeAndDo> findByBookId(long id);
}
