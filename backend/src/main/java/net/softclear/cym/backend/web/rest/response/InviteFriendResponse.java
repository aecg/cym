package net.softclear.cym.backend.web.rest.response;



import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  InviteFriendResponse
 * Descripcion:             Clase que modela la respuestas de los objeto en json
 * @version                 1.0
 * @author                  martin
 * @since                   12/14/16
 *
 */
@Data
@JsonInclude( JsonInclude.Include.NON_NULL)
public class InviteFriendResponse
{
    boolean success;
}
