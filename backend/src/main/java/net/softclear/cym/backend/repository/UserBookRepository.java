package net.softclear.cym.backend.repository;



import net.softclear.cym.backend.model.jpa.UserBook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  BusinessTypeRepository
 * Descripcion:             Contrato para todas las operaciones de la base de datos que involucran la entidad
 * businessType
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public interface UserBookRepository extends JpaRepository<UserBook, Long>
{
    /**
     * Nombre:                  findByUserUsername
     * Descripcion:             Metodo que consulta los libros de un usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   21/03/16
     *
     * @param username nombre del usuario
     * @return lista de libros que tienen el usuario
     */
    List<UserBook> findByUserUsername( String username );


    @Query("SELECT t FROM UserBook t where t.user.username = :username and t.book.id = :book_id")
    UserBook findByUserAndByBook(@Param("username") String username, @Param( "book_id" ) long bookId );
}
