package net.softclear.cym.backend.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import net.softclear.cym.backend.service.OAuthService;
import net.softclear.cym.backend.web.provider.UserProvider;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  UserInterceptor
 * Descripcion:             Interceptr de las llamadas
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Component
public class UserInterceptor extends HandlerInterceptorAdapter
{
	@Autowired
	private OAuthService oauthService;
	@Autowired
	private UserProvider userProvider;


    /**
     * Nombre:                  preHandle.
     * Descripcion:             Metodo que se ejcuta antes de ser atendida la peticion http de los servicios
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @param request datos de la peticion
     * @param response datos de la posible respuesta
     * @param handler objeto con la informacion del evento
     * @return true si se ejecuta false sino
     * @throws Exception
     */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception
    {
        String authHeader;
        String tokenValue;

        authHeader = request.getHeader("Authorization");
		if (authHeader != null)
        {
            tokenValue = authHeader.replace("Bearer", "").trim();
			userProvider.setUser(oauthService.getUserByToken(tokenValue));
		}

        return true;
	}
}
