package net.softclear.cym.backend.web.rest.error;

import net.softclear.cym.backend.exception.ExistException;
import net.softclear.cym.backend.exception.InvalidDataException;
import net.softclear.cym.backend.exception.NotFoundException;
import org.springframework.stereotype.Component;

import net.softclear.cym.backend.exception.ServiceException;
import net.softclear.cym.backend.web.rest.error.to.ErrorResponse;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  ErrorFactoryImpl
 * Descripcion:             Clase que transforma una excepcion en un error entendible
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Component
public class ErrorFactoryImpl implements ErrorFactory
{

	@Override
	public ErrorResponse exceptionFrom(Exception exception)
    {
        String message;
		ErrorCode code = ErrorCode.GENERIC_ERROR;
        message = exception.getMessage();

        if (exception instanceof ServiceException)
            code = ErrorCode.SERVICE_ERROR;

        if (exception instanceof NotFoundException)
            code = ErrorCode.NOT_FOUND;

        if (exception instanceof ExistException)
            code = ErrorCode.USER_EXIST;

        if (exception instanceof InvalidDataException)
            code = ErrorCode.INVALID_DATA;
		
		return new ErrorResponse(code.getValue(), message);
	}

}
