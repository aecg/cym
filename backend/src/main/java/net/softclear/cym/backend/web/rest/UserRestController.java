package net.softclear.cym.backend.web.rest;



import net.softclear.cym.backend.model.jpa.User;
import net.softclear.cym.backend.service.UserService;
import net.softclear.cym.backend.web.rest.response.DataUserResponse;
import net.softclear.cym.backend.web.rest.response.UserResponse;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/user*")
public class UserRestController
{
    //region Atributos

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private UserService userService;

    //endregion

    //region Servicios


    /**
     * Nombre:                  getUserByUserName
     * Descripcion:             Servicio que consulta los datos del usuario dado su nombre de usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   07/03/16
     *
     * @param username nombre de usuario
     * @return datos del usuario
     */
    @RequestMapping(value = "/{username}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody UserResponse getUserByUserName(@PathVariable String username)
    {
        User user = userService.findUserByUserName(username);
        UserResponse response = modelMapper.map(user, UserResponse.class);
        return response;

    }


    /**
     * Nombre:                  registerUser
     * Descripcion:             Metodo que registra los datos de un usuario en la base de datos
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/03/16
     *
     */
    @RequestMapping( method = RequestMethod.POST, consumes = "application/json")
    @ResponseStatus( HttpStatus.OK )
    public void registerUser(@RequestBody UserResponse request)
    {

        User user = modelMapper.map( request, User.class );
        userService.registerUser( user );
    }


    /**
     * Nombre:                  updateUser
     * Descripcion:             Metod que modifica los datos de un usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/03/16
     *
     * @param request datos del usuario a modificar
     */
    @RequestMapping( method = RequestMethod.PUT, consumes = "application/json")
    @ResponseStatus( HttpStatus.OK )
    public void updateUser(@RequestBody DataUserResponse request)
    {

        User user = modelMapper.map(request, User.class);
        userService.updateUser( user );
    }


    /**
     * Nombre:                  updateUser
     * Descripcion:             Metod que modifica los datos de un usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/03/16
     *
     * @param request datos del usuario a modificar
     */
    @RequestMapping( value = "/retrieve", method = RequestMethod.POST, consumes = "application/json")
    @ResponseStatus( HttpStatus.OK )
    public void retrievePassword(@RequestBody DataUserResponse request)
    {

        User user = modelMapper.map(request, User.class);
        userService.retrievePassword( user );
    }



    /**
     * Nombre:                  updateUser
     * Descripcion:             Metod que modifica los datos de un usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/03/16
     *
     * @param request datos del usuario a modificar
     */
    @RequestMapping( value = "/recover_pass", method = RequestMethod.POST, consumes = "application/json")
    @ResponseStatus( HttpStatus.OK )
    public void recoverPass(@RequestBody DataUserResponse request)
    {

        User user = modelMapper.map(request, User.class);
        userService.recoverPassword( user );
    }



    /**
     * Nombre:                  updateUser
     * Descripcion:             Metod que modifica los datos de un usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/03/16
     *
     * @param request datos del usuario a modificar
     */
    @RequestMapping( value = "/change-password", method = RequestMethod.PUT, consumes = "application/json")
    @ResponseStatus( HttpStatus.OK )
    public void changePassword(@RequestBody DataUserResponse request)
    {

        User user = modelMapper.map(request, User.class);
        userService.changePassword( user.getUsername(), user.getPassword() );
    }
    //endregion
}
