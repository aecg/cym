package net.softclear.cym.backend.web.rest.response;



import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  TravelResponse
 * Descripcion:             Metodo que modela la respuesta que se necesita para cuando se consulta travel
 * @version                 1.0
 * @author                  guzmle
 * @since                   08/03/16
 *
 */
@Data
@JsonInclude( JsonInclude.Include.NON_NULL)
public class CountryResponse
{
    private long    id;
    private String  name;
}
