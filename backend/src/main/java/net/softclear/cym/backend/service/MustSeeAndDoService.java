package net.softclear.cym.backend.service;

import net.softclear.cym.backend.model.jpa.*;
import net.softclear.cym.backend.util.MustSeeAndDoRecommendedTO;

import java.util.List;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  TravelService
 * Descripcion:             Clase que se encarga de todos los metodos que involucran la logica de negocio de travel
 * @version                 1.0
 * @author                  guzmle
 * @since                   07/03/16
 *
 */
public interface MustSeeAndDoService
{

    /**
     * Nombre:                  getByLocationId
     * Descripcion:             Metodo que consulta el mustSeeAndDo por id
     * @version                 1.0
     * @author                  guzmle
     * @since                   16/03/16
     *
     * @param id identificador de la localidad del los mustseeanddo
     * @return datos del mustseeanddo
     */
    List<MustSeeAndDo> getByLocationId( long id );


    /**
     * Nombre:                  getByLocationId
     * Descripcion:             Metodo que consulta el mustSeeAndDo por id
     * @version                 1.0
     * @author                  guzmle
     * @since                   16/03/16
     *
     * @param id identificador de la localidad del los mustseeanddo
     * @return datos del mustseeanddo
     */
    List<MustSeeAndDo> getByBook( long id );


    /**
     * Nombre:                  getDetailByBook
     * Descripcion:             Metodo que obtiene un listado de todos los
     *                          detalles de todos los must see and do de un libro
     * @version                 1.0
     *
     * @param bookid identificado del mustseeanddo
     * @return lista de detalles del mustseeanddo
     */
    List<MustSeeAndDoDetail> getDetailByBook(long bookid );

    /**
     * Nombre:                  getDetailByMustSeeAndDo
     * Descripcion:             Metodo que obtiene un listado de los detalles de un must see and do en particular
     * @version                 1.0
     *
     * @param mustseeanddoid identificado del mustseeanddo
     * @return lista de detalles del mustseeanddo
     */
    List<MustSeeAndDoDetail> getDetailByMustSeeAndDo(long mustseeanddoid );

    public MustSeeAndDoQR getQRCode( long id);
    public MustSeeAndDoAR getARCode( long id);

    /**
     * Nombre:                  saveMustSeeAndDo                                                    
     * Descripcion:             Metodo que guarda los datos de un mustseeanddo
     * @version                 1.0                                                                                          
     * @author                  guzmle
     * @since                   12/04/16                                                                               
     *
     * @param obj datos del mustseeanddo
     */
    void saveMustSeeAndDo( MustSeeAndDo obj, List<MustSeeAndDoDetail> list  );

    /**
     * Nombre:                  getCatalogs
     * Descripcion:             Metodo que consulta todos los catalogos del sistema
     * @version                 1.0
     * @author                  guzmle
     * @since                   18/04/16
     *
     * @return lista de catalogoss
     */
    List<Catalog> getCatalogs();

    MustSeeAndDoRecommendedTO getStateMustSeeAndDoRecommended(long id );

    MustSeeAndDo getMustSeeAndDo( long id );

    void optimizeImages();
}
