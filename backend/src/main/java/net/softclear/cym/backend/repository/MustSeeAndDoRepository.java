package net.softclear.cym.backend.repository;

import net.softclear.cym.backend.model.jpa.MustSeeAndDo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Sistema:                 CyM.
 *
 * Nombre:                  BusinessTypeRepository
 * Descripcion:             Contrato para todas las operaciones de la base de datos que involucran la entidad
 * businessType
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public interface MustSeeAndDoRepository extends JpaRepository<MustSeeAndDo, Long> {
    List<MustSeeAndDo> getMustSeeAndDoByLocationId(long id);

    @Query("SELECT B FROM BookMustSeeAndDo A, MustSeeAndDo B  where A.mustSeeAndDo.id = B.id and A.book.id = :id")
    List<MustSeeAndDo> getMustSeeAndDoByBook(@Param( "id" ) long id);
}
