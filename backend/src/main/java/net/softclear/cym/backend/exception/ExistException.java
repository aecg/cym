package net.softclear.cym.backend.exception;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  ServiceException
 * Descripcion:             Excepcion Personalizada de la aplicacion
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public class ExistException extends RuntimeException
{
	private static final long serialVersionUID = -1551291658825173144L;


    /**
     * Nombre:                  ServiceException.
     * Descripcion:             Constructor de la clase
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     */
	public ExistException()
    {

	}


    /**
     * Nombre:                  ServiceException.
     * Descripcion:             Constructor de la clase
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @param message mensaje de la excepcion
     */
	public ExistException( String message )
    {
		super(message);
	}


    /**
     * Nombre:                  ServiceException.
     * Descripcion:             Contructor de la clase
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @param message mensaje de la excepcion
     * @param throwable excepcion que ocasiona esta excepcion
     */
	public ExistException( String message, Throwable throwable )
    {
		super(message, throwable);
	}


    /**
     * Nombre:                  ServiceException.
     * Descripcion:             Constructor de la excepcion
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @param throwable excepcion que la ocasiona
     */
	public ExistException( Throwable throwable )
    {
		super(throwable);
	}
}
