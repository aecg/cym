package net.softclear.cym.backend.service.implementation;

import net.softclear.cym.backend.config.MessagesConfig;
import net.softclear.cym.backend.config.SettingsConfig;
import net.softclear.cym.backend.exception.NotFoundException;
import net.softclear.cym.backend.exception.ServiceException;
import net.softclear.cym.backend.model.jpa.*;
import net.softclear.cym.backend.repository.*;
import net.softclear.cym.backend.service.MustSeeAndDoService;
import net.softclear.cym.backend.util.MustSeeAndDoRecommendedTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

/**
 * Sistema:                 CyM.
 * <p/>
 * Nombre:                  UserServiceImpl
 * Descripcion:             Clase que implementa el contrato del servicio
 *
 * @author guzmle
 * @version 1.0
 * @since 11/30/2015
 */
@Service
@Transactional
public class MustSeeAndDoServiceImpl implements MustSeeAndDoService
{
    //region Atributos

    private static Logger logger = LoggerFactory.getLogger( MustSeeAndDoServiceImpl.class );
    @Autowired
    private MustSeeAndDoDetailRepository            repository;
    @Autowired
    private MustSeeAndDoRepository                  mustSeeAndDoRepository;
    @Autowired
    private CatalogRepository                       catalogRepository;
    @Autowired
    private MustSeeAndDoRealityRepository           mustSeeAndDoRealityRepository;
    @Autowired
    private MessagesConfig                          messagesConfig;
    @Autowired
    private MustSeeAndDoQRRepository                mustSeeAndDoQRRepository;
    @Autowired
    private MustSeeAndDoARRepository                mustSeeAndDoARRepository;
    @Autowired
    private StateMustSeeAndDoRecommendedRepository  stateMustSeeAndDoRecommendedRepository;
    @Autowired
    private BookMustSeeAndDoRepository bookMustSeeAndDoRepository;
    @Autowired
    private SettingsConfig settingsConfig;

    String GIF = "gif";
    String PNG = "png";
    String JPG = "jpg";
    String BMP = "bmp";
    String JPEG = "jpeg";

    //endregion

    //region Metodos


    @Override
    public List<MustSeeAndDoDetail> getDetailByBook(long bookid )
    {

        List<MustSeeAndDoDetail> response = new ArrayList<MustSeeAndDoDetail>();
        try
        {
            List<MustSeeAndDo> mustSeeAndDos = mustSeeAndDoRepository.getMustSeeAndDoByBook( bookid );
            for (MustSeeAndDo mustSeeAndDo : mustSeeAndDos) {
                List<MustSeeAndDoDetail> mustSeeAndDoDetails = repository.findByMustseeanddoId( mustSeeAndDo.getId() );
                response.addAll(mustSeeAndDoDetails);
            }

            if ( response.size() < 0 ) {
                throw new NotFoundException(messagesConfig.ERROR_NO_DATA_FOUND);
            }

        }
        catch ( Exception e )
        {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }

        return response;
    }

    @Override
    public List<MustSeeAndDoDetail> getDetailByMustSeeAndDo(long mustseeanddoid ) {

        List<MustSeeAndDoDetail> response = new ArrayList<MustSeeAndDoDetail>();
        try {
            List<MustSeeAndDoDetail> mustSeeAndDoDetails = repository.findByMustseeanddoId( mustseeanddoid );
            response.addAll(mustSeeAndDoDetails);

            if ( response.size() < 0 ) {
                throw new NotFoundException(messagesConfig.ERROR_NO_DATA_FOUND);
            }
        }
        catch ( Exception e ) {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }

        return response;
    }

    @Override
    public MustSeeAndDoQR getQRCode(long id) {
        MustSeeAndDoQR mustSeeAndDoQR = mustSeeAndDoQRRepository.findByMustseeanddoId(id);
        if (mustSeeAndDoQR != null) {
            return mustSeeAndDoQR;
        }
        return new MustSeeAndDoQR();
        //throw new NotFoundException(messagesConfig.ERROR_NO_DATA_FOUND);
    }

    @Override
    public MustSeeAndDoAR getARCode(long id) {
        MustSeeAndDoAR mustSeeAndDoAR = mustSeeAndDoARRepository.findByMustseeanddoId(id);
        if (mustSeeAndDoAR != null) {
            return mustSeeAndDoAR;
        }
        return new MustSeeAndDoAR();
        //throw new NotFoundException(messagesConfig.ERROR_NO_DATA_FOUND);
    }

    @Override
    public void saveMustSeeAndDo( MustSeeAndDo obj, List<MustSeeAndDoDetail> list )
    {

        List<MustSeeAndDoReality> realities = mustSeeAndDoRealityRepository.findByMustseeanddoId( obj.getId() );
        mustSeeAndDoRealityRepository.delete( realities );

        if ( obj.getRealities() != null && obj.getRealities().size() > 0 )
        {
            for ( MustSeeAndDoReality reality : obj.getRealities() )
            {
                reality.setMustseeanddo( obj );
            }
        }

        MustSeeAndDoQR mustSeeAndDoQR = mustSeeAndDoQRRepository.findByMustseeanddoId( obj.getId() );
        if (mustSeeAndDoQR != null) {
            mustSeeAndDoQRRepository.delete(mustSeeAndDoQR);
        }
        MustSeeAndDoQR msadQR = obj.getQrcode();
        if ( msadQR != null ) {
            msadQR.setMustseeanddo(obj);
        }

        MustSeeAndDoAR mustSeeAndDoAR = mustSeeAndDoARRepository.findByMustseeanddoId( obj.getId() );
        if (mustSeeAndDoAR != null) {
            mustSeeAndDoARRepository.delete(mustSeeAndDoAR);
        }
        MustSeeAndDoAR msadAR = obj.getArcode();
        if ( msadAR != null ) {
            msadAR.setMustseeanddo(obj);
        }

        obj = mustSeeAndDoRepository.save( obj );

        String imageDirectory = settingsConfig.pathImageAbsolute + File.separator + settingsConfig.msadsFolder + File.separator;
        String imageName = obj.getId() + ".png";

        if (obj.getImage() != null) {
            // salvando imagen
            try {
                byte[] imageByteArray = obj.getImage();

                OutputStream imageOutFile = new FileOutputStream(imageDirectory + imageName);
                imageOutFile.write(imageByteArray);
                imageOutFile.close();
            }
            catch (Exception e) {
                throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
            }

            obj.setImagePath(imageName);
            obj = mustSeeAndDoRepository.save( obj );
        }
        else {
            // eliminando imagen
            try {
                File file = new File(imageDirectory + imageName);
                file.delete();
            }
            catch(Exception e) {
                e.printStackTrace();
            }
            obj.setImagePath(null);
            obj = mustSeeAndDoRepository.save( obj );
        }

        if ( list != null )
        {
            repository.delete( repository.findByMustseeanddoId( obj.getId() ) );
            for ( MustSeeAndDoDetail must : list ) {
                must.setMustseeanddo( obj );
            }

            repository.save( list );
        }
    }


    @Override
    public List<Catalog> getCatalogs()
    {

        List<Catalog> response;
        try
        {
            response = catalogRepository.findAllByOrderByNameAsc( );
        }
        catch ( Exception e )
        {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }

        return response;

    }


    @Override
    public List<MustSeeAndDo> getByBook( long id )
    {

        List<MustSeeAndDo> response = new ArrayList<>();
        try
        {

            List<BookMustSeeAndDo> bookMustSeeAndDos = bookMustSeeAndDoRepository.findByBookId( id );

            for(BookMustSeeAndDo bookMustSeeAndDo : bookMustSeeAndDos ) {
                response.add(bookMustSeeAndDo.getMustSeeAndDo());
            }

        }
        catch ( Exception e )
        {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }

        return response;
    }


    @Override
    public List<MustSeeAndDo> getByLocationId( long id )
    {

        List<MustSeeAndDo> response;
        try {
            if(id > 0) {
                response = mustSeeAndDoRepository.getMustSeeAndDoByLocationId(id);
            } else {
                response = mustSeeAndDoRepository.findAll();
            }
        }
        catch ( Exception e )
        {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }

        return response;
    }

    @Override
    public MustSeeAndDoRecommendedTO getStateMustSeeAndDoRecommended(long id ) {

        MustSeeAndDoRecommendedTO response = new MustSeeAndDoRecommendedTO();

        try {
            List<StateMustSeeAndDoRecommended> stateMustSeeAndDoRecommendeds = stateMustSeeAndDoRecommendedRepository.findByStateId( id );

            if ( stateMustSeeAndDoRecommendeds.size() < 0 ) {
                throw new NotFoundException(messagesConfig.ERROR_NO_DATA_FOUND);
            }
            response.setStateMustSeeAndDoRecommendeds(stateMustSeeAndDoRecommendeds);

            List<MustSeeAndDo> mustSeeAndDos = new ArrayList<>();
            List<MustSeeAndDoDetail> mustSeeAndDoDetailsList = new ArrayList<MustSeeAndDoDetail>();

            for (StateMustSeeAndDoRecommended stateMustSeeAndDoRecommended: stateMustSeeAndDoRecommendeds) {
                MustSeeAndDo mustSeeAndDo = mustSeeAndDoRepository.findOne(stateMustSeeAndDoRecommended.getMustSeeAndDo().getId());
                mustSeeAndDos.add(mustSeeAndDo);

                List<MustSeeAndDoDetail> mustSeeAndDoDetails = repository.findByMustseeanddoId( mustSeeAndDo.getId() );
                mustSeeAndDoDetailsList.addAll(mustSeeAndDoDetails);
            }
            response.setMustSeeAndDos(mustSeeAndDos);
            response.setMustSeeAndDoDetails(mustSeeAndDoDetailsList);
        }
        catch ( Exception e ) {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }

        return response;
    }

    @Override
    public MustSeeAndDo getMustSeeAndDo( long id ) {
        MustSeeAndDo mustSeeAndDo = mustSeeAndDoRepository.findOne(id);
        return mustSeeAndDo;
    }

    @Override
    public void optimizeImages() {
        String imageDirectory  = settingsConfig.pathImageAbsolute + File.separator +
                settingsConfig.msadsFolder + File.separator;
        String optimizedmagesDirectory = settingsConfig.pathImageAbsolute + File.separator +
                settingsConfig.msadsFolder + File.separator +
                settingsConfig.convertedFolder + File.separator;
        final File dir = new File(optimizedmagesDirectory);
        for (final File imgFile : dir.listFiles()) {

            if(acceptOptimizedImage(imgFile)){
                String imageName = imgFile.getName();
                String imageId = imageName.replaceFirst("[.][^.]+$", "");

                MustSeeAndDo mustSeeAndDo = mustSeeAndDoRepository.findOne(Long.parseLong(imageId));

                String oldImage = mustSeeAndDo.getImagePath();
                File oldImageFile = new File(imageDirectory + oldImage);
                oldImageFile.delete();

                byte[] imageByteArray = readContentIntoByteArray(imgFile);
                mustSeeAndDo.setImage(imageByteArray);
                mustSeeAndDo.setImagePath(imageName);
                mustSeeAndDoRepository.save(mustSeeAndDo);

                // salvando imagen
                try {
                    OutputStream imageOutFile = new FileOutputStream(imageDirectory + imageName);
                    imageOutFile.write(imageByteArray);
                    imageOutFile.close();
                } catch (Exception e) {
                    throw new ServiceException(messagesConfig.ERROR_GENERIC, e);
                }
            }
        }
    }

    private boolean acceptOptimizedImage(File file) {
        if(file != null) {
            if(file.isDirectory()) {
                return false;
            }
            String extension = getExtension(file);
            if(extension != null && isSupported(extension)) {
                return true;
            }
        }
        return false;
    }

    private String getExtension(File file) {
        if(file != null) {
            String filename = file.getName();
            int dot = filename.lastIndexOf('.');
            if(dot > 0 && dot < filename.length()-1) {
                return filename.substring(dot + 1).toLowerCase();
            }
        }
        return null;
    }

    private boolean isSupported(String ext) {
        return ext.equalsIgnoreCase(GIF) || ext.equalsIgnoreCase(PNG) ||
                ext.equalsIgnoreCase(JPG) || ext.equalsIgnoreCase(BMP) ||
                ext.equalsIgnoreCase(JPEG);
    }

    private static byte[] readContentIntoByteArray(File file) {
        FileInputStream fileInputStream = null;
        byte[] bFile = new byte[(int) file.length()];
        try {
            RandomAccessFile f = new RandomAccessFile(file, "r");
            f.readFully(bFile);
        }
        catch (Exception e) { }
        return bFile;
    }
}
