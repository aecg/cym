package net.softclear.cym.backend.web.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * Sistema:                 CyM.
 *
 * Nombre:                  InviteFriendRequest
 * Descripcion:             Clase que modela la petición json para invitar a un amigo a descargar la app
 * @version                 1.0
 * @author                  martin
 * @since                   12/14/16
 *
 */
@Data
@JsonInclude( JsonInclude.Include.NON_NULL)
public class InviteFriendRequest
{
    private User user;
    private List<String> emailFriends;
}

@Data
class User
{
    private String username;
    private String name;
    private String lastname;
}