package net.softclear.cym.backend.config;



import net.softclear.cym.backend.model.jpa.*;
import net.softclear.cym.backend.web.rest.response.*;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Date;



/**
 * Sistema:                 CyM.
 * <p/>
 * Nombre:                  BeanConfig Descripcion:             Clase de configuracion
 *
 * @author guzmle
 * @version 1.0
 * @since 11/30/2015
 */
@Configuration
public class BeanConfig
{
    /**
     * Nombre:                  modelMapper. Descripcion:             Metodo que obtiene el wraper de las entidades
     *
     * @return Objeto Mapeado
     *
     * @version 1.0
     * @author guzmle
     * @since 12/21/2015
     */
    @Bean
    public ModelMapper modelMapper()
    {
        PropertyMap<User, UserResponse> userMap = new PropertyMap<User, UserResponse>()
        {
            protected void configure()
            {
                map().setName( source.getDataUser().getName() );
                map().setEmail( source.getDataUser().getEmail() );
                map().setCreated_at( source.getCreatedAt().getTime() );
                map().setLastname( source.getDataUser().getLastName() );
                map().setLogin_state( source.getEnabled() ? 1 : 0 );
                map().setUsername( source.getUsername() );
                map().setPassword( source.getPassword() );
            }
        };

        PropertyMap<UserResponse, User> userRequestMap = new PropertyMap<UserResponse, User>()
        {
            protected void configure()
            {
                map().getDataUser().setName( source.getName() );
                map().getDataUser().setEmail( source.getUsername() );
                map().getDataUser().setLastName( source.getLastname() );
                map().setUsername( source.getUsername() );
            }
        };


        PropertyMap<CuePoint, CuePointResponse> cuepointMap = new PropertyMap<CuePoint, CuePointResponse>()
        {
            protected void configure()
            {
                map().setUnique_id( source.getTracking().getUniqueId() );
                map().setDate( source.getDate().getTime() );
                map().setGeographic_position( source.getGeographicPosition() );
            }
        };


        PropertyMap<CuePointResponse, CuePoint> cuepointRequestMap = new PropertyMap<CuePointResponse, CuePoint>()
        {
            protected void configure()
            {
                map().setDate( new Date(source.getDate()) );
                map().setGeographicPosition( source.getGeographic_position() );
            }
        };


        PropertyMap<Tracking, TrackingResponse> trackingMap = new PropertyMap<Tracking, TrackingResponse>()
        {
            protected void configure()
            {
                map().setUnique_id( source.getUniqueId() );
                map().setCreated_at( source.getCreatedAt().getTime() );
            }
        };

        PropertyMap<TrackingResponse, Tracking> trackingRequestMap = new PropertyMap<TrackingResponse, Tracking>()
        {
            protected void configure()
            {
                map().setUniqueId( source.getUnique_id() );
                map().setCreatedAt( new Date(source.getCreated_at()) );
            }
        };


        PropertyMap<Travel, TravelResponse> travelMap = new PropertyMap<Travel, TravelResponse>()
        {
            protected void configure()
            {
                map().setUnique_id( source.getUniqueId() );
                map().setDateArrival( source.getDateArrival().getTime() );
                map().setDateGoing( source.getDateGoing().getTime() );
                map().setCountry_id( source.getCountry().getId() );
            }
        };

        PropertyMap<TravelResponse, Travel> travelRequestMap = new PropertyMap<TravelResponse, Travel>()
        {
            protected void configure()
            {
                map().setUniqueId( source.getUnique_id() );
                map().setDateArrival( new Date( source.getDateArrival() ) );
                map().setDateGoing( new Date( source.getDateGoing() ) );
                map().getCountry().setId( source.getCountry_id() );
                map().setCreatedAt( new Date(  ) );
            }
        };


        PropertyMap<TravelMustSeeAndDo, TravelMustSeeAndDoResponse> travelMustSeeAndDoMap = new PropertyMap<TravelMustSeeAndDo,
                TravelMustSeeAndDoResponse>()
        {
            protected void configure()
            {
                map().setMustseeanddo_id( source.getMustSeeAndDo().getId() );
            }
        };


        PropertyMap<TravelMustSeeAndDoResponse, TravelMustSeeAndDo> travelMustSeeAndDoRequestMap = new
                PropertyMap<TravelMustSeeAndDoResponse, TravelMustSeeAndDo>()
        {
            protected void configure()
            {
                map().getMustSeeAndDo().setId( source.getMustseeanddo_id() );
            }
        };


        PropertyMap<Book, BookSimpleResponse> booksSimpleMap = new PropertyMap<Book,
                BookSimpleResponse>()
        {
            protected void configure()
            {
                map().setLast_update( source.getLastUpdate().getTime() );
            }
        };

        PropertyMap<Book, BookResponse> booksMap = new PropertyMap<Book,
                BookResponse>()
        {
            protected void configure()
            {
                map().setLast_update( source.getLastUpdate().getTime() );
            }
        };


        PropertyMap<Destination, DestinationResponse> destinationMap = new PropertyMap<Destination,
                DestinationResponse>()
        {
            protected void configure()
            {
                map().setUnique_id( source.getTravel().getUniqueId() );
                map().setDate( source.getDate().getTime() );
                map().setTravel_id( source.getTravel().getId() );
                map().setLocation_id( source.getLocation().getId() );
                map().setState_id( source.getState().getId() );
            }
        };


        PropertyMap<DestinationResponse, Destination> destinationRequestMap = new PropertyMap<DestinationResponse,
                Destination>()
        {
            protected void configure()
            {
                map().setDate( new Date(source.getDate()) );
                map().getTravel().setId( source.getTravel_id() );
                map().getLocation().setId( source.getLocation_id() );
                map().getState().setId( source.getState_id() );
            }
        };



        PropertyMap<TravelPartner, TravelPartnerResponse> travelPartnerMap = new PropertyMap<TravelPartner,
                TravelPartnerResponse>()
        {
            protected void configure()
            {
                map().setUnique_id( source.getTravel().getUniqueId() );
                map().setTravel_id( source.getTravel().getId() );
                map().setUsername( source.getUser().getUsername() );

            }
        };



        PropertyMap<TravelPartnerResponse, TravelPartner> travelPartnerRequestMap = new PropertyMap<TravelPartnerResponse,
                TravelPartner>()
        {
            protected void configure()
            {
                map().getUser().setUsername( source.getUsername() );

            }
        };


        PropertyMap<LocationBook, LocationBookResponse> locationBookMap = new PropertyMap<LocationBook,
                LocationBookResponse>()
        {
            protected void configure()
            {
                map().setLocation_id( source.getLocation().getId() );
                map().setBook_id( source.getBook().getId() );

            }
        };



        PropertyMap<Location, LocationResponse> locationMap = new PropertyMap<Location,
                LocationResponse>()
        {
            protected void configure()
            {
                map().setGeographic_position( source.getGeographicPosition() );
                map().setState_id( source.getState().getId() );

            }
        };


        PropertyMap<State, StateResponse> stateMap = new PropertyMap<State,
                StateResponse>()
        {
            protected void configure()
            {
                map().setCountry_id( source.getCountry().getId() );

            }
        };


        PropertyMap<MustSeeAndDo, MustSeeAndDoResponse> mustSeeAndDoMap = new PropertyMap<MustSeeAndDo,
                MustSeeAndDoResponse>()
        {
            protected void configure()
            {
                map().setGeographic_position( source.getGeographicPosition() );
                map().setImage_reference( source.getImage() );
                map().setLocation_id( source.getLocation().getId() );
                map().setCountry_id( source.getLocation().getState().getCountry().getId() );
                map().setState_id( source.getLocation().getState().getId() );
                map().setCatalog_id( source.getCatalog().getId() );
                map().setAr( source.getAr() );
                map().setQr( source.getQr() );

                map().getQr_code().setTitle( source.getQrcode().getTitle() );
                map().getQr_code().setSummary( source.getQrcode().getSummary() );
                map().getQr_code().setId( source.getQrcode().getId() );
                map().getQr_code().setMustseeanddo_id( source.getQrcode().getMustseeanddo().getId() );

                map().getAr_code().setId( source.getArcode().getId() );
                map().getAr_code().setImage_binary( source.getArcode().getImageBinary() );
                map().getAr_code().setImage_display( source.getArcode().getImageDisplay() );
                map().getAr_code().setImage_preview( source.getArcode().getImagePreview() );
                map().getAr_code().setMustseeanddo_id( source.getArcode().getMustseeanddo().getId() );
                map().getAr_code().setIs_360( source.getArcode().getIs360() );
                map().getAr_code().setImage_360( source.getArcode().getImage360() );
            }
        };

        PropertyMap<MustSeeAndDoResponse, MustSeeAndDo> mustSeeAndDoMapRequest = new PropertyMap<MustSeeAndDoResponse,
                MustSeeAndDo>()
        {
            protected void configure()
            {
                map().setGeographicPosition( source.getGeographic_position() );
                map().setImage( source.getImage_reference() );
                map().getLocation().setId( source.getLocation_id() );
                map().getCatalog().setId( source.getCatalog_id() );
                map().setAr( source.getAr() );
                map().setQr( source.getQr() );

                map().getQrcode().setId( source.getQr_code().getId() );
                map().getQrcode().setTitle( source.getQr_code().getTitle() );
                map().getQrcode().setSummary( source.getQr_code().getSummary() );
                map().getQrcode().getMustseeanddo().setId( source.getQr_code().getMustseeanddo_id() );

                map().getArcode().setId( source.getAr_code().getId() );
                map().getArcode().setImageBinary( source.getAr_code().getImage_binary() );
                map().getArcode().setImageDisplay( source.getAr_code().getImage_display() );
                map().getArcode().setImagePreview( source.getAr_code().getImage_preview() );
                map().getArcode().getMustseeanddo().setId( source.getAr_code().getMustseeanddo_id() );
                map().getArcode().setImage360( source.getAr_code().getImage_360() );
                map().getArcode().setIs360( source.getAr_code().getIs_360() );
            }
        };


        PropertyMap<MustSeeAndDoDetail, MustSeeAndDoDetailResponse> mustSeeAndDoDetailMap = new PropertyMap<MustSeeAndDoDetail, MustSeeAndDoDetailResponse> ()
        {
            protected void configure()
            {
                map().setMustseeanddo_id( source.getMustseeanddo().getId() );
            }
        };


        PropertyMap<MustSeeAndDoDetailResponse, MustSeeAndDoDetail> mustSeeAndDoDetailMapRequest = new
                PropertyMap<MustSeeAndDoDetailResponse, MustSeeAndDoDetail> ()
        {
            protected void configure()
            {
                map().getMustseeanddo().setId( source.getMustseeanddo_id() );
            }
        };

        PropertyMap<MustSeeAndDoQR, MustSeeAndDoQRResponse> mustSeeAndDoQRMap = new PropertyMap<MustSeeAndDoQR, MustSeeAndDoQRResponse> ()
        {
            protected void configure()
            {
                map().setMustseeanddo_id( source.getMustseeanddo().getId() );
            }
        };

        PropertyMap<MustSeeAndDoAR, MustSeeAndDoARResponse> mustSeeAndDoARMap = new PropertyMap<MustSeeAndDoAR, MustSeeAndDoARResponse> ()
        {
            protected void configure()
            {
                map().setId( source.getId() );
                map().setMustseeanddo_id( source.getMustseeanddo().getId() );
                map().setImage_binary( source.getImageBinary() );
                map().setImage_preview( source.getImagePreview() );
                map().setImage_display( source.getImageDisplay() );
                map().setImage_360( source.getImage360() );
                map().setIs_360( source.getIs360() );
            }
        };

        PropertyMap<StateMustSeeAndDoRecommended, StateMustSeeAndDoRecommendedResponse> stateMustSeeAndDoRecommendedMap = new PropertyMap<StateMustSeeAndDoRecommended, StateMustSeeAndDoRecommendedResponse> ()
        {
            protected void configure()
            {
                map().setId( source.getId() );
                map().setMustseeanddo_id( source.getMustSeeAndDo().getId() );
                map().setState_id( source.getState().getId() );
            }
        };

        PropertyMap<UserBook, UserBookResponse> userBookResponse = new PropertyMap<UserBook,
                UserBookResponse> ()
        {
            protected void configure()
            {
                map().setBook_id( source.getBook().getId() );


            }
        };

        PropertyMap<MustSeeAndDo, MustSeeAndDoSimpleResponse> mustSeeAndDoSimpleMap = new PropertyMap<MustSeeAndDo,
                MustSeeAndDoSimpleResponse>()
        {
            protected void configure()
            {
                map().setId( source.getId() );
                map().setTitle( source.getTitle() );
                map().setSummary( source.getSummary() );
                map().setImagePath( source.getImagePath() );
            }
        };

        ModelMapper response = new ModelMapper();
        response.addMappings( userMap );
        response.addMappings( userRequestMap );
        response.addMappings( cuepointMap );
        response.addMappings( trackingMap );
        response.addMappings( cuepointRequestMap );
        response.addMappings( trackingRequestMap );
        response.addMappings( travelMap );
        response.addMappings( travelRequestMap );
        response.addMappings( travelMustSeeAndDoMap );
        response.addMappings( travelMustSeeAndDoRequestMap );
        response.addMappings( destinationMap );
        response.addMappings( destinationRequestMap );
        response.addMappings( travelPartnerMap );
        response.addMappings( travelPartnerRequestMap );
        response.addMappings( booksMap );
        response.addMappings( booksSimpleMap );
        response.addMappings( locationBookMap );
        response.addMappings( locationMap );
        response.addMappings( stateMap );
        response.addMappings( mustSeeAndDoMap );
        response.addMappings( mustSeeAndDoDetailMap );
        response.addMappings( userBookResponse );
        response.addMappings( mustSeeAndDoDetailMapRequest );
        response.addMappings( mustSeeAndDoMapRequest );
        response.addMappings( mustSeeAndDoQRMap );
        response.addMappings( mustSeeAndDoARMap );
        response.addMappings( stateMustSeeAndDoRecommendedMap );
        response.addMappings( mustSeeAndDoSimpleMap );
        return response;
    }
}
