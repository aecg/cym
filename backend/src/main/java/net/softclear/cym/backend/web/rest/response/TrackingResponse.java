package net.softclear.cym.backend.web.rest.response;



import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  TrackingResponse
 * Descripcion:             Clase que modela la respuesta para el seguimiento
 * @version                 1.0
 * @author                  guzmle
 * @since                   07/03/16
 *
 */
@Data
@JsonInclude( JsonInclude.Include.NON_NULL)
public class TrackingResponse
{
    private String                 name;
    private long                   created_at;
    private String                 unique_id;
    private int                    deleted;
    private List<CuePointResponse> cuepoints;
}
