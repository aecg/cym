package net.softclear.cym.backend.service;

import net.softclear.cym.backend.model.jpa.Travel;
import net.softclear.cym.backend.model.jpa.User;

import java.util.List;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  TravelService
 * Descripcion:             Clase que se encarga de todos los metodos que involucran la logica de negocio de travel
 * @version                 1.0
 * @author                  guzmle
 * @since                   07/03/16
 *
 */
public interface TravelService
{

    /**
     * Nombre:                  getByUserUsername
     * Descripcion:             Metodo que consulta todos los viajes de un usuario por nombre de usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   08/03/16
     *
     * @param username nombre de usuario
     * @return lista de viajes
     */
    List<Travel> getByUserUsername(String username);

    /**
     * Nombre:                  saveTravelUser
     * Descripcion:             Metodo que sincroniza los viajes de un usuario en el servidor
     * @version                 1.0
     * @author                  guzmle
     * @since                   29/03/16
     *
     * @param username nombre del usuario
     * @param list lista de viajes
     */
    void saveTravelUser( String username, List<Travel> list );
}
