package net.softclear.cym.backend.config;



import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;



/**
 * Sistema:                 CyM.
 * <p/>
 * Nombre:                  MessagesConfig Descripcion:             Clase que posee la defincion de todos los mensajes
 * de error del sistema
 *
 * @author guzmle
 * @version 1.0
 * @since 12/22/2015
 */
@Component
public class MessagesConfig
{
    //USUARIOS
    @Value( "${messages.user.NoNull}" )
    public String USER_NOT_NULL;
    @Value( "${messages.user.NameExist}" )
    public String USER_USERNAME_EXIST;
    @Value( "${messages.user.NameExist}" )
    public String USER_USERNAME_NOT_NULL;
    @Value( "${messages.user.passwordNoNull}" )
    public String USER_PASSWORD_NOT_NULL;
    @Value( "${messages.user.DataNoNull}" )
    public String USER_DATA_NOT_NULL;
    @Value( "${messages.user.emailNotNull}" )
    public String USER_EMAIL_NOT_NULL;
    @Value( "${messages.user.nameNotNull}" )
    public String USER_NAME_NOT_NULL;
    @Value( "${messages.user.lastNameNotNull}" )
    public String USER_LAST_NAME_NOT_NULL;
    @Value( "${messages.user.NotFound}" )
    public String USER_NOT_FOUND;
    @Value( "${messages.user.dataCorrupt}" )
    public String USER_DATA_CORRUPT;
    @Value( "${messages.messagingError}" )
    public String MESSAGIN_ERROR;

    //PAISES
    @Value( "${messages.country.nameInvalid}" )
    public String COUNTRY_NAME_INVALID;

    //ESTADOS
    @Value( "${messages.state.nameInvalid}" )
    public String STATE_NAME_INVALID;
    @Value( "${messages.state.countryInvalid}" )
    public String STATE_COUNTRY_INVALID;

    //LOCACIONES
    @Value( "${messages.location.nameInvalid}" )
    public String LOCATION_NAME_INVALID;
    @Value( "${messages.location.stateInvalid}" )
    public String LOCATION_STATE_INVALID;
    @Value( "${messages.location.positionInvalid}" )
    public String LOCATION_POSITION_INVALID;


    //ERRORES
    @Value( "${messages.error.genericError}" )
    public String ERROR_GENERIC;
    @Value( "${messages.error.savingUser}" )
    public String ERROR_SAVING_USER;
    @Value( "${messages.error.noDataFound}" )
    public String ERROR_NO_DATA_FOUND;
    @Value( "${messages.error.userNoRegistered}" )
    public String ERROR_USERNAME_NOT_REGISTERED;
    @Value( "${messages.error.savingUserTrackingError}" )
    public String ERROR_SYNCING_TRACKING_USER;
    @Value( "${messages.error.savingUserTravelError}" )
    public String ERROR_SYNCING_TRAVEL_USER;
    @Value( "${messages.error.savingUserError}" )
    public String ERROR_SYNCING_USER;
    @Value( "${messages.error.bookNotFound}" )
    public String ERROR_BOOK_NOT_FOUND;
}
