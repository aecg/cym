package net.softclear.cym.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;



@Entity
@Table(name="doc_attachment")
@Setter
@Getter
public class DocAttachment implements Serializable
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;


    @Column( name = "url_file", nullable = false )
    private String urlFle;


    @Column( name = "type", nullable = false )
    private String type;


    @ManyToOne()
    @JoinColumn( name = "document_id" )
    private Document document;
}
