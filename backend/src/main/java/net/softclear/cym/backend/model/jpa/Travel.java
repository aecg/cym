package net.softclear.cym.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;



/**
 * Sistema:                 Hermes
 *
 * Nombre:                  Travel
 * Descripcion:             Clase que representa la entidad de viaje
 * @version                 1.0
 * @author                  maguca
 * @since                   03/03/16
 *
 */
@Entity
@Table(name = "travel")
@Setter
@Getter
public class Travel implements Serializable
{

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;


    @Column( nullable = false, length = 255 )
    private String name;


    @Column( name = "budget", nullable = false )
    private double budget;

    @Temporal( TemporalType.TIMESTAMP )
    @Column( name = "created_at", nullable = false )
    private Date createdAt;


    @Temporal( TemporalType.TIMESTAMP )
    @Column( name = "date_arrival", nullable = false )
    private Date dateArrival;


    @Temporal( TemporalType.TIMESTAMP )
    @Column( name = "date_going", nullable = false )
    private Date dateGoing;


    @Column( nullable = false, length = 255 )
    private String description;

    @Column( name = "unique_id" ,length = 255 )
    private String uniqueId;

    @ManyToOne()
    @JoinColumn( name = "country_id" )
    private Country country;


    @ManyToOne()
    @JoinColumn( name = "user_id" )
    private User user;


    @OneToMany( mappedBy = "travel" , cascade = CascadeType.ALL)
    private List<TravelPartner> partner;


    @OneToMany( mappedBy = "travel" , cascade = CascadeType.ALL)
    private List<Destination> destination;

    @Transient
    private boolean deleted;



    @Override
    public boolean equals( Object obj )
    {
        boolean equal = false;
        if(obj instanceof Travel)
        {
            Travel aux = (Travel) obj;

            equal = Objects.equals( aux.getUniqueId(), this.uniqueId );
        }

        return equal;
    }

}