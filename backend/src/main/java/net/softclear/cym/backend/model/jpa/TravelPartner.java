package net.softclear.cym.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;



@Entity
@Table(name = "travel_partner")
@Setter
@Getter
public class TravelPartner implements Serializable
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;


    @ManyToOne()
    @JoinColumn( name = "travel_id" )
    private Travel travel;


    @ManyToOne()
    @JoinColumn( name = "user_id" )
    private User user;

    @Transient
    private boolean deleted;
}
