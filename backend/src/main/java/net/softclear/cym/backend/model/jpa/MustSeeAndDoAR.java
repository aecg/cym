package net.softclear.cym.backend.model.jpa;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "mustseeanddo_ar")
@Setter
@Getter
public class MustSeeAndDoAR implements Serializable {
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;

    @Lob
    @Column( name = "image_preview", nullable = true, columnDefinition="LONGBLOB" )
    private byte[] imagePreview;

    @Lob
    @Column( name = "image_display", nullable = true, columnDefinition="LONGBLOB" )
    private byte[] imageDisplay;

    @Lob
    @Column( name = "image_binary", nullable = true, columnDefinition="LONGBLOB" )
    private byte[] imageBinary;

    @Column( name = "is360", nullable = false )
    private int is360;

    @Lob
    @Column( name = "image_360", nullable = true, columnDefinition="LONGBLOB" )
    private byte[] image360;

    @OneToOne()
    @JoinColumn( name = "mustseeanddo_id" )
    private MustSeeAndDo mustseeanddo;
}
