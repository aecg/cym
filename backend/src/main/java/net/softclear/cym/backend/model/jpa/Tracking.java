package net.softclear.cym.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;



@Entity
@Table(name = "tracking")
@Setter
@Getter
public class Tracking implements Serializable
{

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;


    @Column( nullable = false, length = 255 )
    private String name;


    @Temporal( TemporalType.TIMESTAMP )
    @Column( name = "created_at", nullable = false )
    private Date createdAt;


    @ManyToOne()
    @JoinColumn( name = "user_id" )
    private User user;


    @OneToMany( mappedBy = "tracking", cascade = CascadeType.ALL)
    private List<CuePoint> cuepoints;


    @Column( name = "unique_id" ,length = 255 )
    private String uniqueId;


    @Transient
    private boolean deleted;


    @Override
    public boolean equals( Object obj )
    {
        boolean equal = false;
        if(obj instanceof Tracking)
        {
            Tracking aux = (Tracking) obj;

            equal = Objects.equals( aux.getUniqueId(), this.uniqueId );
        }

        return equal;
    }
}
