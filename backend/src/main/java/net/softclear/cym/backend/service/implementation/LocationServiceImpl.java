package net.softclear.cym.backend.service.implementation;



import net.softclear.cym.backend.config.MessagesConfig;
import net.softclear.cym.backend.exception.ServiceException;
import net.softclear.cym.backend.model.jpa.Country;
import net.softclear.cym.backend.model.jpa.Location;
import net.softclear.cym.backend.model.jpa.State;
import net.softclear.cym.backend.repository.CountryRepository;
import net.softclear.cym.backend.repository.LocationRepository;
import net.softclear.cym.backend.repository.StateRepository;
import net.softclear.cym.backend.service.LocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;



/**
 * Sistema:                 CyM.
 * <p/>
 * Nombre:                  UserServiceImpl Descripcion:             Clase que implementa el contrato del servicio
 *
 * @author guzmle
 * @version 1.0
 * @since 11/30/2015
 */
@Service
@Transactional
public class LocationServiceImpl implements LocationService
{
    //region Atributos

    private static Logger logger = LoggerFactory.getLogger( LocationServiceImpl.class );
    @Autowired
    private LocationRepository locationRepository;
    @Autowired
    private CountryRepository  countryRepository;
    @Autowired
    private StateRepository    stateRepository;
    @Autowired
    private MessagesConfig     messagesConfig;


    //endregion

    //region Metodos


    @Override
    public List<Country> getCountries()
    {

        List<Country> response;
        try
        {
            response = countryRepository.findAll();
        }
        catch ( Exception e )
        {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }

        return response;
    }


    @Override
    public List<State> getStates()
    {
        List<State> response;
        try
        {
            response = stateRepository.findAll();
        }
        catch ( Exception e )
        {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }

        return response;
    }


    @Override
    public List<State> getStatesByCountry( long id )
    {
        List<State> response;
        try
        {
            response = stateRepository.findByCountryIdOrderByName( id );
        }
        catch ( Exception e )
        {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }

        return response;
    }


    @Override
    public List<Location> getLocations()
    {
        List<Location> response;
        try
        {
            response = locationRepository.findAll();
        }
        catch ( Exception e )
        {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }

        return response;
    }


    @Override
    public List<Location> getLocationsByState( long id )
    {
        List<Location> response;
        try
        {
            response = locationRepository.findByStateIdOrderByName(id);
        }
        catch ( Exception e )
        {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }

        return response;
    }


    @Override
    public void saveCountry( Country country )
    {
        if ( country.getName() == null || country.getName().length() <= 0 )
            throw new ServiceException( messagesConfig.COUNTRY_NAME_INVALID );

        countryRepository.save( country );
    }


    @Override
    public void saveState( State state )
    {
        if ( state.getName() == null || state.getName().length() <= 0 )
            throw new ServiceException( messagesConfig.STATE_NAME_INVALID );

        if ( state.getCountry().getId() == null || state.getCountry().getId() <= 0 )
            throw new ServiceException( messagesConfig.STATE_COUNTRY_INVALID );

        stateRepository.save( state );

    }


    @Override
    public void saveLocation( Location location )
    {

        if ( location.getName() == null || location.getName().length() <= 0 )
            throw new ServiceException( messagesConfig.LOCATION_NAME_INVALID );

        if ( location.getState().getId() == null || location.getState().getId() <= 0 )
            throw new ServiceException( messagesConfig.LOCATION_STATE_INVALID );

        if ( location.getGeographicPosition() == null || location.getGeographicPosition().length() <= 0 )
            throw new ServiceException( messagesConfig.LOCATION_POSITION_INVALID );

        locationRepository.save( location );
    }

    //endregion
}
