package net.softclear.cym.backend.service.implementation;



import net.softclear.cym.backend.config.MessagesConfig;
import net.softclear.cym.backend.exception.ServiceException;
import net.softclear.cym.backend.model.jpa.CuePoint;
import net.softclear.cym.backend.model.jpa.Tracking;
import net.softclear.cym.backend.model.jpa.User;
import net.softclear.cym.backend.repository.TrackingRepository;
import net.softclear.cym.backend.repository.UserRepository;
import net.softclear.cym.backend.service.TrackingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;



/**
 * Sistema:                 CyM.
 * <p/>
 * Nombre:                  UserServiceImpl Descripcion:             Clase que implementa el contrato del servicio
 *
 * @author guzmle
 * @version 1.0
 * @since 11/30/2015
 */
@Service
@Transactional
public class TrackingServiceImpl implements TrackingService
{
    //region Atributos

    private static Logger logger = LoggerFactory.getLogger( TrackingServiceImpl.class );
    @Autowired
    private TrackingRepository repository;
    @Autowired
    private UserRepository     userRepository;
    @Autowired
    private MessagesConfig      messagesConfig;

    //endregion

    //region Metodos


    @Override
    public List<Tracking> getTrackingByUserName( String username )
    {

        List<Tracking> response;
        try
        {
            response = repository.findByUserUsername( username );
        }
        catch ( Exception e )
        {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }

        return response;
    }


    @Override
    public void saveTrackingUser( String username, List<Tracking> list )
    {

        User user = userRepository.findByUsername( username );
        if (user == null) {
            throw new ServiceException( messagesConfig.USER_NOT_FOUND );
        }

        try
        {
            List<Tracking> trackings = repository.findByUserUsername( username );

            for ( Tracking track : list )
            {
                int position = trackings.indexOf( track );
                //Si esta el track existe en la bd
                if ( position > -1 )
                {
                    Tracking obj = trackings.get( position );
                    repository.delete(obj);
                    //Si no es eliminado
                    if(!track.isDeleted()) {
                        addTracking(track, user);
                    }
                }
                else {
                    addTracking(track, user);
                }

            }
        }
        catch ( Exception e )
        {
            throw new ServiceException( messagesConfig.ERROR_SYNCING_TRACKING_USER, e );
        }

    }


    /**
     * Nombre:                  addTracking.
     * Descripcion:             Metodo que agrega un tracking
     * @version                 1.0
     * @author                  guzmle
     * @since                   4/7/2016
     *
     * @param track tracking
     */
    private void addTracking(Tracking track, User user)
    {
        track.setId( null );
        track.setUser( user );

        if(track.getUniqueId() == null || track.getUniqueId().length() <= 0) {
            String uniqueID = UUID.randomUUID().toString();
            track.setUniqueId(uniqueID);
        }

        if(track.getCuepoints() != null) {
            for ( CuePoint cuePoint : track.getCuepoints() ) {
                cuePoint.setId( null );
                cuePoint.setTracking( track );
            }
        }

        repository.save( track );
    }

    //endregion
}
