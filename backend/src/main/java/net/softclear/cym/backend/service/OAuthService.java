package net.softclear.cym.backend.service;

import net.softclear.cym.backend.model.jpa.User;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  OAuthService
 * Descripcion:             Contrato para todos los servicios que involucran la autenticacion
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public interface OAuthService
{
    /**
     * Nombre:                  getUserByToken.
     * Descripcion:             Metodo que obtiene los datos del usuario dado el token
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @param tokenValue token del usuario
     * @return datos del usuario
     */
	User getUserByToken(String tokenValue);
}
