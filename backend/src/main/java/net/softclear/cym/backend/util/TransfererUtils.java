package net.softclear.cym.backend.util;



import lombok.Getter;
import lombok.Setter;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  MercadoPagoUtils
 * Descripcion:             Clase que permite el acceso a la informacion de mercado pago
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/29/2015
 *
 */
@Setter
@Getter
public class TransfererUtils
{
    private String token;
    private Double amount;
}
