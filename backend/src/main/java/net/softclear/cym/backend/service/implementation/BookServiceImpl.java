package net.softclear.cym.backend.service.implementation;

import net.softclear.cym.backend.config.MessagesConfig;
import net.softclear.cym.backend.config.SettingsConfig;
import net.softclear.cym.backend.exception.ServiceException;
import net.softclear.cym.backend.model.jpa.*;
import net.softclear.cym.backend.repository.*;
import net.softclear.cym.backend.service.BookService;
import net.softclear.cym.backend.web.rest.response.MustSeeAndDoSimpleResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  UserServiceImpl
 * Descripcion:             Clase que implementa el contrato del servicio
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
@Service
@Transactional
public class BookServiceImpl implements BookService
{
    //region Atributos

    private static Logger logger = LoggerFactory.getLogger( BookServiceImpl.class );
    @Autowired
    private BookRepository         repository;
    @Autowired
    private LocationBookRepository locationBookRepository;
    @Autowired
    private UserBookRepository     userBookRepository;
    @Autowired
    private UserRepository         userRepository;
    @Autowired
    private MessagesConfig         messagesConfig;

    @Autowired
    private MustSeeAndDoRepository mustSeeAndDoRepository;
    @Autowired
    private BookMustSeeAndDoRepository bookMustSeeAndDoRepository;
    @Autowired
    private SettingsConfig settingsConfig;

    String GIF = "gif";
    String PNG = "png";
    String JPG = "jpg";
    String BMP = "bmp";
    String JPEG = "jpeg";

    //endregion

    //region Metodos


    @Override
    public List<Book> getBooks()
    {

        List<Book> response;
        try
        {
            response = repository.findAll();
        }
        catch ( Exception e )
        {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }

        return response;
    }


    @Override
    public List<LocationBook> getLocationsByBooks( long id )
    {

        List<LocationBook> response;
        try
        {
            response = locationBookRepository.findByBookId( id );
        }
        catch ( Exception e )
        {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }

        return response;
    }


    @Override
    public List<Location> getLocationsOfBooks( long id )
    {


        List<Location> response;
        try
        {
            response = repository.findLocationsByBook( id );
        }
        catch ( Exception e )
        {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }


        return response;

    }


    @Override
    public List<State> getStatesOfBooks( long id )
    {

        List<State> response;
        try
        {
            response = repository.findStateByBook( id );
        }
        catch ( Exception e )
        {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }


        return response;
    }


    @Override
    public List<Country> getCountryOfBooks( long id )
    {

        List<Country> response;
        try
        {
            response = repository.findCountryByBook( id );
        }
        catch ( Exception e )
        {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }


        return response;
    }


    @Override
    public List<UserBook> getBooksByUser( String username )
    {
        List<UserBook> response;
        try
        {
            response = userBookRepository.findByUserUsername( username );
        }
        catch ( Exception e )
        {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }

        return response;
    }


    @Override
    public void saveUserBook( String username, long codeBook ) {
        try
        {
            Book book = repository.findOne(codeBook);
            if (book != null) {
                UserBook obj = userBookRepository.findByUserAndByBook(username, codeBook);
                if (obj == null) {
                    User user = userRepository.findByUsername(username);

                    UserBook userBook = new UserBook();
                    userBook.setUser(user);
                    userBook.setBook(book);

                    userBookRepository.save(userBook);
                }
            }
            else {
                throw new ServiceException( messagesConfig.ERROR_BOOK_NOT_FOUND );
            }
        }
        catch ( Exception e ) {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }
    }


    @Override
    public void saveBook( Book book, List<MustSeeAndDoSimpleResponse> list ) {

        book.setLastUpdate( new Date(  ) );
        book = repository.save( book );

        String imageDirectory = settingsConfig.pathImageAbsolute + File.separator + settingsConfig.booksFolder + File.separator;
        String imageName = book.getId() + ".png";

        if (book.getImage() != null) {
            // salvando imagen
            try {
                byte[] imageByteArray = book.getImage();

                OutputStream imageOutFile = new FileOutputStream(imageDirectory + imageName);
                imageOutFile.write(imageByteArray);
                imageOutFile.close();
            }
            catch (Exception e) {
                throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
            }

            book.setImagePath(imageName);
            book = repository.save( book );
        }

        List<BookMustSeeAndDo> bookMustSeeAndDos = new ArrayList<>();
        //List<LocationBook> locationBooks = new ArrayList<>(  );

        if(list != null) {
            //locationBookRepository.delete( locationBookRepository.findByBookId( book.getId() ) );
            bookMustSeeAndDoRepository.delete( bookMustSeeAndDoRepository.findByBookId( book.getId() ) );
            for(MustSeeAndDoSimpleResponse simpleMustSeeAndDoResponse : list ) {
                //LocationBook obj = new LocationBook();
                //obj.setBook( book );
                //obj.setLocation( location );
                //locationBooks.add( obj );

                MustSeeAndDo mustSeeAndDo = mustSeeAndDoRepository.findOne(simpleMustSeeAndDoResponse.getId());
                BookMustSeeAndDo bookMustSeeAndDo = new BookMustSeeAndDo();
                bookMustSeeAndDo.setBook(book);
                bookMustSeeAndDo.setMustSeeAndDo(mustSeeAndDo);

                bookMustSeeAndDos.add( bookMustSeeAndDo );
            }

            //locationBookRepository.save( locationBooks );
            bookMustSeeAndDoRepository.save( bookMustSeeAndDos );
        }

    }

    @Override
    public List<MustSeeAndDo> getMustSeeAndDos(long id) {
        List<MustSeeAndDo> response = new ArrayList<>();

        try {
            List<BookMustSeeAndDo> bookMustSeeAndDos = bookMustSeeAndDoRepository.findByBookId( id );

            for(BookMustSeeAndDo bookMustSeeAndDo : bookMustSeeAndDos ) {
                response.add(bookMustSeeAndDo.getMustSeeAndDo());
            }
        }
        catch ( Exception e ) {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }

        return response;
    }

    @Override
    public Book getBook(long id) {
        Book response = repository.findOne(id);
        return response;
    }


    @Override
    public void optimizeImages() {
        String imageDirectory  = settingsConfig.pathImageAbsolute + File.separator +
                settingsConfig.booksFolder + File.separator;
        String optimizedmagesDirectory = settingsConfig.pathImageAbsolute + File.separator +
                settingsConfig.booksFolder + File.separator +
                settingsConfig.convertedFolder + File.separator;
        final File dir = new File(optimizedmagesDirectory);
        for (final File imgFile : dir.listFiles()) {

            if(acceptOptimizedImage(imgFile)){
                String imageName = imgFile.getName();
                String imageId = imageName.replaceFirst("[.][^.]+$", "");

                Book book = repository.findOne(Long.parseLong(imageId));

                String oldImage = book.getImagePath();
                File oldImageFile = new File(imageDirectory + oldImage);
                oldImageFile.delete();

                byte[] imageByteArray = readContentIntoByteArray(imgFile);
                book.setImage(imageByteArray);
                book.setImagePath(imageName);
                repository.save(book);

                // salvando imagen
                try {
                    OutputStream imageOutFile = new FileOutputStream(imageDirectory + imageName);
                    imageOutFile.write(imageByteArray);
                    imageOutFile.close();
                } catch (Exception e) {
                    throw new ServiceException(messagesConfig.ERROR_GENERIC, e);
                }
            }
        }
    }

    private boolean acceptOptimizedImage(File file) {
        if(file != null) {
            if(file.isDirectory()) {
                return false;
            }
            String extension = getExtension(file);
            if(extension != null && isSupported(extension)) {
                return true;
            }
        }
        return false;
    }

    private String getExtension(File file) {
        if(file != null) {
            String filename = file.getName();
            int dot = filename.lastIndexOf('.');
            if(dot > 0 && dot < filename.length()-1) {
                return filename.substring(dot + 1).toLowerCase();
            }
        }
        return null;
    }

    private boolean isSupported(String ext) {
        return ext.equalsIgnoreCase(GIF) || ext.equalsIgnoreCase(PNG) ||
                ext.equalsIgnoreCase(JPG) || ext.equalsIgnoreCase(BMP) ||
                ext.equalsIgnoreCase(JPEG);
    }

    private static byte[] readContentIntoByteArray(File file) {
        byte[] bFile = new byte[(int) file.length()];
        try {
            RandomAccessFile f = new RandomAccessFile(file, "r");
            f.readFully(bFile);
        }
        catch (Exception e) { }
        return bFile;
    }
    //endregion
}
