package net.softclear.cym.backend.repository;



import net.softclear.cym.backend.model.jpa.Travel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  BusinessTypeRepository
 * Descripcion:             Contrato para todas las operaciones de la base de datos que involucran la entidad
 * businessType
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public interface TravelRepository extends JpaRepository<Travel, Long>
{
    /**
     * Nombre:                  findByUserUsername
     * Descripcion:             Metodo que consulta los viajes dado el nombre de usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   08/03/16
     *
     * @param username nombre de usuario
     * @return lista de viajes del usuario
     */
    List<Travel> findByUserUsername(String username);
}
