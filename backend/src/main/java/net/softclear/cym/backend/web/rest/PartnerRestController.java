package net.softclear.cym.backend.web.rest;

import net.softclear.cym.backend.model.jpa.User;
import net.softclear.cym.backend.service.UserService;
import net.softclear.cym.backend.web.rest.response.InviteFriendRequest;
import net.softclear.cym.backend.web.rest.response.InviteFriendResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/invite*")
public class PartnerRestController
{
    //region Atributos

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private UserService userService;

    //endregion

    //region Servicios


    /**
     * Nombre:                  inviteFriend
     * Descripcion:             método que invita a usar la app
     * @author                  martin
     * @since                   07/03/16
     *
     * @return success
     */
    @RequestMapping( method = RequestMethod.POST, consumes = "application/json")
    @ResponseStatus( HttpStatus.OK )
    public @ResponseBody InviteFriendResponse inviteFriend(@RequestBody InviteFriendRequest request)
    {
        InviteFriendResponse response = new InviteFriendResponse();
        try {
            User user = modelMapper.map(request.getUser(), User.class);
            userService.inviteFriends(user, request.getEmailFriends());
            response.setSuccess(true);
        }
        catch (Exception e) {
            response.setSuccess(false);
        }

        return response;
    }
    //endregion
}
