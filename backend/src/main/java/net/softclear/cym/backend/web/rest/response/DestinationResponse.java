package net.softclear.cym.backend.web.rest.response;



import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import net.softclear.cym.backend.model.jpa.Location;
import net.softclear.cym.backend.model.jpa.State;
import net.softclear.cym.backend.model.jpa.Travel;
import net.softclear.cym.backend.model.jpa.TravelMustSeeAndDo;

import javax.persistence.*;
import java.util.Date;
import java.util.List;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  DestinationResponse
 * Descripcion:             Clase que mode la respuesta json de los objetos destination
 * @version                 1.0
 * @author                  guzmle
 * @since                   08/03/16
 *
 */
@Data
@JsonInclude( JsonInclude.Include.NON_NULL)
public class DestinationResponse
{
    private long date;
    private String description;
    private long travel_id;
    private long state_id;
    private long location_id;
    private String unique_id;
    private List<TravelMustSeeAndDoResponse> mustseeanddo;
}
