package net.softclear.cym.backend.service;



import net.softclear.cym.backend.model.jpa.Tracking;
import net.softclear.cym.backend.model.jpa.User;

import java.util.List;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  TrackingService
 * Descripcion:             Clase que se encarga de las operaciones sobre tracking
 * @version                 1.0
 * @author                  guzmle
 * @since                   07/03/16
 *
 */
public interface TrackingService
{

    /**
     * Nombre:                  getTrackingByUserName
     * Descripcion:             Metodo que consulta todos los tracking del usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   07/03/16
     *
     * @param username datos del usuario
     * @return lista de tracking del usuario
     */
    List<Tracking> getTrackingByUserName(String username);


    /**
     * Nombre:                  saveTrackingUser
     * Descripcion:             Metodo que salva la lista de seguimiento gps del usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   29/03/16
     *
     * @param username nombre del usuario
     * @param list lista de seguimientos;
     */
    void saveTrackingUser( String username, List<Tracking> list );
}
