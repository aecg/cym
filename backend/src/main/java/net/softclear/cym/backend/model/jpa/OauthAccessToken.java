package net.softclear.cym.backend.model.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  OauthAccessToken
 * Descripcion:             Clase que modela el objeto que se encarga de manipular el token de los usuarios
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
@Entity
@Table(name="oauth_access_token")
@Setter
@Getter
public class OauthAccessToken implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="token_id", length=256)
    private String tokenId;

    @Lob
    private byte[] authentication;

    @Column(name="authentication_id", length=256)
    private String authenticationId;

    @Column(name="client_id", length=256)
    private String clientId;

    @Column(name="refresh_token", length=256)
    private String refreshToken;

    @Lob
    private byte[] token;

    @Column(name="user_name", length=256)
    private String userName;

}
