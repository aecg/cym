package net.softclear.cym.backend.repository;

import net.softclear.cym.backend.model.jpa.MustSeeAndDoQR;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MustSeeAndDoQRRepository extends JpaRepository<MustSeeAndDoQR, Long> {
    MustSeeAndDoQR findByMustseeanddoId(long id);
}
