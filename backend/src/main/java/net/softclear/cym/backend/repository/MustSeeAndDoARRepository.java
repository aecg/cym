package net.softclear.cym.backend.repository;

import net.softclear.cym.backend.model.jpa.MustSeeAndDoAR;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MustSeeAndDoARRepository extends JpaRepository<MustSeeAndDoAR, Long> {
    MustSeeAndDoAR findByMustseeanddoId(long id);
}
