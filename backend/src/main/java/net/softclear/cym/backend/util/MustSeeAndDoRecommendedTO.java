package net.softclear.cym.backend.util;

import lombok.Getter;
import lombok.Setter;
import net.softclear.cym.backend.model.jpa.MustSeeAndDo;
import net.softclear.cym.backend.model.jpa.MustSeeAndDoDetail;
import net.softclear.cym.backend.model.jpa.StateMustSeeAndDoRecommended;

import java.util.List;

@Setter
@Getter
public class MustSeeAndDoRecommendedTO {
    private List<StateMustSeeAndDoRecommended> stateMustSeeAndDoRecommendeds;
    private List<MustSeeAndDo> mustSeeAndDos;
    private List<MustSeeAndDoDetail> mustSeeAndDoDetails;
}
