package net.softclear.cym.backend.service;

import net.softclear.cym.backend.model.jpa.User;

import java.util.List;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  UserService
 * Descripcion:             Contrato para los servicios que involucran la entidad Usuario
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public interface UserService
{
    /**
     * Nombre:                  createUser.
     * Descripcion:             Metodo que se usa para crear un usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     * @param user datos del usuario
     * @return usuario creado
     */
	User createUser(User user, String captcha);


    /**
     * Nombre:                  findUserByUserName
     * Descripcion:             Metodo que consulta los datos del usuario dado su login
     * @version                 1.0
     * @author                  guzmle
     * @since                   07/03/16
     *
     * @param username datos del usuario
     * @return datos del usuario completos
     */
    User findUserByUserName( String username );


    /**
     * Metodo que consulta todos los usuarios
     * @return lista de usuarios
     */
    List<User> getAll(String username);


    /**
     * Nombre:                  registerUser
     * Descripcion:             Metodo que registra los datos de un usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/03/16
     *
     * @param user
     */
    void registerUser( User user );


    /**
     * Nombre:                  updateUser
     * Descripcion:             Metodo que modifica los datos de un usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/03/16
     *
     * @param user datos del usuario
     */
    void updateUser( User user );


    /**
     * Nombre:                  retrievePassword
     * Descripcion:             Metodo que envia un correo de recuperacion de clave
     * @version                 1.0
     * @author                  guzmle
     * @since                   03/05/16
     *
     * @param user datos del usuario
     */
    void retrievePassword( User user );


    /**
     * Nombre:                   Metodo que recupera el usuario de un backoffice
     * Descripcion:
     * @version                 1.0
     * @author                  guzmle
     * @since                   06/05/16
     *
     * @param user datos del usuario
     */
    void recoverPassword( User user );


    /**
     * Nombre:                  changePassword
     * Descripcion:             Metodo que cambia el password del usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   06/05/16
     *
     * @param username nombre de usuario
     * @param password y password
     */
    void changePassword( String username, String password );

    /**
     * Nombre:                  inviteFriend
     * Descripcion:             Metodo que invita a amigos a usar la app
     * @version                 1.0
     * @author                  martin
     * @since                   12/14/16
     *
     * @param user usuario que invita
     * @param friendEmails direcciones de correo de los amigos a invitar
     */
    void inviteFriends( User user, List<String> friendEmails );
}
