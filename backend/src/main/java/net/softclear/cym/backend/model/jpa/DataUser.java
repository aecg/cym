package net.softclear.cym.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;



@Entity
@Table(name="data_user")
@Setter
@Getter
public class DataUser implements Serializable
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;


    @Column( name = "name", nullable = true )
    private String name;


    @Column( name = "last_name", nullable = true )
    private String lastName;


    @Column( name = "email", nullable = false )
    private String email;


    @Column( name = "phone", nullable = true )
    private String phone;


    @OneToOne
    @JoinColumn( name = "user_id" )
    private User user;
}
