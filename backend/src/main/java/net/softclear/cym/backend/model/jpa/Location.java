package net.softclear.cym.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  State
 * Descripcion:             Clase que emula la entidad state dentro del sistema
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/9/2015
 *
 */
@Entity
@Table(name="location")
@Setter
@Getter
public class Location implements Serializable
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;

    @Column( name = "name", nullable = false )
    private String name;

    @Column( name = "geographic_position", nullable = false )
    private String geographicPosition;

    @ManyToOne()
    @JoinColumn( name = "state_id" )
    private State state;
}
