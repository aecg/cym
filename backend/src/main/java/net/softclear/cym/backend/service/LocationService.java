package net.softclear.cym.backend.service;

import net.softclear.cym.backend.model.jpa.Country;
import net.softclear.cym.backend.model.jpa.Location;
import net.softclear.cym.backend.model.jpa.State;
import net.softclear.cym.backend.model.jpa.User;

import java.util.List;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  UserService
 * Descripcion:             Contrato para los servicios que involucran la entidad Usuario
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public interface LocationService
{
    /**
     * Nombre:                  getCountries
     * Descripcion:             Metodo que consulta todos los paises registrados en la base de datos
     * @version                 1.0
     * @author                  guzmle
     * @since                   16/03/16
     *
     * @return lista de paises
     */
    List<Country> getCountries();


    /**
     * Nombre:                  getStateByContryId
     * Descripcion:             Metodo que consulta los estados de un paise
     * @version                 1.0
     * @author                  guzmle
     * @since                   16/03/16
     *
     * @return lista de estados
     */
    List<State> getStates();


    /**
     * Nombre:                  getStateByContryId
     * Descripcion:             Metodo que consulta los estados de un paise
     * @version                 1.0
     * @author                  guzmle
     * @since                   16/03/16
     *
     * @return lista de estados
     */
    List<State> getStatesByCountry(long id);


    /**
     * Nombre:                  getLocationsByStateId
     * Descripcion:             Metoo que consulta las locaciones de un estado
     * @version                 1.0
     * @author                  guzmle
     * @since                   16/03/16
     *
     * @return lista de locaciones
     */
    List<Location> getLocations();


    /**
     * Nombre:                  getLocationsByStateId
     * Descripcion:             Metoo que consulta las locaciones de un estado
     * @version                 1.0
     * @author                  guzmle
     * @since                   16/03/16
     *
     * @return lista de locaciones
     */
    List<Location> getLocationsByState(long id);


    /**
     * Nombre:                  saveCountry
     * Descripcion:             Metodo que agrega los datos de un pais
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/04/16
     *
     * @param country pais a agregar
     */
    void saveCountry(Country country);


    /**
     * Nombre:                  saveState
     * Descripcion:             Metodo que guarda los datos de un estado
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/04/16
     *
     * @param state estado que se va a almacenar
     */
    void saveState( State state );


    /**
     * Nombre:                  saveLocation
     * Descripcion:             Metodo que guarda los datos de una locacion
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/04/16
     *
     * @param location datos de la locacion
     */
    void saveLocation( Location location );
}
