package net.softclear.cym.backend.web.rest.response;



import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  UserResponse
 * Descripcion:             Clase que modela la respuestas de los bjeto en json
 * @version                 1.0
 * @author                  guzmle
 * @since                   07/03/16
 *
 */
@Data
@JsonInclude( JsonInclude.Include.NON_NULL)
public class UserResponse
{
    private String username;
    private long created_at;
    private int login_state;
    private String name;
    private String lastname;
    private String email;
    private String password;
}
