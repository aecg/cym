package net.softclear.cym.backend.exception;



/**
 * Sistema:                 CyM.
 * <p/>
 * Nombre:                  ServiceException Descripcion:             Excepcion Personalizada de la aplicacion
 *
 * @author guzmle
 * @version 1.0
 * @since 11/30/2015
 */
public class InvalidDataException extends RuntimeException
{
    private static final long serialVersionUID = -1551291658825173144L;


    /**
     * Nombre:                  ServiceException. Descripcion:             Constructor de la clase
     *
     * @version 1.0
     * @author guzmle
     * @since 12/22/2015
     */
    public InvalidDataException()
    {

    }


    /**
     * Nombre:                  ServiceException. Descripcion:             Constructor de la clase
     *
     * @param message mensaje de la excepcion
     *
     * @version 1.0
     * @author guzmle
     * @since 12/22/2015
     */
    public InvalidDataException( String message )
    {
        super( message );
    }


    /**
     * Nombre:                  ServiceException. Descripcion:             Contructor de la clase
     *
     * @param message   mensaje de la excepcion
     * @param throwable excepcion que ocasiona esta excepcion
     *
     * @version 1.0
     * @author guzmle
     * @since 12/22/2015
     */
    public InvalidDataException( String message, Throwable throwable )
    {
        super( message, throwable );
    }


    /**
     * Nombre:                  ServiceException. Descripcion:             Constructor de la excepcion
     *
     * @param throwable excepcion que la ocasiona
     *
     * @version 1.0
     * @author guzmle
     * @since 12/22/2015
     */
    public InvalidDataException( Throwable throwable )
    {
        super( throwable );
    }
}
