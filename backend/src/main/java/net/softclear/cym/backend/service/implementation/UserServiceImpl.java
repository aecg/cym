package net.softclear.cym.backend.service.implementation;



import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import net.softclear.cym.backend.config.MessagesConfig;
import net.softclear.cym.backend.exception.ExistException;
import net.softclear.cym.backend.exception.InvalidDataException;
import net.softclear.cym.backend.exception.NotFoundException;
import net.softclear.cym.backend.exception.ServiceException;
import net.softclear.cym.backend.model.jpa.Authority;
import net.softclear.cym.backend.model.jpa.User;
import net.softclear.cym.backend.repository.AuthorityRepository;
import net.softclear.cym.backend.repository.UserRepository;
import net.softclear.cym.backend.service.UserService;
import net.softclear.cym.backend.util.UtilString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import javax.persistence.NoResultException;
import java.io.IOException;
import java.io.StringWriter;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * Sistema:                 CyM.
 * <p/>
 * Nombre:                  UserServiceImpl Descripcion:             Clase que implementa el contrato del servicio
 *
 * @author guzmle
 * @version 1.0
 * @since 11/30/2015
 */
@Service
@Transactional
public class UserServiceImpl implements UserService
{
    //region Atributos

    private static Logger logger = LoggerFactory.getLogger( UserServiceImpl.class );
    @Autowired
    private JavaMailSender      mailSender;
    @Autowired
    private UserRepository      userRepository;
    @Autowired
    private AuthorityRepository authorityRepository;
    @Autowired
    private UtilString          utilString;
    @Autowired
    private MessagesConfig      messagesConfig;
    @Autowired
    private Configuration       freemarkerConfiguration;
    @Value( "${email.pathRecover}" )
    private String              pathRecover;

    //endregion

    //region Metodos


    /**
     * Nombre:                  createUserValidation.
     * Descripcion:             Metodo que realiza la validacion de los
     * datos del usuario
     *
     * @param user datos del usuario a validar
     *
     * @version 1.0
     * @author guzmle
     * @since 11/30/2015
     */
    private void createUserValidation( User user )
    {
        logger.debug( "Entrando en el metodo createUserValidation" );

        if ( user == null )
            throw new InvalidDataException( messagesConfig.USER_NOT_NULL );

        if ( user.getUsername() == null )
            throw new InvalidDataException( messagesConfig.USER_NAME_NOT_NULL );


        logger.debug( "Saliendo del metodo createUserValidation" );
    }


    @Override
    public User createUser( User user, String captcha )
    {
        logger.debug( "Entrando en el metodo createUser" );

        Authority authority;

        if ( userRepository.findByUsername( user.getUsername() ) != null )
            throw new ServiceException( messagesConfig.USER_USERNAME_EXIST );

        user.setEnabled( true );
        user.setCreatedAt( new Date() );
        try
        {
            user.setPassword( utilString.getHash( user.getPassword() ) );
            userRepository.save( user );
            authority = new Authority();
            authority.setUser( user );
            authority.setAuthority( "ROLE_ADMIN" );
            authorityRepository.save( authority );
        }
        catch ( Exception e )
        {
            logger.error( messagesConfig.ERROR_SAVING_USER, e );
            throw new ServiceException( messagesConfig.ERROR_SAVING_USER, e );
        }

        logger.debug( "Saliendo del metodo createUser" );
        return user;
    }


    @Override
    public User findUserByUserName( String username )
    {
        User user;
        user = userRepository.findByUsername( username );

        if ( user == null )
            throw new NotFoundException( messagesConfig.USER_NOT_FOUND );

        return user;
    }


    @Override
    public List<User> getAll( String username )
    {
        List<User> response;
        try
        {
            response = userRepository.findAllExceptCurrent( username );
            if ( response.size() <= 0 )
                throw new NotFoundException( messagesConfig.ERROR_NO_DATA_FOUND );
        }
        catch ( Exception e )
        {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }

        return response;
    }


    @Override
    public void registerUser( User user )
    {
        createUserValidation( user );

        if ( userRepository.findByUsername( user.getUsername() ) != null )
            throw new ExistException( messagesConfig.USER_USERNAME_EXIST );

        user.getDataUser().setUser( user );
        user.setCreatedAt( new Date() );
        user.setEnabled( true );


        userRepository.save( user );
    }


    @Override
    public void updateUser( User user )
    {
        createUserValidation( user );

        User obj = userRepository.findByUsername( user.getUsername() );
        if ( obj == null ) {
            throw new ServiceException(messagesConfig.ERROR_USERNAME_NOT_REGISTERED);
        }

        try
        {
            obj.setPassword( user.getPassword() );
            obj.getDataUser().setEmail( user.getDataUser().getEmail() );
            obj.getDataUser().setPhone( user.getDataUser().getPhone() );
            obj.getDataUser().setLastName( user.getDataUser().getLastName() );
            obj.getDataUser().setName( user.getDataUser().getName() );
            obj.getDataUser().setUser( obj );
            userRepository.save( obj );
        }
        catch ( Exception e )
        {
            throw new ServiceException( messagesConfig.ERROR_SYNCING_USER, e );
        }

    }


    @Override
    public void retrievePassword( User user )
    {

        logger.debug( "Entrando en el metodo retrieveUser" );
        try
        {
            final Template template = freemarkerConfiguration.getTemplate("mail.ftl");
            final Map<String, Object> params = new HashMap<>();
            StringWriter output;
            MimeMessage msg;
            String password = user.getPassword();

            output = new StringWriter();
            user = userRepository.findByUsername( user.getUsername() );
            params.put( "nombre", user.getDataUser().getName() == null ? "" : user.getDataUser().getName() );
            params.put( "apellido", user.getDataUser().getLastName() == null ? "" : user.getDataUser().getLastName() );
            params.put( "password", password );
            template.process( params, output );

            msg = mailSender.createMimeMessage();
            msg.setRecipients( Message.RecipientType.TO, user.getDataUser().getEmail() );
            msg.setSubject( "Recuperación de CYM-AR Credenciales de Seguridad" );
            msg.setText( output.toString(), "utf-8", "html");


            mailSender.send( msg );

        }
        catch ( NoResultException | TemplateException e )
        {
            logger.error(messagesConfig.ERROR_USERNAME_NOT_REGISTERED, e);
            throw new ServiceException( messagesConfig.ERROR_USERNAME_NOT_REGISTERED );
        }
        catch ( IOException | MessagingException e )
        {
            logger.error( messagesConfig.ERROR_GENERIC, e);
            throw new ServiceException( messagesConfig.ERROR_GENERIC );
        }
        logger.debug( "Saliendo del metodo retrieveUser" );

    }


    @Override
    public void recoverPassword( User user )
    {
        logger.debug( "Entrando en el metodo recoverPassword" );
        try
        {
            final Template template = freemarkerConfiguration.getTemplate("mailBackOffice.ftl");
            final Map<String, Object> params = new HashMap<>();
            String value;
            String pathPage;
            StringWriter output;
            MimeMessage msg;

            output = new StringWriter();
            user = userRepository.findByDataUserEmail( user.getDataUser().getEmail() );
            value = user.getId() + ";" + utilString.getHash( user.getUsername() );
            pathPage = pathRecover + value;
            params.put( "nombre", user.getDataUser().getName() == null ? "" : user.getDataUser().getName() );
            params.put( "apellido", user.getDataUser().getLastName() == null ? "" : user.getDataUser().getLastName() );
            params.put( "url", pathPage );
            template.process( params, output );

            msg = mailSender.createMimeMessage();
            msg.setRecipients( Message.RecipientType.TO, user.getDataUser().getEmail() );
            msg.setSubject( "Recuperación Contraseña de Usuario CrowdEI" );
            msg.setText( output.toString(), "utf-8", "html");

            mailSender.send( msg );

        }
        catch ( NoSuchAlgorithmException e )
        {
            logger.error("Error armando la url de recuperacion", e);
            throw new ServiceException("Error armando la url de recuperacion", e);
        }
        catch ( NoResultException | TemplateException e )
        {
            logger.error(messagesConfig.USER_NOT_FOUND, e);
            throw new ServiceException( messagesConfig.USER_NOT_FOUND );
        }
        catch ( IOException | MessagingException e )
        {
            logger.error(messagesConfig.ERROR_GENERIC, e);
            throw new ServiceException( messagesConfig.ERROR_GENERIC );
        }
        logger.debug( "Saliendo del metodo recoverPassword" );
    }



    @Override
    public void changePassword(String username, String password)
    {
        logger.debug( "Entrando en el metodo changePassword" );
        final long id = Long.parseLong( username.split( ";" )[ 0 ] );
        final User user = userRepository.findOne( id );

        if (user == null)
            throw new ServiceException( messagesConfig.USER_NOT_FOUND );

        try
        {
            user.setPassword( password );
            userRepository.save( user );
        }
        catch (Exception e)
        {
            logger.error("Error saving user into database", e);
            throw new ServiceException("Error saving user into database", e);
        }
        logger.debug( "Saliendo del metodo changePassword" );
    }

    @Override
    public void inviteFriends( User user, List<String> friendEmails )
    {

        logger.debug( "Entrando en el metodo inviteFriend" );
        try
        {
            final Template template = freemarkerConfiguration.getTemplate("invitation.ftl");
            final Map<String, Object> params = new HashMap<>();
            StringWriter output;
            MimeMessage msg;

            output = new StringWriter();
            params.put( "email", user.getUsername() );
            params.put( "url_google_play_store", "https://play.google.com/store/apps/details?id=net.softclear.ei.cym.itripmx" );
            params.put( "url_apple_store", "https://itunes.apple.com/us/app/itrip-mx/id1189714828?ls=1&mt=8" );
            template.process( params, output );

            msg = mailSender.createMimeMessage();
            msg.setSubject(MimeUtility.encodeText("Invitación a probar iTrip","UTF-8","B"));

            msg.setText( output.toString(), "utf-8", "html");

            for (String friendEmail : friendEmails) {
                msg.setRecipients( Message.RecipientType.TO, friendEmail );
                mailSender.send( msg );
            }
        }
        catch ( NoResultException | TemplateException e )
        {
            logger.error(messagesConfig.ERROR_USERNAME_NOT_REGISTERED, e);
            throw new ServiceException( messagesConfig.ERROR_USERNAME_NOT_REGISTERED );
        }
        catch ( IOException | MessagingException e )
        {
            logger.error( messagesConfig.ERROR_GENERIC, e);
            throw new ServiceException( messagesConfig.ERROR_GENERIC );
        }
        logger.debug( "Saliendo del metodo inviteFriend" );

    }

    //endregion
}
