package net.softclear.cym.backend.model.jpa;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  User
 * Descripcion:             Clase que modela la entidad usuario dentro del sistema
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
@Entity
@Table(name = "user")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;

    @Temporal( TemporalType.TIMESTAMP )
    @Column( name = "created_at", nullable = false )
    private Date createdAt;

    @Column( name = "enabled", nullable = false )
    private Boolean enabled;

    @Column( nullable = false, length = 255 )
    private String password;

    @Column( nullable = false, length = 50, unique = true )
    private String username;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private DataUser dataUser;


    /**
     * Nombre:                  User.
     * Descripcion:             Constructor de la clase
     * @version 1.0
     * @author guzmle
     * @since 12/22/2015
     *
     * @param varUsername datos del nombre de usuario
     * @param varPassword datos del password
     */
    public User( String varUsername, String varPassword )
    {
        this.username = varUsername;
        this.password = varPassword;
    }
}
