package net.softclear.cym.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;



@Entity
@Table(name="document")
@Setter
@Getter
public class Document implements Serializable
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;


    @Column( name = "name", nullable = false )
    private String name;


    @Column( name = "description", nullable = false )
    private String description;


    @Column( name = "geographic_position", nullable = false )
    private String geographicPosition;


    @Temporal( TemporalType.TIMESTAMP )
    @Column( name = "date", nullable = false )
    private Date date;


    @ManyToOne()
    @JoinColumn( name = "user_id" )
    private User user;
}