package net.softclear.cym.backend.web.rest.response;



import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;


/**
 * Sistema:                 CyM.
 *
 * Nombre:                  UserResponse
 * Descripcion:             Clase que modela la respuestas de los bjeto en json
 * @version                 1.0
 * @author                  guzmle
 * @since                   07/03/16
 *
 */
@Data
@JsonInclude( JsonInclude.Include.NON_NULL)
public class ListMustSeeAndDoSimpleResponse
{
    private String username;
    private List<MustSeeAndDoSimpleResponse> mustseeanddo;
}
