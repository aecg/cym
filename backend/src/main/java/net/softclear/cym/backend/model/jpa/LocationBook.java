package net.softclear.cym.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;



@Entity
@Table(name = "location_book")
@Setter
@Getter
public class LocationBook implements Serializable
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;


    @ManyToOne()
    @JoinColumn( name = "book_id" )
    private Book book;


    @ManyToOne()
    @JoinColumn( name = "location_id" )
    private Location location;
}
