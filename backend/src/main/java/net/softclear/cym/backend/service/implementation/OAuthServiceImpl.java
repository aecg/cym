package net.softclear.cym.backend.service.implementation;

import net.softclear.cym.backend.service.OAuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import net.softclear.cym.backend.model.jpa.User;
import net.softclear.cym.backend.repository.UserRepository;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  OAuthServiceImpl
 * Descripcion:             Clase que implementa el contrato requerido para los servicios sobre autenticacion
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
@Service
@Transactional
public class OAuthServiceImpl implements OAuthService
{
    //region Atributos

    private static Logger logger = LoggerFactory.getLogger( OAuthServiceImpl.class );
    @Autowired
    private TokenStore     tokenStore;
    @Autowired
    private UserRepository userRepository;

    //endregion

    //region Implementacion


    @Override
    public User getUserByToken( String tokenValue )
    {
        logger.debug( "Entrando al metodo getUserByToken" );

        final String username = tokenStore.readAuthentication( tokenValue ).getName();

        logger.debug( "Saliendo del metodo getUserByToken" );
        return userRepository.findByUsername( username );
    }

    //endregion

}
