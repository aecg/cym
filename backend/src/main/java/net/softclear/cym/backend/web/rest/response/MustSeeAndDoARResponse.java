package net.softclear.cym.backend.web.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * Created by softapple on 7/6/16.
 */
@Data
@JsonInclude( JsonInclude.Include.NON_NULL)
public class MustSeeAndDoARResponse {
    private long    id;
    private byte[]  image_preview;
    private byte[]  image_display;
    private byte[]  image_binary;
    private int     is_360;
    private byte[]  image_360;
    private long    mustseeanddo_id;
}
