package net.softclear.cym.backend.repository.implementation;



import net.softclear.cym.backend.model.jpa.*;
import net.softclear.cym.backend.repository.BookRepositoryCustom;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;



public class BookRepositoryImpl implements BookRepositoryCustom
{

    //region Atributos

    /**
     * Objeto que posee la definicion de la persistencia.
     */
    @PersistenceContext
    private EntityManager entityManager;

    //endregion


    //region Implementacion

    @Override
    public List<Location> findLocationsByBook( long id )
    {
        List<Location> response;



        List<Predicate> predicates = new ArrayList<>();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Book> criteriaQuery = criteriaBuilder.createQuery( Book.class );
        Root<LocationBook> root = criteriaQuery.from( LocationBook.class );
        Join join = root.join("book");


        predicates.add( criteriaBuilder.equal( join.get( "id" ), id ) );
        criteriaQuery.where( predicates.toArray( new Predicate[ predicates.size() ] ) );
        criteriaQuery.select( root.get( "location" ) ).distinct( true );

        Query query = entityManager.createQuery(criteriaQuery);
        response = query.getResultList();

        return response;
    }


    @Override
    public List<State> findStateByBook( long id )
    {
        List<State> response;
        List<Predicate> predicates = new ArrayList<>();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Book> criteriaQuery = criteriaBuilder.createQuery( Book.class );
        Root<LocationBook> root = criteriaQuery.from( LocationBook.class );
        Join join = root.join("book");
        Join joinLocation = root.join( "location" );


        predicates.add( criteriaBuilder.equal( join.get( "id" ), id ) );
        criteriaQuery.where( predicates.toArray( new Predicate[ predicates.size() ] ) );
        criteriaQuery.select( joinLocation.get( "state" ) ).distinct( true );

        Query query = entityManager.createQuery(criteriaQuery);
        response = query.getResultList();

        return response;
    }


    @Override
    public List<Country> findCountryByBook( long id )
    {
        List<Country> response;
        List<Predicate> predicates = new ArrayList<>();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Book> criteriaQuery = criteriaBuilder.createQuery( Book.class );
        Root<LocationBook> root = criteriaQuery.from( LocationBook.class );
        Join join = root.join( "book" );
        Join joinLocation = root.join( "location" );
        Join joinState = joinLocation.join( "state" );


        predicates.add( criteriaBuilder.equal( join.get( "id" ), id ) );
        criteriaQuery.where( predicates.toArray( new Predicate[ predicates.size() ] ) );
        criteriaQuery.select( joinState.get( "country" ) ).distinct( true );

        Query query = entityManager.createQuery(criteriaQuery);
        response = query.getResultList();

        return response;
    }

    //endregion
}
