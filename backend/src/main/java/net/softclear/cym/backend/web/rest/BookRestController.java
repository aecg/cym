package net.softclear.cym.backend.web.rest;



import net.softclear.cym.backend.model.jpa.*;
import net.softclear.cym.backend.service.BookService;
import net.softclear.cym.backend.web.rest.response.*;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.List;



@RestController
public class BookRestController
{
    //region Atributos

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private BookService service;

    //endregion

    //region Servicios


    /**
     * Nombre:                  getUserByUserName
     * Descripcion:             Servicio que consulta los datos del usuario dado su nombre de usuario
     * @version 1.0
     * @author guzmle
     * @since 07/03/16
     *
     * @return datos del usuario
     */
    @RequestMapping( value = "/listbooks", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    ListBooksResponse getBooks()
    {
        ListBooksResponse response = new ListBooksResponse();
        List<Book> books = service.getBooks();

        Type listDTOType = new TypeToken<List<BookResponse>>(){}.getType();
        List<BookResponse> list = modelMapper.map(books, listDTOType);

        response.setBooks( list );
        return response;
    }

    @RequestMapping( value = "/listbookssimple", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    ListBooksSimpleResponse getBooksSimple()
    {
        ListBooksSimpleResponse response = new ListBooksSimpleResponse();
        List<Book> books = service.getBooks();

        Type listDTOType = new TypeToken<List<BookSimpleResponse>>(){}.getType();
        List<BookSimpleResponse> list = modelMapper.map(books, listDTOType);

        response.setBooks( list );
        return response;
    }

    /**
     * Nombre:                  getUserByUserName
     * Descripcion:             Servicio que consulta los datos del usuario dado su nombre de usuario
     * @version 1.0
     * @author guzmle
     * @since 07/03/16
     *
     * @return datos del usuario
     */
    @RequestMapping( value = "/books/{username}", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    ListUserBookResponse getBooks(@PathVariable String username)
    {


        ListUserBookResponse response = new ListUserBookResponse();
        List<UserBook> books = service.getBooksByUser( username );

        Type listDTOType = new TypeToken<List<UserBookResponse>>(){}.getType();
        List<UserBookResponse> list = modelMapper.map(books, listDTOType);

        response.setUsername( username );
        response.setBooks( list );
        return response;

    }


    /**
     * Nombre:                  getUserByUserName
     * Descripcion:             Servicio que consulta los datos del usuario dado su nombre de usuario
     * @version 1.0
     * @author guzmle
     * @since 07/03/16
     *
     * @return datos del usuario
     */
    /*
    @RequestMapping( value = "/book/{id}", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    ListBooksLocationResponse getLocationsByBook(@PathVariable long id)
    {
        ListBooksLocationResponse response = new ListBooksLocationResponse();
        List<LocationBook> books = service.getLocationsByBooks( id );

        Type listDTOType = new TypeToken<List<LocationBookResponse>>(){}.getType();
        List<LocationBookResponse> list = modelMapper.map(books, listDTOType);

        response.setLocation_book( list );
        return response;

    }
    */


    /**
     * Nombre:                  getUserByUserName
     * Descripcion:             Servicio que consulta los datos del usuario dado su nombre de usuario
     * @version 1.0
     * @author guzmle
     * @since 07/03/16
     *
     * @return datos del usuario
     */
    @RequestMapping( value = "/booklocations/{id}", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    ListLocationResponse getLocations(@PathVariable long id)
    {
        ListLocationResponse response = new ListLocationResponse();
        List<Location> objs = service.getLocationsOfBooks( id );

        Type listDTOType = new TypeToken<List<LocationResponse>>(){}.getType();
        List<LocationResponse> list = modelMapper.map(objs, listDTOType);

        response.setLocations( list );
        return response;

    }


    /**
     * Nombre:                  getUserByUserName
     * Descripcion:             Servicio que consulta los datos del usuario dado su nombre de usuario
     * @version 1.0
     * @author guzmle
     * @since 07/03/16
     *
     * @return datos del usuario
     */
    @RequestMapping( value = "/bookstates/{id}", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    ListStateResponse getStates(@PathVariable long id)
    {

        ListStateResponse response = new ListStateResponse();
        List<State> objs = service.getStatesOfBooks( id );

        Type listDTOType = new TypeToken<List<StateResponse>>(){}.getType();
        List<StateResponse> list = modelMapper.map(objs, listDTOType);

        response.setStates( list );
        return response;

    }


    /**
     * Nombre:                  getUserByUserName
     * Descripcion:             Servicio que consulta los datos del usuario dado su nombre de usuario
     * @version 1.0
     * @author guzmle
     * @since 07/03/16
     *
     * @return datos del usuario
     */
    @RequestMapping( value = "/bookcountries/{id}", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    ListCountryResponse getCountries(@PathVariable long id)
    {

        ListCountryResponse response = new ListCountryResponse();
        List<Country> objs = service.getCountryOfBooks( id );

        Type listDTOType = new TypeToken<List<CountryResponse>>(){}.getType();
        List<CountryResponse> list = modelMapper.map(objs, listDTOType);

        response.setCountries( list );
        return response;

    }


    /**
     * Nombre:                  saveUserBook
     * Descripcion:             Metodo que almacena la relacion del usuario con el libro
     * @version                 1.0
     * @author                  guzmle
     * @since                   29/03/16
     *
     * @param request datos del usuario y del libro
     */
    @RequestMapping (value = "/createuserbook", method = RequestMethod.POST, consumes = "application/json")
    @ResponseStatus( HttpStatus.OK )
    public void saveUserBook (@RequestBody UserBookRequest request)
    {
        service.saveUserBook(request.getUsername(), request.getBook_id());
    }


    /**
     * Nombre:                  saveUserBook
     * Descripcion:             Metodo que almacena la relacion del usuario con el libro
     * @version                 1.0
     * @author                  guzmle
     * @since                   29/03/16
     *
     * @param request datos del usuario y del libro
     */
    @RequestMapping (value = "/book", method = RequestMethod.POST, consumes = "application/json")
    @ResponseStatus( HttpStatus.OK )
    public void saveBook (@RequestBody AddBookRequest request)
    {

        Type listDTOType = new TypeToken<List<MustSeeAndDoSimpleResponse>>(){}.getType();
        List<MustSeeAndDoSimpleResponse> list_mustseeanddo = modelMapper.map(request.getMustseeanddos(), listDTOType);
        Book book = modelMapper.map(request.getBook(), Book.class);

        //List<Location> list = modelMapper.map(request.getLocations(), listDTOType);
        //service.saveBook( book, list );

        service.saveBook( book, list_mustseeanddo );
    }


    @RequestMapping( value = "/bookmustseeanddos/{id}", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    ListMustSeeAndDoSimpleResponse getMustSeeAndDos(@PathVariable long id)
    {
        ListMustSeeAndDoSimpleResponse response = new ListMustSeeAndDoSimpleResponse();
        List<MustSeeAndDo> mustSeeAndDos = service.getMustSeeAndDos( id );

        Type listDTOType = new TypeToken<List<MustSeeAndDoSimpleResponse>>(){}.getType();
        List<MustSeeAndDoSimpleResponse> list = modelMapper.map(mustSeeAndDos, listDTOType);

        response.setMustseeanddo( list );
        return response;
    }


    @RequestMapping( value = "/book/{id}", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    BookResponse getBook(@PathVariable long id)
    {
        Book book = service.getBook(id);

        Type listDTOType = new TypeToken<BookResponse>(){}.getType();
        BookResponse response = modelMapper.map(book, listDTOType);

        return response;
    }

    @RequestMapping( value = "/book/optimize", method = RequestMethod.GET )
    public void optimizeImage( ) {
        service.optimizeImages();
    }

    //endregion
}
