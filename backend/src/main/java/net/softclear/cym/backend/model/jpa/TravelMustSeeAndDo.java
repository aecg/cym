package net.softclear.cym.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;



@Entity
@Table(name = "travel_mustseeanddo")
@Setter
@Getter
public class TravelMustSeeAndDo implements Serializable
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;


    @ManyToOne()
    @JoinColumn( name = "destination_id" )
    private Destination destination;


    @ManyToOne()
    @JoinColumn( name = "mustseeanddo_id" )
    private MustSeeAndDo mustSeeAndDo;
}