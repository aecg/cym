package net.softclear.cym.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;



/**
 * Sistema:                 Hermes
 *
 * Nombre:                  CuePoint
 * Descripcion:             Clase que modela la entidad cuepoint
 * @version                 1.0
 * @author                  maguca
 * @since                   03/03/16
 *
 */

@Entity
@Table(name="cuepoint")
@Setter
@Getter
public class CuePoint implements Serializable
{

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;


    @Column( name = "name", nullable = false )
    private String name;


    @Column( name = "description", nullable = false )
    private String description;


    @Column( name = "geographic_position", nullable = false )
    private String geographicPosition;


    @Temporal( TemporalType.TIMESTAMP )
    @Column( name = "date", nullable = false )
    private Date date;


    @ManyToOne()
    @JoinColumn( name = "tracking_id" )
    private Tracking tracking;


    @Transient
    private boolean modified;


    @Override
    public boolean equals( Object obj )
    {
        boolean equal = false;
        if(obj instanceof CuePoint)
        {
            CuePoint aux = (CuePoint) obj;

            equal = Objects.equals( aux.getId(), this.id );
        }

        return equal;
    }

}