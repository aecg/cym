package net.softclear.cym.backend.web.provider;

import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import lombok.Data;
import net.softclear.cym.backend.model.jpa.User;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  UserProvider
 * Descripcion:             Clase que posee el objeto del usuario almacenado
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Component
@Profile("prod")
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = "request")
@Data
public class UserProvider
{
	private User user;
}
