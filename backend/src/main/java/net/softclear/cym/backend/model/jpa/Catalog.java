package net.softclear.cym.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  Country
 * Descripcion:             Clase que emula la entidad Country dentro del sistema
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/9/2015
 *
 */
@Entity
@Table(name="catalag")
@Setter
@Getter
public class Catalog implements Serializable
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;

    @Column( name = "name", nullable = false )
    private String name;
}
