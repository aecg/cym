package net.softclear.cym.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;



@Entity
@Table(name = "mustseeanddo_detail")
@Setter
@Getter
public class MustSeeAndDoDetail implements Serializable
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;


    @Column( name = "title", nullable = false )
    private String title;


    @Column( name = "summary", nullable = false, length = 1000 )
    private String summary;


    @ManyToOne()
    @JoinColumn( name = "mustseeanddo_id" )
    private MustSeeAndDo mustseeanddo;
}