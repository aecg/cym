package net.softclear.cym.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;



@Entity
@Table(name="place")
@Setter
@Getter
public class Place implements Serializable
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;


    @Column( name = "name", nullable = false )
    private String name;


    @Column( name = "description", nullable = false )
    private String description;
}
