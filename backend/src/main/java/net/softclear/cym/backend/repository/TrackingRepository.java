package net.softclear.cym.backend.repository;



import net.softclear.cym.backend.model.jpa.Tracking;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  BusinessTypeRepository
 * Descripcion:             Contrato para todas las operaciones de la base de datos que involucran la entidad
 * businessType
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public interface TrackingRepository extends JpaRepository<Tracking, Long>
{
    /**
     * Nombre:                  findByUserUsername
     * Descripcion:             Metodo que consulta los datos del seguimiento por el nombre de usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   07/03/16
     *
     * @param username datos del usuario
     * @return lista de seguimientos
     */
    List<Tracking> findByUserUsername(String username);
}
