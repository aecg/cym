package net.softclear.cym.backend.repository;



import net.softclear.cym.backend.model.jpa.MustSeeAndDoDetail;
import net.softclear.cym.backend.model.jpa.MustSeeAndDoReality;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  BusinessTypeRepository
 * Descripcion:             Contrato para todas las operaciones de la base de datos que involucran la entidad
 * businessType
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public interface MustSeeAndDoRealityRepository extends JpaRepository<MustSeeAndDoReality, Long>
{
    List<MustSeeAndDoReality> findByMustseeanddoId( long id );

}
