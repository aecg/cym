package net.softclear.cym.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;



@Entity
@Table(name = "user_book")
@Setter
@Getter
public class UserBook implements Serializable
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;


    @ManyToOne()
    @JoinColumn( name = "book_id" )
    private Book book;


    @ManyToOne()
    @JoinColumn( name = "user_id" )
    private User user;
}
