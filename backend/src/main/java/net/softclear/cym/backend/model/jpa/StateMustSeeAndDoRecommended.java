package net.softclear.cym.backend.model.jpa;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "state_recommended_mustseeanddo")
@Setter
@Getter
public class StateMustSeeAndDoRecommended implements Serializable
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;

    @ManyToOne()
    @JoinColumn( name = "state_id" )
    private State state;


    @ManyToOne()
    @JoinColumn( name = "must_see_and_do_id" )
    private MustSeeAndDo mustSeeAndDo;
}
