package net.softclear.cym.backend.web.rest.response;



import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  TravelResponse
 * Descripcion:             Metodo que modela la respuesta que se necesita para cuando se consulta travel
 * @version                 1.0
 * @author                  guzmle
 * @since                   08/03/16
 *
 */
@Data
@JsonInclude( JsonInclude.Include.NON_NULL)
public class MustSeeAndDoResponse
{
    private long    id;
    private String  title;
    private String  summary;
    private String  geographic_position;
    private long    location_id;
    private long    state_id;
    private long    country_id;
    private long    catalog_id;
    private int     ar;
    private int     qr;
    private double  price;
    private byte[]  image_reference;
    private List<Obj> realities;

    private MustSeeAndDoARResponse     ar_code;
    private MustSeeAndDoQRResponse     qr_code;
    private String  imagePath;
}

@Data
class Obj
{
    private byte[] image;
}
