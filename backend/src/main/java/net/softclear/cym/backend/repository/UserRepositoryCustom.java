package net.softclear.cym.backend.repository;



import net.softclear.cym.backend.model.jpa.Country;
import net.softclear.cym.backend.model.jpa.State;
import net.softclear.cym.backend.model.jpa.User;

import java.util.List;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  StateRepositoryCustom
 * Descripcion:             Contrato para el respositorio de estados
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/9/2015
 *
 */
public interface UserRepositoryCustom
{
    /**
     * Nombre:                  findAllExceptCurrent
     * Descripcion:             Metodo que consulta todos los usuarios excepto el enviado
     * @version                 1.0
     * @author                  guzmle
     * @since                   17/03/16
     *
     * @param username nombre de usuario
     * @return lista de usuario excepto el enviado
     */
	List<User> findAllExceptCurrent( String username );
}
