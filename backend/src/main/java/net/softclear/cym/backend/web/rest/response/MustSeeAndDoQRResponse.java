package net.softclear.cym.backend.web.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * Created by softapple on 7/6/16.
 */
@Data
@JsonInclude( JsonInclude.Include.NON_NULL)
public class MustSeeAndDoQRResponse {
    private long    id;
    private String  title;
    private String  summary;
    private long    mustseeanddo_id;
}
