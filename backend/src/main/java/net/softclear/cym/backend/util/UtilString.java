package net.softclear.cym.backend.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  UtilString
 * Descripcion:             Clase que permite hacer operaciones con cadenas
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Component
public class UtilString
{
	private static final String ALGORITMO = "SHA1";


    /**
     * Nombre:                  getHash.
     * Descripcion:             Metodo que obtiene el hash de una cadena
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @param text texto a transformar
     * @return retorna el has de la cadena
     * @throws NoSuchAlgorithmException excepcion que ocurre cuando no se consigue el algoritmo
     */
	public String getHash(String text) throws NoSuchAlgorithmException
    {
		MessageDigest messageDigest;
        messageDigest = MessageDigest.getInstance(ALGORITMO);
		return getHex(messageDigest.digest(text.getBytes()));
	}


    /**
     * Nombre:                  getBase64.
     * Descripcion:             Metodo que obtiene el string de base64
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @param cipherText texto cifrado
     * @return texto en base64
     */
	public String getBase64(byte[] cipherText)
    {
		return Base64.getEncoder().encodeToString(cipherText);
	}


    /**
     * Nombre:                  getHex.
     * Descripcion:             Metodo que obtiene el hexadecimal de una cadena
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @param digestText texto en bytes
     * @return texto en hexadecimal
     */
	public String getHex(byte[] digestText)
    {
		StringBuffer result;
        result = new StringBuffer();
		for (byte b : digestText) result.append( String.format( "%02x", b ) );
		return result.toString();
	}


    /**
     * Nombre:                  validateCaptcha.
     * Descripcion:             Metodo que valida un captcha
     * @version                 1.0
     * @author                  guzmle
     * @since                   1/5/2016
     *
     * @param response respuesta del captcha obtenido desde el cliente
     * @return true si es vaido false si no
     */
    public boolean validateCaptcha( String response )
    {
        Client client;
        WebResource webResource;
        ClientResponse clientResponse;

        client = Client.create();
        String url = "https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s";
        url = String.format( url, "6LcunBITAAAAALq4Wpg6orpUC96rnjp6nbFJVWrG", response, "localhost" );

        webResource = client.resource( url );
        clientResponse = webResource.get( ClientResponse.class );

        if (clientResponse.getStatus() != HttpStatus.OK.value())
            throw new RuntimeException( "Failed : HTTP error code : " + clientResponse.getStatus() );

        return true;
    }
}
