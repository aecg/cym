package net.softclear.cym.backend.util;



import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.concurrent.TimeUnit;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  DateUtils
 * Descripcion:             Clase que realiza las operaciones sobre las fechas
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Component
public class DateUtils
{
    private static final long HOUR_PER_DAY = 24;
    private static final long MINUTE_PER_HOUR = 60;
    private static final long SECOND_PER_MINUTE = 60;
    private static final long MILISECOND_PER_SECOND = 1000;
    private static final long MILLSECS_PER_DAY = HOUR_PER_DAY * MINUTE_PER_HOUR * SECOND_PER_MINUTE *
            MILISECOND_PER_SECOND;


    /**
     * Nombre:                  getDaysLeft.
     * Descripcion:             Metodo que obtiene la diferencia entre 2 fechas
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @param initial fecha inicial
     * @param end fecha final
     * @return diferencia entre las fechas en dias
     */
    public long getDaysLeft(Date initial, Date end)
    {
        long diferencia;
        diferencia = ( end.getTime() - initial.getTime() );
        return TimeUnit.DAYS.convert(diferencia, TimeUnit.MILLISECONDS);
    }


    /**
     * Metodo que obtiene la diferencias entre las fechas en dias horas minutos y segundos.
     * @param initial fecha inicial
     * @param end fecha final
     * @return diferencia entre las fechas
     */
    public String getTimeLeft(Date initial, Date end)
    {
        String retorno = "";
        long diferencia;
        long diffDays;
        long daysMill;
        long diffHours;
        long hoursMill;
        long diffMinutes;
        long minMill;
        long diffSeconds;

        diferencia = ( end.getTime() - initial.getTime() );
        diffDays = diferencia / (HOUR_PER_DAY * MINUTE_PER_HOUR * SECOND_PER_MINUTE * MILISECOND_PER_SECOND);
        daysMill = diffDays * (HOUR_PER_DAY * MINUTE_PER_HOUR * SECOND_PER_MINUTE * MILISECOND_PER_SECOND);
        diffHours = (diferencia - daysMill ) / (MINUTE_PER_HOUR * SECOND_PER_MINUTE * MILISECOND_PER_SECOND);
        hoursMill = diffHours * (MINUTE_PER_HOUR * SECOND_PER_MINUTE * MILISECOND_PER_SECOND);
        diffMinutes = (diferencia - (daysMill + hoursMill)) / (SECOND_PER_MINUTE * MILISECOND_PER_SECOND);
        minMill = diffMinutes * (SECOND_PER_MINUTE * MILISECOND_PER_SECOND);
        diffSeconds = (diferencia - (daysMill + hoursMill + minMill)) / (MILISECOND_PER_SECOND);

        if(diffDays > 0)
            retorno += diffDays + " Dias ";
        if(diffHours > 0)
            retorno += diffHours + " Horas ";
        if(diffMinutes > 0)
            retorno += diffMinutes + " Minutos ";
        if(diffSeconds > 0)
            retorno += diffSeconds + " Segundos ";

        return retorno;
    }
}
