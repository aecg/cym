package net.softclear.cym.backend.repository.implementation;


import net.softclear.cym.backend.model.jpa.*;
import net.softclear.cym.backend.config.SettingsConfig;
import net.softclear.cym.backend.repository.UserRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;



public class UserRepositoryImpl implements UserRepositoryCustom
{

    //region Atributos

    /**
     * Objeto que posee la definicion de la persistencia.
     */
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SettingsConfig settingsConfig;
    //endregion


    //region Implementacion

    @Override
    public List<User> findAllExceptCurrent( String username )
    {
        List<User> response;

        List<Predicate> predicates = new ArrayList<>();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery( User.class );
        Root<User> root = criteriaQuery.from( User.class );


        predicates.add( criteriaBuilder.notEqual( root.get( "username" ), username ) );
        predicates.add( criteriaBuilder.notEqual( root.get( "username" ), settingsConfig.adminUser ) );

        criteriaQuery.where( predicates.toArray( new Predicate[ predicates.size() ] ) );
        criteriaQuery.select( root ).distinct( true );

        Query query = entityManager.createQuery(criteriaQuery);
        response = query.getResultList();

        return response;
    }

    //endregion
}
