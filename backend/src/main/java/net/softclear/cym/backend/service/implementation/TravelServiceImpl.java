package net.softclear.cym.backend.service.implementation;



import net.softclear.cym.backend.config.MessagesConfig;
import net.softclear.cym.backend.exception.ServiceException;
import net.softclear.cym.backend.model.jpa.*;
import net.softclear.cym.backend.repository.TrackingRepository;
import net.softclear.cym.backend.repository.TravelRepository;
import net.softclear.cym.backend.repository.UserRepository;
import net.softclear.cym.backend.service.TrackingService;
import net.softclear.cym.backend.service.TravelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;



/**
 * Sistema:                 CyM.
 * <p/>
 * Nombre:                  UserServiceImpl Descripcion:             Clase que implementa el contrato del servicio
 *
 * @author guzmle
 * @version 1.0
 * @since 11/30/2015
 */
@Service
@Transactional
public class TravelServiceImpl implements TravelService
{
    //region Atributos

    private static Logger logger = LoggerFactory.getLogger( TravelServiceImpl.class );
    @Autowired
    private TravelRepository repository;
    @Autowired
    private UserRepository   userRepository;
    @Autowired
    private MessagesConfig   messagesConfig;

    //endregion

    //region Metodos


    @Override
    public List<Travel> getByUserUsername( String username )
    {

        List<Travel> response;
        try
        {
            response = repository.findByUserUsername( username );
        }
        catch ( Exception e )
        {
            throw new ServiceException( messagesConfig.ERROR_GENERIC, e );
        }

        return response;
    }


    @Override
    public void saveTravelUser( String username, List<Travel> list )
    {
        User user = userRepository.findByUsername( username );
        if (user == null) {
            throw new ServiceException( messagesConfig.USER_NOT_FOUND );
        }
        try
        {
            List<Travel> travels = repository.findByUserUsername( username );

            for ( Travel travel : list )
            {
                int position = travels.indexOf( travel );
                //Si esta el track existe en la bd
                if ( position > -1 )
                {
                    Travel obj = travels.get( position );
                    repository.delete( obj );
                    //Si no es eliminado
                    if ( !travel.isDeleted() ) {
                        addTravel(travel, user);
                    }
                }
                else {
                    addTravel(travel, user);
                }
            }
        }
        catch ( Exception e )
        {
            throw new ServiceException( messagesConfig.ERROR_SYNCING_TRAVEL_USER, e );
        }

    }


    /**
     * Nombre:                  addTravel. Descripcion:             Metodo que agrega los datos del viaje
     *
     * @param travel datos del viaje a agregar
     *
     * @version 1.0
     * @author guzmle
     * @since 4/7/2016
     */
    private void addTravel( Travel travel, User user )
    {
        travel.setId( null );
        travel.setUser( user );


        if ( travel.getUniqueId() == null || travel.getUniqueId().length() <= 0 ) {
            String uniqueID = UUID.randomUUID().toString();
            travel.setUniqueId(uniqueID);
        }

        if ( travel.getDestination() != null ) {
            for ( Destination destination : travel.getDestination() ) {
                destination.setId( null );
                destination.setTravel( travel );

                if ( destination.getLocation().getId() == 0 ) {
                    destination.setLocation(null);
                }

                if ( destination.getMustseeanddo() != null ) {
                    for ( TravelMustSeeAndDo must : destination.getMustseeanddo() ) {
                        must.setId( null );
                        must.setDestination( destination );
                    }
                }
            }
        }

        if ( travel.getPartner() != null ) {
            List<TravelPartner> partnersToRemove = new ArrayList<TravelPartner>();
            for ( TravelPartner partner : travel.getPartner() ) {
                if (! partner.isDeleted()) {
                    partner.setId(null);
                    partner.setUser(userRepository.findByUsername(partner.getUser().getUsername()));
                    partner.setTravel(travel);
                }
                else {
                    partnersToRemove.add(partner);
                }
            }
            for ( TravelPartner partner : partnersToRemove ) {
                travel.getPartner().remove(partner);
            }
        }

        repository.save( travel );
    }

    //endregion
}
