package net.softclear.cym.backend.web.rest;

import net.softclear.cym.backend.model.jpa.*;
import net.softclear.cym.backend.service.MustSeeAndDoService;
import net.softclear.cym.backend.util.MustSeeAndDoRecommendedTO;
import net.softclear.cym.backend.web.rest.response.*;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.List;

@RestController
public class MustSeeAndDoRestController
{
    //region Atributos

    @Autowired
    private ModelMapper         modelMapper;
    @Autowired
    private MustSeeAndDoService service;

    //endregion

    //region Servicios


    /**
     * Nombre:                  getUserByUserName
     * Descripcion:             Servicio que consulta los datos del usuario dado su nombre de usuario
     * @version 1.0
     * @author guzmle
     * @since 07/03/16
     *
     * @return datos del usuario
     */
    @RequestMapping( value = "/mustseeanddo/{id}", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    ListMustSeeAndDoResponse getByBook( @PathVariable long id )
    {

        ListMustSeeAndDoResponse response = new ListMustSeeAndDoResponse();
        List<MustSeeAndDo> mustSeeAndDos = service.getByBook( id );

        Type listDTOType = new TypeToken<List<MustSeeAndDoResponse>>(){}.getType();
        List<MustSeeAndDoResponse> list = modelMapper.map(mustSeeAndDos, listDTOType);

        response.setMustseeanddo( list );
        return response;

    }


    /**
     * Nombre:                  getCatalogs
     * Descripcion:             Metodo que obtiene todos los catalogos registrados en el sistema
     * @version                 1.0
     * @author                  guzmle
     * @since                   18/04/16
     *
     * @return
     */
    @RequestMapping( value = "/catalogs", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    ListCatalogResponse getCatalogs()
    {

        ListCatalogResponse response = new ListCatalogResponse();
        List<Catalog> objs = service.getCatalogs();

        Type listDTOType = new TypeToken<List<CatalogResponse>>(){}.getType();
        List<CatalogResponse> list = modelMapper.map(objs, listDTOType);

        response.setCatalogs( list );
        return response;

    }


    /**
     * Nombre:                  getUserByUserName
     * Descripcion:             Servicio que consulta los datos del usuario dado su nombre de usuario
     * @version 1.0
     * @author guzmle
     * @since 07/03/16
     *
     * @return datos del usuario
     */
    @RequestMapping( value = "/location/{id}/mustseeanddos", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    ListMustSeeAndDoResponse getByLocation( @PathVariable long id )
    {

        ListMustSeeAndDoResponse response = new ListMustSeeAndDoResponse();
        List<MustSeeAndDo> travels = service.getByLocationId( id );

        Type listDTOType = new TypeToken<List<MustSeeAndDoResponse>>(){}.getType();
        List<MustSeeAndDoResponse> list = modelMapper.map(travels, listDTOType);

        response.setMustseeanddo( list );
        return response;

    }

    @RequestMapping( value = "/location/{id}/mustseeanddossimple", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    ListMustSeeAndDoSimpleResponse getSimpleByLocation(@PathVariable long id )
    {

        ListMustSeeAndDoSimpleResponse response = new ListMustSeeAndDoSimpleResponse();
        List<MustSeeAndDo> travels = service.getByLocationId( id );

        Type listDTOType = new TypeToken<List<MustSeeAndDoSimpleResponse>>(){}.getType();
        List<MustSeeAndDoSimpleResponse> list = modelMapper.map(travels, listDTOType);

        response.setMustseeanddo( list );
        return response;

    }

    /**
     * Nombre:                  getDetailsByBook
     * Descripcion:             Servicio que obtiene los detalles de los must see and do de un libro
     *
     * @return los detalles de must see and do's
     */
    @RequestMapping( value = "/msddetail/book/{id}", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    ListMustSeeAndDoDetailResponse getDetailsByBook( @PathVariable long id )
    {

        ListMustSeeAndDoDetailResponse response = new ListMustSeeAndDoDetailResponse();
        List<MustSeeAndDoDetail> travels = service.getDetailByBook( id );

        Type listDTOType = new TypeToken<List<MustSeeAndDoDetailResponse>>(){}.getType();
        List<MustSeeAndDoDetailResponse> list = modelMapper.map(travels, listDTOType);

        response.setMustseeanddo_detail( list );
        return response;

    }

    /**
     * Nombre:                  getDetailsByMustSeeAndDo
     * Descripcion:             Servicio que obtiene los detalles de un must see and do
     *
     * @return los detalles de un must see and do
     */
    @RequestMapping( value = "/msddetail/msad/{id}", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    ListMustSeeAndDoDetailResponse getDetailsByMustSeeAndDo( @PathVariable long id )
    {

        ListMustSeeAndDoDetailResponse response = new ListMustSeeAndDoDetailResponse();
        List<MustSeeAndDoDetail> travels = service.getDetailByMustSeeAndDo( id );

        Type listDTOType = new TypeToken<List<MustSeeAndDoDetailResponse>>(){}.getType();
        List<MustSeeAndDoDetailResponse> list = modelMapper.map(travels, listDTOType);

        response.setMustseeanddo_detail( list );
        return response;

    }

    @RequestMapping( value = "/msdqr/{id}", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    MustSeeAndDoQRResponse getQRCode( @PathVariable long id ) {

        MustSeeAndDoQR mustSeeAndDoQR = service.getQRCode(id);

        Type mustSeeAndDoType = new TypeToken<MustSeeAndDoQRResponse>(){}.getType();
        MustSeeAndDoQRResponse response = modelMapper.map(mustSeeAndDoQR, mustSeeAndDoType);

        return response;
    }

    @RequestMapping( value = "/msdar/{id}", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    MustSeeAndDoARResponse getARCode( @PathVariable long id ) {

        MustSeeAndDoAR mustSeeAndDoAR = service.getARCode(id);

        Type mustSeeAndDoARType = new TypeToken<MustSeeAndDoARResponse>(){}.getType();
        MustSeeAndDoARResponse response = modelMapper.map(mustSeeAndDoAR, mustSeeAndDoARType);

        return response;
    }

    /**
     * Nombre:                  saveLocation
     * Descripcion:             Metodo que guarda los datos de una locacion
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/04/16
     *
     * @param request datos que se van almacenar
     */
    @RequestMapping( value = "/mustseeanddo", method = RequestMethod.POST, consumes = "application/json" )
    @ResponseStatus( HttpStatus.OK )
    public void saveMustSeeAndDo(@RequestBody AddMustSeeAndDoRequest request)
    {
        MustSeeAndDo obj = modelMapper.map(request.getMust(), MustSeeAndDo.class);

        Type listDTOType = new TypeToken<List<MustSeeAndDoDetail>>(){}.getType();
        List<MustSeeAndDoDetail> list = modelMapper.map(request.getDetail(), listDTOType);

        service.saveMustSeeAndDo( obj, list );
    }


    @RequestMapping( value = "/msdrecommended/{stateid}", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    ListStateMustSeeAndDoRecommendedResponse getRecommended( @PathVariable long stateid )
    {

        MustSeeAndDoRecommendedTO mustSeeAndDoRecommendedTO = service.getStateMustSeeAndDoRecommended ( stateid );

        ListStateMustSeeAndDoRecommendedResponse response = new ListStateMustSeeAndDoRecommendedResponse();

        List<StateMustSeeAndDoRecommended> stateMustSeeAndDoRecommendeds = mustSeeAndDoRecommendedTO.getStateMustSeeAndDoRecommendeds();
        List<MustSeeAndDo> mustSeeAndDos = mustSeeAndDoRecommendedTO.getMustSeeAndDos();
        List<MustSeeAndDoDetail> mustSeeAndDoDetails = mustSeeAndDoRecommendedTO.getMustSeeAndDoDetails();

        Type listDTOType = new TypeToken<List<StateMustSeeAndDoRecommendedResponse>>(){}.getType();
        List<StateMustSeeAndDoRecommendedResponse> stateMustSeeAndDoRecommendedResponseList = modelMapper.map(stateMustSeeAndDoRecommendeds, listDTOType);

        Type listDTOType2 = new TypeToken<List<MustSeeAndDoResponse>>(){}.getType();
        List<MustSeeAndDoResponse> mustSeeAndDoResponseList = modelMapper.map(mustSeeAndDos, listDTOType2);

        Type listDTOType3 = new TypeToken<List<MustSeeAndDoDetailResponse>>(){}.getType();
        List<MustSeeAndDoDetailResponse> mustSeeAndDoDetailResponseList = modelMapper.map(mustSeeAndDoDetails, listDTOType3);

        response.setStateMustSeeAndDoRecommendedResponses ( stateMustSeeAndDoRecommendedResponseList );
        response.setMustSeeAndDoResponses ( mustSeeAndDoResponseList );
        response.setMustSeeAndDoDetailResponses( mustSeeAndDoDetailResponseList );

        return response;
    }

    @RequestMapping( value = "/msad/{id}", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    MustSeeAndDoResponse getMustSeeAndDo( @PathVariable long id ) {
        MustSeeAndDo mustSeeAndDo = service.getMustSeeAndDo(id);

        Type msadDTOType = new TypeToken<MustSeeAndDoResponse>(){}.getType();
        MustSeeAndDoResponse response = modelMapper.map(mustSeeAndDo, msadDTOType);

        return response;
    }

    @RequestMapping( value = "/msad/optimize", method = RequestMethod.GET )
    public void optimizeImage( ) {
        service.optimizeImages();
    }
    //endregion
}
