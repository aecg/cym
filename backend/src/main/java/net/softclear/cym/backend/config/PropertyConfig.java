package net.softclear.cym.backend.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  PropertyConfig
 * Descripcion:             Clase que define las propiedades que se usan dentro del proyecto
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Configuration
@PropertySources({
        @PropertySource("classpath:database.properties"),
        @PropertySource("classpath:messages.properties"),
        @PropertySource("classpath:mail.properties"),
        @PropertySource("classpath:settings.properties")
})
public class PropertyConfig
{
    /**
     * Nombre:                  propertySourcesPlaceholderConfigurer.
     * Descripcion:             Metodo que obtiene las propieades que se obtienen
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @return objeto
     */
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer()
    {
		return new PropertySourcesPlaceholderConfigurer();
	}


    /**
     * Instantiates a new FooUtilityService. Private to prevent instantiation
     */
    protected PropertyConfig()
    {
    }
}
