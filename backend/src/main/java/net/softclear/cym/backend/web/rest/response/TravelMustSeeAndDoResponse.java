package net.softclear.cym.backend.web.rest.response;



import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  TravelMustSeeAndDoResponse
 * Descripcion:             Clase que modela la entidad travel must see and do para la respuesta json
 * @version                 1.0
 * @author                  guzmle
 * @since                   08/03/16
 *
 */
@Data
@JsonInclude( JsonInclude.Include.NON_NULL)
public class TravelMustSeeAndDoResponse
{
    private long   mustseeanddo_id;
}
