package net.softclear.cym.backend.repository;



import net.softclear.cym.backend.model.jpa.LocationBook;
import net.softclear.cym.backend.model.jpa.MustSeeAndDo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  BusinessTypeRepository
 * Descripcion:             Contrato para todas las operaciones de la base de datos que involucran la entidad
 * businessType
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public interface LocationBookRepository extends JpaRepository<LocationBook, Long>
{
    /**
     * Nombre:                  findByBookId
     * Descripcion:             Metodo que consulta todas las relaciones entre locaciones y libros por identificado del libro
     * @version                 1.0
     * @author                  guzmle
     * @since                   16/03/16
     *
     * @param id identificador del libro
     * @return lista de relaciones
     */
    List<LocationBook> findByBookId(long id);

}
