package net.softclear.cym.backend.web.rest.error;



/**
 * Sistema:                 CyM.
 * <p/>
 * Nombre:                  ErrorCode Descripcion:             Enumerado que permite asignar un codigo de error a una
 * excepcion
 *
 * @author guzmle
 * @version 1.0
 * @since 12/22/2015
 */
public enum ErrorCode
{
    /**
     * codigo de error para cualquier excepcion.
     */
    SERVICE_ERROR( 1l ),


    /**
     * codigo de error para cualquier excepcion.
     */
    NOT_FOUND( 100L ),


    /**
     * codigo de error para cualquier excepcion.
     */
    USER_EXIST( 101L ),


    /**
     * codigo de error para una excepcion generica.
     */
    GENERIC_ERROR( 200L ),


    /**
     * Codigo de error para los valores invalidos
     */
    INVALID_DATA( 300L );


    private Long value;


    ErrorCode( Long code )
    {
        this.value = code;
    }


    public Long getValue()
    {
        return value;
    }
}
