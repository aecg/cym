package net.softclear.cym.backend.web.rest;

import net.softclear.cym.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  OAuthController
 * Descripcion:             Servicio para todas las operaciones del oauth * @version                 1.0
 * @author                  guzmle
 * @since                   12/3/2015
 *
 */
@Controller
public class OAuthController
{
    @Autowired
    private TokenStore  tokenStore;
    @Autowired
    private UserService userService;


    /**
     * Nombre:                  logout.
     * Descripcion:             Servicio que realiza el logout de la aplicacion
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @param request datos de la peticion
     */
    @RequestMapping( value = "/oauth/revoke-token", method = RequestMethod.GET )
    @ResponseStatus( HttpStatus.OK )
    public void logout( HttpServletRequest request )
    {
        String authHeader;
        String tokenValue;
        OAuth2AccessToken accessToken;

        authHeader = request.getHeader( "Authorization" );
        if ( authHeader != null )
        {
            tokenValue = authHeader.replace( "Bearer", "" ).trim();
            accessToken = tokenStore.readAccessToken( tokenValue );
            tokenStore.removeAccessToken( accessToken );
        }
    }


}
