package net.softclear.cym.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;



@Entity
@Table(name = "seedo_attachment")
@Setter
@Getter
public class SeeDoAttachment implements Serializable
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;


    @Column( name = "name", nullable = false )
    private String name;


    @Lob
    @Column( name = "file", nullable = false, columnDefinition="LONGBLOB" )
    private byte[] file;


    @ManyToOne()
    @JoinColumn( name = "mustseeanddo_id" )
    private MustSeeAndDo mustSeeAndDo;
}
