package net.softclear.cym.backend.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  OAuth2ServerConfig
 * Descripcion:             Clase que configura el OAuth en la aplicacion
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
@Configuration
public class OAuth2ServerConfig
{
    /**
     * Clase que configura el acceso a los recursos del sistema.
     */
	@Configuration
	@EnableResourceServer
	protected static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter
    {

		@Override
		public void configure(HttpSecurity http) throws Exception
        {
			http.authorizeRequests()
				.antMatchers("/desktop/**").access("hasRole('ROLE_DESKTOP')")
			    .antMatchers("/web*//**").access("hasRole('ROLE_ADMIN')");
		}

	}


    /**
     * Clase que configura la autorizacion del acceso a los recursos del sistema.
     */
	@Configuration
	@EnableAuthorizationServer
	protected static class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter
    {
		@Autowired
		@Qualifier("authenticationManagerBean")
		private AuthenticationManager authenticationManager;
		@Autowired
		private DataSource dataSource;


		@Override
		public void configure(ClientDetailsServiceConfigurer clients) throws Exception
        {
			clients.inMemory()
	 		    .withClient("frontend")
 			        .authorizedGrantTypes("password")
 			        .scopes("read", "write");
		}


		@Bean
		public TokenStore tokenStore()
        {
			return new JdbcTokenStore(dataSource);
		}


		@Override
		public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception
        {
			endpoints.tokenStore(tokenStore())
					.authenticationManager(authenticationManager);
		}
	}
}
