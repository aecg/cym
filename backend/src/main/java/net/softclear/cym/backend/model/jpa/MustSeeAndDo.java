package net.softclear.cym.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  Country
 * Descripcion:             Clase que emula la entidad Country dentro del sistema
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/9/2015
 *
 */
@Entity
@Table(name="mustseeanddo")
@Setter
@Getter
public class MustSeeAndDo implements Serializable
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;


    @Column( name = "title", nullable = false )
    private String title;


    @Column( name = "summary", nullable = false, length = 1000 )
    private String summary;


    @Lob
    @Column( name = "image", nullable = true, columnDefinition="LONGBLOB" )
    private byte[] image;


    @Column( name = "ar", nullable = false )
    private int ar;

    @Column( name = "qr", nullable = false )
    private int qr;


    @Column( name = "geographic_position", nullable = false )
    private String geographicPosition;


    @Column( name = "price", nullable = false )
    private double price;


    @ManyToOne()
    @JoinColumn( name = "location_id" )
    private Location location;


    @ManyToOne()
    @JoinColumn( name = "catalog_id" )
    private Catalog catalog;


    @OneToMany( mappedBy = "mustseeanddo", cascade = CascadeType.ALL )
    private List<MustSeeAndDoReality> realities;

    @OneToOne(mappedBy = "mustseeanddo", cascade = CascadeType.ALL)
    private MustSeeAndDoQR qrcode;

    @OneToOne(mappedBy = "mustseeanddo", cascade = CascadeType.ALL)
    private MustSeeAndDoAR arcode;

    @Column( nullable = true )
    private String imagePath;
}