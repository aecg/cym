package net.softclear.cym.backend.repository;



import net.softclear.cym.backend.model.jpa.Country;
import net.softclear.cym.backend.model.jpa.Location;
import net.softclear.cym.backend.model.jpa.State;

import java.util.List;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  StateRepositoryCustom
 * Descripcion:             Contrato para el respositorio de estados
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/9/2015
 *
 */
public interface BookRepositoryCustom
{
    /**
     * Nombre:                  findLocationsByBook
     * Descripcion:             Metodo que consulta las locaciones de un libro
     * @version                 1.0
     * @author                  guzmle
     * @since                   17/03/16
     *
     * @param id codigo del libro
     * @return lista de locaciones
     */
	List<Location> findLocationsByBook( long id );


    /**
     * Nombre:                  findStateByBook
     * Descripcion:             Metodo que consulta los estados de un libro
     * @version                 1.0
     * @author                  guzmle
     * @since                   17/03/16
     *
     * @param id identificador del libro
     * @return lista de estados
     */
    List<State> findStateByBook( long id );


    /**
     * Nombre:                  findCountryByBook
     * Descripcion:             Metodo que consulta los paises de un libro
     * @version                 1.0
     * @author                  guzmle
     * @since                   17/03/16
     *
     * @param id indetificador del libro
     * @return lista de paises
     */
    List<Country> findCountryByBook( long id );
}
