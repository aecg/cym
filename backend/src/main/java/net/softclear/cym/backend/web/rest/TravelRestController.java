package net.softclear.cym.backend.web.rest;



import net.softclear.cym.backend.model.jpa.Tracking;
import net.softclear.cym.backend.model.jpa.Travel;
import net.softclear.cym.backend.service.TravelService;
import net.softclear.cym.backend.web.rest.response.ListTrackingResponse;
import net.softclear.cym.backend.web.rest.response.ListTravelResponse;
import net.softclear.cym.backend.web.rest.response.TravelResponse;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.List;



@RestController
@RequestMapping("/travels*")
public class TravelRestController
{
    //region Atributos

    @Autowired
    private ModelMapper   modelMapper;
    @Autowired
    private TravelService service;

    //endregion

    //region Servicios


    /**
     * Nombre:                  getUserByUserName
     * Descripcion:             Servicio que consulta los datos del usuario dado su nombre de usuario
     * @version 1.0
     * @author guzmle
     * @since 07/03/16
     *
     * @return datos del usuario
     */
    @RequestMapping( value = "/{username}", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    ListTravelResponse getAll( @PathVariable String username )
    {

        ListTravelResponse response = new ListTravelResponse();
        List<Travel> travels = service.getByUserUsername( username );

        Type listDTOType = new TypeToken<List<TravelResponse>>(){}.getType();
        List<TravelResponse> list = modelMapper.map(travels, listDTOType);

        response.setUsername( username );
        response.setTravels( list );
        return response;

    }


    /**
     * Nombre:                  saveUserBook
     * Descripcion:             Metodo que almacena la relacion del usuario con el libro
     * @version                 1.0
     * @author                  guzmle
     * @since                   29/03/16
     *
     * @param request datos del usuario y del libro
     */
    @RequestMapping ( method = RequestMethod.PUT, consumes = "application/json")
    @ResponseStatus( HttpStatus.OK )
    public void saveTravelUser (@RequestBody ListTravelResponse request)
    {
        Type listDTOType = new TypeToken<List<Travel>>(){}.getType();
        List<Travel> list = modelMapper.map( request.getTravels(), listDTOType );
        service.saveTravelUser( request.getUsername(), list );
    }
    //endregion
}
