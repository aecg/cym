package net.softclear.cym.backend.web.rest;

import net.softclear.cym.backend.model.jpa.*;
import net.softclear.cym.backend.service.LocationService;
import net.softclear.cym.backend.web.rest.response.*;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;



/**
 * Sistema:                 CrowdEI.
 *
 * Nombre:                  UserRestController
 * Descripcion:             Clase que posee la definicion y la implementacion de los servicios que
 * involucran a la entidad usuario
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/4/2015
 *
 */
@RestController
public class LocationRestController
{
    //region Attributes

    @Autowired
    private ModelMapper     modelMapper;
    @Autowired
    private LocationService service;

    //endregion

    //region Services


    /**
     * Nombre:                  getAllCountries.
     * Descripcion:             Metodo que obtiene todos los paises almacenados en la base de datos
     * @version 1.0
     * @author guzmle
     * @since 12/9/2015
     *
     * @return lista de paises
     */
    @RequestMapping( value = "/countries", method = RequestMethod.GET, produces = "application/json" )
    public
    @ResponseBody
    ListCountryResponse getAllCountries()
    {

        ListCountryResponse response = new ListCountryResponse();
        List<Country> objs = service.getCountries( );

        Type listDTOType = new TypeToken<List<CountryResponse>>(){}.getType();
        List<CountryResponse> list = modelMapper.map(objs, listDTOType);

        response.setCountries( list );
        return response;
    }


    /**
     * Nombre:                  getAllStates.
     * Descripcion:             Metodo que obtiene todos los estado de un pais
     * @version 1.0
     * @author guzmle
     * @since 12/9/2015
     *
     * @param id identificador del pais
     * @return lista de estados
     */
    @RequestMapping( value = "/states", method = RequestMethod.GET, produces =
            "application/json" )
    public
    @ResponseBody
    ListStateResponse getAllStates( )
    {

        ListStateResponse response = new ListStateResponse();
        List<State> objs = service.getStates();

        Type listDTOType = new TypeToken<List<StateResponse>>(){}.getType();
        List<StateResponse> list = modelMapper.map(objs, listDTOType);

        response.setStates( list );
        return response;
    }



    /**
     * Nombre:                  getAllStates.
     * Descripcion:             Metodo que obtiene todos los estado de un pais
     * @version 1.0
     * @author guzmle
     * @since 12/9/2015
     *
     * @param id identificador del pais
     * @return lista de estados
     */
    @RequestMapping( value = "/country/{id}/states", method = RequestMethod.GET, produces =
            "application/json" )
    public
    @ResponseBody
    ListStateResponse getStatesByCountry( @PathVariable int id )
    {

        ListStateResponse response = new ListStateResponse();
        List<State> objs = service.getStatesByCountry( id );

        Type listDTOType = new TypeToken<List<StateResponse>>(){}.getType();
        List<StateResponse> list = modelMapper.map(objs, listDTOType);

        response.setStates( list );
        return response;
    }


    /**
     * Nombre:                  getAllCities.
     * Descripcion:             Metodo que obtiene todas las ciudades de un estado
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/9/2015
     *
     * @param id identificador del estado
     * @return lista de ciudades
     */
    @RequestMapping( value = "/locations", method = RequestMethod.GET, produces =
            "application/json" )
    public @ResponseBody ListLocationResponse getAllCities( )
    {

        ListLocationResponse response = new ListLocationResponse();
        List<Location> objs = service.getLocations();

        Type listDTOType = new TypeToken<List<LocationResponse>>(){}.getType();
        List<LocationResponse> list = modelMapper.map(objs, listDTOType);

        response.setLocations( list );
        return response;
    }


    /**
     * Nombre:                  getAllCities.
     * Descripcion:             Metodo que obtiene todas las ciudades de un estado
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/9/2015
     *
     * @param id identificador del estado
     * @return lista de ciudades
     */
    @RequestMapping( value = "/state/{id}/locations", method = RequestMethod.GET, produces =
            "application/json" )
    public @ResponseBody ListLocationResponse getCitiesByState( @PathVariable long id )
    {

        ListLocationResponse response = new ListLocationResponse();
        List<Location> objs = service.getLocationsByState(id);

        Type listDTOType = new TypeToken<List<LocationResponse>>(){}.getType();
        List<LocationResponse> list = modelMapper.map(objs, listDTOType);

        response.setLocations( list );
        return response;
    }


    /**
     * Nombre:                  addCountry
     * Descripcion:             Metodo que agrega o modifica los datos de un pais
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/04/16
     *
     * @param request datos del pais a modificar o a agregar
     */
    @RequestMapping( value = "/countries", method = RequestMethod.POST, consumes = "application/json" )
    @ResponseStatus( HttpStatus.OK )
    public void saveCountry(@RequestBody CountryResponse request)
    {

        Country country = modelMapper.map(request, Country.class);
        service.saveCountry( country );
    }


    /**
     * Nombre:                  addCountry
     * Descripcion:             Metodo que agrega o modifica los datos de un pais
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/04/16
     *
     * @param request datos del pais a modificar o a agregar
     */
    @RequestMapping( value = "/states", method = RequestMethod.POST, consumes = "application/json" )
    @ResponseStatus( HttpStatus.OK )
    public void saveState(@RequestBody StateResponse request)
    {

        State obj = modelMapper.map(request, State.class);
        service.saveState( obj );
    }


    /**
     * Nombre:                  saveLocation
     * Descripcion:             Metodo que guarda los datos de una locacion
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/04/16
     *
     * @param request datos que se van almacenar
     */
    @RequestMapping( value = "/locations", method = RequestMethod.POST, consumes = "application/json" )
    @ResponseStatus( HttpStatus.OK )
    public void saveLocation(@RequestBody LocationResponse request)
    {
        Location obj = modelMapper.map(request, Location.class);
        service.saveLocation( obj );
    }
    //endregion
}
