package net.softclear.cym.backend.web.rest.response;



import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;



@Data
@JsonInclude( JsonInclude.Include.NON_NULL)
public class DataUserResponse
{
    private String username;
    private String password;
    private Item dataUser;
}


@Data
class Item
{
    private String name;
    private String lastname;
    private String email;
    private String phone;
}