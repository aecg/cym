package net.softclear.cym.backend.service;

import net.softclear.cym.backend.model.jpa.*;
import net.softclear.cym.backend.web.rest.response.MustSeeAndDoSimpleResponse;

import java.util.List;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  UserService
 * Descripcion:             Contrato para los servicios que involucran la entidad Usuario
 * @version                 1.0
 * @author                  guzmle
 * @since                   11/30/2015
 *
 */
public interface BookService
{
    /**
     * Nombre:                  getBooks
     * Descripcion:             Metodo que consulta todos los libros de la base de datos
     * @version                 1.0
     * @author                  guzmle
     * @since                   16/03/16
     *
     * @return lista de libros
     */
	List<Book> getBooks();

    /**
     * Nombre:                  getLocationsByBooks
     * Descripcion:             Metodo que consulta las locaciones de un libro
     * @version                 1.0
     * @author                  guzmle
     * @since                   16/03/16
     *
     * @param id identificador del libr
     * @return lista de relaciones
     */
    List<LocationBook> getLocationsByBooks(long id);

    /**
     * Nombre:                  getLocationsOfBooks
     * Descripcion:             Metodo que consulta la lista de locaciones de un libro
     * @version                 1.0
     * @author                  guzmle
     * @since                   17/03/16
     *
     * @param id codigo del libro
     * @return lista de locaciones
     */
    List<Location> getLocationsOfBooks(long id);

    /**
     * Nombre:                  getStatesOfBooks
     * Descripcion:             Metodo que consulta la lista de locaciones de un libro
     * @version                 1.0
     * @author                  guzmle
     * @since                   17/03/16
     *
     * @param id codigo del libro
     * @return lista de locaciones
     */
    List<State> getStatesOfBooks(long id);

    /**
     * Nombre:                  getCountryOfBooks
     * Descripcion:             Metodo que consulta los paises de un libro
     * @version                 1.0
     * @author                  guzmle
     * @since                   17/03/16
     *
     * @param id id del libro
     * @return lista de paises
     */
    List<Country> getCountryOfBooks(long id);

    /**
     * Nombre:                  getBooksByUser
     * Descripcion:             Metodo que consulta los libros de un usuario
     * @version                 1.0
     * @author                  guzmle
     * @since                   21/03/16
     *
     * @param username nombre del usuario
     * @return lista de libros del usuario
     */
    List<UserBook> getBooksByUser(String username);

    /**
     * Nombre:                  saveUserBook
     * Descripcion:             Metodo que guarda la relacion de un usuario con el libro
     * @version                 1.0
     * @author                  guzmle
     * @since                   29/03/16
     *
     * @param username nombre del usuario
     * @param codeBook codigo del libro
     */
    void saveUserBook( String username, long codeBook );

    /**
     * Nombre:                  saveBook
     * Descripcion:             Metodo que guarda los datos de un libro
     * @version                 1.0
     * @author                  guzmle
     * @since                   13/04/16
     *
     * @param book datos del libro
     * @param list lista de MustSeeAndDo de libros
     */
    void saveBook( Book book, List<MustSeeAndDoSimpleResponse> list );

    List<MustSeeAndDo> getMustSeeAndDos(long id);

    Book getBook(long id);

    void optimizeImages();
}
