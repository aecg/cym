package net.softclear.cym.backend.web.rest.response;



import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  CuePointResponse
 * Descripcion:             Clase que modela el cuepoint para el json
 * @version                 1.0
 * @author                  guzmle
 * @since                   07/03/16
 *
 */
@Data
@JsonInclude( JsonInclude.Include.NON_NULL)
public class CuePointResponse
{
    private String name;
    private String description;
    private String geographic_position;
    private long date;
    private String unique_id;
    private int modified;
}
