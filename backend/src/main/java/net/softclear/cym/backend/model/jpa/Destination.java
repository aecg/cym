package net.softclear.cym.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;



@Entity
@Table(name="destination")
@Setter
@Getter
public class Destination implements Serializable
{

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;


    @Temporal( TemporalType.TIMESTAMP )
    @Column( name = "date", nullable = false )
    private Date date;


    @Column( name = "description", nullable = true )
    private String description;


    @ManyToOne()
    @JoinColumn( name = "travel_id" )
    private Travel travel;


    @ManyToOne()
    @JoinColumn( name = "state_id", nullable = true )
    private State state;


    @ManyToOne()
    @JoinColumn( name = "location_id", nullable = true )
    private Location location;


    @OneToMany( mappedBy = "destination", cascade = CascadeType.ALL )
    private List<TravelMustSeeAndDo> mustseeanddo;
}
