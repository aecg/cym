package net.softclear.cym.backend.model.jpa;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;



@Entity
@Table(name = "mustseeanddo_reality")
@Setter
@Getter
public class MustSeeAndDoReality implements Serializable
{
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    @Column( unique = true, nullable = false )
    private Long id;

    @Lob
    @Column( name = "image", nullable = false, columnDefinition="LONGBLOB" )
    private byte[] image;

    @ManyToOne()
    @JoinColumn( name = "mustseeanddo_id" )
    private MustSeeAndDo mustseeanddo;
}