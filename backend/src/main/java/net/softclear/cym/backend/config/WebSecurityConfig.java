package net.softclear.cym.backend.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  WebSecurityConfig
 * Descripcion:             Clase que permite la configuracion del site
 * @version                 1.0
 * @author                  guzmle
 * @since                   12/22/2015
 *
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter
{
	private static final String SQL_USERS_BY_USERNAME = "select username, password, enabled " +
            "from user where username = ?";
	private static final String SQL_AUTHORITIES_BY_USERNAME = "select u.username, a.authority " +
            "from authority a inner join user u on u.id = a.user_id where u.username = ?";
	
	@Autowired
	private DataSource dataSource;


    /**
     * Nombre:                  globalUserDetails.
     * Descripcion:             Metodo que asigna el detalle de los usuarios
     * @version                 1.0
     * @author                  guzmle
     * @since                   12/22/2015
     *
     * @param auth objeto con la autorizacion
     * @throws Exception excepcion por si ocurre un error
     */
	@Autowired
    public void globalUserDetails(AuthenticationManagerBuilder auth) throws Exception
    {
		auth.jdbcAuthentication()
			.usersByUsernameQuery(SQL_USERS_BY_USERNAME)
			.authoritiesByUsernameQuery(SQL_AUTHORITIES_BY_USERNAME)
			.dataSource(dataSource);
    }


    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception
    {
        return super.authenticationManagerBean();
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http.csrf().disable();
    }
}
