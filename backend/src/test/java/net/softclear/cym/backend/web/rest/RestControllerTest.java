package net.softclear.cym.backend.web.rest;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import net.softclear.cym.backend.config.BeanConfig;
import net.softclear.cym.backend.config.DatabaseConfig;
import net.softclear.cym.backend.config.PropertyConfig;
import net.softclear.cym.backend.web.provider.UserProvider;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration
@ActiveProfiles("test")
@Transactional
@Ignore
public class RestControllerTest {
	@Configuration
	@EnableWebMvc
	@ComponentScan(
			basePackages = {
                    "net.softclear.cym.backend.service",
                    "net.softclear.cym.backend.web",
                    "net.softclear.cym.backend.util"
			},
			basePackageClasses = {
					BeanConfig.class,
					DatabaseConfig.class,
					PropertyConfig.class
			}			
	)
	public static class Config {

		@Bean
		public UserProvider userProvider() {
			return new UserProvider();
		}
		
	}
}
