package net.softclear.cym.backend.service;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import net.softclear.cym.backend.config.BeanConfig;
import net.softclear.cym.backend.config.DatabaseConfig;
import net.softclear.cym.backend.config.PropertyConfig;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@WebAppConfiguration
@ActiveProfiles("test")
@Transactional
@Ignore
public class ServiceTest {

	@Configuration
	@ComponentScan(
			basePackages = {
                    "net.softclear.cym.backend.service",
                    "net.softclear.cym.backend.util"
			},
			basePackageClasses = {
					BeanConfig.class,
					DatabaseConfig.class,
					PropertyConfig.class
			}			
	)
	public static class Config
    {
        /**
         * Nombre:                  freemarkerConfig. Descripcion:             Metodo que inicializa el bean de
         * freemarker
         *
         * @return Returna la configuracion
         *
         * @version 1.0
         * @author guzmle
         * @since 12/14/2015
         */
        @Bean( name = "freemarkerConf" )
        public FreeMarkerConfigurationFactoryBean freemarkerConfig()
        {
            final FreeMarkerConfigurationFactoryBean factory = new FreeMarkerConfigurationFactoryBean();
            factory.setTemplateLoaderPath( "classpath:templates" );
            factory.setDefaultEncoding( "UTF-8" );
            return factory;
        }
    }

}
