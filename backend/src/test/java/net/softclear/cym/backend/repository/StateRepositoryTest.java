package net.softclear.cym.backend.repository;



import net.softclear.cym.backend.model.jpa.Country;
import net.softclear.cym.backend.model.jpa.State;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  BusinessTypeRepositoryTest
 * Descripcion:             Clase que prueba el repositorio de business type
 * @version                 1.0
 * @author                  guzmle
 * @since                   02/12/15
 *
 */
public class StateRepositoryTest extends RepositoryTest {

    //region Atributos

    @Autowired
    private StateRepository   repository;
    @Autowired
    private CountryRepository countryRepository;
    private Country           country;

    //endregion

    //region inits


    @Before
    public void before()
    {
        country = new Country();
        country.setName( "pais" );
        countryRepository.save( country );
    }


    /**
     * Nombre:                  getUser
     * Descripcion:             Metodo que obtiene un usuario
     * @version 1.0
     * @author guzmle
     * @since 11/30/2015
     *
     * @param description nombre del usuario
     * @return usuario
     */
    private State getObject( String description )
    {
        State retorno = new State();
        retorno.setName( description );
        retorno.setCountry( country );

        return retorno;
    }

    //endregion


    /**
     * Nombre:                  saveTest
     * Descripcion:             Metodo que realiza la prueba que se guarde un objeto en la base de datos
     * @version 1.0
     * @author guzmle
     * @since 11/30/2015
     *
     */
    @Test
    public void saveTest()
    {
        State obj = repository.save( getObject( "user" ) );
        assertTrue( obj.getId() != 0 );
    }



    /**
     * Nombre:                  saveTest
     * Descripcion:             Metodo que realiza la prueba que se guarde un objeto en la base de datos
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void updateTest()
    {
        State obj = repository.save( getObject( "user" ) );
        obj.setName( "123" );
        State objModified =  repository.save( obj );
        assertEquals( obj.getId(), objModified.getId() );
        assertEquals( objModified.getName(), "123" );
    }


    /**
     * Nombre:                  findAll
     * Descripcion:             Metodo que prueba que todos los objetos se busquen
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void findAll()
    {
        repository.save( getObject( "user1" ) );
        repository.save( getObject( "user2" ) );
        repository.save( getObject( "user3" ) );

        Pageable pageable = new PageRequest(0, 3);
        Page<State> firstPage = repository.findAll(pageable);

        assertTrue(firstPage.getTotalPages() > 0);
        assertTrue( firstPage.getTotalElements() > 0 );
        assertEquals( firstPage.getNumber(), 0 );
    }


    /**
     * Nombre:                  findAll
     * Descripcion:             Metodo que prueba que todos los objetos se busquen
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void findByCountry()
    {
        repository.save( getObject( "user1" ) );

        List<State> list = repository.findByCountryOrderByName( country );

        assertTrue( list.size() > 0 );
    }
}
