package net.softclear.cym.backend.service;



import net.softclear.cym.backend.model.jpa.CuePoint;
import net.softclear.cym.backend.model.jpa.Tracking;
import net.softclear.cym.backend.model.jpa.User;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertTrue;



public class TrackingServiceTest extends ServiceTest
{
    @Autowired
    private TrackingService service;

    private String username;

    @Before
    public void beforeTest()
    {
        username = "ojuanibatrupon@gmail.com";
    }

    @Test
    public void testInsert()
    {
        Tracking obj = new Tracking();
        obj.setCreatedAt( new Date(  ) );
        obj.setName( "Prueba" );
        obj.setDeleted( false );

        CuePoint cuePoint = new CuePoint();
        cuePoint.setName( "Prueba" );
        cuePoint.setDate( new Date() );
        cuePoint.setDescription( "Descripcion" );
        cuePoint.setGeographicPosition( "123" );

        obj.setCuepoints( new ArrayList<>(  ) );
        obj.getCuepoints().add( cuePoint );
        List<Tracking> list = new ArrayList<>(  );
        list.add( obj );
        service.saveTrackingUser( username, list );

        List<Tracking> listSaved = service.getTrackingByUserName( username );
    }
}
