package net.softclear.cym.backend.repository;



import net.softclear.cym.backend.model.jpa.DataUser;
import net.softclear.cym.backend.model.jpa.User;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;



/**
 * Sistema:                 CyM.
 *
 * Nombre:                  BusinessTypeRepositoryTest
 * Descripcion:             Clase que prueba el repositorio de business type
 * @version                 1.0
 * @author                  guzmle
 * @since                   02/12/15
 *
 */
public class UserRepositoryTest extends RepositoryTest {

    //region Atributos
    
    @Autowired
    private UserRepository repository;

    //endregion

    //region inits


    @Before
    public void before()
    {
    }


    /**
     * Nombre:                  getUser
     * Descripcion:             Metodo que obtiene un usuario
     * @version 1.0
     * @author guzmle
     * @since 11/30/2015
     *
     * @param description nombre del usuario
     * @return usuario
     */
    private User getObject( String description )
    {
        User obj = new User();
        obj.setUsername( description );
        obj.setCreatedAt( new Date() );
        obj.setEnabled( true );
        obj.setPassword( "1234" );
        obj.setDataUser( new DataUser() );
        obj.getDataUser().setEmail( "prueba@gmail.com" );
        obj.getDataUser().setLastName( "last name" );
        obj.getDataUser().setName( "name" );
        obj.getDataUser().setPhone( "2125503167" );

        return obj;
    }

    //endregion


    /**
     * Nombre:                  saveTest
     * Descripcion:             Metodo que realiza la prueba que se guarde un objeto en la base de datos
     * @version 1.0
     * @author guzmle
     * @since 11/30/2015
     *
     */
    @Test
    public void saveTest()
    {
        User obj = repository.save( getObject( "user" ) );
        assertTrue( obj.getId() != 0 );
    }



    /**
     * Nombre:                  saveTest
     * Descripcion:             Metodo que realiza la prueba que se guarde un objeto en la base de datos
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void updateTest()
    {
        User obj = repository.save( getObject( "user" ) );
        obj.setPassword( "123" );
        User objModified =  repository.save( obj );
        assertEquals( obj.getId(), objModified.getId() );
        assertEquals( objModified.getPassword(), "123" );
    }


    /**
     * Nombre:                  findAll
     * Descripcion:             Metodo que prueba que todos los objetos se busquen
     * @version                 1.0
     * @author                  guzmle
     * @since                   11/30/2015
     *
     */
    @Test
    public void findAll()
    {
        repository.save( getObject( "user1" ) );
        repository.save( getObject( "user2" ) );
        repository.save( getObject( "user3" ) );

        Pageable pageable = new PageRequest(0, 3);
        Page<User> firstPage = repository.findAll(pageable);

        assertTrue(firstPage.getTotalPages() > 0);
        assertTrue( firstPage.getTotalElements() > 0 );
        assertEquals( firstPage.getNumber(), 0 );
    }


    @Test
    public void findByUserNameTest()
    {
        repository.save( getObject( "user1" ) );

        User obj = repository.findByUsername( "user1" );

        assertNotNull( obj );
        assertNotNull( obj.getDataUser() );



    }

}
