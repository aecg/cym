CREATE DATABASE  IF NOT EXISTS `cym` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `cym`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: cym
-- ------------------------------------------------------
-- Server version	5.6.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authority`
--

DROP TABLE IF EXISTS `authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authority` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `authority` varchar(50) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_i4mv9hr2gi3sj1j2is5fl1ljw` (`user_id`),
  CONSTRAINT `FK_i4mv9hr2gi3sj1j2is5fl1ljw` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authority`
--

LOCK TABLES `authority` WRITE;
/*!40000 ALTER TABLE `authority` DISABLE KEYS */;
/*!40000 ALTER TABLE `authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `last_update` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (1,'Este libro contiene informacion acerca de lugares turisticos de mayor importancia en Venezuela','2016-04-08 11:25:16','Turismo Venezuela'),(2,'Este libro contiene informacion acerca de lugares turisticos de Margarita','2016-04-08 11:25:17','Turismo Margarita');
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'México'),(2,'Venezuela');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuepoint`
--

DROP TABLE IF EXISTS `cuepoint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuepoint` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `description` varchar(255) NOT NULL,
  `geographic_position` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `tracking_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_l6gu2oqttbtahoaafsmcg30fa` (`tracking_id`),
  CONSTRAINT `FK_l6gu2oqttbtahoaafsmcg30fa` FOREIGN KEY (`tracking_id`) REFERENCES `tracking` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuepoint`
--

LOCK TABLES `cuepoint` WRITE;
/*!40000 ALTER TABLE `cuepoint` DISABLE KEYS */;
/*!40000 ALTER TABLE `cuepoint` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_user`
--

DROP TABLE IF EXISTS `data_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_e12lafxpgew6w9v5xtrp4ujcq` (`user_id`),
  CONSTRAINT `FK_e12lafxpgew6w9v5xtrp4ujcq` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_user`
--

LOCK TABLES `data_user` WRITE;
/*!40000 ALTER TABLE `data_user` DISABLE KEYS */;
INSERT INTO `data_user` VALUES (1,'ojuanibatrupon@gmail.com','Guzman','Leonardo','02125503167',1),(2,'bismarckp@gmail.com','Ponce','Bismark','0212122321',2);
/*!40000 ALTER TABLE `data_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `destination`
--

DROP TABLE IF EXISTS `destination`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `destination` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `location_id` bigint(20) DEFAULT NULL,
  `state_id` bigint(20) DEFAULT NULL,
  `travel_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_iv593eaufyds9xhxki8atatxo` (`location_id`),
  KEY `FK_i148ep2wh5ti7x6hivl1vtykn` (`state_id`),
  KEY `FK_ro2nf77kn5rso1mmes4g1gipx` (`travel_id`),
  CONSTRAINT `FK_ro2nf77kn5rso1mmes4g1gipx` FOREIGN KEY (`travel_id`) REFERENCES `travel` (`id`),
  CONSTRAINT `FK_i148ep2wh5ti7x6hivl1vtykn` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`),
  CONSTRAINT `FK_iv593eaufyds9xhxki8atatxo` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `destination`
--

LOCK TABLES `destination` WRITE;
/*!40000 ALTER TABLE `destination` DISABLE KEYS */;
/*!40000 ALTER TABLE `destination` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doc_attachment`
--

DROP TABLE IF EXISTS `doc_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_attachment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `url_file` varchar(255) NOT NULL,
  `document_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_j1b5y9fw4ssyjg5j5ixrmo6qv` (`document_id`),
  CONSTRAINT `FK_j1b5y9fw4ssyjg5j5ixrmo6qv` FOREIGN KEY (`document_id`) REFERENCES `document` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_attachment`
--

LOCK TABLES `doc_attachment` WRITE;
/*!40000 ALTER TABLE `doc_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `doc_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `description` varchar(255) NOT NULL,
  `geographic_position` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_esb799ihho7ek44mndbsvmyit` (`user_id`),
  CONSTRAINT `FK_esb799ihho7ek44mndbsvmyit` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document`
--

LOCK TABLES `document` WRITE;
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
/*!40000 ALTER TABLE `document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `geographic_position` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `state_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_2x1hlllq5e61121b4glmrx5vv` (`state_id`),
  CONSTRAINT `FK_2x1hlllq5e61121b4glmrx5vv` FOREIGN KEY (`state_id`) REFERENCES `state` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (1,'10.4686988,-67.0314549,11','Villa Vicente Guerrero',1),(2,'10.4686988,-67.0324549,11','Huamantla',1),(3,'10.4686988,-67.0334549,11','Ciudad de Apizaco',1),(4,'10.4686988,-67.0344549,11','Santa Ana Chiautempan',2),(5,'10.4686988,-67.0354549,11','Zacatelco',2),(6,'10.4686988,-67.0364549,11','Calpulalpan',2),(7,'10.4686988,-67.0374549,11','Contla',3),(8,'10.4686988,-67.0384549,11','Papalotla',3),(9,'10.4686988,-67.0394549,11','Ocotlán',3),(10,'10.4616988,-67.0304549,11','La Magdalena Tlaltelulco',3),(11,'10.4626988,-67.0304549,11','Caracas',4),(12,'10.4636988,-67.0304549,11','San Antonio de Los Altos',4),(13,'10.4646988,-67.0304549,11','La Asunción',5),(14,'10.4656988,-67.0304549,11','Porlamar',5),(15,'10.4666988,-67.0304549,11','Pampatar',5),(16,'10.4676988,-67.0304549,11','Juan Griego',5),(17,'10.4696988,-67.0304549,11','Isla de Cubagua',5);
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location_book`
--

DROP TABLE IF EXISTS `location_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location_book` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `book_id` bigint(20) DEFAULT NULL,
  `location_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_csr9td448k5folwse2670guxq` (`book_id`),
  KEY `FK_1q5wjbhsofk9p7k4ac2y3mjd0` (`location_id`),
  CONSTRAINT `FK_1q5wjbhsofk9p7k4ac2y3mjd0` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`),
  CONSTRAINT `FK_csr9td448k5folwse2670guxq` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location_book`
--

LOCK TABLES `location_book` WRITE;
/*!40000 ALTER TABLE `location_book` DISABLE KEYS */;
INSERT INTO `location_book` VALUES (1,1,1),(2,1,2),(10,2,3),(11,2,4);
/*!40000 ALTER TABLE `location_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mustseeanddo`
--

DROP TABLE IF EXISTS `mustseeanddo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mustseeanddo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `geographic_position` varchar(255) NOT NULL,
  `image` longblob,
  `price` double NOT NULL,
  `qr` int(11) NOT NULL,
  `summary` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url_ar` varchar(255) NOT NULL,
  `url_qr` varchar(255) NOT NULL,
  `location_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_j7i3h9bw9ym4udect73vipg5o` (`location_id`),
  CONSTRAINT `FK_j7i3h9bw9ym4udect73vipg5o` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mustseeanddo`
--

LOCK TABLES `mustseeanddo` WRITE;
/*!40000 ALTER TABLE `mustseeanddo` DISABLE KEYS */;
INSERT INTO `mustseeanddo` VALUES (1,'10.5104352,-66.9611459','����\0JFIF\0\0\0\0\0\0��\0�\0	( \Z%!1\"%)+0.. 383,7(-.+\n\n\n\r-% %---75---//3/---//52/---/-------------/------------��\0\0�\0�\0��\0\0\0\0\0\0\0\0\0\0\0\0\0\0��\0K\0\0\0\0!1AQa\"2Bq��Rr���#3Sbs��Tc���4������$C�������\0\Z\0\0\0\0\0\0\0\0\0\0\0\0��\06\0\0\0\0\0\0!1AQ\"aq��2�����#R�B�3Cbr���\0\0\0?\0�\0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� �Q�J���b\0������\"3�`�\'���=��&Ea�>^��\0��^��q�y�V������l5v�V/�?�x��7ס�<w�U��/�rܻ�u��	e�`rt���X=�|Ĕ��Oj)x>!��[3o�q�5�uZ�]��Z���[l�>3�>Gᘌ)o)?Ϙ��KiM��?>�C�5_֮���\0�-�ޯڊ����?��K�����Ke����W(���\"�KU?u��o�/鶛\'���\\��VwGťx���������Y~�x<�����=[uv�u���q�ڹqIx#���<��Ǒ�W��s�踢�&A��%��N\Z*��>$g��[�ǁ������;�u���\0�>\'��\\��+�_y�6�^�e\'�S����е���ڸ���_b���/{ix?���?�6�9�_m/�xی}�;�G߯#�/r���2���Omՠ�6ۻ�`ۧ�\'���M�a�6@���\\�g�\0io��~�W�����I��u�í�+\r�}�}˃�n��}Ì�p�/)��-�����K�O�v�P�k�Q�9km�ˑ��V���y�R1�Ž���7�z�El�������У�(�m8�����r�\rRḝZ�a�y������z���L�e�10٠�>��Ы_\\���e��Sr��jX�(���1�./\r#(�e2D�!\0@\0@\n������X���*İ����SD���U���O�C����4�g���l��1=}$6��3�5��݉˚�Jc5�00H����=�\Z�z�f�\0���y�rVvb������TOjog���}�\rm�p�)��=�����o:+ne�*�y��ՙ�\Z̛����l�u\'���Z��Rm[5���xd$�)��8Mu*��$b�Wg7�}�\\R`gN�:``鉃���n�\0w�{�B1\nV|�!|��r��#M�8����ڍ#ؚ{�Uq�&ʗ�E)[0B�}��gt=f9F���-�w��\0F�ΩIBiJ]V��s��[S��m6�0)`�b/�u�������UrUn��/�,׵y��w4Q���bgN�:^�/V��u��2i/py��M�׈�5pR�O�5�d�&�Fz�d�gF���\"�����<���=\'�~�UK�Z�Dm�y�<�h\0@1w\0N\0�I�����i,������TH�p6�R��n�����#���ӧ{<}G�$��}{����Z�v�op��z�֠�����[-�6�2�z�*��\0���ѫ�a��4M�h�P�´<����(��\0$J�jO�����\'���uX� ���l�=+�d��{\'=�?���O�F�_�Я|��\'�2��o�\'���jќ���|�F��$i��\0�%��F�mE�`,�95�q�#�7���R�������El����mm����3<�/��x�����yH{YCu���з��Ś����*�s��GPGC�4\'�nMft鉜:a��$�\0sπ����߸�)��S�E	�s��*S�x�L��M����lTF��<w.>�=~�wN�i��U���<lo6�	8P�����d\'{�َ��=L{8��i�3���Y��3�����4��ĭ�7X�}V##�O1��Y��Il��2g�����P�ӽS��y��h�~>%N���wr�.V�n�g����S�鲧VK(!�iY����jv7,�K�u<f;��\"�ˊ˾�7Z�ɴ��Ǐ����˫ʂk�5iq����]��m�<pT�겞jè?�#/�\nk�pyG�vS� \Z�WRY��Qi�����.Ǯ�=�N8��_*�&_g�����)�n�p��=>���:0e#!���s�sX����z6A�\0��6�t&����u\'��Y]r��(��Et�jl����}NYɮ�}Px����|φ}J�Un[��|�Y=N\\�V�l��^\\�\0�Q�G��OS6�y�Z���Ў�d���wj�\0(�S�c���O_!�a���u�=M]2���l��Zm��Pw���}�+<�������ܺ/��n������D;�gb�Ř�$�ˣ��e9M�O&�:pݳ�ߺ��V\"�\0d-x��quڶ+��[w���q��×<�)X�+��[V���2��g���:��un,�i��&�K�&����������C}O���֯��-_�ԏ���ޣ-�����yX��g���z�ٖ�ќ�O(���U��1��{,^���H�m�܃����_�,�1{+{�ZyMm=���\07��j�@+�B��Y����r�{��̂�Vo��R�o\Z�R�/��H�I$�I9$�$�d��iI-�͜��Y��-�u����A�$&��� �d�y3��n�u+�����9o�CN�T_qn�b�.�ˊO����ъ���S��2���\'8��q��wӻ�T�����>M)؜=Ǖ��\0�n��U�-{#�u����z���P�O`#{\0�y0�S��J��<��ĿOSV\'�a�8��n24�����F\n�*�y�����FBpRXd�\'�u}��%�<�cn��S5��P�a��W2\\VS^̷K��]ή�w�����v�M������yOBz:��x�=3<�j�o=:퍋1-ee�+�6�����V��FN��8o�N�W��ե���m>����ٻ��ZPn\r�������真s�٩�{+=H�iи�jXX݂�i�a8����S�\0����������6������l�xݾx~����\02�,վX�5�O���Y�/����/4�����>���c�c�]���ӺZg�1�Y(u+�	烟c���>S\\^�.CϚ���/�Hvi�)l�^;�}�Ne�zĸy:�\0�7�����.��nsǙ�G�P1\"���MU�;�՗96:�5���>H=\n�y��z��Y����S�6rl��Ozt�� �T6G0JqF����wf�og%�M�I Z��\r�|23Lg���c�����l�t[�Ϳ\'�\0�\'�5�����~突?a����fP~;��3��Y��J4��}K퉤��m=��W������\"<&[������6�\r,_��2ۺ]�cgQe{ʻ�w�n��\n��>F�_s?\"W�M/�\Z���f�|����?��Mq�Y��b�:��#�g�\0�]��r^�Y�N{-�����^΂�ۏ�n�p�bFS�>^D�\r\n��e��H{ \r⥲y`��<zL�=K��k�iS��m��vXN��������/�A�����v����gmZY>�2sW��5��Ny����ʐDҧ���dp�u��+��l7E�1׽��Pι�1��(h�ě���\n��r1�~Q��F1�5��Y�O��l-F��Jt����wc�.y��xL�+��?en���m��٣�5��~a��x� 2����V�g��d4�����ݟ�ꏘ������j�\0j���{=7�gM�m��3bשk����k:/w͘��R�;�0q2_f�v����Q���;�v2����5��*�#���9�\'��E��b��;D)S�����;\"�Bn��c�#�]M�YF]V��Dq.=NSC��O�SݒJ��Y_�����Z2V�i_�\'�N�i߱�v_ӽ(��i�m��ʰ�Yz0�\0�Ev)�%Ժ�O��\n˝�r[Ki���a�/WP=����\\)��ʹ�!ω��g�t��oE6����Ê��L���\0.sT&���VV����L����\'N���������a_��۔S��:y3M��Y���\\R``遃���������xg�Jl=���U�x�����4��S��c�����֪]�u�\0ǽ9��\r����7ٵ��WyI��:`gN��&�Pܨ^�yٝڗ��>�2��R�����Fs|��lI�NI�|�e�cqW�t$n�hl���\0�c�D��{*3��B�Y8A����S��.�M��x]k\r�z��sDoX/=�g�31��{I�\\\r��5W����gf��g�,��]`g�c�D^-c�%@8���J�Ae�Bo��i�R���=K!����Si_n��)S꧞NX�������z�c?ӯ�6z�c�%F��\'�k�Zy���es���_���/��SDjXGE(.�KgWzwv.GC�|e��*����3G\r��6��O�Ա%,Q���z���,V��{�����YK������>���I�ѵM�� ��*�y2��l��b�<���2��>�z�e`�p�����%�W	8�IqE��Ъ�����Λ�o�������dd��m/y}Q��(4������\0��U�(�89=VSɔ����	�����<29� ``�/I��e]J�Y��2}貛;3S�3M]�J��\n�.(03��&hZuu�Z�.v�O�+yE�����h�t\'.�|�z���������\"����|����\'����_�+̼���03������뫭��w����v�r��_.�j=w���/*10\r�\r�gv�g�1¢Yݽ���d,�Ae�Bo���@WT��S��1mU�=�g��X\'EǏ,sRmg�|;��+8�W�sz�S�c�a˻c�|<\0�@\0�a��,��,����Y}�����\n��Yݹ*��$	�,PYd�7�t�\'d٫?��r(�,�w�8�{g>��1��_�Y�r]?:��[�b���ݛ��\Z*E4��9���c��I��l��ԏV��\\q�VX �W�K��O��2Q���!eq�.2YG�6Y�����1��Z�=}�#���J���ڎ���x7齂�s�[��Ŝ��ѵM��9VU��e=D�*�X��&�%T����gܾ�Np�`�Xz�|?\"dn��8�E�y��˄���1\r�Z���G-[r,�#�ux3�mb�q�:��uY�˽uzvF����#���L���\0��-���Q]��<21� e����`*A��q����!8�EŖW7	).F[N�K/�p�|Q���pq�F�9G�AFo8�C2ҳQ3Q��ֿ���>�����Dw��D��h�f������^�Ң��=��챩���Q=�E���W��/�2��ʌ7l�\'}jTN�\'�o��;|��=��[T6�k�:��ֳ�+�P\"������=�9���A�{����,p���woe@�,PYd냛�&j� ����̡�}E�=���S�x��?�Y��Թ����y�\Zvݪ�t��R�K�9^x=��F/���3���d��!kKǂ� ����+��,p���woeGS��[e�,�psxG_���>��\0\r�%4jÿ���:��A�W���-�`��f���r]\r�S�;0�y���읗N����\n�ө=I<�>3ʜ�9mK���\n&H�\0@-�YJ��\"C:�O(䢤��7m�N�Yp[Jx�e��}������������OS�UŮ5�b���;Y�j�u�� ��e<�OPg�]�k(�U��w�j���\0��������vUӞ�\'���/֯k��ǽr>�P��ՙ(NAY�K�<W��Npyڏ2ر�>F�^��l��U��e=G�s����Q+pxds$@�o��ک��ܖS�}���R�6��F�ۥ>q��~�y��4��f�t����#}���QF��ճF�t�z$�{��3�b�_�)U�\0_����za�?���Et���Ν&�{�=�{V��~���ݯ�2�v�Q�����������^Tn�謹��8$�8UQ�;��������N�9�\"F��\"�t�|�Y�#\r{L�٬U>\'�*����>=:z��i-�p�����?��u<��z�<@��yc;��X�3��I�r\\�N�ӆ�7��en��Ys��8$�8TQ�;���s?�	u�(,���������\Z��вh�e��X��R��|}n+�]�qy~��z��:}���������R-u��(�U\0y<�ܞY�(���ӇD\0@\0@>\0�6�\0gWt�)5�ꌵLy�@z�}��#��iԴ���~番���w/��H�Mh$�ޖV����YI�\"z�������bz{7�4h�\Zp��ka���*z�>bN��-�W7V�.��4ڕ��ɬ��8�l}��+����v��̕v,lO��W�j�#*Ê��L���\0#�\'	���3�������-����{�o��[��\\��W��O$���=�;�eb�0�J��A�9dd�����3�iŗWY�Π�)#x�O�F�l���c�4��Z�Gyc��ݛ� }��f)%�7.�BXU��5!����;%����e��߶*}�r-��U�5�\0	\n^`�8�X�\08�!$�I�\0�I���,�	gr&m��\0�R����KZ���\0JhY[o��\0�}�\rAr��\"htV\\���I8UQ�;�����Yb��!\\�#hkQk��NOu�]��^Ó0浏e>\'�*뭷�>=:z��i-�p��Љ�tb��n���Z�f�=\0�Hd��wq�F�m��?u��Q��(��*���]���8��I�L�l�\r�\0�<�=����n�+��4ދ���CZ9�ٯ��Zy�j^s��׼�(�,wy�w��\0\0��0���\0� \0� \0� ;g�U�w$��s�/�>��9���Q*�LZ�.��\r�[EhÃ�Q�X��S�\\��z��/C�?���t�9�|x����fF�P��ՙ5��G����#�z��2���j|�ر�>D}f���FU�e<�OQ��������Q�����L�����+��7�2�{-ç�-����x���wM��\n_Z�T1��~v�G��;F���Ei���I�K�Qg5wg�R�8�\0���\0���JQ��̾�񌻱�>ls�ͨ ~ew�<��wj�7��\"��C��2Tnn}<�4Z+.}����p���=�}U��d�8��0����CZ���i���k�r,9�c�O��ʺ�m�Ϗ��l�IlC�>�\0B�������snl�W7XE��(R�=ߴ�q*&7dr��Rª<^�zV��MZp��p����-w��4^�����]ڙX�z��cZ��Lơ\0@\0@\0@\0����U{���9��G��Ss�Y\\ڝ4o�O��ݢ��#o���M�,_p	��\'���2[+���|A�5���_�Jf�#M�]��̚��#�V��O�|��pyڏ3Ev,lO���t��9����⯸Jx�&�\0�T�J��j���\0<KT\ZR����}�N��W�R����I��.��En���-�y���:K���Z�V�6��~r���޼���_s�7��ں�ɍ�Sa\'\n�F�{*�����u���I��nE�\r�5�{�hkQP���y܌5�9=�ǲ���u���ǧOS��Kb:��*��R[�kg=�w�����C݌��� ���\0=��Sj���>��5NOi/C�{)����r��Xw��Y����=g�}��ܸ枅R�|Y{)/\0@\0@\0@\0��6z_ST�g�<���N�$����6��\\��{>�=��W:�a����j�6G1>J�\'L�f�FXVM�{@V�%Â8�6FY|A�}��(��$�x��t�(4��?/�f���]n�R�Մ6+DZ����bCJ�$�.<>E���F1��)�d00t���	mƋq,B�(�����d|fmSq��x��դ���%��-�]�����ё[>�1kJ���j\0\n�Y<yGOK�R�/Rz�ԛ�8y�Fk3�vηQh��.�@2Ǡ2�,�qڑeU��l��^��z��t�8�-�,ǋ7��\0!�>z��rg��R�\n(����\0� \0� \0� \0� ~��������YO�?�YmWJ�mD�Q���ٙ��{bݥ�r���uǪ�#�:}�ܢ�ܲ����gD�.J�/(\'�ij��\0Ҵ�\0���ܷ�^k�����\'�U�����&���3��q��hCX~-���˷j\\��\0�#D{7�[���a����ĻWh����G��ŏ���)����EJ�b\'��o��h�ܬe���9>���<+���̏��O\nc����\0@\0@\0@\0@\0@�GAU���P�~c��Џ8NP{Q+���-ǔv��v�<^��َ�E���\0?�{zmTmX�ϛ��C��+6^�Rѿ�7�>��\'�80�Q-�.P��o_�$�=���Ⱥ�;V�[z��O�dyK!%(�\"3���_#J!bFI �I�9��Yb)��I�6�`�NR�.:�\0i�7�J��gi�{ˮkkep[���vj�m�_F� =���_���ӯ�5\Z�һ�t�Y^���ű�U:Z�4��\'�by��xVY+%�#說5Gf(� X \0� \0� \0� \0� \0� \Z�:t�\Z�P��DN�8��2����ygk�$�l�P-F}�^z7������:]b��.\'�k4��=�\"�i���H�����O֯w�/��\'�����{q����ӳ}\0���az��ǘ��N���:�#��s>�l��dl�6��(�_�S?{t��N�V�[1��i��sږ������WR\n�@��\0��~sŔ��Y�0�`���\"H@\0@\0@\0@\0@\0@�( �2\'\r���.��f��v���,��#�Lt��徭[�v��#ʻ@�-��F��3����Z��*�n��!U���;�x��8���hv��q��z%u�P�\0\0\0\0\0@	淝���IaA�\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@?��',200,1,'El Teatro Villa Vicente Guerrero','Teatro Villa Vicente Guerrero','','',1),(2,'10.5104352,-66.9411459','����\0JFIF\0\0\0\0\0\0��\0�\0	( \Z%!1\"%)+0.. 383,7(-.+\n\n\n\r-% %---75---//3/---//52/---/-------------/------------��\0\0�\0�\0��\0\0\0\0\0\0\0\0\0\0\0\0\0\0��\0K\0\0\0\0!1AQa\"2Bq��Rr���#3Sbs��Tc���4������$C�������\0\Z\0\0\0\0\0\0\0\0\0\0\0\0��\06\0\0\0\0\0\0!1AQ\"aq��2�����#R�B�3Cbr���\0\0\0?\0�\0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� �Q�J���b\0������\"3�`�\'���=��&Ea�>^��\0��^��q�y�V������l5v�V/�?�x��7ס�<w�U��/�rܻ�u��	e�`rt���X=�|Ĕ��Oj)x>!��[3o�q�5�uZ�]��Z���[l�>3�>Gᘌ)o)?Ϙ��KiM��?>�C�5_֮���\0�-�ޯڊ����?��K�����Ke����W(���\"�KU?u��o�/鶛\'���\\��VwGťx���������Y~�x<�����=[uv�u���q�ڹqIx#���<��Ǒ�W��s�踢�&A��%��N\Z*��>$g��[�ǁ������;�u���\0�>\'��\\��+�_y�6�^�e\'�S����е���ڸ���_b���/{ix?���?�6�9�_m/�xی}�;�G߯#�/r���2���Omՠ�6ۻ�`ۧ�\'���M�a�6@���\\�g�\0io��~�W�����I��u�í�+\r�}�}˃�n��}Ì�p�/)��-�����K�O�v�P�k�Q�9km�ˑ��V���y�R1�Ž���7�z�El�������У�(�m8�����r�\rRḝZ�a�y������z���L�e�10٠�>��Ы_\\���e��Sr��jX�(���1�./\r#(�e2D�!\0@\0@\n������X���*İ����SD���U���O�C����4�g���l��1=}$6��3�5��݉˚�Jc5�00H����=�\Z�z�f�\0���y�rVvb������TOjog���}�\rm�p�)��=�����o:+ne�*�y��ՙ�\Z̛����l�u\'���Z��Rm[5���xd$�)��8Mu*��$b�Wg7�}�\\R`gN�:``鉃���n�\0w�{�B1\nV|�!|��r��#M�8����ڍ#ؚ{�Uq�&ʗ�E)[0B�}��gt=f9F���-�w��\0F�ΩIBiJ]V��s��[S��m6�0)`�b/�u�������UrUn��/�,׵y��w4Q���bgN�:^�/V��u��2i/py��M�׈�5pR�O�5�d�&�Fz�d�gF���\"�����<���=\'�~�UK�Z�Dm�y�<�h\0@1w\0N\0�I�����i,������TH�p6�R��n�����#���ӧ{<}G�$��}{����Z�v�op��z�֠�����[-�6�2�z�*��\0���ѫ�a��4M�h�P�´<����(��\0$J�jO�����\'���uX� ���l�=+�d��{\'=�?���O�F�_�Я|��\'�2��o�\'���jќ���|�F��$i��\0�%��F�mE�`,�95�q�#�7���R�������El����mm����3<�/��x�����yH{YCu���з��Ś����*�s��GPGC�4\'�nMft鉜:a��$�\0sπ����߸�)��S�E	�s��*S�x�L��M����lTF��<w.>�=~�wN�i��U���<lo6�	8P�����d\'{�َ��=L{8��i�3���Y��3�����4��ĭ�7X�}V##�O1��Y��Il��2g�����P�ӽS��y��h�~>%N���wr�.V�n�g����S�鲧VK(!�iY����jv7,�K�u<f;��\"�ˊ˾�7Z�ɴ��Ǐ����˫ʂk�5iq����]��m�<pT�겞jè?�#/�\nk�pyG�vS� \Z�WRY��Qi�����.Ǯ�=�N8��_*�&_g�����)�n�p��=>���:0e#!���s�sX����z6A�\0��6�t&����u\'��Y]r��(��Et�jl����}NYɮ�}Px����|φ}J�Un[��|�Y=N\\�V�l��^\\�\0�Q�G��OS6�y�Z���Ў�d���wj�\0(�S�c���O_!�a���u�=M]2���l��Zm��Pw���}�+<�������ܺ/��n������D;�gb�Ř�$�ˣ��e9M�O&�:pݳ�ߺ��V\"�\0d-x��quڶ+��[w���q��×<�)X�+��[V���2��g���:��un,�i��&�K�&����������C}O���֯��-_�ԏ���ޣ-�����yX��g���z�ٖ�ќ�O(���U��1��{,^���H�m�܃����_�,�1{+{�ZyMm=���\07��j�@+�B��Y����r�{��̂�Vo��R�o\Z�R�/��H�I$�I9$�$�d��iI-�͜��Y��-�u����A�$&��� �d�y3��n�u+�����9o�CN�T_qn�b�.�ˊO����ъ���S��2���\'8��q��wӻ�T�����>M)؜=Ǖ��\0�n��U�-{#�u����z���P�O`#{\0�y0�S��J��<��ĿOSV\'�a�8��n24�����F\n�*�y�����FBpRXd�\'�u}��%�<�cn��S5��P�a��W2\\VS^̷K��]ή�w�����v�M������yOBz:��x�=3<�j�o=:퍋1-ee�+�6�����V��FN��8o�N�W��ե���m>����ٻ��ZPn\r�������真s�٩�{+=H�iи�jXX݂�i�a8����S�\0����������6������l�xݾx~����\02�,վX�5�O���Y�/����/4�����>���c�c�]���ӺZg�1�Y(u+�	烟c���>S\\^�.CϚ���/�Hvi�)l�^;�}�Ne�zĸy:�\0�7�����.��nsǙ�G�P1\"���MU�;�՗96:�5���>H=\n�y��z��Y����S�6rl��Ozt�� �T6G0JqF����wf�og%�M�I Z��\r�|23Lg���c�����l�t[�Ϳ\'�\0�\'�5�����~突?a����fP~;��3��Y��J4��}K퉤��m=��W������\"<&[������6�\r,_��2ۺ]�cgQe{ʻ�w�n��\n��>F�_s?\"W�M/�\Z���f�|����?��Mq�Y��b�:��#�g�\0�]��r^�Y�N{-�����^΂�ۏ�n�p�bFS�>^D�\r\n��e��H{ \r⥲y`��<zL�=K��k�iS��m��vXN��������/�A�����v����gmZY>�2sW��5��Ny����ʐDҧ���dp�u��+��l7E�1׽��Pι�1��(h�ě���\n��r1�~Q��F1�5��Y�O��l-F��Jt����wc�.y��xL�+��?en���m��٣�5��~a��x� 2����V�g��d4�����ݟ�ꏘ������j�\0j���{=7�gM�m��3bשk����k:/w͘��R�;�0q2_f�v����Q���;�v2����5��*�#���9�\'��E��b��;D)S�����;\"�Bn��c�#�]M�YF]V��Dq.=NSC��O�SݒJ��Y_�����Z2V�i_�\'�N�i߱�v_ӽ(��i�m��ʰ�Yz0�\0�Ev)�%Ժ�O��\n˝�r[Ki���a�/WP=����\\)��ʹ�!ω��g�t��oE6����Ê��L���\0.sT&���VV����L����\'N���������a_��۔S��:y3M��Y���\\R``遃���������xg�Jl=���U�x�����4��S��c�����֪]�u�\0ǽ9��\r����7ٵ��WyI��:`gN��&�Pܨ^�yٝڗ��>�2��R�����Fs|��lI�NI�|�e�cqW�t$n�hl���\0�c�D��{*3��B�Y8A����S��.�M��x]k\r�z��sDoX/=�g�31��{I�\\\r��5W����gf��g�,��]`g�c�D^-c�%@8���J�Ae�Bo��i�R���=K!����Si_n��)S꧞NX�������z�c?ӯ�6z�c�%F��\'�k�Zy���es���_���/��SDjXGE(.�KgWzwv.GC�|e��*����3G\r��6��O�Ա%,Q���z���,V��{�����YK������>���I�ѵM�� ��*�y2��l��b�<���2��>�z�e`�p�����%�W	8�IqE��Ъ�����Λ�o�������dd��m/y}Q��(4������\0��U�(�89=VSɔ����	�����<29� ``�/I��e]J�Y��2}貛;3S�3M]�J��\n�.(03��&hZuu�Z�.v�O�+yE�����h�t\'.�|�z���������\"����|����\'����_�+̼���03������뫭��w����v�r��_.�j=w���/*10\r�\r�gv�g�1¢Yݽ���d,�Ae�Bo���@WT��S��1mU�=�g��X\'EǏ,sRmg�|;��+8�W�sz�S�c�a˻c�|<\0�@\0�a��,��,����Y}�����\n��Yݹ*��$	�,PYd�7�t�\'d٫?��r(�,�w�8�{g>��1��_�Y�r]?:��[�b���ݛ��\Z*E4��9���c��I��l��ԏV��\\q�VX �W�K��O��2Q���!eq�.2YG�6Y�����1��Z�=}�#���J���ڎ���x7齂�s�[��Ŝ��ѵM��9VU��e=D�*�X��&�%T����gܾ�Np�`�Xz�|?\"dn��8�E�y��˄���1\r�Z���G-[r,�#�ux3�mb�q�:��uY�˽uzvF����#���L���\0��-���Q]��<21� e����`*A��q����!8�EŖW7	).F[N�K/�p�|Q���pq�F�9G�AFo8�C2ҳQ3Q��ֿ���>�����Dw��D��h�f������^�Ң��=��챩���Q=�E���W��/�2��ʌ7l�\'}jTN�\'�o��;|��=��[T6�k�:��ֳ�+�P\"������=�9���A�{����,p���woe@�,PYd냛�&j� ����̡�}E�=���S�x��?�Y��Թ����y�\Zvݪ�t��R�K�9^x=��F/���3���d��!kKǂ� ����+��,p���woeGS��[e�,�psxG_���>��\0\r�%4jÿ���:��A�W���-�`��f���r]\r�S�;0�y���읗N����\n�ө=I<�>3ʜ�9mK���\n&H�\0@-�YJ��\"C:�O(䢤��7m�N�Yp[Jx�e��}������������OS�UŮ5�b���;Y�j�u�� ��e<�OPg�]�k(�U��w�j���\0��������vUӞ�\'���/֯k��ǽr>�P��ՙ(NAY�K�<W��Npyڏ2ر�>F�^��l��U��e=G�s����Q+pxds$@�o��ک��ܖS�}���R�6��F�ۥ>q��~�y��4��f�t����#}���QF��ճF�t�z$�{��3�b�_�)U�\0_����za�?���Et���Ν&�{�=�{V��~���ݯ�2�v�Q�����������^Tn�謹��8$�8UQ�;��������N�9�\"F��\"�t�|�Y�#\r{L�٬U>\'�*����>=:z��i-�p�����?��u<��z�<@��yc;��X�3��I�r\\�N�ӆ�7��en��Ys��8$�8TQ�;���s?�	u�(,���������\Z��вh�e��X��R��|}n+�]�qy~��z��:}���������R-u��(�U\0y<�ܞY�(���ӇD\0@\0@>\0�6�\0gWt�)5�ꌵLy�@z�}��#��iԴ���~番���w/��H�Mh$�ޖV����YI�\"z�������bz{7�4h�\Zp��ka���*z�>bN��-�W7V�.��4ڕ��ɬ��8�l}��+����v��̕v,lO��W�j�#*Ê��L���\0#�\'	���3�������-����{�o��[��\\��W��O$���=�;�eb�0�J��A�9dd�����3�iŗWY�Π�)#x�O�F�l���c�4��Z�Gyc��ݛ� }��f)%�7.�BXU��5!����;%����e��߶*}�r-��U�5�\0	\n^`�8�X�\08�!$�I�\0�I���,�	gr&m��\0�R����KZ���\0JhY[o��\0�}�\rAr��\"htV\\���I8UQ�;�����Yb��!\\�#hkQk��NOu�]��^Ó0浏e>\'�*뭷�>=:z��i-�p��Љ�tb��n���Z�f�=\0�Hd��wq�F�m��?u��Q��(��*���]���8��I�L�l�\r�\0�<�=����n�+��4ދ���CZ9�ٯ��Zy�j^s��׼�(�,wy�w��\0\0��0���\0� \0� \0� ;g�U�w$��s�/�>��9���Q*�LZ�.��\r�[EhÃ�Q�X��S�\\��z��/C�?���t�9�|x����fF�P��ՙ5��G����#�z��2���j|�ر�>D}f���FU�e<�OQ��������Q�����L�����+��7�2�{-ç�-����x���wM��\n_Z�T1��~v�G��;F���Ei���I�K�Qg5wg�R�8�\0���\0���JQ��̾�񌻱�>ls�ͨ ~ew�<��wj�7��\"��C��2Tnn}<�4Z+.}����p���=�}U��d�8��0����CZ���i���k�r,9�c�O��ʺ�m�Ϗ��l�IlC�>�\0B�������snl�W7XE��(R�=ߴ�q*&7dr��Rª<^�zV��MZp��p����-w��4^�����]ڙX�z��cZ��Lơ\0@\0@\0@\0����U{���9��G��Ss�Y\\ڝ4o�O��ݢ��#o���M�,_p	��\'���2[+���|A�5���_�Jf�#M�]��̚��#�V��O�|��pyڏ3Ev,lO���t��9����⯸Jx�&�\0�T�J��j���\0<KT\ZR����}�N��W�R����I��.��En���-�y���:K���Z�V�6��~r���޼���_s�7��ں�ɍ�Sa\'\n�F�{*�����u���I��nE�\r�5�{�hkQP���y܌5�9=�ǲ���u���ǧOS��Kb:��*��R[�kg=�w�����C݌��� ���\0=��Sj���>��5NOi/C�{)����r��Xw��Y����=g�}��ܸ枅R�|Y{)/\0@\0@\0@\0��6z_ST�g�<���N�$����6��\\��{>�=��W:�a����j�6G1>J�\'L�f�FXVM�{@V�%Â8�6FY|A�}��(��$�x��t�(4��?/�f���]n�R�Մ6+DZ����bCJ�$�.<>E���F1��)�d00t���	mƋq,B�(�����d|fmSq��x��դ���%��-�]�����ё[>�1kJ���j\0\n�Y<yGOK�R�/Rz�ԛ�8y�Fk3�vηQh��.�@2Ǡ2�,�qڑeU��l��^��z��t�8�-�,ǋ7��\0!�>z��rg��R�\n(����\0� \0� \0� \0� ~��������YO�?�YmWJ�mD�Q���ٙ��{bݥ�r���uǪ�#�:}�ܢ�ܲ����gD�.J�/(\'�ij��\0Ҵ�\0���ܷ�^k�����\'�U�����&���3��q��hCX~-���˷j\\��\0�#D{7�[���a����ĻWh����G��ŏ���)����EJ�b\'��o��h�ܬe���9>���<+���̏��O\nc����\0@\0@\0@\0@\0@�GAU���P�~c��Џ8NP{Q+���-ǔv��v�<^��َ�E���\0?�{zmTmX�ϛ��C��+6^�Rѿ�7�>��\'�80�Q-�.P��o_�$�=���Ⱥ�;V�[z��O�dyK!%(�\"3���_#J!bFI �I�9��Yb)��I�6�`�NR�.:�\0i�7�J��gi�{ˮkkep[���vj�m�_F� =���_���ӯ�5\Z�һ�t�Y^���ű�U:Z�4��\'�by��xVY+%�#說5Gf(� X \0� \0� \0� \0� \0� \Z�:t�\Z�P��DN�8��2����ygk�$�l�P-F}�^z7������:]b��.\'�k4��=�\"�i���H�����O֯w�/��\'�����{q����ӳ}\0���az��ǘ��N���:�#��s>�l��dl�6��(�_�S?{t��N�V�[1��i��sږ������WR\n�@��\0��~sŔ��Y�0�`���\"H@\0@\0@\0@\0@\0@�( �2\'\r���.��f��v���,��#�Lt��徭[�v��#ʻ@�-��F��3����Z��*�n��!U���;�x��8���hv��q��z%u�P�\0\0\0\0\0@	淝���IaA�\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@?��',150,0,'La Plaza Villa Vicente Guerrero','Plaza Villa Vicente Guerrero','','',2),(3,'10.6004352,-66.9511459','����\0JFIF\0\0\0\0\0\0��\0�\0	( \Z%!1\"%)+0.. 383,7(-.+\n\n\n\r-% %---75---//3/---//52/---/-------------/------------��\0\0�\0�\0��\0\0\0\0\0\0\0\0\0\0\0\0\0\0��\0K\0\0\0\0!1AQa\"2Bq��Rr���#3Sbs��Tc���4������$C�������\0\Z\0\0\0\0\0\0\0\0\0\0\0\0��\06\0\0\0\0\0\0!1AQ\"aq��2�����#R�B�3Cbr���\0\0\0?\0�\0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� �Q�J���b\0������\"3�`�\'���=��&Ea�>^��\0��^��q�y�V������l5v�V/�?�x��7ס�<w�U��/�rܻ�u��	e�`rt���X=�|Ĕ��Oj)x>!��[3o�q�5�uZ�]��Z���[l�>3�>Gᘌ)o)?Ϙ��KiM��?>�C�5_֮���\0�-�ޯڊ����?��K�����Ke����W(���\"�KU?u��o�/鶛\'���\\��VwGťx���������Y~�x<�����=[uv�u���q�ڹqIx#���<��Ǒ�W��s�踢�&A��%��N\Z*��>$g��[�ǁ������;�u���\0�>\'��\\��+�_y�6�^�e\'�S����е���ڸ���_b���/{ix?���?�6�9�_m/�xی}�;�G߯#�/r���2���Omՠ�6ۻ�`ۧ�\'���M�a�6@���\\�g�\0io��~�W�����I��u�í�+\r�}�}˃�n��}Ì�p�/)��-�����K�O�v�P�k�Q�9km�ˑ��V���y�R1�Ž���7�z�El�������У�(�m8�����r�\rRḝZ�a�y������z���L�e�10٠�>��Ы_\\���e��Sr��jX�(���1�./\r#(�e2D�!\0@\0@\n������X���*İ����SD���U���O�C����4�g���l��1=}$6��3�5��݉˚�Jc5�00H����=�\Z�z�f�\0���y�rVvb������TOjog���}�\rm�p�)��=�����o:+ne�*�y��ՙ�\Z̛����l�u\'���Z��Rm[5���xd$�)��8Mu*��$b�Wg7�}�\\R`gN�:``鉃���n�\0w�{�B1\nV|�!|��r��#M�8����ڍ#ؚ{�Uq�&ʗ�E)[0B�}��gt=f9F���-�w��\0F�ΩIBiJ]V��s��[S��m6�0)`�b/�u�������UrUn��/�,׵y��w4Q���bgN�:^�/V��u��2i/py��M�׈�5pR�O�5�d�&�Fz�d�gF���\"�����<���=\'�~�UK�Z�Dm�y�<�h\0@1w\0N\0�I�����i,������TH�p6�R��n�����#���ӧ{<}G�$��}{����Z�v�op��z�֠�����[-�6�2�z�*��\0���ѫ�a��4M�h�P�´<����(��\0$J�jO�����\'���uX� ���l�=+�d��{\'=�?���O�F�_�Я|��\'�2��o�\'���jќ���|�F��$i��\0�%��F�mE�`,�95�q�#�7���R�������El����mm����3<�/��x�����yH{YCu���з��Ś����*�s��GPGC�4\'�nMft鉜:a��$�\0sπ����߸�)��S�E	�s��*S�x�L��M����lTF��<w.>�=~�wN�i��U���<lo6�	8P�����d\'{�َ��=L{8��i�3���Y��3�����4��ĭ�7X�}V##�O1��Y��Il��2g�����P�ӽS��y��h�~>%N���wr�.V�n�g����S�鲧VK(!�iY����jv7,�K�u<f;��\"�ˊ˾�7Z�ɴ��Ǐ����˫ʂk�5iq����]��m�<pT�겞jè?�#/�\nk�pyG�vS� \Z�WRY��Qi�����.Ǯ�=�N8��_*�&_g�����)�n�p��=>���:0e#!���s�sX����z6A�\0��6�t&����u\'��Y]r��(��Et�jl����}NYɮ�}Px����|φ}J�Un[��|�Y=N\\�V�l��^\\�\0�Q�G��OS6�y�Z���Ў�d���wj�\0(�S�c���O_!�a���u�=M]2���l��Zm��Pw���}�+<�������ܺ/��n������D;�gb�Ř�$�ˣ��e9M�O&�:pݳ�ߺ��V\"�\0d-x��quڶ+��[w���q��×<�)X�+��[V���2��g���:��un,�i��&�K�&����������C}O���֯��-_�ԏ���ޣ-�����yX��g���z�ٖ�ќ�O(���U��1��{,^���H�m�܃����_�,�1{+{�ZyMm=���\07��j�@+�B��Y����r�{��̂�Vo��R�o\Z�R�/��H�I$�I9$�$�d��iI-�͜��Y��-�u����A�$&��� �d�y3��n�u+�����9o�CN�T_qn�b�.�ˊO����ъ���S��2���\'8��q��wӻ�T�����>M)؜=Ǖ��\0�n��U�-{#�u����z���P�O`#{\0�y0�S��J��<��ĿOSV\'�a�8��n24�����F\n�*�y�����FBpRXd�\'�u}��%�<�cn��S5��P�a��W2\\VS^̷K��]ή�w�����v�M������yOBz:��x�=3<�j�o=:퍋1-ee�+�6�����V��FN��8o�N�W��ե���m>����ٻ��ZPn\r�������真s�٩�{+=H�iи�jXX݂�i�a8����S�\0����������6������l�xݾx~����\02�,վX�5�O���Y�/����/4�����>���c�c�]���ӺZg�1�Y(u+�	烟c���>S\\^�.CϚ���/�Hvi�)l�^;�}�Ne�zĸy:�\0�7�����.��nsǙ�G�P1\"���MU�;�՗96:�5���>H=\n�y��z��Y����S�6rl��Ozt�� �T6G0JqF����wf�og%�M�I Z��\r�|23Lg���c�����l�t[�Ϳ\'�\0�\'�5�����~突?a����fP~;��3��Y��J4��}K퉤��m=��W������\"<&[������6�\r,_��2ۺ]�cgQe{ʻ�w�n��\n��>F�_s?\"W�M/�\Z���f�|����?��Mq�Y��b�:��#�g�\0�]��r^�Y�N{-�����^΂�ۏ�n�p�bFS�>^D�\r\n��e��H{ \r⥲y`��<zL�=K��k�iS��m��vXN��������/�A�����v����gmZY>�2sW��5��Ny����ʐDҧ���dp�u��+��l7E�1׽��Pι�1��(h�ě���\n��r1�~Q��F1�5��Y�O��l-F��Jt����wc�.y��xL�+��?en���m��٣�5��~a��x� 2����V�g��d4�����ݟ�ꏘ������j�\0j���{=7�gM�m��3bשk����k:/w͘��R�;�0q2_f�v����Q���;�v2����5��*�#���9�\'��E��b��;D)S�����;\"�Bn��c�#�]M�YF]V��Dq.=NSC��O�SݒJ��Y_�����Z2V�i_�\'�N�i߱�v_ӽ(��i�m��ʰ�Yz0�\0�Ev)�%Ժ�O��\n˝�r[Ki���a�/WP=����\\)��ʹ�!ω��g�t��oE6����Ê��L���\0.sT&���VV����L����\'N���������a_��۔S��:y3M��Y���\\R``遃���������xg�Jl=���U�x�����4��S��c�����֪]�u�\0ǽ9��\r����7ٵ��WyI��:`gN��&�Pܨ^�yٝڗ��>�2��R�����Fs|��lI�NI�|�e�cqW�t$n�hl���\0�c�D��{*3��B�Y8A����S��.�M��x]k\r�z��sDoX/=�g�31��{I�\\\r��5W����gf��g�,��]`g�c�D^-c�%@8���J�Ae�Bo��i�R���=K!����Si_n��)S꧞NX�������z�c?ӯ�6z�c�%F��\'�k�Zy���es���_���/��SDjXGE(.�KgWzwv.GC�|e��*����3G\r��6��O�Ա%,Q���z���,V��{�����YK������>���I�ѵM�� ��*�y2��l��b�<���2��>�z�e`�p�����%�W	8�IqE��Ъ�����Λ�o�������dd��m/y}Q��(4������\0��U�(�89=VSɔ����	�����<29� ``�/I��e]J�Y��2}貛;3S�3M]�J��\n�.(03��&hZuu�Z�.v�O�+yE�����h�t\'.�|�z���������\"����|����\'����_�+̼���03������뫭��w����v�r��_.�j=w���/*10\r�\r�gv�g�1¢Yݽ���d,�Ae�Bo���@WT��S��1mU�=�g��X\'EǏ,sRmg�|;��+8�W�sz�S�c�a˻c�|<\0�@\0�a��,��,����Y}�����\n��Yݹ*��$	�,PYd�7�t�\'d٫?��r(�,�w�8�{g>��1��_�Y�r]?:��[�b���ݛ��\Z*E4��9���c��I��l��ԏV��\\q�VX �W�K��O��2Q���!eq�.2YG�6Y�����1��Z�=}�#���J���ڎ���x7齂�s�[��Ŝ��ѵM��9VU��e=D�*�X��&�%T����gܾ�Np�`�Xz�|?\"dn��8�E�y��˄���1\r�Z���G-[r,�#�ux3�mb�q�:��uY�˽uzvF����#���L���\0��-���Q]��<21� e����`*A��q����!8�EŖW7	).F[N�K/�p�|Q���pq�F�9G�AFo8�C2ҳQ3Q��ֿ���>�����Dw��D��h�f������^�Ң��=��챩���Q=�E���W��/�2��ʌ7l�\'}jTN�\'�o��;|��=��[T6�k�:��ֳ�+�P\"������=�9���A�{����,p���woe@�,PYd냛�&j� ����̡�}E�=���S�x��?�Y��Թ����y�\Zvݪ�t��R�K�9^x=��F/���3���d��!kKǂ� ����+��,p���woeGS��[e�,�psxG_���>��\0\r�%4jÿ���:��A�W���-�`��f���r]\r�S�;0�y���읗N����\n�ө=I<�>3ʜ�9mK���\n&H�\0@-�YJ��\"C:�O(䢤��7m�N�Yp[Jx�e��}������������OS�UŮ5�b���;Y�j�u�� ��e<�OPg�]�k(�U��w�j���\0��������vUӞ�\'���/֯k��ǽr>�P��ՙ(NAY�K�<W��Npyڏ2ر�>F�^��l��U��e=G�s����Q+pxds$@�o��ک��ܖS�}���R�6��F�ۥ>q��~�y��4��f�t����#}���QF��ճF�t�z$�{��3�b�_�)U�\0_����za�?���Et���Ν&�{�=�{V��~���ݯ�2�v�Q�����������^Tn�謹��8$�8UQ�;��������N�9�\"F��\"�t�|�Y�#\r{L�٬U>\'�*����>=:z��i-�p�����?��u<��z�<@��yc;��X�3��I�r\\�N�ӆ�7��en��Ys��8$�8TQ�;���s?�	u�(,���������\Z��вh�e��X��R��|}n+�]�qy~��z��:}���������R-u��(�U\0y<�ܞY�(���ӇD\0@\0@>\0�6�\0gWt�)5�ꌵLy�@z�}��#��iԴ���~番���w/��H�Mh$�ޖV����YI�\"z�������bz{7�4h�\Zp��ka���*z�>bN��-�W7V�.��4ڕ��ɬ��8�l}��+����v��̕v,lO��W�j�#*Ê��L���\0#�\'	���3�������-����{�o��[��\\��W��O$���=�;�eb�0�J��A�9dd�����3�iŗWY�Π�)#x�O�F�l���c�4��Z�Gyc��ݛ� }��f)%�7.�BXU��5!����;%����e��߶*}�r-��U�5�\0	\n^`�8�X�\08�!$�I�\0�I���,�	gr&m��\0�R����KZ���\0JhY[o��\0�}�\rAr��\"htV\\���I8UQ�;�����Yb��!\\�#hkQk��NOu�]��^Ó0浏e>\'�*뭷�>=:z��i-�p��Љ�tb��n���Z�f�=\0�Hd��wq�F�m��?u��Q��(��*���]���8��I�L�l�\r�\0�<�=����n�+��4ދ���CZ9�ٯ��Zy�j^s��׼�(�,wy�w��\0\0��0���\0� \0� \0� ;g�U�w$��s�/�>��9���Q*�LZ�.��\r�[EhÃ�Q�X��S�\\��z��/C�?���t�9�|x����fF�P��ՙ5��G����#�z��2���j|�ر�>D}f���FU�e<�OQ��������Q�����L�����+��7�2�{-ç�-����x���wM��\n_Z�T1��~v�G��;F���Ei���I�K�Qg5wg�R�8�\0���\0���JQ��̾�񌻱�>ls�ͨ ~ew�<��wj�7��\"��C��2Tnn}<�4Z+.}����p���=�}U��d�8��0����CZ���i���k�r,9�c�O��ʺ�m�Ϗ��l�IlC�>�\0B�������snl�W7XE��(R�=ߴ�q*&7dr��Rª<^�zV��MZp��p����-w��4^�����]ڙX�z��cZ��Lơ\0@\0@\0@\0����U{���9��G��Ss�Y\\ڝ4o�O��ݢ��#o���M�,_p	��\'���2[+���|A�5���_�Jf�#M�]��̚��#�V��O�|��pyڏ3Ev,lO���t��9����⯸Jx�&�\0�T�J��j���\0<KT\ZR����}�N��W�R����I��.��En���-�y���:K���Z�V�6��~r���޼���_s�7��ں�ɍ�Sa\'\n�F�{*�����u���I��nE�\r�5�{�hkQP���y܌5�9=�ǲ���u���ǧOS��Kb:��*��R[�kg=�w�����C݌��� ���\0=��Sj���>��5NOi/C�{)����r��Xw��Y����=g�}��ܸ枅R�|Y{)/\0@\0@\0@\0��6z_ST�g�<���N�$����6��\\��{>�=��W:�a����j�6G1>J�\'L�f�FXVM�{@V�%Â8�6FY|A�}��(��$�x��t�(4��?/�f���]n�R�Մ6+DZ����bCJ�$�.<>E���F1��)�d00t���	mƋq,B�(�����d|fmSq��x��դ���%��-�]�����ё[>�1kJ���j\0\n�Y<yGOK�R�/Rz�ԛ�8y�Fk3�vηQh��.�@2Ǡ2�,�qڑeU��l��^��z��t�8�-�,ǋ7��\0!�>z��rg��R�\n(����\0� \0� \0� \0� ~��������YO�?�YmWJ�mD�Q���ٙ��{bݥ�r���uǪ�#�:}�ܢ�ܲ����gD�.J�/(\'�ij��\0Ҵ�\0���ܷ�^k�����\'�U�����&���3��q��hCX~-���˷j\\��\0�#D{7�[���a����ĻWh����G��ŏ���)����EJ�b\'��o��h�ܬe���9>���<+���̏��O\nc����\0@\0@\0@\0@\0@�GAU���P�~c��Џ8NP{Q+���-ǔv��v�<^��َ�E���\0?�{zmTmX�ϛ��C��+6^�Rѿ�7�>��\'�80�Q-�.P��o_�$�=���Ⱥ�;V�[z��O�dyK!%(�\"3���_#J!bFI �I�9��Yb)��I�6�`�NR�.:�\0i�7�J��gi�{ˮkkep[���vj�m�_F� =���_���ӯ�5\Z�һ�t�Y^���ű�U:Z�4��\'�by��xVY+%�#說5Gf(� X \0� \0� \0� \0� \0� \Z�:t�\Z�P��DN�8��2����ygk�$�l�P-F}�^z7������:]b��.\'�k4��=�\"�i���H�����O֯w�/��\'�����{q����ӳ}\0���az��ǘ��N���:�#��s>�l��dl�6��(�_�S?{t��N�V�[1��i��sږ������WR\n�@��\0��~sŔ��Y�0�`���\"H@\0@\0@\0@\0@\0@�( �2\'\r���.��f��v���,��#�Lt��徭[�v��#ʻ@�-��F��3����Z��*�n��!U���;�x��8���hv��q��z%u�P�\0\0\0\0\0@	淝���IaA�\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@?��',500,1,'El Teatro Huamantla','Teatro Huamantla','','',3),(4,'10.4004352,-66.9511459','����\0JFIF\0\0\0\0\0\0��\0�\0	( \Z%!1\"%)+0.. 383,7(-.+\n\n\n\r-% %---75---//3/---//52/---/-------------/------------��\0\0�\0�\0��\0\0\0\0\0\0\0\0\0\0\0\0\0\0��\0K\0\0\0\0!1AQa\"2Bq��Rr���#3Sbs��Tc���4������$C�������\0\Z\0\0\0\0\0\0\0\0\0\0\0\0��\06\0\0\0\0\0\0!1AQ\"aq��2�����#R�B�3Cbr���\0\0\0?\0�\0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� �Q�J���b\0������\"3�`�\'���=��&Ea�>^��\0��^��q�y�V������l5v�V/�?�x��7ס�<w�U��/�rܻ�u��	e�`rt���X=�|Ĕ��Oj)x>!��[3o�q�5�uZ�]��Z���[l�>3�>Gᘌ)o)?Ϙ��KiM��?>�C�5_֮���\0�-�ޯڊ����?��K�����Ke����W(���\"�KU?u��o�/鶛\'���\\��VwGťx���������Y~�x<�����=[uv�u���q�ڹqIx#���<��Ǒ�W��s�踢�&A��%��N\Z*��>$g��[�ǁ������;�u���\0�>\'��\\��+�_y�6�^�e\'�S����е���ڸ���_b���/{ix?���?�6�9�_m/�xی}�;�G߯#�/r���2���Omՠ�6ۻ�`ۧ�\'���M�a�6@���\\�g�\0io��~�W�����I��u�í�+\r�}�}˃�n��}Ì�p�/)��-�����K�O�v�P�k�Q�9km�ˑ��V���y�R1�Ž���7�z�El�������У�(�m8�����r�\rRḝZ�a�y������z���L�e�10٠�>��Ы_\\���e��Sr��jX�(���1�./\r#(�e2D�!\0@\0@\n������X���*İ����SD���U���O�C����4�g���l��1=}$6��3�5��݉˚�Jc5�00H����=�\Z�z�f�\0���y�rVvb������TOjog���}�\rm�p�)��=�����o:+ne�*�y��ՙ�\Z̛����l�u\'���Z��Rm[5���xd$�)��8Mu*��$b�Wg7�}�\\R`gN�:``鉃���n�\0w�{�B1\nV|�!|��r��#M�8����ڍ#ؚ{�Uq�&ʗ�E)[0B�}��gt=f9F���-�w��\0F�ΩIBiJ]V��s��[S��m6�0)`�b/�u�������UrUn��/�,׵y��w4Q���bgN�:^�/V��u��2i/py��M�׈�5pR�O�5�d�&�Fz�d�gF���\"�����<���=\'�~�UK�Z�Dm�y�<�h\0@1w\0N\0�I�����i,������TH�p6�R��n�����#���ӧ{<}G�$��}{����Z�v�op��z�֠�����[-�6�2�z�*��\0���ѫ�a��4M�h�P�´<����(��\0$J�jO�����\'���uX� ���l�=+�d��{\'=�?���O�F�_�Я|��\'�2��o�\'���jќ���|�F��$i��\0�%��F�mE�`,�95�q�#�7���R�������El����mm����3<�/��x�����yH{YCu���з��Ś����*�s��GPGC�4\'�nMft鉜:a��$�\0sπ����߸�)��S�E	�s��*S�x�L��M����lTF��<w.>�=~�wN�i��U���<lo6�	8P�����d\'{�َ��=L{8��i�3���Y��3�����4��ĭ�7X�}V##�O1��Y��Il��2g�����P�ӽS��y��h�~>%N���wr�.V�n�g����S�鲧VK(!�iY����jv7,�K�u<f;��\"�ˊ˾�7Z�ɴ��Ǐ����˫ʂk�5iq����]��m�<pT�겞jè?�#/�\nk�pyG�vS� \Z�WRY��Qi�����.Ǯ�=�N8��_*�&_g�����)�n�p��=>���:0e#!���s�sX����z6A�\0��6�t&����u\'��Y]r��(��Et�jl����}NYɮ�}Px����|φ}J�Un[��|�Y=N\\�V�l��^\\�\0�Q�G��OS6�y�Z���Ў�d���wj�\0(�S�c���O_!�a���u�=M]2���l��Zm��Pw���}�+<�������ܺ/��n������D;�gb�Ř�$�ˣ��e9M�O&�:pݳ�ߺ��V\"�\0d-x��quڶ+��[w���q��×<�)X�+��[V���2��g���:��un,�i��&�K�&����������C}O���֯��-_�ԏ���ޣ-�����yX��g���z�ٖ�ќ�O(���U��1��{,^���H�m�܃����_�,�1{+{�ZyMm=���\07��j�@+�B��Y����r�{��̂�Vo��R�o\Z�R�/��H�I$�I9$�$�d��iI-�͜��Y��-�u����A�$&��� �d�y3��n�u+�����9o�CN�T_qn�b�.�ˊO����ъ���S��2���\'8��q��wӻ�T�����>M)؜=Ǖ��\0�n��U�-{#�u����z���P�O`#{\0�y0�S��J��<��ĿOSV\'�a�8��n24�����F\n�*�y�����FBpRXd�\'�u}��%�<�cn��S5��P�a��W2\\VS^̷K��]ή�w�����v�M������yOBz:��x�=3<�j�o=:퍋1-ee�+�6�����V��FN��8o�N�W��ե���m>����ٻ��ZPn\r�������真s�٩�{+=H�iи�jXX݂�i�a8����S�\0����������6������l�xݾx~����\02�,վX�5�O���Y�/����/4�����>���c�c�]���ӺZg�1�Y(u+�	烟c���>S\\^�.CϚ���/�Hvi�)l�^;�}�Ne�zĸy:�\0�7�����.��nsǙ�G�P1\"���MU�;�՗96:�5���>H=\n�y��z��Y����S�6rl��Ozt�� �T6G0JqF����wf�og%�M�I Z��\r�|23Lg���c�����l�t[�Ϳ\'�\0�\'�5�����~突?a����fP~;��3��Y��J4��}K퉤��m=��W������\"<&[������6�\r,_��2ۺ]�cgQe{ʻ�w�n��\n��>F�_s?\"W�M/�\Z���f�|����?��Mq�Y��b�:��#�g�\0�]��r^�Y�N{-�����^΂�ۏ�n�p�bFS�>^D�\r\n��e��H{ \r⥲y`��<zL�=K��k�iS��m��vXN��������/�A�����v����gmZY>�2sW��5��Ny����ʐDҧ���dp�u��+��l7E�1׽��Pι�1��(h�ě���\n��r1�~Q��F1�5��Y�O��l-F��Jt����wc�.y��xL�+��?en���m��٣�5��~a��x� 2����V�g��d4�����ݟ�ꏘ������j�\0j���{=7�gM�m��3bשk����k:/w͘��R�;�0q2_f�v����Q���;�v2����5��*�#���9�\'��E��b��;D)S�����;\"�Bn��c�#�]M�YF]V��Dq.=NSC��O�SݒJ��Y_�����Z2V�i_�\'�N�i߱�v_ӽ(��i�m��ʰ�Yz0�\0�Ev)�%Ժ�O��\n˝�r[Ki���a�/WP=����\\)��ʹ�!ω��g�t��oE6����Ê��L���\0.sT&���VV����L����\'N���������a_��۔S��:y3M��Y���\\R``遃���������xg�Jl=���U�x�����4��S��c�����֪]�u�\0ǽ9��\r����7ٵ��WyI��:`gN��&�Pܨ^�yٝڗ��>�2��R�����Fs|��lI�NI�|�e�cqW�t$n�hl���\0�c�D��{*3��B�Y8A����S��.�M��x]k\r�z��sDoX/=�g�31��{I�\\\r��5W����gf��g�,��]`g�c�D^-c�%@8���J�Ae�Bo��i�R���=K!����Si_n��)S꧞NX�������z�c?ӯ�6z�c�%F��\'�k�Zy���es���_���/��SDjXGE(.�KgWzwv.GC�|e��*����3G\r��6��O�Ա%,Q���z���,V��{�����YK������>���I�ѵM�� ��*�y2��l��b�<���2��>�z�e`�p�����%�W	8�IqE��Ъ�����Λ�o�������dd��m/y}Q��(4������\0��U�(�89=VSɔ����	�����<29� ``�/I��e]J�Y��2}貛;3S�3M]�J��\n�.(03��&hZuu�Z�.v�O�+yE�����h�t\'.�|�z���������\"����|����\'����_�+̼���03������뫭��w����v�r��_.�j=w���/*10\r�\r�gv�g�1¢Yݽ���d,�Ae�Bo���@WT��S��1mU�=�g��X\'EǏ,sRmg�|;��+8�W�sz�S�c�a˻c�|<\0�@\0�a��,��,����Y}�����\n��Yݹ*��$	�,PYd�7�t�\'d٫?��r(�,�w�8�{g>��1��_�Y�r]?:��[�b���ݛ��\Z*E4��9���c��I��l��ԏV��\\q�VX �W�K��O��2Q���!eq�.2YG�6Y�����1��Z�=}�#���J���ڎ���x7齂�s�[��Ŝ��ѵM��9VU��e=D�*�X��&�%T����gܾ�Np�`�Xz�|?\"dn��8�E�y��˄���1\r�Z���G-[r,�#�ux3�mb�q�:��uY�˽uzvF����#���L���\0��-���Q]��<21� e����`*A��q����!8�EŖW7	).F[N�K/�p�|Q���pq�F�9G�AFo8�C2ҳQ3Q��ֿ���>�����Dw��D��h�f������^�Ң��=��챩���Q=�E���W��/�2��ʌ7l�\'}jTN�\'�o��;|��=��[T6�k�:��ֳ�+�P\"������=�9���A�{����,p���woe@�,PYd냛�&j� ����̡�}E�=���S�x��?�Y��Թ����y�\Zvݪ�t��R�K�9^x=��F/���3���d��!kKǂ� ����+��,p���woeGS��[e�,�psxG_���>��\0\r�%4jÿ���:��A�W���-�`��f���r]\r�S�;0�y���읗N����\n�ө=I<�>3ʜ�9mK���\n&H�\0@-�YJ��\"C:�O(䢤��7m�N�Yp[Jx�e��}������������OS�UŮ5�b���;Y�j�u�� ��e<�OPg�]�k(�U��w�j���\0��������vUӞ�\'���/֯k��ǽr>�P��ՙ(NAY�K�<W��Npyڏ2ر�>F�^��l��U��e=G�s����Q+pxds$@�o��ک��ܖS�}���R�6��F�ۥ>q��~�y��4��f�t����#}���QF��ճF�t�z$�{��3�b�_�)U�\0_����za�?���Et���Ν&�{�=�{V��~���ݯ�2�v�Q�����������^Tn�謹��8$�8UQ�;��������N�9�\"F��\"�t�|�Y�#\r{L�٬U>\'�*����>=:z��i-�p�����?��u<��z�<@��yc;��X�3��I�r\\�N�ӆ�7��en��Ys��8$�8TQ�;���s?�	u�(,���������\Z��вh�e��X��R��|}n+�]�qy~��z��:}���������R-u��(�U\0y<�ܞY�(���ӇD\0@\0@>\0�6�\0gWt�)5�ꌵLy�@z�}��#��iԴ���~番���w/��H�Mh$�ޖV����YI�\"z�������bz{7�4h�\Zp��ka���*z�>bN��-�W7V�.��4ڕ��ɬ��8�l}��+����v��̕v,lO��W�j�#*Ê��L���\0#�\'	���3�������-����{�o��[��\\��W��O$���=�;�eb�0�J��A�9dd�����3�iŗWY�Π�)#x�O�F�l���c�4��Z�Gyc��ݛ� }��f)%�7.�BXU��5!����;%����e��߶*}�r-��U�5�\0	\n^`�8�X�\08�!$�I�\0�I���,�	gr&m��\0�R����KZ���\0JhY[o��\0�}�\rAr��\"htV\\���I8UQ�;�����Yb��!\\�#hkQk��NOu�]��^Ó0浏e>\'�*뭷�>=:z��i-�p��Љ�tb��n���Z�f�=\0�Hd��wq�F�m��?u��Q��(��*���]���8��I�L�l�\r�\0�<�=����n�+��4ދ���CZ9�ٯ��Zy�j^s��׼�(�,wy�w��\0\0��0���\0� \0� \0� ;g�U�w$��s�/�>��9���Q*�LZ�.��\r�[EhÃ�Q�X��S�\\��z��/C�?���t�9�|x����fF�P��ՙ5��G����#�z��2���j|�ر�>D}f���FU�e<�OQ��������Q�����L�����+��7�2�{-ç�-����x���wM��\n_Z�T1��~v�G��;F���Ei���I�K�Qg5wg�R�8�\0���\0���JQ��̾�񌻱�>ls�ͨ ~ew�<��wj�7��\"��C��2Tnn}<�4Z+.}����p���=�}U��d�8��0����CZ���i���k�r,9�c�O��ʺ�m�Ϗ��l�IlC�>�\0B�������snl�W7XE��(R�=ߴ�q*&7dr��Rª<^�zV��MZp��p����-w��4^�����]ڙX�z��cZ��Lơ\0@\0@\0@\0����U{���9��G��Ss�Y\\ڝ4o�O��ݢ��#o���M�,_p	��\'���2[+���|A�5���_�Jf�#M�]��̚��#�V��O�|��pyڏ3Ev,lO���t��9����⯸Jx�&�\0�T�J��j���\0<KT\ZR����}�N��W�R����I��.��En���-�y���:K���Z�V�6��~r���޼���_s�7��ں�ɍ�Sa\'\n�F�{*�����u���I��nE�\r�5�{�hkQP���y܌5�9=�ǲ���u���ǧOS��Kb:��*��R[�kg=�w�����C݌��� ���\0=��Sj���>��5NOi/C�{)����r��Xw��Y����=g�}��ܸ枅R�|Y{)/\0@\0@\0@\0��6z_ST�g�<���N�$����6��\\��{>�=��W:�a����j�6G1>J�\'L�f�FXVM�{@V�%Â8�6FY|A�}��(��$�x��t�(4��?/�f���]n�R�Մ6+DZ����bCJ�$�.<>E���F1��)�d00t���	mƋq,B�(�����d|fmSq��x��դ���%��-�]�����ё[>�1kJ���j\0\n�Y<yGOK�R�/Rz�ԛ�8y�Fk3�vηQh��.�@2Ǡ2�,�qڑeU��l��^��z��t�8�-�,ǋ7��\0!�>z��rg��R�\n(����\0� \0� \0� \0� ~��������YO�?�YmWJ�mD�Q���ٙ��{bݥ�r���uǪ�#�:}�ܢ�ܲ����gD�.J�/(\'�ij��\0Ҵ�\0���ܷ�^k�����\'�U�����&���3��q��hCX~-���˷j\\��\0�#D{7�[���a����ĻWh����G��ŏ���)����EJ�b\'��o��h�ܬe���9>���<+���̏��O\nc����\0@\0@\0@\0@\0@�GAU���P�~c��Џ8NP{Q+���-ǔv��v�<^��َ�E���\0?�{zmTmX�ϛ��C��+6^�Rѿ�7�>��\'�80�Q-�.P��o_�$�=���Ⱥ�;V�[z��O�dyK!%(�\"3���_#J!bFI �I�9��Yb)��I�6�`�NR�.:�\0i�7�J��gi�{ˮkkep[���vj�m�_F� =���_���ӯ�5\Z�һ�t�Y^���ű�U:Z�4��\'�by��xVY+%�#說5Gf(� X \0� \0� \0� \0� \0� \Z�:t�\Z�P��DN�8��2����ygk�$�l�P-F}�^z7������:]b��.\'�k4��=�\"�i���H�����O֯w�/��\'�����{q����ӳ}\0���az��ǘ��N���:�#��s>�l��dl�6��(�_�S?{t��N�V�[1��i��sږ������WR\n�@��\0��~sŔ��Y�0�`���\"H@\0@\0@\0@\0@\0@�( �2\'\r���.��f��v���,��#�Lt��徭[�v��#ʻ@�-��F��3����Z��*�n��!U���;�x��8���hv��q��z%u�P�\0\0\0\0\0@	淝���IaA�\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@\0@?��',150,0,'La Plaza Huamantla','Plaza Huamantla','','',4);
/*!40000 ALTER TABLE `mustseeanddo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mustseeanddo_datail`
--

DROP TABLE IF EXISTS `mustseeanddo_datail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mustseeanddo_datail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `summary` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `mustseeanddo_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_l3ay8r5hew2tdeg4g62kuw5vl` (`mustseeanddo_id`),
  CONSTRAINT `FK_l3ay8r5hew2tdeg4g62kuw5vl` FOREIGN KEY (`mustseeanddo_id`) REFERENCES `mustseeanddo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mustseeanddo_datail`
--

LOCK TABLES `mustseeanddo_datail` WRITE;
/*!40000 ALTER TABLE `mustseeanddo_datail` DISABLE KEYS */;
INSERT INTO `mustseeanddo_datail` VALUES (1,'Detalle#1 El Teatro Villa Vicente Guerrero','Titulo#1 Teatro Villa Vicente Guerrero ',1),(2,'Detalle#1 El Plaza Villa Vicente Guerrero','Titulo#1 Plaza Villa Vicente Guerrero ',2),(3,'Detalle#2 El Plaza Villa Vicente Guerrero','Titulo#2 Plaza Villa Vicente Guerrero ',2),(4,'Detalle#1 El Teatro Huamantla','Titulo#1 Teatro Huamantla ',3),(5,'Detalle#2 El Teatro Huamantla','Titulo#2 Teatro Huamantla ',3),(6,'Detalle#3 El Teatro Huamantla','Titulo#3 Teatro Huamantla ',3),(7,'Detalle#1 El Plaza Huamantla','Titulo#1 Plaza Huamantla ',4);
/*!40000 ALTER TABLE `mustseeanddo_datail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `place`
--

DROP TABLE IF EXISTS `place`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `place` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `place`
--

LOCK TABLES `place` WRITE;
/*!40000 ALTER TABLE `place` DISABLE KEYS */;
/*!40000 ALTER TABLE `place` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seedo_attachment`
--

DROP TABLE IF EXISTS `seedo_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seedo_attachment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file` tinyblob NOT NULL,
  `name` varchar(255) NOT NULL,
  `mustseeanddo_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_jkol4jdxrq3wcy8b8pwm82l2s` (`mustseeanddo_id`),
  CONSTRAINT `FK_jkol4jdxrq3wcy8b8pwm82l2s` FOREIGN KEY (`mustseeanddo_id`) REFERENCES `mustseeanddo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seedo_attachment`
--

LOCK TABLES `seedo_attachment` WRITE;
/*!40000 ALTER TABLE `seedo_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `seedo_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state`
--

DROP TABLE IF EXISTS `state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `country_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_lxoqjm8644epv72af3k3jpalx` (`country_id`),
  CONSTRAINT `FK_lxoqjm8644epv72af3k3jpalx` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state`
--

LOCK TABLES `state` WRITE;
/*!40000 ALTER TABLE `state` DISABLE KEYS */;
INSERT INTO `state` VALUES (1,'Tlaxcala',1),(2,'Aguascalientes',1),(3,'Baja California',1),(4,'Distrito Capital',2),(5,'Nueva Esparta',2);
/*!40000 ALTER TABLE `state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tracking`
--

DROP TABLE IF EXISTS `tracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tracking` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_2rykyguadprppyw7h5n0dxgae` (`user_id`),
  CONSTRAINT `FK_2rykyguadprppyw7h5n0dxgae` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tracking`
--

LOCK TABLES `tracking` WRITE;
/*!40000 ALTER TABLE `tracking` DISABLE KEYS */;
/*!40000 ALTER TABLE `tracking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `travel`
--

DROP TABLE IF EXISTS `travel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `travel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `budget` double NOT NULL,
  `created_at` datetime NOT NULL,
  `date_arrival` datetime NOT NULL,
  `date_going` datetime NOT NULL,
  `description` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `country_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_fshtx7dymeckmq7pxyu63nkfn` (`country_id`),
  KEY `FK_mnu8phiteeyotooyltveato7h` (`user_id`),
  CONSTRAINT `FK_mnu8phiteeyotooyltveato7h` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_fshtx7dymeckmq7pxyu63nkfn` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `travel`
--

LOCK TABLES `travel` WRITE;
/*!40000 ALTER TABLE `travel` DISABLE KEYS */;
/*!40000 ALTER TABLE `travel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `travel_mustseeanddo`
--

DROP TABLE IF EXISTS `travel_mustseeanddo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `travel_mustseeanddo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `destination_id` bigint(20) DEFAULT NULL,
  `mustseeanddo_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_gj8qyk5sofymk2yyscftw80s3` (`destination_id`),
  KEY `FK_hxnikoux2hl2lfmm9wu0me4k8` (`mustseeanddo_id`),
  CONSTRAINT `FK_hxnikoux2hl2lfmm9wu0me4k8` FOREIGN KEY (`mustseeanddo_id`) REFERENCES `mustseeanddo` (`id`),
  CONSTRAINT `FK_gj8qyk5sofymk2yyscftw80s3` FOREIGN KEY (`destination_id`) REFERENCES `destination` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `travel_mustseeanddo`
--

LOCK TABLES `travel_mustseeanddo` WRITE;
/*!40000 ALTER TABLE `travel_mustseeanddo` DISABLE KEYS */;
/*!40000 ALTER TABLE `travel_mustseeanddo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `travel_partner`
--

DROP TABLE IF EXISTS `travel_partner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `travel_partner` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `travel_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_aofmeu8kxtri5aw0tt0f8oett` (`travel_id`),
  KEY `FK_5ri2kfwcuig4bhnw180b4lsmh` (`user_id`),
  CONSTRAINT `FK_5ri2kfwcuig4bhnw180b4lsmh` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_aofmeu8kxtri5aw0tt0f8oett` FOREIGN KEY (`travel_id`) REFERENCES `travel` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `travel_partner`
--

LOCK TABLES `travel_partner` WRITE;
/*!40000 ALTER TABLE `travel_partner` DISABLE KEYS */;
/*!40000 ALTER TABLE `travel_partner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `enabled` bit(1) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_sb8bbouer5wak8vyiiy4pf2bx` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'2016-02-02 00:00:00','','1234','ojuanibatrupon@gmail.com'),(2,'2016-02-02 00:00:00','','1234','bismarckpm@gmail.com');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_book`
--

DROP TABLE IF EXISTS `user_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_book` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `book_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_953y2aqv7204gbduoa7tc5n9c` (`book_id`),
  KEY `FK_cspke4dyu3phbfgfm3rikfxe` (`user_id`),
  CONSTRAINT `FK_cspke4dyu3phbfgfm3rikfxe` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_953y2aqv7204gbduoa7tc5n9c` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_book`
--

LOCK TABLES `user_book` WRITE;
/*!40000 ALTER TABLE `user_book` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_book` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-08 19:24:05
