angular.module('starter.controllers', ['ionic', 'ngCordova'])

    .controller('DashCtrl', function ($scope)
    {
    })

    .controller('FileCtrl', function ($scope, $ionicLoading)
    {

        $scope.download = function ()
        {
            $ionicLoading.show({
                template: 'Loading...'
            });
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs)
                {
                    fs.root.getDirectory(
                        "ExampleProject",
                        {
                            create: true
                        },
                        function (dirEntry)
                        {
                            dirEntry.getFile(
                                "test.png",
                                {
                                    create: true,
                                    exclusive: false
                                },
                                function gotFileEntry(fe)
                                {
                                    var p = fe.toURL();
                                    fe.remove();
                                    ft = new FileTransfer();
                                    ft.download(
                                        encodeURI("http://ionicframework.com/img/ionic-logo-blog.png"),
                                        p,
                                        function (entry)
                                        {
                                            $ionicLoading.hide();
                                            alert("file:///root/sdcard/Pictures");
                                            $scope.imgFile = "file:///root/sdcard/Pictures/"//entry.toURL();
                                        },
                                        function (error)
                                        {
                                            $ionicLoading.hide();
                                            alert("Download Error Source -> " + error.source);
                                        },
                                        false,
                                        null
                                    );
                                },
                                function ()
                                {
                                    $ionicLoading.hide();
                                    console.log("Get file failed");
                                }
                            );
                        }
                    );
                },
                function ()
                {
                    $ionicLoading.hide();
                    console.log("Request for filesystem failed");
                });
        }

        $scope.load = function ()
        {
            $ionicLoading.show({
                template: 'Loading...'
            });
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs)
                {
                    fs.root.getDirectory(
                        "ExampleProject",
                        {
                            create: false
                        },
                        function (dirEntry)
                        {
                            dirEntry.getFile(
                                "test.png",
                                {
                                    create: false,
                                    exclusive: false
                                },
                                function gotFileEntry(fe)
                                {
                                    $ionicLoading.hide();
                                    $scope.imgFile = fe.toURL();
                                },
                                function (error)
                                {
                                    $ionicLoading.hide();
                                    console.log("Error getting file");
                                }
                            );
                        }
                    );
                },
                function ()
                {
                    $ionicLoading.hide();
                    console.log("Error requesting filesystem");
                });
        }

    })

    .controller('LectorCtrl', function ($scope, $cordovaBarcodeScanner)
    {
        $scope.leerCodigo = function ()
        {
            console.log('En controller');
            $cordovaBarcodeScanner.scan().then(function (imagescaneda)
            {
                alert(imagescaneda.text);
            }, function (error)
            {
                alert("Ocurrio error : " + error);
            });
        }
    })

    .controller('CamaraCtrl', function ($scope, $cordovaCamera, $cordovaFile)
    {
        $scope.pictureUrl = 'http://placehold.it/300x300';

        $scope.tomarFoto = function ()
        {
            var options = {
                destinationType: Camera.DestinationType.DATA_URL,
                encodingType: Camera.EncodingType.JPEG,
                saveToPhotoAlbum: true
            }
            $cordovaCamera.getPicture(options).then(function (data)
                {
                    $scope.pictureUrl = 'data:image/jpeg:base64,' + data;
                    //Grab the file name of the photo in the temporary directory
                    var currentName = data.replace(/^.*[\\\/]/, '');
                    alert(currentName);
                    //Create a new name for the photo
                    var d = new Date(),
                        n = d.getTime(),
                        newFileName = n + ".jpg";
                    alert("tempDirectory" + cordova.file.tempDirectory);
                    alert("datadirectory" + cordova.file.dataDirectory);
                    //Move the file to permanent storage
                    $cordovaFile.moveFile(cordova.file.tempDirectory, currentName, cordova.file.dataDirectory, newFileName).then(function (success)
                    {

                        //success.nativeURL will contain the path to the photo in permanent storage, do whatever you wish with it, e.g:
                        alert(success.nativeURL);


                    }, function (error)
                    {
                        alert("an error occured" + error);
                    });
                }, function (error)
                {
                    console.log('camara error ' + angular.toJson(data))
                }
            )
        };
    })

    .factory('Markers', function ($http)
    {

        var markers = [];

        return {
            getMarkers: function (params)
            {

                return $http.get("http://localhost:8090/", {params: params}).then(function (response)
                {
                    markers = [{'id': '1', 'name': 'marca 1', 'lat': '10.519838', 'lng': '-66.923222'}, {
                        'id': '2',
                        'name': 'marca 2',
                        'lat': '10.519310',
                        'lng': '-66.918394'
                    }, {'id': '3', 'name': 'marca 3', 'lat': '10.517200', 'lng': '-66.927557'}];
                    return markers;
                });

            }
        }

    })

    .factory('GoogleMaps', function ($cordovaGeolocation, $ionicLoading, $rootScope, $cordovaNetwork, Markers, ConnectivityMonitor)
    {

        var markerCache = [];
        var apiKey = false;
        var map = null;

        function initMap()
        {

            var options = {timeout: 10000, enableHighAccuracy: true};

            $cordovaGeolocation.getCurrentPosition(options).then(function (position)
            {
                //Carga Inicial
                //Obtengo la posición actual del usuario
                var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                //Configuro el mapa par la posicion actual

                var mapOptions = {
                    center: latLng,
                    zoom: 10,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                //asigno a la interfaz los valores del mapa posicion inicial
                map = new google.maps.Map(document.getElementById("map"), mapOptions);
                //Creo la marca para la posicion inicial

                var marker = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    title: 'Posicion Inicial'
                });

                //Esperando que cargue el mapa
                google.maps.event.addListenerOnce(map, 'idle', function ()
                {
                    loadMarkers($rootScope);

                    //Recarga las marcas cuando se mueve el mapa
                    google.maps.event.addListener(map, 'dragend', function ()
                    {
                        console.log("moviendo");
                        loadMarkers($rootScope);
                    });

                    //Recarga las marcas cuando se acerca el mapa
                    google.maps.event.addListener(map, 'zoom_changed', function ()
                    {
                        console.log("acercando");
                        loadMarkers($rootScope);
                    });
                });
                console.log(map);
                $rootScope.map = map;
                enableMap();

            }, function (error)
            {
                console.log("No se obtuvo localizacion" + error);
            });

        }

        function enableMap()
        {
            $ionicLoading.hide();
        }

        function disableMap()
        {
            $ionicLoading.show({
                template: 'Debe estar conectado a internet para ver el mapa.'
            });
        }

        function loadGoogleMaps()
        {

            $ionicLoading.show({
                template: 'Cargando el Mapa'
            });

            //Esta función se llamará una vez que el SDK se ha cargado
            window.mapInit = function ()
            {
                initMap();
            };

            //Creamos un script para insertar en la página
            var script = document.createElement("script");
            script.type = "text/javascript";
            script.id = "googleMaps";
            //Nota la función de retorno en la URL es la que hemos creado anteriormente
            if (apiKey)
            {
                script.src = 'http://maps.google.com/maps/api/js?key=' + apiKey + '&sensor=true&callback=mapInit';
            }
            else
            {
                script.src = 'http://maps.google.com/maps/api/js?sensor=true&callback=mapInit';
            }
            document.body.appendChild(script);
        }

        function checkLoaded()
        {
            if (typeof google == "undefined" || typeof google.maps == "undefined")
            {
                loadGoogleMaps();
            }
            else
            {
                enableMap();
            }
        }

        function loadMarkers()
        {

            var center = map.getCenter();
            var bounds = map.getBounds();
            var zoom = 15;

            //Hacemos mas legibles los objetos devueltos por google
            var centerNorm = {
                lat: center.lat(),
                lng: center.lng()
            };
            var boundsNorm = {
                northeast: {
                    lat: bounds.getNorthEast().lat(),
                    lng: bounds.getNorthEast().lng()
                },
                southwest: {
                    lat: bounds.getSouthWest().lat(),
                    lng: bounds.getSouthWest().lng()
                }
            };

            var boundingRadius = getBoundingRadius(centerNorm, boundsNorm);

            var params = {
                "center": centerNorm,
                "bounds": boundsNorm,
                "zoom": zoom,
                "boundingRadius": boundingRadius
            };

            var markers = Markers.getMarkers(params).then(function (markers)
            {

                console.log("Markers: ", markers.length);

                var records = markers;

                for (var i = 0; i < records.length; i++)
                {

                    var record = records[i]
                    var latLng = new google.maps.LatLng(record.lng, record.lat);

                    var marker = new google.maps.Marker({
                        map: map,
                        animation: google.maps.Animation.DROP,
                        position: latLng
                    });

                    google.maps.event.addListener(marker, 'click', function ()
                    {
                        infoWindow.open(map, marker);
                    });
                    console.log(map);
                    $rootScope.map = map;
                    enableMap();

                    /*var record = records[i];

                     // Comprobamos si el marcador ya se ha agregado
                     if (!markerExists(record.lat, record.lng)) {
                     var markerPos = new google.maps.LatLng(record.lng, record.lat);

                     // agregamos el marcador
                     var marker = new google.maps.Marker({
                     map: map,
                     animation: google.maps.Animation.DROP,
                     position: markerPos
                     });

                     // Agregue el marcador al markerCache para añadir de nuevo más tarde
                     var markerData = {
                     lat: record.lat,
                     lng: record.lng,
                     marker: marker
                     };

                     markerCache.push(markerData);

                     var infoWindowContent = "<h4>" + record.name + "</h4>";

                     addInfoWindow(marker, infoWindowContent, record, rootScope);
                     }*/

                }

            });
        }

        function markerExists(lat, lng)
        {
            var exists = false;
            var cache = markerCache;
            for (var i = 0; i < cache.length; i++)
            {
                if (cache[i].lat === lat && cache[i].lng === lng)
                {
                    exists = true;
                }
            }

            return exists;
        }

        function getBoundingRadius(center, bounds)
        {
            return getDistanceBetweenPoints(center, bounds.northeast, 'miles');
        }


        function getDistanceBetweenPoints(pos1, pos2, units)
        {

            var earthRadius = {
                miles: 3958.8,
                km: 6371
            };

            var R = earthRadius[units || 'miles'];
            var lat1 = pos1.lat;
            var lon1 = pos1.lng;
            var lat2 = pos2.lat;
            var lon2 = pos2.lng;

            var dLat = toRad((lat2 - lat1));
            var dLon = toRad((lon2 - lon1));
            var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
                Math.sin(dLon / 2) *
                Math.sin(dLon / 2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            var d = R * c;

            return d;

        }

        function toRad(x)
        {
            return x * Math.PI / 180;
        }

        function addInfoWindow(marker, message, record, rootScope)
        {

            var infoWindow = new google.maps.InfoWindow({
                content: message
            });

            google.maps.event.addListener(marker, 'click', function ()
            {
                infoWindow.open(map, marker);
            });
            console.log(map);
            rootScope.map = map;
        }

        function addConnectivityListeners()
        {

            if (ionic.Platform.isWebView())
            {

                //Comprueba si el mapa ya está cargado cuando el usuario se conecta , si no, lo carga
                $rootScope.$on('$cordovaNetwork:online', function (event, networkState)
                {
                    checkLoaded();
                });

                //Deshabilita el mapa si no tiene red
                $rootScope.$on('$cordovaNetwork:offline', function (event, networkState)
                {
                    disableMap();
                });

            }
            else
            {

                //Igual que el anterior pero para cuando no se están ejecutando en un dispositivo
                window.addEventListener("online", function (e)
                {
                    checkLoaded();
                }, false);

                window.addEventListener("offline", function (e)
                {
                    disableMap();
                }, false);
            }

        }

        return {
            init: function (key)
            {

                if (typeof key != "undefined")
                {
                    apiKey = key;
                }

                if (typeof google == "undefined" || typeof google.maps == "undefined")
                {

                    console.warn("Google Maps SDK needs to be loaded");

                    disableMap();

                    if (ConnectivityMonitor.isOnline())
                    {
                        loadGoogleMaps();
                    }
                }
                else
                {
                    if (ConnectivityMonitor.isOnline())
                    {
                        initMap();
                        enableMap();
                    }
                    else
                    {
                        disableMap();
                    }
                }

                addConnectivityListeners();

            }
        }

    })

    .factory('ConnectivityMonitor', function ($rootScope, $cordovaNetwork)
    {

        return {
            isOnline: function ()
            {

                /* if(ionic.Platform.isWebView()){
                 return $cordovaNetwork.isOnline();
                 } else {
                 return navigator.onLine;
                 }*/
                return true;

            },
            ifOffline: function ()
            {

                /* if(ionic.Platform.isWebView()){
                 return !$cordovaNetwork.isOnline();
                 } else {
                 return !navigator.onLine;
                 }*/
                return true;

            }
        }
    })

    .controller('MapCtrl', function ($scope, $state, $cordovaGeolocation)
    {

    })

    .controller('ImagenCtrl', function ($scope, $cordovaCamera, $cordovaFile)
    {
        // 1
        $scope.images = [];


        $scope.addImage = function ()
        {
            // 2
            var options = {
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.CAMERA, // Camera.PictureSourceType.PHOTOLIBRARY
                allowEdit: false,
                encodingType: Camera.EncodingType.JPEG,
                popoverOptions: CameraPopoverOptions,
            };

            // 3
            $cordovaCamera.getPicture(options).then(function (imageData)
            {

                // 4
                onImageSuccess(imageData);

                function onImageSuccess(fileURI)
                {
                    createFileEntry(fileURI);
                }

                function createFileEntry(fileURI)
                {
                    window.resolveLocalFileSystemURL(fileURI, copyFile, fail);
                }

                // 5
                function copyFile(fileEntry)
                {
                    var name = fileEntry.fullPath.substr(fileEntry.fullPath.lastIndexOf('/') + 1);
                    var newName = makeid() + name;

                    window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function (fileSystem2)
                        {
                            fileEntry.copyTo(
                                fileSystem2,
                                newName,
                                onCopySuccess,
                                fail
                            );
                        },
                        fail);
                }

                // 6
                function onCopySuccess(entry)
                {
                    $scope.$apply(function ()
                    {
                        $scope.images.push(entry.nativeURL);
                    });
                }

                function fail(error)
                {
                    console.log("fail: " + error.code);
                }

                function makeid()
                {
                    var text = "";
                    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

                    for (var i = 0; i < 5; i++)
                    {
                        text += possible.charAt(Math.floor(Math.random() * possible.length));
                    }
                    return text;
                }

            }, function (err)
            {
                console.log(err);
            });
        }

        $scope.urlForImage = function (imageName)
        {
            var name = imageName.substr(imageName.lastIndexOf('/') + 1);
            var trueOrigin = cordova.file.dataDirectory + name;
            return trueOrigin;
        }

        $scope.sendEmail = function ()
        {
            // 1
            var bodyText = "<h2>Look at this images!</h2>";
            if (null != $scope.images)
            {
                var images = [];
                var savedImages = $scope.images;
                for (var i = 0; i < savedImages.length; i++)
                {
                    // 2
                    images.push("" + $scope.urlForImage(savedImages[i]));
                    // 3
                    images[i] = images[i].replace('file://', '');
                }

                // 4
                window.plugin.email.open({
                        to: ["bismarckpm@gmail.com"], // email addresses for TO field
                        cc: Array, // email addresses for CC field
                        bcc: Array, // email addresses for BCC field
                        attachments: images, // file paths or base64 data streams
                        subject: "Imagen de prueba", // subject of the email
                        body: bodyText, // email body (for HTML, set isHtml to true)
                        isHtml: true, // indicats if the body is HTML or plain text
                    }, function ()
                    {
                        console.log('email view dismissed');
                    },
                    this);
            }
        }
    });

;


