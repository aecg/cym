/*==============================================================*/
/* DBMS name:      SQLLite                                   */
/* Created on:     06/12/2015 8:02:13                           */
/*==============================================================*/


/*==============================================================*/
/* Table: catalog                                               */
/*==============================================================*/
CREATE TABLE IF NOT EXISTS catalog
(
  id                   INTEGER PRIMARY KEY AUTOINCREMENT,
  name                 TEXT not null,
  displayable          INTEGER not null
);

/*==============================================================*/
/* Table: user                                                  */
/*==============================================================*/
CREATE TABLE IF NOT EXISTS  user
(
   id                   INTEGER PRIMARY KEY AUTOINCREMENT,
   username             TEXT not null,
   password             TEXT null,
   created_at           NUMERIC null,
   enabled              INTEGER null,
   login_state          INTEGER null
);

/*==============================================================*/
/* Table: tracking                                              */
/*==============================================================*/
CREATE TABLE IF NOT EXISTS  tracking
(
   id                   INTEGER PRIMARY KEY AUTOINCREMENT,
   name                 TEXT not null,
   created_at           NUMERIC not null,
   user_id              REFERENCES user(id),
   deleted              INTEGER null,
   unique_id            TEXT null
);


/*==============================================================*/
/* Table: travel                                                */
/*==============================================================*/
CREATE TABLE IF NOT EXISTS travel
(
   id                   INTEGER PRIMARY KEY AUTOINCREMENT,
   name                 TEXT not null,
   budget               NUMERIC(15,2) not null,
   dateGoing            NUMERIC not null,
   dateArrival          NUMERIC not null,
   description          TEXT not null,
   country_id           REFERENCES country(id),
   user_id              REFERENCES user(id),
   deleted              INTEGER null,
   unique_id            TEXT null
);



/*==============================================================*/
/* Table: travel_mustseeanddo                    */
/*==============================================================*/
CREATE TABLE IF NOT EXISTS travel_mustseeanddo
(
   id                		   INTEGER PRIMARY KEY AUTOINCREMENT,
   destination_id     		   REFERENCES travel(id),
   mustseeanddo_id			   REFERENCES mustseeanddo(id),
   deleted                     INTEGER null,
   unique_id                   TEXT null,
   order_position              INTEGER null
);



/*==============================================================*/
/* Table: country                                               */
/*==============================================================*/
CREATE TABLE IF NOT EXISTS  country
(
   id                   INTEGER PRIMARY KEY,
   name                 TEXT not null
);


/*==============================================================*/
/* Table: cuepoint                                              */
/*==============================================================*/
CREATE TABLE IF NOT EXISTS  cuepoint
(
   id                   INTEGER PRIMARY KEY AUTOINCREMENT,
   name                 TEXT not null,
   description			TEXT not null,
   geographic_position  TEXT not null,
   date					NUMERIC not null,
   tracking_id          REFERENCES tracking(id),
   deleted              INTEGER null,
   unique_id            TEXT null
);



/*==============================================================*/
/* Table: datauser                                              */
/*==============================================================*/
CREATE TABLE IF NOT EXISTS  datauser
(
   id                   INTEGER PRIMARY KEY AUTOINCREMENT,
   name                 TEXT null,
   lastname             TEXT null,
   email                TEXT null,
   phone                TEXT null,
   user_id 		        REFERENCES user(id),
   language             TEXT null,
   image_profile        BLOB null
);



/*==============================================================*/
/* Table: destination                                           */
/*==============================================================*/
CREATE TABLE IF NOT EXISTS  destination
(
   id                   INTEGER PRIMARY KEY AUTOINCREMENT,
   date                 NUMERIC not null,
   description          TEXT null,
   state_id             REFERENCES state(id),
   location_id          REFERENCES location(id),
   travel_id            REFERENCES travel(id),
   deleted              INTEGER null,
   unique_id            TEXT null,
   order_position       INTEGER null
);



/*==============================================================*/
/* Table: doc_attachment                                        */
/*==============================================================*/
CREATE TABLE IF NOT EXISTS  doc_attachment
(
   id                   INTEGER PRIMARY KEY AUTOINCREMENT,
   document_id          INTEGER not null,
   url_file             TEXT not null,
   type                 TEXT not null
);



/*==============================================================*/
/* Table: document                                              */
/*==============================================================*/
CREATE TABLE IF NOT EXISTS  document
(
   id                   INTEGER PRIMARY KEY AUTOINCREMENT,
   name                 TEXT not null,
   description          TEXT not null,
   geographic_position  TEXT not null,
   date                 NUMERIC not null,
   user_id              REFERENCES user(id)

);

/*==============================================================*/
/* Table: state                                                 */
/*==============================================================*/
CREATE TABLE IF NOT EXISTS  state
(
   id                   INTEGER PRIMARY KEY,
   name                 TEXT,
   country_id           REFERENCES country(id)
);


/*==============================================================*/
/* Table: location                                              */
/*==============================================================*/
CREATE TABLE IF NOT EXISTS  location
(
   id                   INTEGER PRIMARY KEY,
   name                 TEXT not null,
   geographic_position  TEXT not null,
   state_id             REFERENCES state(id)
);


/*==============================================================*/
/* Table: mustseeanddo                                          */
/*==============================================================*/
CREATE TABLE IF NOT EXISTS  mustseeanddo
(
   id                   INTEGER PRIMARY KEY,
   title                TEXT not null,
   summary              TEXT not null,
   image_reference      BLOB not null,
   geographic_position  TEXT not null,
   qr                   INTEGER not null,
   ar                   INTEGER not null,
   location_id          REFERENCES location(id),
   price                NUMERIC(15,2) not null,
   catalog_id           REFERENCES catalog(id)
);


/*==============================================================*/
/* Table: mustseeanddo_qr                                   */
/*==============================================================*/
CREATE TABLE IF NOT EXISTS  mustseeanddo_qr
(
   id                   INTEGER PRIMARY KEY,
   mustseeanddo_id      REFERENCES mustseeanddo(id),
   title                TEXT null,
   summary              TEXT null
);

/*==============================================================*/
/* Table: mustseeanddo_ar                                   */
/*==============================================================*/
CREATE TABLE IF NOT EXISTS  mustseeanddo_ar
(
   id                   INTEGER PRIMARY KEY,
   mustseeanddo_id      REFERENCES mustseeanddo(id),
   image_preview        BLOB null,
   image_display        BLOB null,
   image_binary         BLOB null,
   is_360               INTEGER null,
   image_360            BLOB null
);

/*==============================================================*/
/* Table: mustseeanddo_detail                                   */
/*==============================================================*/
CREATE TABLE IF NOT EXISTS  mustseeanddo_detail
(
   id                   INTEGER PRIMARY KEY,
   mustseeanddo_id      REFERENCES mustseeanddo(id),
   title                TEXT null,
   summary              TEXT null,
   image_reference      BLOB null
);

/*==============================================================*/
/* Table: book                                                  */
/*==============================================================*/
CREATE TABLE IF NOT EXISTS  book
(
   id                   INTEGER PRIMARY KEY,
   name                 TEXT not null,
   description          TEXT not null,
   image_reference      BLOB null,
   last_update          NUMERIC not null
);

/*==============================================================*/
/* Table: book_mustseeanddo                                         */
/*==============================================================*/
CREATE TABLE IF NOT EXISTS book_mustseeanddo
(
   id                   INTEGER PRIMARY KEY,
   mustseeanddo_id      REFERENCES mustseeanddo(id),
   book_id              REFERENCES book(id)
);

/*==============================================================*/
/* Table: user_book                                             */
/*==============================================================*/
CREATE TABLE IF NOT EXISTS user_book
(
   id                   INTEGER PRIMARY KEY AUTOINCREMENT,
   user_id              REFERENCES location(id),
   book_id              REFERENCES book(id)
);

/*==============================================================*/
/* Table: state_recommended_mustseeanddo                        */
/*==============================================================*/

CREATE TABLE IF NOT EXISTS state_recommended_mustseeanddo
(
  id                   INTEGER PRIMARY KEY AUTOINCREMENT,
  mustseeanddo_id      REFERENCES mustseeanddo(id),
  state_id             REFERENCES state(id)
);

INSERT INTO country (id, name) VALUES (154,'México');

INSERT INTO state (id, name, country_id) VALUES (2454,'Tlaxcala', 154);

INSERT INTO location (id, name, state_id, geographic_position) VALUES (252764, 'Amaxac De Guerrero', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252766, 'Apetatitlán de Antonio Carvajal', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252767, 'Apizaco', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252771, 'Atlangatepec', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252776, 'Calpulalpan', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252778, 'Chiautempan', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252780, 'Contla de Juan Cuamatzi', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252786, 'Cuaxomulco', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252788, 'Españita', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252795, 'Huamantla', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252796, 'Hueyotlipan', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252799, 'Ixtacuixtla de Mariano Matamoros', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252800, 'Ixtenco', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252811, 'Nanacamilpa', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252812, 'Nativitas', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252815, 'Panotla', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252851, 'San Pablo Del Monte', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252856, 'Sanctórum de Lázaro Cárdenas', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252862, 'Santa Cruz Tlaxcala', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252887, 'Tepeyanco', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252889, 'Terrenate', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252890, 'Tetla de La Solidaridad', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252899, 'Tlaxcala', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252901, 'Tlaxco', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252908, 'Tzompantepec', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252912, 'Xalostoc', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252915, 'Xaltocan', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252920, 'Yauhquemehcan', 2454 , '');
INSERT INTO location (id, name, state_id, geographic_position) VALUES (252921, 'Zacatelco', 2454 , '');


INSERT INTO catalog (id, name, displayable) VALUES (1, 'Arqueología', 1);
INSERT INTO catalog (id, name, displayable) VALUES (2, 'Arte popular', 0);
INSERT INTO catalog (id, name, displayable) VALUES (3, 'Ciudad de Tlaxcala', 1);
INSERT INTO catalog (id, name, displayable) VALUES (4, 'Ecoturismo', 1);
INSERT INTO catalog (id, name, displayable) VALUES (5, 'Fiestas y ferias', 0);
INSERT INTO catalog (id, name, displayable) VALUES (6, 'Gastronomía', 0);
INSERT INTO catalog (id, name, displayable) VALUES (7, 'Haciendas y ganaderías', 1);
INSERT INTO catalog (id, name, displayable) VALUES (8, 'Arte Sacro', 1);
INSERT INTO catalog (id, name, displayable) VALUES (9, 'Cultura', 1);
INSERT INTO catalog (id, name, displayable) VALUES (10, 'Balnearios y Centros Holísticos', 1);

INSERT INTO user (id, username, password, created_at, enabled, login_state) VALUES (1, 'admin', '123456', 1449521526000, 1, 0);
INSERT INTO authority (id, authority, user_id) VALUES (1, 'ROLE_ADMIN', 1);

UPDATE location set geographic_position = '19.3667,-98.1833' WHERE id = 252764;
UPDATE location set geographic_position = '19.3333,-98.1833' WHERE id = 252766;
UPDATE location set geographic_position = '19.3843,-97.9282' WHERE id = 252767;
UPDATE location set geographic_position = '19.5333,-98.2042' WHERE id = 252771;
UPDATE location set geographic_position = '19.5833,-98.5833' WHERE id = 252776;
UPDATE location set geographic_position = '19.3872,-98.2333' WHERE id = 252778;
UPDATE location set geographic_position = '19.3455,-98.1927' WHERE id = 252780;
UPDATE location set geographic_position = '19.35,-98.1' WHERE id = 252786;
UPDATE location set geographic_position = '19.4667,-98.4167' WHERE id = 252788;
UPDATE location set geographic_position = '19.5586,-97.9167' WHERE id = 252795;
UPDATE location set geographic_position = '19.4833,-98.35' WHERE id = 252796;
UPDATE location set geographic_position = '19.3333,-98.3667' WHERE id = 252799;
UPDATE location set geographic_position = '19.25,-97.8931' WHERE id = 252800;
UPDATE location set geographic_position = '19.4833,-98.55' WHERE id = 252811;
UPDATE location set geographic_position = '19.2333,-98.3167' WHERE id = 252812;
UPDATE location set geographic_position = '19.3167,-98.2667' WHERE id = 252815;
UPDATE location set geographic_position = '19.1281,-98.192' WHERE id = 252851;
UPDATE location set geographic_position = '19.5,-98.4667' WHERE id = 252856;
UPDATE location set geographic_position = '19.35,-98.1333' WHERE id = 252862;
UPDATE location set geographic_position = '19.2458,-98.2333' WHERE id = 252887;
UPDATE location set geographic_position = '19.4833,-97.9167' WHERE id = 252889;
UPDATE location set geographic_position = '19.4333,-98.1' WHERE id = 252890;
UPDATE location set geographic_position = '19.2458,-98.312' WHERE id = 252899;
UPDATE location set geographic_position = '19.6575,-98.3375' WHERE id = 252901;
UPDATE location set geographic_position = '19.3833,-98.1' WHERE id = 252908;
UPDATE location set geographic_position = '19.4,-98.05' WHERE id = 252912;
UPDATE location set geographic_position = '19.4167,-98.2' WHERE id = 252915;
UPDATE location set geographic_position = '19.3956,-98.1722' WHERE id = 252920;
UPDATE location set geographic_position = '19.2153,-98.2389' WHERE id = 252921;


