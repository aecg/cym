'use strict';

(function() {
	angular.module('cym').directive('messages', messages);

	function messages() {
	    var directive = {
	        restrict: 'E',
	        templateUrl: 'templates/messages.html',
	        scope: {
	        	messages: '=src'
	        }
	    };
	    return directive;
	}
	
})();