'use strict';

(function() {
	angular.module('cym').directive('googlemaps', googlemaps);

	function googlemaps() {
		var directive = {
			restrict: 'E',
			templateUrl: 'templates/googlemaps.html'
		};
		return directive;
	}	
})();