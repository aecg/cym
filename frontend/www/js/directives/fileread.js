'use strict';

(function() {
	angular.module('cym').directive('fileread', fileread);

	function fileread() {
	    var directive = {

			scope: {
            	fileread: "="
        	},
	        link: function (scope, element, attributes) {
	            element.bind("change", function (changeEvent) {
	                var reader = new FileReader();
	                reader.onload = function (loadEvent) {
	                    scope.$apply(function () {
	                        scope.fileread = loadEvent.target.fileName;
	                        //scope.fileread = changeEvent.target.files[0];
	                    });
	                }
	                //console.log(changeEvent.target.files[0]);
	                reader.readAsDataURL(changeEvent.target.files[0]);
	            });
	        }
	    };
	    return directive;
	}
	
})();