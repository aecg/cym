/**
 * Sistema:                 CyM-AR
 * Siglas:                  cym
 *
 * Nombre:                  travelservice.js
 * Descripcion:             Servicio de travel
 * @version                 1.0
 * @author                  Leonor Guzman
 * @since                   12/9/2015
 *
 */
'use strict';
(function ()
{
    angular.module('cym').service('TravelService', ['DaoTrip', 'DaoCountry', 'DaoState', 'DaoLocation', 'DaoMustSeeAndDo', 'DaoUsers', '$filter', 'SyncronizationProxy', 'UserProxy', TravelService]);

    function TravelService(daoTrip, daoCountry ,daoState, daoLocation, daoMustSeeAndDo, daoUsers,$filter, SyncronizationProxy, UserProxy)
    {


        var self = this;
        var travels = {};
        var allTravelsUser = {
            travels: {
                destiny: []
            },
            user_id: 0
        };


        /**
         * Nombre:
         * Descripcion:             metodo que limpia la variable travels del servicio
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/15/2015
         *
         */
        self.infoClear = function ()
        {
            travels = {};
        };

        /**
         * Nombre:
         * Descripcion:             metodo que limpia la variable travels del servicio
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/15/2015
         *
         */
        self.destinyClear = function ()
        {
            travels.destiny = [];
        };


        /**
         * Nombre:                  setInfoTravels
         * Descripcion:             metodo que mantiene en la sesion los datos del viaje
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/10/2015
         *
         * @param data
         */
        self.setInfoTravels = function (data) {
            travels = data;
        };

        /**
         * Nombre:
         * Descripcion:             metodo que mantiene en sesion para su obtecion los datos del vijae
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/10/2015
         *
         */
        self.getInfoTravels = function ()
        {
            return travels;
        };

        /**
         * Nombre:                  setInfoTravels
         * Descripcion:             metodo que mantiene en la sesion los datos del viaje
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/10/2015
         *
         * @param data
         */
        self.setAllTravelsUser = function (data)
        {
            allTravelsUser = data;
        };

        /**
         * Nombre:
         * Descripcion:             metodo que mantiene en sesion para su obtecion los datos del vijae
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/10/2015
         *
         */
        self.getAllTravelsUser = function ()
        {
            return allTravelsUser;
        };

        /**
         * Nombre:
         * Descripcion:             metodo que mantiene en sesion para su obtecion los datos del vijae
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/10/2015
         *
         */
        self.clearAllTravelsUser = function ()
        {
            allTravelsUser = {
                travels: {
                    destiny: []
                },
                user_id: 0
            };
        };

        /**
         * Nombre:
         * Descripcion:             metodo que inserta el vijae
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/13/2015
         *
         */
        self.addTravel = function ()
        {
            //console.log('self.addTravel::init');
            var promises = [];

            var defAddTrip = new $.Deferred();
            var defAddTravelMustSeeAndDo = new $.Deferred();

            promises.push(defAddTrip);
            promises.push(defAddTravelMustSeeAndDo);

            daoTrip.insertTrip(travels).then(function (res) {
                //console.log('daoTrip.insertTrip::done');
                defAddTrip.resolve();
                self.addTravelMustSeeAndDo(res.insertId).then(function() {
                    self.infoClear();
                    defAddTravelMustSeeAndDo.resolve();
                });
            });
            return $.when.apply(undefined, promises).promise();
        };


        /**
         * Nombre:
         * Descripcion:             metodo que agrega los musseando desde el id de locacion
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/13/2015
         *
         */
        self.addTravelMustSeeAndDo = function (travel_inserted_id) {
            //console.log('addTravelMustSeeAndDo::init');
            var promises = [];

            var defAddTravelMustSeeAndDo = new $.Deferred();

            promises.push(defAddTravelMustSeeAndDo);

            if (travels.destiny) {
                var destiny = angular.copy(travels.destiny);

                daoMustSeeAndDo.insertDestination(destiny, travel_inserted_id).then(function() {
                    defAddTravelMustSeeAndDo.resolve();
                });
            }
            else {
                defAddTravelMustSeeAndDo.resolve();
            }
            return $.when.apply(undefined, promises).promise();
        };

        /**
         * Nombre:
         * Descripcion:             meodo que selecciona el must see and do de acuerdo al travel
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/14/2015
         *
         */
        function selectAllMustSeeAndDo() {
            var success = function (res) {
                var promises = [];
                var defSelectAllMustSeeAndDo = new $.Deferred();

                promises.push(defSelectAllMustSeeAndDo);

                //console.log('selectAllMustSeeAndDo::success::res.rows.length::'+res.rows.length);
                var musTssee = [];

                for (var i = 0; i < res.rows.length; i++) {
                    musTssee.push(res.rows.item(i));
                }
                //console.log('selectAllMustSeeAndDo::musTssee::'+JSON.stringify(musTssee));
                //console.log('selectAllMustSeeAndDo::success::travels.destiny.length::'+travels.destiny.length);

                for (var j = 0; j < travels.destiny.length; j++) {

                    var travelDestiny = travels.destiny[j];
                    //console.log('selectAllMustSeeAndDo::travelDestiny::'+JSON.stringify(travelDestiny));
                    var mustseeanddoOnDate = _.filter(musTssee, function(msad) { return msad.date === travelDestiny.date; });
                    //console.log('selectAllMustSeeAndDo::mustseeanddoOnDate::'+JSON.stringify(mustseeanddoOnDate));

                    if(travelDestiny.locations) {

                        //console.log('selectAllMustSeeAndDo::success::travelDestiny.locations.length::'+travelDestiny.locations.length);
                        for (var k = 0; k < travelDestiny.locations.length; k++) {
                            var travelLocation = travelDestiny.locations[k];
                            //console.log('selectAllMustSeeAndDo::travelLocation::'+JSON.stringify(travelLocation));
                            var mustseeanddoOnLocation = _.filter(mustseeanddoOnDate, function(msad) { return msad.location_id === travelLocation.id; });
                            //console.log('selectAllMustSeeAndDo::mustseeanddoOnLocation::'+JSON.stringify(mustseeanddoOnLocation));
                            travelLocation.mustSeeAndDo = mustseeanddoOnLocation;
                        }
                    }
                }
                //console.log('selectAllMustSeeAndDo::travels::'+JSON.stringify(travels));

                defSelectAllMustSeeAndDo.resolve();
                return $.when.apply(undefined, promises).promise();
            };
            //console.log('selectAllMustSeeAndDo::travels::'+JSON.stringify(travels));
            return daoMustSeeAndDo.selectMustSeeAndDoTravelId(travels.id).then(success);
        }

        /**
         * Nombre:
         * Descripcion:             metodo que selecciona las localidaddes deacuerdo al id del travel
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/14/2015
         *
         */
        function selectAllLocation() {
            //console.log('selectAllLocation::init');
            var promises = [];
            var defSelectAllLocation = new $.Deferred();
            promises.push(defSelectAllLocation);
            if (travels.destiny) {

                //console.log('selectAllLocation::travels.destiny.length::'+travels.destiny.length);
                for (var q = 0; q < travels.destiny.length; q++) {

                    var defTravelDestiny = new $.Deferred();
                    promises.push(defTravelDestiny);
                    $.queue('destiny', processLocationTravel(travels.id, travels.destiny[q], defTravelDestiny));
                }
                $.dequeue('destiny');
            }
            defSelectAllLocation.resolve();
            return $.when.apply(undefined, promises).promise();
        }

        function processLocationTravel(travelId, destiny, deferred) {
            var processLocation = function(res) {
                //console.log('processLocation::destiny::'+JSON.stringify(destiny));
                //console.log('processLocation::res.rows.length::'+res.rows.length);
                //console.log('processLocation::res.rows::'+JSON.stringify(res.rows));

                if (destiny.locations == null) {
                    destiny.locations = [];
                }
                for (var j = 0; j < res.rows.length; j++) {
                    var location = res.rows.item(j);
                    //console.log('processLocation::location::'+JSON.stringify(location));
                    if ((location.state_id == destiny.id) && (location.date == destiny.date)) {
                        destiny.locations.push(location);
                    }
                    //console.log('processLocation::destiny.locations::'+JSON.stringify(destiny.locations));
                }
                deferred.resolve();
            }
            //console.log('processLocationTravel::init');
            return daoLocation.selectLocationTravelId(travelId, destiny.date).then(processLocation);
        }

        /**
         * Nombre:
         * Descripcion:             metodo que carga los estados a editar
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/14/2015
         *
         */
        function selectAllState() {
            var success = function (res) {
                var promises = [];
                var defSelectAllState = new $.Deferred();
                promises.push(defSelectAllState);

                //console.log('selectAllState::res.rows.length::'+res.rows.length);
                //console.log('selectAllState::res.rows::'+JSON.stringify(res.rows));
                if (travels.destiny == null) {
                    travels.destiny = [];
                }

                for (var i = 0; i < res.rows.length; i++) {
                    var defCurrentState = new $.Deferred();
                    promises.push(defCurrentState);

                    //console.log('selectAllState::res.rows.item('+i+')::'+JSON.stringify(res.rows.item(i)));
                    var filter = $filter('filter')(travels.destiny, {date: res.rows.item(i).date});
                    if (filter.length == 0) {
                        travels.destiny.push(res.rows.item(i));
                    }
                    defCurrentState.resolve();
                }
                defSelectAllState.resolve();
                //console.log('selectAllState::travels.destiny::'+JSON.stringify(travels.destiny));

                return $.when.apply(undefined, promises).promise();
            };
            //console.log('selectAllState::init');
            return daoState.selectStateTravelId(travels.id).then(success);
        }

        /**
         * Nombre:
         * Descripcion:             metodo que selectciona el pais segun el travel
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/14/2015
         *
         */
        function selectAllCountry() {
            var success = function (res) {
                var promises = [];
                var defAllCountry = new $.Deferred();
                promises.push(defAllCountry);

                //console.log('selectAllCountry::success::res.rows.length::'+res.rows.length);
                if (res.rows.length > 0) {
                    if (travels.country == null) {
                        travels.country = {};
                    }
                    travels.country = res.rows.item(0);
                    //console.log('selectAllCountry::success::travels.country::'+JSON.stringify(travels.country));
                }
                defAllCountry.resolve();

                return $.when.apply(undefined, promises).promise();
            };
            //console.log('selectAllCountry::init');
            return daoCountry.selectCountryTravelId(travels.id).then(success);
        }

        /**
         * Nombre:                  getTravels
         * Descripcion:             metodo que obtiene todos los viajes
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/9/2015
         *
         */
        self.getTravels = function ()
        {
            daoTrip.selectAllTrip(allTravelsUser);
        };

        /**
         * Nombre:
         * Descripcion:             metodo que remueve el viaje la lista
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/14/2015
         *
         * @param data
         */
        function removeTripAllTravels(data)
        {
            self.removeToArray(data, allTravelsUser.travels);
        }

        /**
         * Nombre:
         * Descripcion:             metodo que agrega el viaje la lista
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/14/2015
         *
         */
        function addTripAllTravels(id)
        {
            var trip = {};

            trip.id = id;
            trip.name = travels.name;
            trip.budget = travels.budget;
            trip.dateArrival = travels.dateArrival;
            trip.dateGoing = travels.budget;
            trip.description = travels.description;
            trip.country = travels.country;
            self.pushToArray(trip, allTravelsUser.travels);
        }

        /**
         * Nombre:                  deleteTravels
         * Descripcion:             metodo que elimina  ek viaje
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/9/2015
         *
         * @param data
         */
        self.deleteTravels = function (data)
        {
            daoTrip.removeTrip(data);
            removeTripAllTravels(data);
        };

        /**
         * Nombre:                  getTrip
         * Descripcion:             metodo que obtiene los datos del trip
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/9/2015
         *
         * @param data
         */
        self.getTrip = function (data) {
            var success = function (res) {
                var promises = [];
                var defGetTrip= new $.Deferred();
                var defAllCountry = new $.Deferred();
                var defAllState = new $.Deferred();
                var defAllLocation = new $.Deferred();
                var defAllMustSeeAndDo = new $.Deferred();

                promises.push(defGetTrip);
                promises.push(defAllCountry);
                promises.push(defAllState);
                promises.push(defAllLocation);
                promises.push(defAllMustSeeAndDo);

                //console.log('self.getTrip::success::res.rows.length::'+res.rows.length);
                if (res.rows.length > 0) {
                    travels.id = res.rows.item(0).id;
                    travels.name = res.rows.item(0).name;
                    travels.budget = res.rows.item(0).budget;
                    travels.dateArrival = new Date(res.rows.item(0).dateArrival);
                    travels.dateGoing = new Date(res.rows.item(0).dateGoing);
                    travels.description = res.rows.item(0).description;

                    //console.log('self.getTrip::travels::'+JSON.stringify(travels));

                    selectAllCountry().then(function() {
                        defAllCountry.resolve();
                        //console.log('self.getTrip::selectAllCountry::done');

                        selectAllState().then(function() {
                            defAllState.resolve();
                            //console.log('self.getTrip::selectAllState::done');

                            //console.log('self.getTrip::travels.destiny::'+JSON.stringify(travels.destiny));
                            //console.log('self.getTrip::travels::'+JSON.stringify(travels));
                            if (travels.destiny) {

                                selectAllLocation().then(function() {
                                    defAllLocation.resolve();
                                    //console.log('self.getTrip::selectAllLocation::done');
                                    //console.log('self.getTrip::travels::'+JSON.stringify(travels));

                                    selectAllMustSeeAndDo().then(function() {
                                        defAllMustSeeAndDo.resolve();
                                        //console.log('self.getTrip::selectAllMustSeeAndDo::done');
                                    });
                                });
                            }
                            else {
                                defAllLocation.resolve();
                                defAllMustSeeAndDo.resolve();
                            }
                        });
                    });
                }
                else {
                    defAllCountry.resolve();
                    defAllState.resolve();
                    defAllLocation.resolve();
                    defAllMustSeeAndDo.resolve();
                }
                defGetTrip.resolve();
                //console.log('self.getTrip::promises::'+promises.length);
                return $.when.apply(undefined, promises).promise();
            };
            //console.log('self.getTrip::init');
            self.infoClear();
            //console.log('self.getTrip::data::'+JSON.stringify(data));
            return daoTrip.selectTripId(data.id).then(success);
        };


        self.autoUpdateTrip = function () {
            var promises = [];
            var defUpdateTrip = new $.Deferred();
            var updTrip = angular.copy(travels);

            daoTrip.toUpdateTrip(updTrip).then(function() {
                defUpdateTrip.resolve();
            });
            return $.when.apply(undefined, promises).promise();
        }

        self.autoUpdateDestiny = function () {
            var promises = [];
            //console.log('self.updateTrip::init');
            //console.log('self.updateTrip::travels::'+JSON.stringify(travels));

            var defTravelDestiny = new $.Deferred();
            var defAddTravelMustSeeAndDo = new $.Deferred();

            promises.push(defTravelDestiny);
            promises.push(defAddTravelMustSeeAndDo);

            var updTrip = angular.copy(travels);
            //console.log('self.updateTrip::updTrip::'+JSON.stringify(updTrip));
            if (travels.destiny) {
                var removMus = angular.copy(travels);
                daoMustSeeAndDo.removeMustSeeAndDo(removMus.id).then(function() {
                    defTravelDestiny.resolve();
                    //console.log('daoMustSeeAndDo.removeMustSeeAndDo::done');

                    var updTripdes;
                    if (travels.id == null && updTrip != null) {
                        self.setInfoTravels(updTrip);
                        updTripdes = angular.copy(updTrip);
                    }
                    else if (updTrip == null && travels != null) {
                        self.setInfoTravels(travels);
                        updTripdes = angular.copy(travels);
                    }
                    else if(travels != null && travels.destiny != null) {
                        updTripdes = angular.copy(travels);
                    }
                    //console.log('self.updateTrip::updTripdes::'+JSON.stringify(updTripdes));
                    self.addTravelMustSeeAndDo(updTripdes.id).then(function() {
                        defAddTravelMustSeeAndDo.resolve();
                        //console.log('self.addTravelMustSeeAndDo::done');
                    });
                })
            }
            else {
                defTravelDestiny.resolve();
                defAddTravelMustSeeAndDo.resolve();
            }

            return $.when.apply(undefined, promises).promise();
        };

        /**
         * Nombre:                  updateTrip
         * Descripcion:             metodo que actualiza el trip segun el id
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/9/2015
         *
         */
        self.updateTrip = function () {
            var promises = [];
            //console.log('self.updateTrip::init');
            //console.log('self.updateTrip::travels::'+JSON.stringify(travels));

            var defUpdateTrip = new $.Deferred();
            var defTravelDestiny = new $.Deferred();
            var defAddTravelMustSeeAndDo = new $.Deferred();

            promises.push(defUpdateTrip);
            promises.push(defTravelDestiny);
            promises.push(defAddTravelMustSeeAndDo);

            var updTrip = angular.copy(travels);
            //console.log('self.updateTrip::updTrip::'+JSON.stringify(updTrip));
            daoTrip.toUpdateTrip(updTrip).then(function() {
                defUpdateTrip.resolve();
                //console.log('daoTrip.toUpdateTrip::done');

                if (travels.destiny) {
                    var removMus = angular.copy(travels);
                    daoMustSeeAndDo.removeMustSeeAndDo(removMus.id).then(function() {
                        defTravelDestiny.resolve();
                        //console.log('daoMustSeeAndDo.removeMustSeeAndDo::done');

                        var updTripdes;
                        if (travels.id == null && updTrip != null) {
                            self.setInfoTravels(updTrip);
                            updTripdes = angular.copy(updTrip);
                        }
                        else if (updTrip == null && travels != null) {
                            self.setInfoTravels(travels);
                            updTripdes = angular.copy(travels);
                        }
                        else if(travels != null && travels.destiny != null) {
                            updTripdes = angular.copy(travels);
                        }
                        //console.log('self.updateTrip::updTripdes::'+JSON.stringify(updTripdes));
                        self.addTravelMustSeeAndDo(updTripdes.id).then(function() {
                            self.infoClear();
                            defAddTravelMustSeeAndDo.resolve();
                            //console.log('self.addTravelMustSeeAndDo::done');
                        });
                    })
                }
                else {
                    defTravelDestiny.resolve();
                    defAddTravelMustSeeAndDo.resolve();
                }
            });
            return $.when.apply(undefined, promises).promise();
        };

        /**
         * Nombre:                  getCountry
         * Descripcion:             metodo que retorna los paises disponibles
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/10/2015
         *
         */
        self.getAllLocationCountries = function () {
            return daoCountry.selectAllCountry();
        };

        /**
         * Nombre:
         * Descripcion:             metodo para obtener la lista de estados
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/10/2015
         *
         * @returns {*}
         */
        self.selectAllLocationStatesId = function (id)
        {
            return daoState.selecAllStateId(id);

        };

        self.selectStatesByCountryAndId = function (countryid, stateid)
        {
            //console.log('calling daoState.selecStateByCountryAndId::');
            return daoState.selecStateByCountryAndId(countryid, stateid);
        };

        /**
         * Nombre:
         * Descripcion:             metodo que obtiene las locaciones segun id
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/11/2015
         *
         * @param id
         */
        self.getLocationId = function (id)
        {
            return daoLocation.selecLocationId(id);
        };

        /**
         * Nombre:
         * Descripcion:             metodo que busca los must and do asociados al id
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/11/2015
         *
         * @param id
         * @returns {*}
         */
        self.getMustAndDoId = function (id)
        {
            return daoMustSeeAndDo.selectMustAndDoId(id);
        };

        /**
         * Nombre:
         * Descripcion:             metodo que busca los must and do asociados al stado
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/11/2015
         *
         * @param id
         * @returns {*}
         */
        self.getMustFromState = function (id, budget)
        {
            //console.log('this.getMustFromState::id::'+id+'::budget::'+budget);
            return daoMustSeeAndDo.getMustFromState(id, budget);
        };


        /**
         * Descripcion:             metodo que carga el destiny
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   1/8/2016
         *
         */
        self.destinyOrderToState = function ()
        {
            var tempDestiny = [];
            if (travels.destiny)
            {
                if (travels.destiny.state)
                {
                    for (var i = 0; i < travels.destiny.state.length; i++)
                    {
                        self.pushToArray(travels.destiny.state[i], tempDestiny);
                        if (travels.destiny.locations)
                        {
                            tempDestiny[i].locations = [];
                            for (var j = 0; j < travels.destiny.locations.length; j++)
                            {
                                if (travels.destiny.locations[j].state_id == tempDestiny[i].id)
                                {
                                    self.pushToArray(travels.destiny.locations[j], tempDestiny[i].locations);
                                    if (travels.destiny.mustSeeAndDo)
                                    {
                                        var positionLocationTemp = tempDestiny[i].locations.indexOf(travels.destiny.locations[j]);
                                        tempDestiny[i].locations[positionLocationTemp].mustSeeAndDo = [];
                                        for (var k = 0; k < travels.destiny.mustSeeAndDo.length; k++)
                                        {
                                            if (travels.destiny.mustSeeAndDo[k].location_id == tempDestiny[i].locations[positionLocationTemp].id)
                                            {
                                                self.pushToArray(travels.destiny.mustSeeAndDo[k], tempDestiny[i].locations[positionLocationTemp].mustSeeAndDo);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return tempDestiny;
        };


        /**
         * Descripcion:             metodo que  agregar a un array
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   1/6/2016
         *
         * @param element
         * @param array
         */
        self.pushToArray = function (element, array)
        {
            if (array != null && element != null)
            {
                if (self.elemenExist(element, array))
                {
                    self.removeToArray(element, array);
                    array.push(element);
                }
                else
                {
                    array.push(element);
                }
            }
        };

        /**
         * Descripcion:             metodo que elimina de un determinado array
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   1/6/2016
         *
         * @param element
         * @param array
         */
        self.removeToArray = function (element, array)
        {
            if (array != null && element != null)
            {
                for (var i = 0; i < array.length; i++)
                {
                    if (array[i].id == element.id)
                    {
                        array.push(i, 1);
                    }
                }
            }
        };


        /**
         * Descripcion:             metodo que compara un iten con un array segun parametro
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   1/6/2016
         *
         * @param element
         * @param array
         */
        self.elemenExist = function (element, array)
        {
            if (array != null && element != null)
            {
                for (var x = 0; x < array.length; x++)
                {
                    if (array[x].id == element.id)
                    {
                        return true;
                    }
                }
            }
            return false;
        };

        self.getLocationById = function (id)
        {
            return daoLocation.getLocationById(id);
        };
    }
})
();
