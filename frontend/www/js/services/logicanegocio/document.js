'use strict';

(function ()
{
    angular.module('cym').service('DocumentService', ['$cordovaSQLite','$filter', DocumentService]);

    function DocumentService($cordovaSQLite, $filter)
    {
        var self = this;

        var allDocuments = {
            documents: [],
            total: 0
        };

        var icons = [];

        this.allDocuments = function ()
        {
            return allDocuments;
        };

        this.clearAllDocuments = function ()
        {
            allDocuments = {
                documents: [],
                total: 0
            };
        };

        self.initIcons = function() {
            icons = [];
            icons.push({id: 1, ext: ".pdf", src: "img/pdf.jpg", type: "application/pdf"});
            icons.push({id: 2, ext: ".doc", src: "img/word.png", type: "application/msword"});
            icons.push({id: 3, ext: ".docx", src: "img/word.png", type: "application/msword"});
            icons.push({id: 4, ext: ".jpeg", src: "img/jpeg-icon.png", type: "image/jpeg"});
            icons.push({id: 5, ext: ".jpg", src: "img/jpg-icon.png", type: "image/jpeg"});
            icons.push({id: 6, ext: ".png", src: "img/png-icon.png", type: "image/png"});
        }

        self.getAllIcons = function ()
        {
            return icons;
        };

        self.getIcon = function(docname) {
            var name = docname.toLowerCase();
            //console.log('self.getIcon::name::'+name);
            return $.grep(icons, function(t) {
                //console.log('self.getIcon::t.ext::'+t.ext);
                return name.indexOf( t.ext ) > 0;
            });
        };

        self.getType = function(docname) {
            var name = docname.toLowerCase();
            //console.log('self.getType::name::'+name);
            return $.grep(icons, function(t) {
                //console.log('self.getType::t.ext::'+t.ext);
                return name.indexOf( t.ext ) > 0;
            });
        };

        this.getDocuments = function (user_id, messages)
        {

            var success = function (res) {

                var promises = [];
                var deferred = new $.Deferred();
                promises.push(deferred);

                //console.log('getDocuments::res.rows.length::'+res.rows.length);
                if (res.rows.length > 0) {
                    for (var i = 0; i < res.rows.length; i++) {
                        var deferred_processAttachment = new $.Deferred();
                        promises.push(deferred_processAttachment);

                        var doc = res.rows.item(i);
                        //console.log('getDocuments::doc::'+JSON.stringify(doc));

                        $.queue('attachments', self.processAttachments(doc, deferred_processAttachment));

                        allDocuments.documents.push(doc);
                    }
                    $.dequeue('attachments');
                    allDocuments.total = res.rows.length;
                }

                self.initIcons();

                //console.log('getDocuments::allDocuments::'+JSON.stringify(allDocuments));
                deferred.resolve(allDocuments);

                return $.when.apply(undefined, promises).promise();
            };

            var error = function (res)
            {
                console.log(JSON.stringify(res));
                messages.error.push($filter('translate')('messageserror-findalldocument'));
                //messages.error.push('Error al buscar todos los documentos');
            };

            self.clearAllDocuments();
            var select_all_docs = 'SELECT id, name, user_id, date FROM document WHERE user_id = ' + user_id;
            return $cordovaSQLite.execute(db, select_all_docs, []).then(success, error);
        };

        this.getDocumentsWithGPS = function (user_id) {

            var success = function (res) {
                var promises = [];
                var deferred = new $.Deferred();
                promises.push(deferred);

                //console.log('getDocumentsWithGPS::res.rows.length::'+res.rows.length);
                if (res.rows.length > 0) {
                    for (var i = 0; i < res.rows.length; i++) {
                        var doc = res.rows.item(i);
                        //console.log('getDocumentsWithGPS::doc::'+JSON.stringify(doc));
                        allDocuments.documents.push(doc);
                    }
                    allDocuments.total = res.rows.length;
                }
                //console.log('getDocumentsWithGPS::allDocuments::'+JSON.stringify(allDocuments));
                deferred.resolve(allDocuments);

                return $.when.apply(undefined, promises).promise();
            };

            self.clearAllDocuments();
            var select_docs_gps = "SELECT id, name, geographic_position FROM document WHERE user_id = ? AND (geographic_position IS NOT NULL AND geographic_position != '')";
            return $cordovaSQLite.execute(db, select_docs_gps, [user_id]).then(success);
        };

        self.processAttachments = function(document, deferred) {

            var processAttachment = function(resAttachment) {
                var promises = [];
                //console.log('processAttachment::init');

                var doc_files = [];
                var doc_icons = [];

                //console.log('processAttachment::resAttachment.rows.length::'+resAttachment.rows.length);
                if (resAttachment.rows.length > 0) {

                    var defProcessAttachment = new $.Deferred();
                    promises.push(defProcessAttachment);

                    for (var j = 0; j < resAttachment.rows.length; j++) {
                        var defCurrentAttachment = new $.Deferred();
                        promises.push(defCurrentAttachment);

                        var obj = resAttachment.rows.item(j);
                        if (obj.url_file != "") {
                            var mimeObj = self.getIcon(obj.url_file);
                            if (mimeObj && mimeObj[0]) {
                                obj.extension = mimeObj[0].src;
                                doc_icons.push(obj.extension);
                            }
                            //console.log('processAttachment::document.id::'+document.id+'obj::'+JSON.stringify(obj));
                            doc_files.push(obj);
                            doc_icons = jQuery.unique( doc_icons );
                        }
                        defCurrentAttachment.resolve();
                    }

                    defProcessAttachment.resolve();
                }
                document.files = doc_files;
                document.icons = doc_icons;
                //console.log('processAttachment::document.id::document::'+JSON.stringify(document));

                deferred.resolve();
                return $.when.apply(undefined, promises).promise();
            }

            self.getDocumentAttachment(document.id).then(processAttachment);
        }


        this.addDocuments = function (document, messages)
        {
            var success = function (res)
            {
                document.id = res.insertId;
                var insert_doc_attach = "INSERT INTO doc_attachment (document_id, url_file, type) VALUES (?, ?, ?)";

                for(var i = 0; i < document.images.length; i++)
                {
                    $cordovaSQLite.execute(db, insert_doc_attach, [document.id, document.images[i].name, document.images[i].type]);
                }

                for(var i = 0; i < document.files.length; i++)
                {
                    $cordovaSQLite.execute(db, insert_doc_attach, [document.id, document.files[i].name, document.files[i].type]);
                }
            };

            var error = function (res)
            {
                messages.error.push($filter('translate')('messageserror-savedocument'));
                //messages.error.push('Error al guardar el documento');
            };

            var doc_date = new Date();
            var doc_date_time = doc_date.getTime();
            var geoposition = 0;
            document.date = doc_date_time;
            var insert_doc = "INSERT INTO document (name, description, geographic_position, date, user_id) VALUES (?, ?, ?, ?, ?)";
            $cordovaSQLite.execute(db, insert_doc, [document.name, document.description, document.geographic_position, doc_date_time, document.user_id]).then(success, error);

        };

        this.getDocument = function (id)
        {
            var select_doc = 'SELECT * FROM document WHERE id ='+id;
            return $cordovaSQLite.execute(db, select_doc, []);
        };

        this.getDocumentAttachment = function (id)
        {
            var select_doc_attach = 'SELECT * FROM doc_attachment WHERE document_id = '+id;
            return $cordovaSQLite.execute(db, select_doc_attach, []);
        };

        this.editDocument = function (document, messages)
        {
            var self = this;
            var success = function (res)
            {
                allDocuments.documents = [];
                allDocuments.total = 0;
                self.getDocuments(document.user_id, messages).then(function() {
                    deferred.resolve(document);
                });
            };

            var error = function (res)
            {
                messages.error.push($filter('translate')('messageserror-updatedocument'));
                //messages.error.push('Error al actualizar el documento');
            };

            var promises = [];
            var deferred = new $.Deferred();
            promises.push(deferred);

            var doc_date = new Date();
            var doc_date_time = doc_date.getTime();

            var update_doc = 'UPDATE document SET name = ?, ' +
                                'description = ?, ' +
                                'geographic_position = ?, ' +
                                'date = ? WHERE id = ?';
            var document_data = [document.name,
                                    document.description,
                                    document.geographic_position,
                                    doc_date_time,
                                    document.id];
            $cordovaSQLite.execute(db, update_doc, document_data).then(success, error);

            return $.when.apply(undefined, promises).promise();
        };

        this.deleteDocument = function (document, messages)
        {
            var self = this;
            var success = function (res)
            {
                allDocuments.documents = [];
                allDocuments.total = 0;
                self.getDocuments(document.user_id, messages);
            };

            var error = function (res)
            {
                messages.error.push($filter('translate')('messageserror-deletedocument'));
                //messages.error.push('Error al eliminar el documento');
            };

            var delete_doc = "DELETE FROM document WHERE id = ?";
            return $cordovaSQLite.execute(db, delete_doc, [document.id]).then(success, error);
        };


        this.deleteAttachment = function (file, messages)
        {
            //console.log("DAO Entrando a eliminar el documento " + file.id);
            var self = this;
            var success = function (res)
            {
                //console.log("DAO se ha eliminado el documento " + file.id + " exitosamente");
            };

            var error = function (res)
            {
                //console.log("DAO NO se ha eliminado el documento " + JSON.stringify(res));
                messages.error.push($filter('translate')('messageserror-deletedocument'));
            };

            var delete_doc = "DELETE FROM doc_attachment  WHERE id = ?";
            $cordovaSQLite.execute(db, delete_doc, [file.id]).then(success, error);

        };


        this.addAttachment = function (id, file, messages)
        {
            console.log("DAO Agregando attachment " + id + " " + JSON.stringify(file));
            var insert_doc_attach = "INSERT INTO doc_attachment (document_id, url_file, type) VALUES (?, ?, ?)";
            $cordovaSQLite.execute(db, insert_doc_attach, [id, file.url_file, file.type]);


        };

    }

})();