'use strict';

(function ()
{
    angular.module('cym').service('MustSeeAndDoService', ['$cordovaSQLite', 'DaoMustSeeAndDo', 'UserService', 'SyncronizationProxy','$filter', MustSeeAndDoService]);

    function MustSeeAndDoService($cordovaSQLite, daoMustSeeAndDo, userServices, SyncronizationProxy, $filter)
    {
        var self = this;
        var Latitude = null;
        var Longitude = null;

        //Method consulting books
        this.getLocalBooks = function (data, messages) {
            var success = function (res) {

                var promises = [];
                var defSuccess = new $.Deferred();
                promises.push(defSuccess);

                //console.log('getLocalBooks::success::res.rows.length::'+res.rows.length);
                for (var i = 0; i < res.rows.length; i++) {
                    var currentBook = res.rows.item(i);
                    data.book.push(currentBook);
                }
                defSuccess.resolve();
                return $.when.apply(undefined, promises).promise().then(function() {
                    data.total = res.rows.length;
                });
            };
            var error = function (res) {
                messages.error.push($filter('translate')('messageserror-downloadsbooks'));
            };

            data.book = [];
            data.total = 0;
            var select_book = "SELECT DISTINCT A.id, A.name, A.image_reference, E.id AS is_360 " +
                                "FROM book A " +
                                "JOIN book_mustseeanddo B ON A.id = B.book_id " +
                                "JOIN mustseeanddo C ON B.mustseeanddo_id = C.id " +
                                "JOIN location D ON D.id = C.location_id " +
                                "LEFT JOIN (SELECT DISTINCT b_.id, b_.name " +
                                            "FROM mustseeanddo m " +
                                            "JOIN mustseeanddo_ar mar ON mar.mustseeanddo_id = m.id " +
                                            "JOIN book_mustseeanddo bm ON bm.mustseeanddo_id = m.id " +
                                            "JOIN book b_ ON bm.book_id = b_.id " +
                                            "WHERE m.location_id = ? AND mar.is_360 = 1) E ON E.id = A.id " +
                                "WHERE C.location_id = ? ORDER BY A.name";
            //console.log('getLocalBooks::select_book::'+select_book);
            return $cordovaSQLite.execute(db, select_book, [data.locationid, data.locationid]).then(success, error);
        };

        //Method to validate duplicates registers
        function addBook (list, book, userLoggedId, messages, deferred) {
            var verifyUserBook = function (resultUserBook) {
                //console.log('verifyUserBook::resultUserBook::'+JSON.stringify(resultUserBook));
                //console.log('verifyUserBook::resultUserBook.rows.length::'+resultUserBook.rows.length);
                //book.pertenece = false;
                //if (resultUserBook.rows.length > 0) {
                    book.pertenece = true;
                //}
                //console.log('verifyUserBook::book.pertenece::'+book.pertenece);
                //console.log('verifyUserBook::book::'+JSON.stringify(book));
                list.push(book);
                deferred.resolve();
            };
            var errorB = function (resB) {
                messages.error.push($filter('translate')('messageserror-finddownloadsbooks'));
            };
            // se verifica si el usuario está logueado o no
            if (userLoggedId === undefined) {
                book.pertenece = true;
                list.push(book);
                deferred.resolve();
            }
            else {
                // se verifica si se ha descargado o no
                var select_user_book = 'SELECT a.* FROM book a join user_book b on a.id = b.book_id where b.user_id = ? and a.id = ?';
                //console.log('addBook::select_user_book::'+select_user_book);
                $cordovaSQLite.execute(db, select_user_book, [userLoggedId, book.id]).then(verifyUserBook, errorB);
            }
        }

        //Method consulting countries
        this.getCountries = function (data, messages)
        {
            data.country = [];
            data.total = 0;
            var success = function (res)
            {
                if (res.rows.length > 0)
                {
                    for (var i = 0; i < res.rows.length; i++)
                    {
                        data.country.push(res.rows.item(i));
                    }
                    data.total = res.rows.length;
                }
            };
            var error = function (res)
            {
                messages.error.push($filter('translate')('messageserror-findcountrys'));
                //messages.error.push('Error al buscar los paises');
            };
            var select_country = "SELECT id, name FROM country";
            $cordovaSQLite.execute(db, select_country, []).then(success, error)
        }

        //Method consulting states
        this.getStates = function (id, data, messages)
        {
            data.states = [];
            data.total = 0;
            var success = function (res)
            {
                if (res.rows.length > 0)
                {
                    for (var i = 0; i < res.rows.length; i++)
                    {
                        data.states.push(res.rows.item(i));
                    }
                    data.total = res.rows.length;
                }
            };
            var error = function (res)
            {
                messages.error.push($filter('translate')('messageserror-findstates'));
                //messages.error.push('Error al buscar los estados');
            };
            var select_state = "SELECT * FROM state where country_id = ?";
            $cordovaSQLite.execute(db, select_state, [id]).then(success, error)
        }

        //Method consulting locations
        this.getLocations = function (id, data, messages)
        {
            data.locations = [];
            data.total = 0;
            var success = function (res)
            {
                if (res.rows.length > 0)
                {
                    for (var i = 0; i < res.rows.length; i++)
                    {
                        data.locations.push(res.rows.item(i));
                    }
                    data.total = res.rows.length;
                }
            };
            var error = function (res)
            {
                messages.error.push($filter('translate')('messageserror-findalllocation'));
                //messages.error.push('Error al buscar todos las localidades');
            };
            var select_location = "SELECT id, name FROM location where state_id = ?";
            $cordovaSQLite.execute(db, select_location, [id]).then(success, error);
        }

        this.getMustSeeAndDos = function (data, messages) {
            data.location = [];
            data.total = 0;

            var success = function (res) {
                var promises = [];
                var defSuccess = new $.Deferred();
                promises.push(defSuccess);

                //console.log('getMustSeeAndDos::success::res.rows.length::'+res.rows.length);
                for (var i = 0; i < res.rows.length; i++) {
                    var currentMustSeeAndDo = res.rows.item(i);
                    currentMustSeeAndDo.distance = null;
                    if (self.Latitude && self.Longitude)
                    {
                        var lat = null;
                        var lng = null;
                        try {
                            var pos = currentMustSeeAndDo.geographic_position.split(",");
                            lat = pos[0].trim();
                            lng = pos[1].trim();
                        }
                        catch(e) {
                            lat = 0;
                            lng = 0;
                        }
                        currentMustSeeAndDo.distance = self.getDistanceFromLatLonInKm(self.Latitude, self.Longitude, lat, lng);
                    }

                    data.location.push(currentMustSeeAndDo);
                }
                //console.log('getMustSeeAndDos::success::data.location::'+JSON.stringify(data.location));
                defSuccess.resolve();
                return $.when.apply(undefined, promises).promise().then(function() {
                    data.total = res.rows.length;
                });
            };

            var select_mustseeanddo = "SELECT A.id, A.title, A.image_reference, A.qr, A.ar, A.price, D.is_360, A.geographic_position " +
            //var select_mustseeanddo = "SELECT A.id, A.title, A.qr, A.ar, A.price, D.is_360, A.geographic_position " +
                                "FROM mustseeanddo A " +
                                "LEFT JOIN mustseeanddo_ar D ON A.id = D.mustseeanddo_id " +
                                "JOIN book_mustseeanddo B ON A.id = B.mustseeanddo_id " +
                                "JOIN book C ON B.book_id = C.id " +
                                "JOIN location E ON E.id = A.location_id " +
                                "WHERE c.id = ? AND e.id = ? " +
                                "ORDER BY A.title";
            //console.log('getMustSeeAndDos::data::'+JSON.stringify(data));
            //console.log('getMustSeeAndDos::select_mustseeanddo::'+select_mustseeanddo);
            return $cordovaSQLite.execute(db, select_mustseeanddo, [data.bookid, data.locationid]).then(success);
        }

        //Method consulting must see and do detail
        this.getLocationDetail = function (id) {
            var select_mustseeanddodetail = 'SELECT * FROM mustseeanddo WHERE id = ' + id;
            //console.log('select_mustseeanddodetail::'+select_mustseeanddodetail);
            return $cordovaSQLite.execute(db, select_mustseeanddodetail, []);
        };

        /**
         * Descripcion:             metodo que obtiene la informacion del mustseeando
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   2/2/2016
         *
         */
        this.getInfor = function (data)
        {
            return daoMustSeeAndDo.getInformation(data);
        };

        //Method to get the book must see and do.
        this.getBookMustSeeAndDo = function (bookid, messages) {

            var processMustSeeAndDoJson = function(res) {
                var promises = [];
                //console.log('processMustSeeAndDoJson::res::'+JSON.stringify(res));

                var defMustSeeAndDo = new $.Deferred();
                promises.push(defMustSeeAndDo);

                $.each(res.data.mustseeanddo, function(i, mustseeanddo) {
                    //console.log('processMustSeeAndDoJson::mustseeanddo::'+JSON.stringify(mustseeanddo));
                    var defMustseeanddo = new $.Deferred();
                    promises.push(defMustseeanddo);
                    $.queue('mustSeeAndDos', self.processMustSeeAndDo(bookid, mustseeanddo, defMustseeanddo));
                });
                $.dequeue('mustSeeAndDos');

                defMustSeeAndDo.resolve();

                return $.when.apply(undefined, promises).promise();
            }
            var error = function (res) {
                messages.error.push($filter('translate')('messageserror-syncbooklocationserver'));
            };

            //console.log('getBookMustSeeAndDo::init');
            return SyncronizationProxy.getBookMustSeeAndDo(bookid).then(processMustSeeAndDoJson, error);
        };

        self.processMustSeeAndDo = function(bookid, mustseeanddo, deferred) {
            //console.log('processMustSeeAndDo::'+JSON.stringify(mustseeanddo));
            var promises = [];
            var defBookMustSeeAndDoAR = new $.Deferred();
            var defBookMustSeeAndDoQR = new $.Deferred();
            var defBookBookMustSeeAndDo = new $.Deferred();

            promises.push(defBookBookMustSeeAndDo);
            promises.push(defBookMustSeeAndDoAR);
            promises.push(defBookMustSeeAndDoQR);

            self.existsMustSeeAndDoById(mustseeanddo.id).then(function(result_exists) {
                //console.log('processMustSeeAndDo::result_exists.rows.length::'+result_exists.rows.length);
                if (result_exists.rows.length == 0) {
                    var query_insert_msd_book = "INSERT INTO mustseeanddo(id, title, summary, image_reference, geographic_position, location_id, qr, ar, price ) values (?, ?, ?, ?, ?, ?, ?, ?,?)";
                    //console.log('processMustSeeAndDo::query_insert_msd_book::'+query_insert_msd_book);
                    var mustseeanddo_obj = [mustseeanddo.id, 
                                            mustseeanddo.title, 
                                            mustseeanddo.summary,
                                            (mustseeanddo.image_reference === undefined?'':mustseeanddo.image_reference),
                                            mustseeanddo.geographic_position, 
                                            mustseeanddo.location_id, 
                                            mustseeanddo.qr, 
                                            mustseeanddo.ar, 
                                            mustseeanddo.price];
                    //console.log('processMustSeeAndDo::mustseeanddo_obj::'+JSON.stringify(mustseeanddo_obj));
                    $cordovaSQLite.execute(db, query_insert_msd_book, mustseeanddo_obj).then(function() {
                        //console.log('processMustSeeAndDo::insert::resolve');
                        self.getBookBookMustSeeAndDo(bookid, mustseeanddo, defBookBookMustSeeAndDo).then(function() {
                            //console.log('processMustSeeAndDo::getBookBookMustSeeAndDo::resolve');
                            self.getBookMustSeeAndDoQR(mustseeanddo, defBookMustSeeAndDoQR).then(function() {
                                self.getBookMustSeeAndDoAR(mustseeanddo, defBookMustSeeAndDoAR);
                            });
                        });
                    });
                }
                else {
                    var query_update_msd_book = 'UPDATE mustseeanddo SET title = ?, summary = ?, ' +
                                                'image_reference = ?, geographic_position =  ?, ' +
                                                'location_id = ?, qr = ?, ar = ?, price = ? ' + 
                                                ' WHERE id = ?';
                    var mustseeanddo_obj = [mustseeanddo.title,
                                            mustseeanddo.summary,
                                            mustseeanddo.image_reference,
                                            mustseeanddo.geographic_position,
                                            mustseeanddo.location_id,
                                            mustseeanddo.qr,
                                            mustseeanddo.ar,
                                            mustseeanddo.price,
                                            mustseeanddo.id];
                    //console.log('processMustSeeAndDo::query_update_msd_book::'+query_update_msd_book);
                    $cordovaSQLite.execute(db, query_update_msd_book, mustseeanddo_obj).then(function() {
                        //console.log('processMustSeeAndDo::update::resolve');
                        self.getBookBookMustSeeAndDo(bookid, mustseeanddo, defBookBookMustSeeAndDo).then(function() {
                            //console.log('processMustSeeAndDo::getBookBookMustSeeAndDo::resolve');
                            self.getBookMustSeeAndDoQR(mustseeanddo, defBookMustSeeAndDoQR).then(function() {
                                self.getBookMustSeeAndDoAR(mustseeanddo, defBookMustSeeAndDoAR);
                            });
                        });
                    });
                }
            });
            deferred.resolve();
            return $.when.apply(undefined, promises).promise();
        }

        self.existsMustSeeAndDoById = function(id) {
            var query = 'SELECT * FROM mustseeanddo where id = ' + id;
            //console.log('existsMustSeeAndDoById::query::'+query);
            return $cordovaSQLite.execute(db, query, []);
        }

        self.getBookMustSeeAndDoQR = function(mustseeanddo, deferred) {
            var promises = [];
            var defMustSeeAndDoQR = new $.Deferred();
            promises.push(defMustSeeAndDoQR);

            //console.log('getBookMustSeeAndDoQR::mustseeanddo.id::' + mustseeanddo.id + '::mustseeanddo.qr::'+mustseeanddo.qr);
            var query_delete = 'DELETE FROM mustseeanddo_qr WHERE mustseeanddo_id = ' + mustseeanddo.id;
            $cordovaSQLite.execute(db, query_delete, []).then(function(res1) {
                deferred.resolve();
                if (mustseeanddo.qr == 1) {
                    var processBookMustSeeAndDoQRJson = function (res) {
                        //console.log(JSON.stringify('getBookMustSeeAndDoQR::mustseeanddo.id::' + mustseeanddo.id + '::res.data::' + res.data));
                        if (res.data.id > 0) {
                            var query_insert_msd_qr = 'INSERT INTO mustseeanddo_qr(id, mustseeanddo_id, title, summary) values (?, ?, ?, ?)';
                            var msd_qr_data = [res.data.id, 
                                                res.data.mustseeanddo_id,
                                                res.data.title, 
                                                res.data.summary];
                            $cordovaSQLite.execute(db, query_insert_msd_qr, msd_qr_data).then(function(res_mustseeando_qr) {
                                defMustSeeAndDoQR.resolve();
                            });
                        }
                        else {
                            defMustSeeAndDoQR.resolve();
                        }
                    };

                    // obtener la data del código QR
                    SyncronizationProxy.getBookMustSeeAndDoQR(mustseeanddo.id).then(processBookMustSeeAndDoQRJson);
                }
                else {
                    defMustSeeAndDoQR.resolve();
                }
            });
            return $.when.apply(undefined, promises).promise();
        }

        self.getBookMustSeeAndDoAR = function(mustseeanddo, deferred) {
            var promises = [];
            var defMustSeeAndDoAR = new $.Deferred();
            promises.push(defMustSeeAndDoAR);
            //console.log('getBookMustSeeAndDoAR::mustseeanddo.id::' + mustseeanddo.id + '::mustseeanddo.ar::'+mustseeanddo.ar);
            var query_delete = 'DELETE FROM mustseeanddo_ar WHERE mustseeanddo_id = ' + mustseeanddo.id;
            $cordovaSQLite.execute(db, query_delete, []).then(function(res1) {
                deferred.resolve();
                if (mustseeanddo.ar == 1) {
                    var processBookMustSeeAndDoARJson = function (res) {
                        if (res.data.id > 0) {
                            var query_insert_msd_ar = 'INSERT INTO mustseeanddo_ar(id, mustseeanddo_id, image_preview, image_display, image_binary, is_360, image_360) values (?, ?, ?, ?, ?, ?, ?)';
                            var msd_ar_data = [res.data.id, 
                                                res.data.mustseeanddo_id,
                                                res.data.image_preview, 
                                                res.data.image_display, 
                                                res.data.image_binary, 
                                                res.data.is_360, 
                                                res.data.image_360];
                            $cordovaSQLite.execute(db, query_insert_msd_ar, msd_ar_data).then(function(res_mustseeando_ar) {
                                defMustSeeAndDoAR.resolve();
                            });
                        }
                        else {
                            defMustSeeAndDoAR.resolve();
                        }
                    };

                    // obtener la data del código QR
                    SyncronizationProxy.getBookMustSeeAndDoAR(mustseeanddo.id).then(processBookMustSeeAndDoARJson);
                }
            });
            return $.when.apply(undefined, promises).promise();
        }

        self.getBookBookMustSeeAndDo = function(book, mustseeanddo, deferred) {
            //console.log('getBookBookMustSeeAndDo::init');
            var promises = [];
            var defBookBookMustSeeAndDo = new $.Deferred();
            promises.push(defBookBookMustSeeAndDo);

            self.existsBookMustSeeAndDoById(book, mustseeanddo.id).then(function(result_exists) {
                if (result_exists.rows.length == 0) {
                    var query_insert_book_mustseeanddo = 'INSERT INTO book_mustseeanddo(mustseeanddo_id, book_id) values (?, ?)';
                    var book_mustseeanddo_obj = [mustseeanddo.id, book];
                    //console.log('getBookBookMustSeeAndDo::book_mustseeanddo_obj::'+JSON.stringify(book_mustseeanddo_obj));
                    $cordovaSQLite.execute(db, query_insert_book_mustseeanddo, book_mustseeanddo_obj).then(function() {
                        //console.log('getBookBookMustSeeAndDo::insert::resolve');
                        defBookBookMustSeeAndDo.resolve();
                    });
                }
                else {
                    defBookBookMustSeeAndDo.resolve();
                }
                deferred.resolve();
            });

            return $.when.apply(undefined, promises).promise();
        }

        self.existsBookMustSeeAndDoById = function(bookid, mustseeanddoid) {
            var query = 'SELECT * FROM book_mustseeanddo where book_id = ? and mustseeanddo_id = ?';
            //console.log('existsMustSeeAndDoById::query::'+query);
            return $cordovaSQLite.execute(db, query, [bookid, mustseeanddoid]);
        }

        //Method to get the book detail must see and do.
        this.getBookMustSeeAndDoDetail = function (bookid, messages) {
            var processBookMustSeeAndDoDetailJson = function(res) {
                var promises = [];
                //console.log('processBookMustSeeAndDoDetailJson::res::'+JSON.stringify(res));

                var defMustSeeAndDoDetail = new $.Deferred();
                promises.push(defMustSeeAndDoDetail);

                $.each(res.data.mustseeanddo_detail, function(i, mustseeanddo_detail) {
                    //console.log('processBookMustSeeAndDoDetailJson::mustseeanddo_detail::'+JSON.stringify(mustseeanddo_detail));
                    var defMustseeanddodetail_obj = new $.Deferred();
                    promises.push(defMustseeanddodetail_obj);
                    $.queue('mustSeeAndDos', self.processMustSeeAndDoDetail(mustseeanddo_detail, defMustseeanddodetail_obj));
                });
                $.dequeue('mustSeeAndDos');

                defMustSeeAndDoDetail.resolve();

                return $.when.apply(undefined, promises).promise();
            };

            var error = function (resmsd) {
                messages.error.push($filter('translate')('messageserror-syncbooklocationserver'));
            };
            //console.log('getBookMustSeeAndDoDetail::init');
            return SyncronizationProxy.getBookMustSeeAndDoDetail(bookid).then(processBookMustSeeAndDoDetailJson, error);
        };

        self.processMustSeeAndDoDetail = function(msad_detail, deferred) {
            //console.log('processMustSeeAndDoDetail::'+JSON.stringify(msad_detail));
            self.existsMustSeeAndDoDetailById(msad_detail.id).then(function(result_exists) {
                //console.log('processMustSeeAndDoDetail::result_exists.rows.length::'+result_exists.rows.length);
                if (result_exists.rows.length == 0) {
                    var query_insert_msddetail = "INSERT INTO mustseeanddo_detail (id, mustseeanddo_id, title, summary) VALUES (?,?,?,?)";
                    var mustseeanddo_detail_obj = [msad_detail.id,
                                            msad_detail.mustseeanddo_id, 
                                            msad_detail.title, 
                                            msad_detail.summary];
                    //console.log('processMustSeeAndDoDetail::query_insert_msddetail::'+query_insert_msddetail);
                    $cordovaSQLite.execute(db, query_insert_msddetail, mustseeanddo_detail_obj).then(function() {
                        //console.log('processMustSeeAndDoDetail::insert::resolve');
                        deferred.resolve();
                    });
                }
                else {
                    var query_update_msddetail = 'UPDATE mustseeanddo_detail SET mustseeanddo_id = ?, title = ?,' +
                                                ' summary =  ? WHERE id = ?';
                    var mustseeanddo_detail_obj = [msad_detail.mustseeanddo_id,
                                            msad_detail.title, 
                                            msad_detail.summary, 
                                            msad_detail.id];
                    //console.log('processMustSeeAndDoDetail::query_update_msddetail::'+query_update_msddetail);
                    $cordovaSQLite.execute(db, query_update_msddetail, mustseeanddo_detail_obj).then(function() {
                        //console.log('processMustSeeAndDoDetail::update::resolve');
                        deferred.resolve();
                    });
                }
            });
        }

        self.existsMustSeeAndDoDetailById = function(id) {
            var query = 'SELECT * FROM mustseeanddo_detail where id = ' + id;
            //console.log('existsMustSeeAndDoDetailById::query::'+query);
            return $cordovaSQLite.execute(db, query, []);
        }

        //Method to get the book state.
        this.getBookStates = function (bookid, messages, callback) {
            var processBookStateJson = function(res) {
                var promises = [];
                //console.log('processBookStateJson::res::'+JSON.stringify(res));

                var defBookState = new $.Deferred();
                promises.push(defBookState);

                $.each(res.data.states, function(i, state) {
                    //console.log('processBookStateJson::state::'+JSON.stringify(state));
                    var defState = new $.Deferred();
                    promises.push(defState);
                    $.queue('states', self.processBookState(state, defState));
                });
                $.dequeue('states');

                defBookState.resolve();

                return $.when.apply(undefined, promises).promise();
            }

            var error = function (resmsd) {
                messages.error.push($filter('translate')('messageserror-syncbooklocationserver'));
            };

            //console.log('getBookStates::init');
            return SyncronizationProxy.getBookStates(bookid).then(processBookStateJson, error);
        };

        self.processBookState = function(state, deferred) {
            //console.log('processBookState::'+JSON.stringify(state));
            self.existsStateById(state.id).then(function(result_exists) {
                //console.log('processBookState::result_exists.rows.length::'+result_exists.rows.length);
                if (result_exists.rows.length == 0) {
                    var query_insert_state = "INSERT INTO state (id, name, country_id) VALUES (?, ?, ?);";
                    var state_obj = [state.id,
                                            state.name, 
                                            state.country_id];
                    //console.log('processBookState::query_insert_state::'+query_insert_state);
                    $cordovaSQLite.execute(db, query_insert_state, state_obj).then(function() {
                        //console.log('processBookState::insert::resolve');
                        deferred.resolve();
                    });
                }
                else {
                    //console.log('processBookState::nothing::resolve');
                    deferred.resolve();
                }
            });
        }

        self.existsStateById = function(id) {
            var query = 'SELECT * FROM state where id = ' + id;
            //console.log('existsStateById::query::'+query);
            return $cordovaSQLite.execute(db, query, []);
        }

        //Method to get the book country.
        this.getBookCountries = function (bookid, messages, callback) {
            var processCountriesJson = function(res) {
                var promises = [];
                //console.log('processCountriesJson::res::'+JSON.stringify(res));

                var defBookCountries = new $.Deferred();
                promises.push(defBookCountries);

                $.each(res.data.countries, function(i, country) {
                    //console.log('processCountriesJson::country::'+JSON.stringify(country));
                    var defCountry = new $.Deferred();
                    promises.push(defCountry);
                    $.queue('countries', self.processBookCountry(country, defCountry));
                });
                $.dequeue('countries');

                defBookCountries.resolve();

                return $.when.apply(undefined, promises).promise();
            }
            var error = function (resmsd) {
                messages.error.push($filter('translate')('messageserror-syncbooklocationserver'));
                //messages.error.push('Error al intentar sincronizar las localidades del libro con el servidor.');
            };
            //console.log('getBookCountries::init');
            return SyncronizationProxy.getBookCountries(bookid).then(processCountriesJson, error);
        };

        self.processBookCountry = function(country, deferred) {
            //console.log('processBookCountry::'+JSON.stringify(country));
            self.existsCountryById(country.id).then(function(result_exists) {
                //console.log('processBookCountry::result_exists.rows.length::'+result_exists.rows.length);
                if (result_exists.rows.length == 0) {
                    var query_insert_country = "INSERT INTO country (id, name) VALUES (?, ?);";
                    var country_obj = [country.id,
                                            country.name,];
                    //console.log('processBookCountry::query_insert_country::'+query_insert_country);
                    $cordovaSQLite.execute(db, query_insert_country, country_obj).then(function() {
                        //console.log('processBookCountry::insert::resolve');
                        deferred.resolve();
                    });
                }
                else {
                    //console.log('processBookCountry::nothing::resolve');
                    deferred.resolve();
                }
            });
        }

        self.existsCountryById = function(id) {
            var query = 'SELECT * FROM country where id = ' + id;
            //console.log('existsCountryById::query::'+query);
            return $cordovaSQLite.execute(db, query, []);
        }

        //Method to create the relation between user and book
        this.createUserBook = function (book_id, user_name, user_id, messages) {
            var verifyExistanceUserBook = function(res) {
                var promises = [];

                if(res.status = 200) {

                    var insertUserBook = function (resSe) {
                        //console.log('createUserBook::insertUserBook::resSe.rows.length::'+resSe.rows.length);
                        if (resSe.rows.length == 0) {
                            var query_insert_user_book = "INSERT INTO user_book (user_id, book_id) VALUES (?, ?);";
                            //console.log('createUserBook::insertUserBook::query_insert_user_book::'+query_insert_user_book);
                            $cordovaSQLite.execute(db, query_insert_user_book, [user_id, book_id]).then(function() {
                                defUserBooks.resolve();
                            });
                        }
                        else {
                            defUserBooks.resolve();
                        }
                    };
                    var errorSe = function (res) {
                        messages.error.push($filter('translate')('messageserror-syncbooklocation'));
                    };

                    var defUserBooks = new $.Deferred();
                    promises.push(defUserBooks);
                    var query_select_user_book = 'SELECT * FROM user_book where user_id = ' + user_id + ' and book_id = ' + book_id;
                    //console.log('createUserBook::verifyExistanceUserBook::insertUserBook::'+query_select_user_book);
                    $cordovaSQLite.execute(db, query_select_user_book, []).then(insertUserBook, errorSe);
                }

                return $.when.apply(undefined, promises).promise();
            };

            /*var errorC = function(res) {
                console.log('mundo');
                messages.error.push($filter('translate')('messageserror-syncbookserver'));
            }*/
            //console.log("createUserBook::init");
            //return SyncronizationProxy.createUserBook(book_id, user_name).then(verifyExistanceUserBook, errorC);
            return SyncronizationProxy.createUserBook(book_id, user_name).then(verifyExistanceUserBook);
        }

        this.getMunicipalities = function (data, messages) {
            var success = function (res) {

                var promises = [];

                var defSuccess = new $.Deferred();
                promises.push(defSuccess);

                //console.log('getMunicipalities::success::res.rows.length::'+res.rows.length);
                for (var i = 0; i < res.rows.length; i++) {
                    var currentMunicipality = res.rows.item(i);
                    data.municipality.push(currentMunicipality);
                }

                defSuccess.resolve();

                return $.when.apply(undefined, promises).promise().then(function() {
                    data.total = res.rows.length;
                });

            };
            var error = function (res) {
                messages.error.push($filter('translate')('messageserror-downloadsbooks'));
            };

            data.municipality = [];
            data.total = 0;
            var select_municipality = "SELECT DISTINCT A.id, A.name, C.id AS is_360 " +
                                        "FROM location A " +
                                        "JOIN mustseeanddo B ON A.id = B.location_id " +
                                        "LEFT JOIN (SELECT DISTINCT l.id, l.name " +
                                                    "FROM mustseeanddo m " +
                                                    "JOIN mustseeanddo_ar mar ON mar.mustseeanddo_id = m.id " +
                                                    "JOIN location l ON l.id = m.location_id " +
                                                    "WHERE mar.is_360 = 1) C ON A.id = C.id " +
                                        "WHERE state_id = ? ORDER BY A.id";
            //console.log('getMunicipalities::select_municipality::'+select_municipality);
            return $cordovaSQLite.execute(db, select_municipality, [data.state]).then(success, error);
        };

        this.getMapLocation = function() {
            //console.log('this.getMapLocation');
            navigator.geolocation.getCurrentPosition(onMapSuccess, onMapError, { enableHighAccuracy: true });
        }

        var onMapSuccess = function (position) {
            self.Latitude = position.coords.latitude;
            self.Longitude = position.coords.longitude;
            //console.log('onMapSuccess::self.Latitude::'+self.Latitude);
            //console.log('onMapSuccess::self.Longitude::'+self.Longitude);
        }

        var onMapError = function(error) {
            console.log('onMapError::code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
        }

        this.getDistanceFromLatLonInKm = function (lat1,lon1,lat2,lon2) {
            var R = 6371; // Radius of the earth in km
            var dLat = deg2rad(lat2-lat1);  // deg2rad below
            var dLon = deg2rad(lon2-lon1); 
            var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
                    Math.sin(dLon/2) * Math.sin(dLon/2); 
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
            var d = R * c; // Distance in km
            return d;
        }

        var deg2rad = function(deg) {
            return deg * (Math.PI/180);
        }

    }
})();
