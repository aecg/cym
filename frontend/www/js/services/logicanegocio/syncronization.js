/**
 * Created by poncbi on 29/03/2016.
 */
'use strict';

(function () {
    angular.module('cym').service('SyncronizationService', ['$cordovaSQLite','$ionicLoading','SyncronizationProxy','UserService','$filter', SyncronizationService]);

    function SyncronizationService($cordovaSQLite, $ionicLoading, SyncronizationProxy, userService, $filter) {

        var self = this;
        var allTracks = {
            username : "",
            trackings: []
        };

        var allTravels = {
            username : "",
            travels: []
        };

        var user = {
           username : "",
           password : "",
        };

        var datauser = {
            name : "",
            lastname : "",
            email : "",
            phone : ""
        }

        var clearObjects = function() {
            allTracks.username = "";
            allTracks.trackings = [];
            allTravels.username = "";
            allTravels.travels = [];
        };

        //Method to upload user data on server
        this.uploadUserData = function(id, username, messages) {

            clearObjects();
            var promises = [];

            var defGetUserTrackings = new $.Deferred();
            var defUpdateTrackingUser = new $.Deferred();
            var defGetUserTravels = new $.Deferred();
            var defUpdateTravelsUser = new $.Deferred();
            var defGetUser = new $.Deferred();
            var defUpdateUser = new $.Deferred();

            promises.push(defGetUserTrackings);
            promises.push(defUpdateTrackingUser);
            promises.push(defGetUserTravels);
            promises.push(defUpdateTravelsUser);
            promises.push(defGetUser);
            promises.push(defUpdateUser);

            //console.log('uploadUserData::pre::self.getObjectJsonTrackings');
            self.getObjectJsonTrackings(id, username, messages).then(function() {
                //console.log('uploadUserData::self.getObjectJsonTrackings::done');
                defGetUserTrackings.resolve(allTracks);
                SyncronizationProxy.updateTrackingUser(allTracks).then(function() {
                    //console.log('uploadUserData::SyncronizationProxy.updateTrackingUser::done');
                    defUpdateTrackingUser.resolve(allTracks);
                    self.getObjectJsonTravels(id, username, messages).then(function() {
                        //console.log('uploadUserData::self.getObjectJsonTravels::done');
                        //console.log('allTravels::'+JSON.stringify(allTravels));
                        defGetUserTravels.resolve(allTravels);
                        SyncronizationProxy.updateTravelsUser(allTravels).then(function() {
                            //console.log('uploadUserData::SyncronizationProxy.updateTravelsUser::done');
                            defUpdateTravelsUser.resolve(allTravels);
                            self.getObjectJsonUser(id, username, messages).then(function() {
                                //console.log('uploadUserData::self.getObjectJsonUser::done');
                                defGetUser.resolve(user);
                                SyncronizationProxy.updateUser(user).then(function() {
                                    //console.log('uploadUserData::SyncronizationProxy.updateUser::done');
                                    defUpdateUser.resolve(user);
                                }, function(error) {
                                    defUpdateUser.reject(error);
                                });
                            });
                        }, function(error) {
                            defGetUser.resolve(user);
                            defUpdateUser.resolve(user);
                            defUpdateTravelsUser.reject(error);
                        });
                    });
                }, function(error) {
                    defGetUserTravels.resolve(allTravels);
                    defUpdateTravelsUser.resolve(allTravels);
                    defGetUser.resolve(user);
                    defUpdateUser.resolve(user);
                    defUpdateTrackingUser.reject(error);
                });
            });

            return $.when.apply(undefined, promises).promise();
        };

        //Method to upload user travels
        this.getObjectJsonTravels = function(id, username, messages) {
            allTravels.username = username;
            allTravels.travels = [];
            var processSelectTravel = function (res) {

                var processTravel = function(travel, deferred) {

                    var processSelectDestinationByTravel = function (result) {
                        //console.log('processSelectDestinationByTravel::travel.id::'+travel.id);
                        //console.log('processSelectDestinationByTravel::travel.id::'+travel.id+'::result::'+result.rows.length);
                        var promises_details = [];

                        var defDestination = new $.Deferred();
                        promises_details.push(defDestination);
                        //console.log('processSelectDestinationByTravel::travel.id::'+travel.id+'::1::promises_details.length::'+promises_details.length);
                        //console.log('processSelectDestinationByTravel::travel.id::'+travel.id+'::1');

                        var processDestination = function(destination, deferredDestination) {
                            var copyDestination = angular.copy(destination);

                            var processSelectMustseeandoByDestination = function(result_msad) {

                                var processMustSeeAndDo = function(mustseeanddo, deferredMustSeeAndDo) {
                                    var msad = {};
                                    msad.mustseeanddo_id = mustseeanddo.mustseeanddo_id;

                                    var copyMsad = angular.copy(msad);

                                    copyDestination.mustseeanddo.push(copyMsad);
                                    deferredMustSeeAndDo.resolve(copyMsad);
                                }

                                copyDestination.mustseeanddo = [];

                                for (var l = 0; l < result_msad.rows.length; l++) {
                                    var currentMsad = result_msad.rows.item(l);
                                    //console.log('currentMsad::'+JSON.stringify(currentMsad));

                                    var defDestinationMsad = new $.Deferred();
                                    promises_details.push(defDestinationMsad);

                                    $.queue('mustseeanddos', processMustSeeAndDo(currentMsad, defDestinationMsad));
                                }
                                $.dequeue('mustseeanddos');

                                travel.destination.push(copyDestination);
                                deferredDestination.resolve(copyDestination);
                                //console.log('processSelectDestinationByTravel::travel.id::'+travel.id+'::8::promises_details.length::'+promises_details.length);
                            }

                            // Travel's Destination's Must See And Must Do
                            var select_mustseeanddobydestination = 'SELECT * FROM travel_mustseeanddo WHERE destination_id = ' + copyDestination.id;
                            //console.log('select_mustseeanddobydestination::'+select_mustseeanddobydestination);
                            $cordovaSQLite.execute(db, select_mustseeanddobydestination, []).then(processSelectMustseeandoByDestination);
                        }

                        for (var j = 0; j < result.rows.length; j++) {

                            var currentDestination = result.rows.item(j);
                            //console.log('processSelectDestinationByTravel::travel.id::'+travel.id+'::'+j+'::currentDestination::'+JSON.stringify(currentDestination));

                            var defTravelDestination = new $.Deferred();
                            promises_details.push(defTravelDestination);
                            //console.log('processSelectDestinationByTravel::travel.id::'+travel.id+'::7::promises_details.length::'+promises_details.length);

                            $.queue('destinations', processDestination(currentDestination, defTravelDestination));
                        }
                        defDestination.resolve(result);
                        //console.log('processSelectDestinationByTravel::travel.id::'+travel.id+'::2::promises_details.length::'+promises_details.length);
                        //console.log('processSelectDestinationByTravel::travel.id::'+travel.id+'::2');
                        $.dequeue('destinations');

                        return $.when.apply(undefined, promises_details).then(function() {
                            //console.log('processSelectDestinationByTravel::travel.id::'+travel.id+'::7::promises_details.length::'+promises_details.length);
                            allTravels.travels.push(angular.copy(travel));
                            //console.log('allTravels::travel::'+travel.id+'::'+JSON.stringify(allTravels));
                            deferred.resolve(travel);
                        });
                    }

                    // Travel's Destination
                    var select_destinationbytravel = 'SELECT * FROM destination WHERE travel_id = ' + travel.id;
                    return $cordovaSQLite.execute(db, select_destinationbytravel, []).then(processSelectDestinationByTravel);
                }

                var promises = [];
                for (var i = 0; i < res.rows.length; i++) {

                    var currentTravel = res.rows.item(i);

                    var defTravel = new $.Deferred();
                    promises.push(defTravel);

                    var copyTravel = angular.copy(currentTravel);
                    copyTravel.destination = [];

                    $.queue('travels', processTravel(copyTravel, defTravel));
                }
                $.dequeue('travels');
                return $.when.apply(undefined, promises).promise();
            };

            var error = function (res) {
                messages.error.push($filter('translate')('messageserror-synctravelserver'));
            };
            var select_travelsbyuser = 'SELECT * FROM travel WHERE user_id = ' + id;
            return $cordovaSQLite.execute(db, select_travelsbyuser, []).then(processSelectTravel, error);
        };

        //Method to upload user trackings
        this.getObjectJsonTrackings = function(id, username, messages) {
            allTracks.username = username;
            allTracks.trackings = [];
            var processSelectTracking = function (res) {
                var processTracking = function(tracking, deferred) {

                    var processSelectCuepointByTracking = function (result) {
                        for (var j = 0; j < result.rows.length; j++) {
                            var currentCuepoint = result.rows.item(j);
                            var copyCuepoint = angular.copy(currentCuepoint);
                            tracking.cuepoints.push(copyCuepoint);
                        }
                        allTracks.trackings.push(angular.copy(tracking));
                        deferred.resolve(tracking);
                    }

                    // Tracking's Cuepoints
                    var select_cuepointbytrack = 'SELECT * FROM cuepoint WHERE tracking_id = ' + copyTracking.id;
                    $cordovaSQLite.execute(db, select_cuepointbytrack, []).then(processSelectCuepointByTracking);
                }

                var promises = [];

                for (var i = 0; i < res.rows.length; i++) {

                    var currentTracking = res.rows.item(i);

                    var defTracking = new $.Deferred();
                    promises.push(defTracking);

                    var copyTracking = angular.copy(currentTracking);
                    copyTracking.cuepoints = [];

                    $.queue('trackings', processTracking(copyTracking, defTracking));
                }
                $.dequeue('trackings');
                return $.when.apply(undefined, promises).promise();
            };
            var error = function (resmsd) {
                messages.error.push($filter('translate')('messageserror-findtrackingusersync'));
            };
            var select_trackingbyuser = 'SELECT * FROM tracking WHERE user_id = ' + id;
            return $cordovaSQLite.execute(db, select_trackingbyuser, []).then(processSelectTracking, error);
        };

        //Method to upload user
        this.getObjectJsonUser = function(id, username, messages) {
            var processSelectUser = function (res)
            {
                if(res.rows.length > 0)
                {
                    user.username = res.rows.item(0).username;
                    user.password = res.rows.item(0).password;
                    datauser.name = res.rows.item(0).name;
                    datauser.lastname = res.rows.item(0).lastname;
                    datauser.email = res.rows.item(0).email;
                    datauser.phone = res.rows.item(0).phone;
                    user.dataUser = datauser;
                }
            };
            var error = function (res) {
                messages.error.push($filter('translate')('messageserror-findusersync'))
            };
            var select_user = 'SELECT a.username, a.password, b.name, b.lastname, b.email, b.phone FROM user a join datauser b on a.id = b.user_id WHERE a.username = "' + username +'"';
            return $cordovaSQLite.execute(db, select_user, []).then(processSelectUser, error);
        };

        //Method to upload user data on server
        this.downloadUserData = function(id, username, messages) {

            var promises = [];

            var defGetUserTravels = new $.Deferred();
            var defGetUserTrackings = new $.Deferred();

            promises.push(defGetUserTravels);
            promises.push(defGetUserTrackings);

            //Get the user travels
            userService.getUserTravels(username, id, messages).then(function() {
                defGetUserTravels.resolve();
                //Get the user trackings
                userService.getUserTrackings(username, id, messages).then(function() {
                    defGetUserTrackings.resolve();
                }, function(error) {
                    console.log('this.downloadUserData::error::userService.getUserTrackings');
                    defGetUserTrackings.reject(error);
                });
            }, function(error) {
                console.log('this.downloadUserData::error::userService.getUserTravels');
                defGetUserTrackings.resolve();
                defGetUserTravels.reject(error);
            });

            return $.when.apply(undefined, promises).promise();
        };

        //Method to delete user data on movil
        this.deleteUserData = function(id, username) {
            var promises = [];

            var defDeleteTravelMustSeeAndDo = new $.Deferred();
            var defDeleteTravelDestination = new $.Deferred();
            var defDeleteTravel = new $.Deferred();
            var defDeleteCuepoint = new $.Deferred();
            var defDeleteTracking = new $.Deferred();
            promises.push(defDeleteTravelMustSeeAndDo);
            promises.push(defDeleteTravelDestination);
            promises.push(defDeleteTravel);
            promises.push(defDeleteCuepoint);
            promises.push(defDeleteTracking);

            var delete_travel_mustseeanddo = 'DELETE FROM travel_mustseeanddo WHERE destination_id in (SELECT b.id FROM travel a join destination b on a.id = b.travel_id WHERE a.user_id =' + id + ')';
            var delete_travel_destination = 'DELETE FROM destination WHERE travel_id in (SELECT id FROM travel WHERE user_id ='+ id + ')'
            var delete_travel = 'DELETE FROM travel WHERE user_id = ' + id;
            var delete_cuepoint = 'DELETE FROM cuepoint WHERE tracking_id in (SELECT id FROM tracking WHERE user_id = '+ id + ')';
            var delete_tracking = 'DELETE FROM tracking WHERE user_id = ' + id;

            $cordovaSQLite.execute(db, delete_travel_mustseeanddo, []).then(function() {
                defDeleteTravelMustSeeAndDo.resolve();
                $cordovaSQLite.execute(db, delete_travel_destination, []).then(function() {
                    defDeleteTravelDestination.resolve();
                    $cordovaSQLite.execute(db, delete_travel, []).then(function() {
                        defDeleteTravel.resolve();
                        $cordovaSQLite.execute(db, delete_cuepoint, []).then(function() {
                            defDeleteCuepoint.resolve();
                            $cordovaSQLite.execute(db, delete_tracking, []).then(function() {
                                defDeleteTracking.resolve();
                            });
                        });
                    });
                });
            });

            return $.when.apply(undefined, promises).promise();
        };


        this.initializeMsadCoords = function() {
            var processMustseeanddoGPS = function (res) {
                var promises = [];

                //console.log('initializeMsadCoords::processMustseeanddoGPS::res.rows.length::'+res.rows.length);
                if(res.rows.length > 0) {
                    var defMsad = new $.Deferred();
                    promises.push(defMsad);

                    for (var i = 0; i < res.rows.length; i++) {

                        var currentMsad = res.rows.item(i);

                        var objMsad = {id: currentMsad.id, 
                                        name: currentMsad.title, 
                                        latlong: currentMsad.geographic_position, 
                                        qr: currentMsad.qr,
                                        ar: currentMsad.ar};
                        //console.log('initializeMsadCoords::objMsad::'+JSON.stringify(objMsad));
                        mustseeanddoCoordinates.push(objMsad);
                    }
    
                    //console.log('initializeMsadCoords::mustseeanddoCoordinates::'+JSON.stringify(mustseeanddoCoordinates));
                    defMsad.resolve();
                }
                return $.when.apply(undefined, promises).promise();
            };
            //console.log('initializeMsadCoords::mustseeanddoCoordinates::'+JSON.stringify(mustseeanddoCoordinates));
            var select_mustseeanddo = "SELECT id, title, geographic_position, qr, ar FROM mustseeanddo";
            return $cordovaSQLite.execute(db, select_mustseeanddo, []).then(processMustseeanddoGPS);
        };

        this.getMustseeanddoCoordinates = function() {
            return mustseeanddoCoordinates;
        }


/*
        this.initializeMsadCoords = function () {
            console.log('initializeMsadCoords::init::mustseeanddoCoordinates::'+JSON.stringify(mustseeanddoCoordinates));
            var promises = [];
            mustseeanddoCoordinates = [];
            db.transaction(function(tr) {
                var select_mustseeanddo = "SELECT id, title, geographic_position, qr, ar FROM mustseeanddo";
                tr.executeSql(select_mustseeanddo, [], function(tr, res) {
                    console.log('initializeMsadCoords::res.rows.length::'+res.rows.length);
                    if (res.rows.length > 0) {

                        var defMsad = new $.Deferred();
                        promises.push(defMsad);

                        for (var i = 0; i < res.rows.length; i++) {

                            var currentMsad = res.rows.item(i);

                            var objMsad = {id: currentMsad.id, 
                                            name: currentMsad.title, 
                                            latlong: currentMsad.geographic_position, 
                                            qr: currentMsad.qr,
                                            ar: currentMsad.ar};
                            mustseeanddoCoordinates.push(objMsad);
                        }
                        defMsad.resolve();
                    }
                    console.log('initializeMsadCoords::mustseeanddoCoordinates::'+JSON.stringify(mustseeanddoCoordinates));
                 });
            });
            return $.when.apply(undefined, promises).promise();
        };
*/

        this.downloadMasterData = function(id, username, messages) {
            var promises = [];

            var defGetBooks = new $.Deferred();
            var defGetUserBooks = new $.Deferred();
            var defGetStateMsadRecommended = new $.Deferred();
            var defInitializeMsadCoords = new $.Deferred();

            promises.push(defGetBooks);
            promises.push(defGetUserBooks);
            promises.push(defGetStateMsadRecommended);
            promises.push(defInitializeMsadCoords);

            //console.log('username::'+JSON.stringify(username));
            userService.getBooks(messages).then(function() {
                //console.log('books updated');
                defGetBooks.resolve();
                userService.getUserBooks(username, id, messages).then(function() {
                    //console.log('user books updated');
                    defGetUserBooks.resolve();
                    // obtener los destinos recomendados de todos los estados
                    userService.getStateMsadRecommended().then(function() {
                        //console.log('msad recommended updated');
                        defGetStateMsadRecommended.resolve();
                        self.initializeMsadCoords().then(function() {
                            defInitializeMsadCoords.resolve();
                        }, function(error) {
                            defInitializeMsadCoords.reject(error);
                        });
                    }, function(error) {
                        defGetStateMsadRecommended.reject(error);
                        defInitializeMsadCoords.reject(error);
                    });
                }, function(error) {
                    defGetUserBooks.reject(error);
                    defGetStateMsadRecommended.reject(error);
                    defInitializeMsadCoords.reject(error);
                });
            }, function(error) {
                defGetBooks.reject(error);
                defGetUserBooks.reject(error);
                defGetStateMsadRecommended.reject();
                defInitializeMsadCoords.reject(error);
            });
            return $.when.apply(undefined, promises).promise();
        };
    }
})();
