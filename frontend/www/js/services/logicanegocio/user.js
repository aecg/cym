'use strict';

(function () {

    angular.module('cym').service('UserService',['$cordovaSQLite', '$rootScope', 'DaoUsers',
            '$translate', '$filter' ,'UserProxy', 'TravelService','SyncronizationProxy', 'SessionFactory', '$ionicLoading', 'GOOGLE_MAPS_APIKEY', 
            '$state', '$ionicHistory', UserService ]);

    function UserService($cordovaSQLite, $rootScope ,daouser, $translate, $filter, UserProxy,travelsService,
                         SyncronizationProxy,SessionFactory, $ionicLoading, GOOGLE_MAPS_APIKEY, $state, $ionicHistory) {
        var self = this;
        var userLogged = {};
        var userP = {};
        var imagebase64 = '';

        /**
         * Descripcion:             metodo que valida si el usuario esta logeado
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   2/26/2016
         *
         * @returns {boolean}
         */
        self.isUserLogged = function () {
            var userLogged_ = self.getUserLogged();
            //console.log('self.isUserLogged::userLogged_::'+JSON.stringify(userLogged_));
            if ((userLogged_ != null) && (userLogged_.id != null) && (userLogged_.id > 0)) {
                SessionFactory.setObject("userLogged", userLogged);
                return true;
            }
            else {
                var obj = SessionFactory.getObject("userLogged");
                if ((obj != null) && (obj.id != null) && (obj.id > 0)) {
                    userLogged = obj;
                    return true;
                }
            }
            return false;
        };

        this.getUserNameLogged = function () {
            return userLogged.username;
        };

        this.getUserLoggedId = function () {
            return userLogged.id;
        };

        self.getUserLogged = function () {
            return userLogged;
        };

        /**
         * Descripcion:             metodo que asigna el usuario logeado
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   2/26/2016
         *
         * @param messages
         */
        self.setUserLogged = function (messages) {
            var success = function (res) {
                //console.log('self.setUserLogged::success::rows.length::'+res.rows.length);
                if (res.rows.length > 0) {
                    //console.log('self.setUserLogged::success::rows.item(0)::'+JSON.stringify(res.rows.item(0)));
                    userLogged = res.rows.item(0);
                    //console.log('self.setUserLogged::success::userLogged::'+JSON.stringify(userLogged));
                }
                else {
                    userLogged = {};
                    SessionFactory.setObject("userLogged", userLogged);
                }

                //query = 'SELECT * FROM user WHERE login_state = 1 AND enabled = 1';
                //$cordovaSQLite.execute(db, query).then(function(result1) {
                    //if (result1.rows.length > 0) {
                        //console.log('self.setUserLogged::debug::result1.rows.length::'+result1.rows.length);
                        //console.log('self.setUserLogged::debug::result1.rows.item(0)::'+JSON.stringify(result1.rows.item(0)));

                        //query = 'SELECT * FROM datauser WHERE user_id = 1';
                        //$cordovaSQLite.execute(db, query).then(function(result2) {
                            //if (result2.rows.length > 0) {
                                //console.log('self.setUserLogged::debug::result2.rows.length::'+result2.rows.length);
                                //console.log('self.setUserLogged::debug::result2.rows.item(0)::'+JSON.stringify(result2.rows.item(0)));
                                //}
                        //});
                    //}
                //});

            };

            var error = function (res) {
                //console.log('Error en sesión');
            };

            //console.log('self.setUserLogged::init');
            var query = 'SELECT a.id, a.username, a.password, b.name, b.lastname, b.language, b.image_profile FROM user a JOIN datauser b on a.id = b.user_id WHERE a.login_state = 1 AND a.enabled = 1';
            //console.log('self.setUserLogged::query::'+query);
            return $cordovaSQLite.execute(db, query).then(success, error);
        };

        self.insertUser = function (username, password, creation_time, enabled, login_state) {
            //console.log('self.insertUser::username::'+username+'::password::'+password+'::creation_time::'+creation_time+'::enabled::'
            //                +enabled+'::login_state::'+login_state);
            var insert_user_query = 'INSERT INTO user (username, password, created_at, enabled, login_state) VALUES (?, ?, ?, ?, ?)';
            var insert_user_data = [username, password, creation_time, enabled, login_state];
            return $cordovaSQLite.execute(db, insert_user_query, insert_user_data);
        }

        self.updateUser = function (username, password, enabled) {
            //console.log('self.updateUser::username::'+username+'::password::'+password+'::enabled::'+enabled);
            var update_user_query = 'UPDATE user SET password = ?, enabled = ? where username = ?';
            var update_user_data = [password, enabled, username];
            return $cordovaSQLite.execute(db, update_user_query, update_user_data);
        }

        self.updateUserGoogle = function (username, enabled) {
            //console.log('self.updateUserGoogle::username::'+username+'::enabled::'+enabled);
            var update_user_query = 'UPDATE user SET enabled = ? where username = ?';
            var update_user_data = [enabled, username];
            return $cordovaSQLite.execute(db, update_user_query, update_user_data);
        }

        self.insertDataUser = function (email, user_id) {
            //console.log('self.insertDataUser::email'+email+'::user_id::'+user_id);
            var insert_datauser_query = 'INSERT INTO datauser (email, user_id) VALUES (?, ?)';
            var insert_datauser_data = [email, user_id]; 
            return $cordovaSQLite.execute(db, insert_datauser_query, insert_datauser_data);
        }

        self.updateDataUser = function (name, lastname, email) {
            //console.log('self.updateDataUser::name::'+name+'::lastname::'+lastname+'::email'+email);
            var update_datauser_query = 'UPDATE datauser SET name = ?, lastname = ? WHERE email = ?';
            var update_datauser_data = [name, lastname, email]; 
            return $cordovaSQLite.execute(db, update_datauser_query, update_datauser_data);
        }

        self.insertDataUserGoogle = function (email, user_id, user_name, user_lastname, user_image_profile) {
            var insert_datauser_query = 'INSERT INTO datauser (email, name, lastname, user_id, image_profile) VALUES (?, ?, ?, ?, ?)';
            var insert_datauser_data = [email, user_name, user_lastname, user_id, user_image_profile]; 
            //console.log('self.insertDataUserGoogle::insert_datauser_data::'+JSON.stringify(insert_datauser_data));
            return $cordovaSQLite.execute(db, insert_datauser_query, insert_datauser_data);
        }

        self.validateUserLocal = function(email) {
            var query = 'SELECT id FROM user WHERE username = ?';
            console.log('self.validateUserLocal::query::'+query+'::email::'+email);
            return $cordovaSQLite.execute(db, query, [email]);
        }

        self.authenticateUser = function(email, password) {
            var query = 'SELECT a.id, a.username, a.password, ' +
                            'b.name, b.lastname, b.language, b.image_profile ' +
                            'FROM user a LEFT JOIN datauser b on a.id = b.user_id ' +
                            'WHERE a.username = ? and a.password = ?';
            console.log('self.authenticateUser::email::'+email+'::password:'+password+'::query::'+query);
            return $cordovaSQLite.execute(db, query, [email, password]);
        }

        self.authenticateUserGoogle = function(email) {
            var query = 'SELECT a.id, a.username, ' +
                            'b.name, b.lastname, b.language, b.image_profile ' +
                            'FROM user a LEFT JOIN datauser b on a.id = b.user_id ' +
                            'WHERE a.username = ? and a.enabled = 1';
            return $cordovaSQLite.execute(db, query, [email]);
        }

        self.insertUserInitialBooks = function (user_id) {

            var promises = [];
            var initialBookIds = [1, 2, 3];

            $.each( initialBookIds, function( key, value ) {

                var defUserBooks = new $.Deferred();
                promises.push(defUserBooks);
                var query_select_user_book = 'SELECT * FROM user_book where user_id = ? and book_id = ?';
                $cordovaSQLite.execute(db, query_select_user_book, [user_id, value]).then(function (resSe) {
                    //alert('self.insertUserInitialBooks::resSe.rows.length::'+resSe.rows.length);
                    if (resSe.rows.length == 0) {
                        var query_insert_user_book = 'INSERT INTO user_book (user_id, book_id) VALUES (?, ?);';
                        //console.log('createUserBook::insertUserBook::query_insert_user_book::'+query_insert_user_book);
                        $cordovaSQLite.execute(db, query_insert_user_book, [user_id, value]).then(function(resInitialBooks) {
                            //alert('self.insertUserInitialBooks::inserted::'+resInitialBooks.insertId+'::'+user_id+'::'+value);
                            defUserBooks.resolve();
                        });
                    }
                    else {
                        defUserBooks.resolve();
                    }
                });
            });

            return $.when.apply(undefined, promises).promise();
        }

        self.verifyGetUserValidate = function(result, username, password) {
            //console.log('self.verifyGetUserValidate::init');
            var promises_verify_get = [];

            var defVerifyGetUserValidate = new $.Deferred();
            var defCreateUpdateUser = new $.Deferred();
            var defUpdateDataUser = new $.Deferred();

            promises_verify_get.push(defVerifyGetUserValidate);
            promises_verify_get.push(defCreateUpdateUser);
            promises_verify_get.push(defUpdateDataUser);

            if(result.data.username == "") {
                // el usuario no existe en el servidor, se crea remotamente
                var newUser = {};
                newUser.username = username;
                newUser.password = password;
                UserProxy.createUser(newUser).then(function() {
                    //console.log('self.verifyGetUserValidate::post::UserProxy.createUser');
                    defCreateUpdateUser.resolve();
                    defUpdateDataUser.resolve();
                });
            }
            else {
                // el usuario existe en el servidor, se actualizan los datos locales
                var user_lastname = null;
                //console.log('self.verifyGetUserValidate::user_lastname::'+user_lastname+'::result.data.lastname::'+result.data.lastname);
                if (typeof result.data.lastname != "undefined") {
                    user_lastname = result.data.lastname;
                }
                var user_name = null;
                //console.log('self.verifyGetUserValidate::user_name::'+user_name+'::result.data.name::'+result.data.name);
                if (typeof result.data.name != "undefined") {
                    user_name = result.data.name;
                }
                self.updateUser(result.data.username, result.data.password, 1).then(function() {
                    //console.log('self.verifyGetUserValidate::post::self.updateUser');
                    defCreateUpdateUser.resolve();
                    self.updateDataUser(user_name, user_lastname, result.data.username).then(function() {
                        //console.log('self.verifyGetUserValidate::post::self.updateDataUser');
                        defUpdateDataUser.resolve();
                    });
                });
            }
            defVerifyGetUserValidate.resolve();

            return $.when.apply(undefined, promises_verify_get).promise();
        }

        self.verifyGetUserGoogleValidate = function(result, username, user_first, user_lastname) {
            //console.log('self::verifyGetUserGoogleValidate::result.data.username::'+result.data.username);
            var promises_verify_google = [];
 
            var defVerifyGetUserValidate = new $.Deferred();
            var defCreateUpdateUser = new $.Deferred();

            promises_verify_google.push(defVerifyGetUserValidate);
            promises_verify_google.push(defCreateUpdateUser);

            if(result.data.username == "") {
                // el usuario no existe en el servidor, se crea remotamente
                var newUser = {};
                newUser.username = username;
                newUser.name = user_first;
                newUser.lastname = user_lastname;
                newUser.password = '';
                UserProxy.createUser(newUser).then(function() {
                    //console.log('self.verifyGetUserGoogleValidate::post::UserProxy.createUser');
                    defCreateUpdateUser.resolve();
                });
            }
            else {
                // el usuario existe en el servidor, se actualizan los datos locales
                self.updateUserGoogle(result.data.username, 1).then(function() {
                    //console.log('self.verifyGetUserGoogleValidate::post::self.updateUser');
                    defCreateUpdateUser.resolve();
                });
            }
            defVerifyGetUserValidate.resolve();

            return $.when.apply(undefined, promises_verify_google).promise();
        }

        self.createUserLocally = function(user) {
            console.log('self.createUserLocally::init');
            var promises_create = [];

            var defValidateUserLocal = new $.Deferred();
            var defInsertUserLocal = new $.Deferred();
            var defInsertDataUserLocal = new $.Deferred();
            var defInsertUserInitialBooks = new $.Deferred();

            promises_create.push(defValidateUserLocal);
            promises_create.push(defInsertUserLocal);
            promises_create.push(defInsertDataUserLocal);
            promises_create.push(defInsertUserInitialBooks);

            var creation_date = new Date();
            var creation_time = creation_date.getTime();

            // verificar si el usuario está en la bbdd local
            self.validateUserLocal(user.email).then(function(result) {
                defValidateUserLocal.resolve();

                console.log('self.createUserLocally::rows.length::'+result.rows.length);
                if (result.rows.length == 0) {

                    self.insertUser(user.email, user.password, creation_time, 1, null).then(function(result_insert) {
                        console.log('self.createUserLocally::usuario insertado localmente::'+result_insert.insertId);
                        defInsertUserLocal.resolve();

                        self.insertDataUser(user.email, result_insert.insertId).then(function(result_insert_du) {
                            console.log('self.createUserLocally::insertada data user::result_insert_du.insertId::'+result_insert_du.insertId);
                            defInsertDataUserLocal.resolve();

                            self.insertUserInitialBooks(result_insert.insertId).then(function() {
                                //alert('self.createUserLocally::insertada los user books');
                                defInsertUserInitialBooks.resolve();
                            });
                        });
                    });
                }
                else {
                    defInsertUserLocal.resolve();
                    defInsertDataUserLocal.resolve();
                    defInsertUserInitialBooks.resolve();
                }
            });
            return $.when.apply(undefined, promises_create).promise();
        }

        self.createUserGoogleLocally = function(user_data) {
            //console.log('self.createUserGoogleLocally::init');
            var promises_create = [];

            var defValidateUserLocal = new $.Deferred();
            var defInsertUserLocal = new $.Deferred();
            var defLoadImage = new $.Deferred();
            var defInsertDataUserLocal = new $.Deferred();
            var defInsertUserInitialBooks = new $.Deferred();

            promises_create.push(defValidateUserLocal);
            promises_create.push(defInsertUserLocal);
            promises_create.push(defLoadImage);
            promises_create.push(defInsertDataUserLocal);
            promises_create.push(defInsertUserInitialBooks);

            var creation_date = new Date();
            var creation_time = creation_date.getTime();

            // verificar si el usuario está en la bbdd local
            self.validateUserLocal(user_data.email).then(function(result) {
                defValidateUserLocal.resolve();

                //console.log('self.createUserGoogleLocally::rows.length::'+result.rows.length);
                if (result.rows.length == 0) {

                    self.insertUser(user_data.email, '', creation_time, 1, null).then(function(result_insert) {
                        //console.log('self.createUserGoogleLocally::usuario insertado localmente::'+result_insert.insertId);
                        defInsertUserLocal.resolve();

                        var resDisplayName = user_data.displayName.split(' ');
                        var userName = '';
                        var userLastname = '';
                        //console.log('self.loginGoogle::resDisplayName.length::'+resDisplayName.length);
                        if (resDisplayName.length > 1) {
                            userName = resDisplayName[0];
                            //console.log('self.loginGoogle::userName::'+userName);
                            userLastname = user_data.displayName.slice(resDisplayName[0].length + 1);
                            //console.log('self.loginGoogle::userLastname::'+userLastname);
                        }
                        else {
                            userName = user_data.displayName;
                            //console.log('self.loginGoogle::userName::'+userName);
                        }

                        if (user_data.imageUrl != null) {
                            self.loadImage(user_data).then(function(result_image) {
                                //console.log('self.createUserGoogleLocally::post::loadImage');
                                defLoadImage.resolve();
                                self.insertDataUserGoogle(user_data.email, result_insert.insertId, userName, userLastname, imagebase64).then(function() {
                                    //console.log('self.createUserGoogleLocally::insertada data user');
                                    defInsertDataUserLocal.resolve();

                                    self.insertUserInitialBooks(result_insert.insertId).then(function() {
                                        //alert('self.createUserGoogleLocally::insertada los user books');
                                        defInsertUserInitialBooks.resolve();
                                    });
                                });
                            });
                        }
                        else {
                            defLoadImage.resolve();
                            self.insertDataUserGoogle(user_data.email, result_insert.insertId, userName, userLastname, null).then(function() {
                                //console.log('self.createUserGoogleLocally::insertada data user');
                                defInsertDataUserLocal.resolve();

                                self.insertUserInitialBooks(result_insert.insertId).then(function() {
                                    //alert('self.createUserGoogleLocally::insertada los user books');
                                    defInsertUserInitialBooks.resolve();
                                });
                            });
                        }
                    });
                }
                else {
                    defInsertUserLocal.resolve();
                    defLoadImage.resolve();
                    defInsertDataUserLocal.resolve();
                    defInsertUserInitialBooks.resolve();
                }
            });
            return $.when.apply(undefined, promises_create).promise();
        }

        self.verifyUserRemotely = function(user, messages) {
            console.log('self.verifyUserRemotely::init');
            var promises_verify = [];

            var defVerifyUserRemotely = new $.Deferred();
            var defGetUserValidate = new $.Deferred();
            var defVerifyGetUserValidate = new $.Deferred();

            promises_verify.push(defVerifyUserRemotely);
            promises_verify.push(defGetUserValidate);
            promises_verify.push(defVerifyGetUserValidate);

            console.log('self.verifyUserRemotely::$rootScope.isOnline::'+$rootScope.isOnline);
            if ($rootScope.isOnline) {
                UserProxy.getUserValidate(user.email).then(function(result) {
                    defGetUserValidate.resolve();
                    console.log('self.verifyUserRemotely::post::UserProxy.getUserValidate::result::'+JSON.stringify(result));

                    self.verifyGetUserValidate(result, user.email, user.password).then(function() {
                        //console.log('self.verifyUserRemotely::post::verifyGetUserValidate');
                        defVerifyGetUserValidate.resolve();
                    });
                }, function(error) {
                    console.log('self.verifyUserRemotely::error::'+JSON.stringify(error));
                    defGetUserValidate.resolve();
                    if ((error.data != null) && (error.data.code == 100)) {

                        var defCreateUser = new $.Deferred();
                        promises_verify.push(defCreateUser);

                        // el usuario no existe en el servidor, se crea remotamente
                        var newUser = {};
                        newUser.username = user.email;
                        newUser.password = user.password;
                        UserProxy.createUser(newUser).then(function() {
                            //console.log('self.verifyUserRemotely::post::UserProxy.createUser');
                            defCreateUser.resolve();
                            defVerifyGetUserValidate.resolve();
                        });

                    }
                    else {
                        //messages.error.push($filter('translate')('messageserror-noserver'));
                        defGetUserValidate.resolve();
                        defVerifyGetUserValidate.resolve();
                    }
                });
            }
            else {
                defGetUserValidate.resolve();
                defVerifyGetUserValidate.resolve();
            }
            defVerifyUserRemotely.resolve();

            return $.when.apply(undefined, promises_verify).promise();
        }

        self.verifyUserGoogleRemotely = function(user_data, messages) {
            //console.log('self.verifyUserGoogleRemotely::init');
            var promises_verify = [];

            var defVerifyUserRemotely = new $.Deferred();
            var defGetUserValidate = new $.Deferred();
            var defVerifyGetUserValidate = new $.Deferred();

            promises_verify.push(defVerifyUserRemotely);
            promises_verify.push(defGetUserValidate);
            promises_verify.push(defVerifyGetUserValidate);

            if ($rootScope.isOnline) {
                UserProxy.getUserValidate(user_data.email).then(function(result) {
                    defGetUserValidate.resolve();
                    //console.log('self.verifyUserRemotely::post::UserProxy.getUserValidate::result::'+JSON.stringify(result));

                    var resDisplayName = user_data.displayName.split(' ');
                    var userName = '';
                    var userLastname = '';
                    //console.log('self.loginGoogle::resDisplayName.length::'+resDisplayName.length);
                    if (resDisplayName.length > 1) {
                        userName = resDisplayName[0];
                        //console.log('self.loginGoogle::userName::'+userName);
                        userLastname = user_data.displayName.slice(resDisplayName[0].length + 1);
                        //console.log('self.loginGoogle::userLastname::'+userLastname);
                    }
                    else {
                        userName = user_data.displayName;
                        //console.log('self.loginGoogle::userName::'+userName);
                    }

                    self.verifyGetUserGoogleValidate(result, user_data.email, userName, userLastname).then(function() {
                        //console.log('self.verifyUserRemotely::post::verifyGetUserValidate');
                        defVerifyGetUserValidate.resolve();
                    });
                }, function(error) {
                    //console.log('self.verifyUserGoogleRemotely::error::'+JSON.stringify(error));
                    defGetUserValidate.resolve();
                    if ((error.data != null) && (error.data.code == 100)) {

                        // el usuario no existe en el servidor, se crea remotamente
                        var defCreateUser = new $.Deferred();
                        promises_verify.push(defCreateUser);

                        var newUser = {};

                        newUser.username = user_data.email;

                        var resDisplayName = user_data.displayName.split(' ');
                        var userName = '';
                        var userLastname = '';
                        //console.log('self.verifyUserGoogleRemotely::resDisplayName.length::'+resDisplayName.length);
                        if (resDisplayName.length > 1) {
                            userName = resDisplayName[0];
                            //console.log('self.verifyUserGoogleRemotely::userName::'+userName);
                            userLastname = user_data.displayName.slice(resDisplayName[0].length + 1);
                            //console.log('self.verifyUserGoogleRemotely::userLastname::'+userLastname);
                        }
                        else {
                            userName = user_data.displayName;
                            //console.log('self.verifyUserGoogleRemotely::userName::'+userName);
                        }

                        newUser.name = userName;
                        newUser.lastname = userLastname;
                        newUser.password = '';
                        UserProxy.createUser(newUser).then(function() {
                            //console.log('self.verifyGetUserGoogleValidate::post::UserProxy.createUser');
                            defCreateUser.resolve();
                            defVerifyGetUserValidate.resolve();
                        });
                    }
                    else {
                        //messages.error.push($filter('translate')('messageserror-noserver'));
                        defGetUserValidate.resolve();
                        defVerifyGetUserValidate.resolve();
                    }
                });
            }
            else {
                defGetUserValidate.resolve();
                defVerifyGetUserValidate.resolve();
            }
            defVerifyUserRemotely.resolve();

            return $.when.apply(undefined, promises_verify).promise();
        }

        self.login = function (user, messages, page) {
            console.log('self.login::$rootScope.isOnline::'+$rootScope.isOnline+'::user::'+JSON.stringify(user)+'::page::'+page);

            var promises = [];

            var defCreateUserLocal = new $.Deferred();
            //var defVerifyUserRemotelly = new $.Deferred();
            var defAuthenticateUser = new $.Deferred();
            var defUpdateStatusLogin = new $.Deferred();
            //var defSynchronizeData = new $.Deferred();

            promises.push(defCreateUserLocal);
            //promises.push(defVerifyUserRemotelly);
            promises.push(defAuthenticateUser);

            // verificar si el usuario está en la bbdd local
            // en caso negativo, se crea
            self.createUserLocally(user).then(function() {
                console.log('self.login::post::createUserLocally');
                defCreateUserLocal.resolve();

                //self.verifyUserRemotely(user, messages).then(function() {
                    //console.log('self.login::post::verifyUserRemotely');
                    //defVerifyUserRemotelly.resolve();

                    // el usuario está en la bbdd local
                    // autenticamos el usuario, localmente
                    self.authenticateUser(user.email, user.password).then(function(result_authenticate) {
                        console.log('self.login::post::authenticateUser::rows.length::'+result_authenticate.rows.length);
                        defAuthenticateUser.resolve();

                        if (result_authenticate.rows.length > 0) {

                            promises.push(defUpdateStatusLogin);

                            var userItem = result_authenticate.rows.item(0);
                            //console.log('self.login::userItem::'+JSON.stringify(userItem));
                            userLogged.id = userItem.id;
                            userLogged.username = userItem.username;
                            userLogged.name = userItem.name;
                            userLogged.lastname = userItem.lastname;
                            userLogged.password = userItem.password;
                            userLogged.language = userItem.language;
                            userLogged.image_profile = userItem.image_profile;
                            //console.log('self.login::userLogged::'+JSON.stringify(userLogged));

                            self.updateStatusLogin(self.getUserLogged().username).then(function() {
                                defUpdateStatusLogin.resolve();
                                //console.log('self.login::post::updateStatusLogin');

                                //if ($rootScope.isOnline) {
                                    //$ionicLoading.show({
                                    //    template: $filter('translate')('messagetemplate-startingsync')
                                    //});
                                    //promises.push(defSynchronizeData);

                                    // si hay data precargada, no haría falta sincronización inicial
                                    //self.synchronizeData(userLogged, messages).then(function() {
                                        //console.log('self.login::self.synchronizeData::done');
                                        //defSynchronizeData.resolve();
                                        //$ionicLoading.hide();

                                        self.showPage(page);

                                    //});
                                //}
                            });
                        }
                        else {
                            userLogged = {};
                            //console.log('self.login::post::USER NOT AUTHENTICATED');
                            messages.error.push($filter('translate')('messageserror-userpasswordinvalid'));
                        }
                    });
                //});
            });

            return $.when.apply(undefined, promises).promise();
        };

        self.loginGoogle = function (user_data, messages, page) {
            //console.log('self.loginGoogle::$rootScope.isOnline::'+$rootScope.isOnline+'::user_data::'+JSON.stringify(user_data));

            var promises = [];

            var defCreateUserLocal = new $.Deferred();
            //var defVerifyUserRemotelly = new $.Deferred();
            var defAuthenticateUser = new $.Deferred();
            var defUpdateStatusLogin = new $.Deferred();
            //var defSynchronizeData = new $.Deferred();

            promises.push(defCreateUserLocal);
            //promises.push(defVerifyUserRemotelly);
            promises.push(defAuthenticateUser);

            self.createUserGoogleLocally(user_data).then(function() {
                //console.log('self.loginGoogle::post::createUserGoogleLocally');
                defCreateUserLocal.resolve();

                //self.verifyUserGoogleRemotely(user_data, messages).then(function() {
                    //console.log('self.loginGoogle::post::verifyUserGoogleRemotely');
                    //defVerifyUserRemotelly.resolve();

                    // el usuario está en la bbdd local
                    // autenticamos el usuario, localmente
                    self.authenticateUserGoogle(user_data.email).then(function(result_authenticate) {
                        //console.log('self.loginGoogle::post::authenticateUserGoogle::rows.length::'+result_authenticate.rows.length);
                        defAuthenticateUser.resolve();
                        if (result_authenticate.rows.length > 0) {

                            promises.push(defUpdateStatusLogin);

                            var userItem = result_authenticate.rows.item(0);
                            userLogged.id = userItem.id;
                            userLogged.username = userItem.username;
                            userLogged.name = userItem.name;
                            userLogged.lastname = userItem.lastname;
                            userLogged.language = userItem.language;
                            userLogged.image_profile = userItem.image_profile;

                            self.updateStatusLogin(self.getUserLogged().username).then(function() {
                                defUpdateStatusLogin.resolve();
                                //console.log('self.loginGoogle::post::updateStatusLogin');

                                //if ($rootScope.isOnline) {
                                //    $ionicLoading.show({
                                //        template: $filter('translate')('messagetemplate-startingsync')
                                //    });
                                //    promises.push(defSynchronizeData);

                                    // si hay data precargada, no haría falta sincronización inicial
                                    //self.synchronizeData(userLogged, messages).then(function() {
                                        //console.log('self.loginGoogle::post::synchronizeData');
                                //        defSynchronizeData.resolve();
                                //        $ionicLoading.hide();

                                        self.showPage(page);

                                    //});
                                //}
                            });
                        }
                        else {
                            userLogged = {};
                            //console.log('self.loginGoogle::post::USER NOT AUTHENTICATED');
                            messages.error.push($filter('translate')('messageserror-userpasswordinvalid'));
                        }
                    });
                //});

            });

            return $.when.apply(undefined, promises).promise();
        };


        /**
         * Descripcion:             metodo de carga de la imagen de google plus.
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   1/11/2016
         *
         */
        self.loadImage = function (user_data) {
            //console.log('self.loadImage::init');

            var promises = [];
            var defLoadImage = new $.Deferred();
            promises.push(defLoadImage);

            var outputFormat = null;
            var url = user_data.imageUrl;
            var img = new Image();
            img.src = url;
            //console.log('self.loadImage::url::'+url);
            img.crossOrigin = 'Anonymous';
            img.onload = function() {
                var canvas = document.createElement('CANVAS');
                var ctx = canvas.getContext('2d');
                var dataURL;
                canvas.height = this.height;
                canvas.width = this.width;
                ctx.drawImage(this, 0, 0);
                dataURL = canvas.toDataURL(outputFormat);
                imagebase64 = dataURL.replace(/^data:image\/(png|jpg);base64,/, '');
                canvas = null;
                //console.log('self.loadImage::end');
                defLoadImage.resolve();
            };
            return $.when.apply(undefined, promises).promise();
        };

        /**
         * Descripcion:             metodo de salida
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   1/11/2016
         *
         */
        self.logout = function (messages) {
            var success = function (res) {
                userLogged = {};
                var obj = {};
                SessionFactory.setObject("userLogged", obj);
                SessionFactory.setObject("trackingsMap", obj);
                SessionFactory.setObject("documentsMap", obj);
            };
            var error = function (res) {
                console.log('Error cerrando sesión::'+res.message);
                success(res);
            };
            var query_update = 'UPDATE user SET login_state = 0 where id = ?';
            $cordovaSQLite.execute(db, query_update, [self.getUserLogged().id]).then(success, error);
        };

        /**
         * Descripcion:             metodo que actualiza la clave
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   2/26/2016
         *
         * @returns {*}
         */
        self.updatePass = function (user, message) {
            return daouser.updatePass(user, message);
        };

        /**
         * Descripcion:             metodo que carga todos los datos de un usuario
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   2/26/2016
         *
         */
        self.loadInfo = function (messages) {
            var success = function (res)
            {
                if (res.rows.length > 0)
                {
                    userLogged = res.rows.item(0);
                }
                else
                {
                    messages.error.push($filter('translate')('messageserror-loadprofilenofound'));
                    //messages.error.push('Error cargando datos perfil. Datos no encontrados');
                }
            };
            var error = function (res)
            {
                messages.error.push($filter('translate')('messageserror-loadprofile'));
                //messages.error.push('Error cargando datos perfil');
            };
            var query = "SELECT a.id, a.username, a.password, b.name, b.lastname, b.language FROM user a LEFT JOIN datauser b on a.id = b.user_id WHERE a.username = ? and a.password = ? and a.enabled = 1";
            //var query = "SELECT id, username FROM user WHERE username = ? and password = ? and enabled = 1";
            $cordovaSQLite.execute(db, query, [self.getUserLogged().name, self.getUserLogged().pass]).then(success, error);
        };

        //Method to get the books on server.
        self.getBooks = function(messages) {
            var processBookJson = function (res) {
                //console.log('self.getBooks::res::'+JSON.stringify(res));
                var promises = [];

                $.each(res.data.books, function(i, book) {
                    //console.log('self.getBooks::book::'+JSON.stringify(book));
                    //console.log('self.getBooks::processBookJson::book::'+book.id);
                    var defBook = new $.Deferred();
                    promises.push(defBook);
                    $.queue('books', self.processBook(book, defBook));
                });
                $.dequeue('books');

                return $.when.apply(undefined, promises).promise();
            };

            //console.log('self.getBooks:: init')
            return SyncronizationProxy.getBooks().then(processBookJson);
        }

        // Method to insert data into travel table
        self.processBook = function(book, deferred) {

            var insertBook = function(result) {
                //console.log('insertBook::result::'+result.rows.length);
                var promisesBook = [];
                var defInsertBook = new $.Deferred();
                promisesBook.push(defInsertBook);

                var finishProcessBook = function() {
                    defInsertBook.resolve();
                    deferred.resolve();
                }

                if (result.rows.length == 0) {
                    var query_insert_book = 'INSERT INTO book(id, name, description, last_update, image_reference) VALUES (?,?,?,?,?)';
                    //console.log('insertBook::book.id::'+book.id+'::query_insert_book::' + query_insert_book);
                    var book_data = [book.id, 
                                        book.name, 
                                        book.description, 
                                        book.last_update,
                                        book.image];
                    $cordovaSQLite.execute(db, query_insert_book, book_data).then(function(res) {
                        console.log('book::new::'+res.insertId+'::'+book.name);
                        finishProcessBook(res);
                    });
                }
                else {
                    var last_update = result.rows.item(0).last_update;
                    //console.log('insertBook::book.id::'+book.id + '::book.last_update::' + book.last_update.toString()+'::last_update::' + last_update.toString());
                    if (book.last_update > last_update) {
                        var query_update_book = 'UPDATE book SET name = ?, ' +
                                                            'description = ?, last_update = ?, ' +
                                                            'image_reference = ? ' +
                                                            'WHERE id = ?';
                        var book_data = [book.name,
                                            book.description,
                                            book.last_update,
                                            book.image,
                                            book.id];
                        console.log('insertBook::book.id::'+book.id+'::query_update_book::' + query_update_book);
                        $cordovaSQLite.execute(db, query_update_book, book_data).then(finishProcessBook);
                    }
                    else {
                        finishProcessBook();
                    }
                }
                return $.when.apply(undefined, promisesBook).promise();
            }

            console.log('self.processBook::' + book.id);
            //alert('self.processBook::' + book.id);
            var query_select_book = 'SELECT last_update FROM book where id = ' + book.id;
            $cordovaSQLite.execute(db, query_select_book, []).then(insertBook);
        }

        //Method to get the user books on server.
        self.getUserBooks = function(username, user_id, messages) {
            var processUserBooksJson = function (res) {
                var promises = [];

                if (res.rows.length > 0) {

                    var defUserBooks = new $.Deferred();
                    promises.push(defUserBooks);

                    console.log('self.getUserBooks::processUserBooksJson::res.rows.length::'+res.rows.length);
                    for (var i = 0; i < res.rows.length; i++) {
                        var defBook = new $.Deferred();
                        promises.push(defBook);
                        var currentBookId = res.rows.item(i);
                        //console.log('self.getUserBooks::processUserBooksJson::currentBookId::'+JSON.stringify(currentBookId));

                        $.queue('usersBook', self.processUserBook(currentBookId.id, user_id, messages, defBook));
                    }

                    $.dequeue('usersBook');

                    defUserBooks.resolve();
                    return $.when.apply(undefined, promises).promise();
                }
            };

            //console.log('self.getUserBooks::username::'+username);
            //return SyncronizationProxy.getUserBooks(username).then(processUserBooksJson);
            var query_book = 'select id from book';
            return $cordovaSQLite.execute(db, query_book, []).then(processUserBooksJson);
        }

        // Method to insert data into travel table
        self.processUserBook = function(book_id, user_id, messages, deferred) {
            console.log('self.processUserBook::book_id::'+book_id+'::user_id::'+user_id+'::done');
            var promisesUserBook = [];

            var defGetBookMustSeeAndDo = new $.Deferred();
            var defGetBookMustSeeAndDoDetail = new $.Deferred();
            var defGetBookStates = new $.Deferred();
            var defGetBookCountries = new $.Deferred();
            var defQueryBook = new $.Deferred();

            promisesUserBook.push(defGetBookMustSeeAndDo);
            promisesUserBook.push(defGetBookMustSeeAndDoDetail);
            promisesUserBook.push(defGetBookStates);
            promisesUserBook.push(defGetBookCountries);
            promisesUserBook.push(defQueryBook);

            self.getBookMustSeeAndDo(book_id, messages).then(function() {
                console.log('self.getBookMustSeeAndDo::book_id::'+book_id+'::done');
                defGetBookMustSeeAndDo.resolve(book_id);
                self.getBookMustSeeAndDoDetail(book_id, messages).then(function() {
                    console.log('self.getBookMustSeeAndDoDetail::book_id::'+book_id+'::done');
                    defGetBookMustSeeAndDoDetail.resolve(book_id);
                    self.getBookStates(book_id, messages).then(function() {
                        console.log('self.getBookStates::book_id::'+book_id+'::done');
                        defGetBookStates.resolve(book_id);
                        self.getBookCountries(book_id, messages).then(function() {
                            console.log('self.getBookCountries::book_id::'+book_id+'::done');
                            defGetBookCountries.resolve(book_id);
                            self.processInsertUserBook(book_id, user_id).then(function() {
                                console.log('self.processInsertUserBook::book_id::'+book_id+'::user_id::'+user_id+'::done');
                                defQueryBook.resolve(book_id);
                                console.log('self.processUserBook::'+book_id+'::done');
                                deferred.resolve();
                            });
                        });
                    });
                });
            });

            return $.when.apply(undefined, promisesUserBook).promise();
        }

        self.processInsertUserBook = function(book_id, user_id) {
            var insertUserBook = function (resSe) {
                var promises = [];

                if (resSe.rows.length == 0) {
                    var defInsertUserBook = new $.Deferred();
                    promises.push(defInsertUserBook);

                    var query_insert_user_book = 'INSERT INTO user_book (user_id, book_id) values (?, ?)';
                    var user_book_data = [user_id, book_id];
                    $cordovaSQLite.execute(db, query_insert_user_book, user_book_data).then(function(res_user_book) {
                        //console.log('self.processInsertUserBook::new::'+res_user_book.insertId);
                        defInsertUserBook.resolve();
                    });

                    return $.when.apply(undefined, promises).promise();
                }
            };

            var query_select_userbook = 'SELECT * FROM user_book where user_id = ? and book_id = ?';
            //console.log('query_select_userbook::'+query_select_userbook)
            return $cordovaSQLite.execute(db, query_select_userbook, [user_id, book_id]).then(insertUserBook);
        }

        //Method to get the user travels on server.
        self.getUserTravels = function(username, user_id, messages) {
            var processTravelJson = function (res) {
                var promises = [];
                $.each(res.data.travels, function(i, travel) {
                    //console.log('processTravelJson::'+JSON.stringify(travel));
                    var defTravel = new $.Deferred();
                    promises.push(defTravel);
                    $.queue('usersTravels', self.processTravel(travel, user_id, defTravel));
                });
                $.dequeue('usersTravels');

                return $.when.apply(undefined, promises).promise();
            };
            //console.log('self.getUserTravels:: init');
            return SyncronizationProxy.getUserTravels(username).then(processTravelJson);
        };

        // Method to insert data into travel table
        self.processTravel = function(travel, user_id, deferred) {

            var insertTravelSuccess = function(result) {
                var promisesTravel = [];
                var travel_id = result.insertId;
                $.each(destinations, function(i, destination) {
                    //console.log('insertTravelSuccess::'+JSON.stringify(destination));
                    var defDestination = new $.Deferred();
                    promisesTravel.push(defDestination);

                    self.processDestination(destination, travel_id, defDestination);
                });

                deferred.resolve();
                return $.when.apply(undefined, promisesTravel).promise();
            }

            var destinations = travel.destination;

            // validates if travel exists already
            self.existsTravelByUniqueId(travel.unique_id).then(function(result_unique) {
                if (result_unique.rows.length == 0) {
                    //console.log('self.processTravel::'+travel.name);

                    var query_insert_travels = "INSERT INTO travel (name, budget, dateGoing, dateArrival, description, country_id, user_id, deleted, unique_id) VALUES (?,?,?,?,?,?,?,?,?)";

                    var travel_data = [travel.name, travel.budget, travel.dateGoing, travel.dateArrival, 
                                        travel.description, travel.country_id, user_id, 0, travel.unique_id];
                    return $cordovaSQLite.execute(db, query_insert_travels, travel_data).then(insertTravelSuccess);
                }
                else {
                    deferred.resolve();
                }
            });
        }

        self.existsTravelByUniqueId = function(uniqueId) {
            var query = 'SELECT * FROM travel WHERE unique_id = "' + uniqueId + '"';
            return $cordovaSQLite.execute(db, query, []);
        }

        // Method to insert data into travel's destination table
        self.processDestination = function(destination, travel_id, deferred) {

            var insertDestinationSuccess = function(result) {
                var promisesDestinations = [];
                var destination_id = result.insertId;
                $.each(mustseeanddos, function(i, mustseeanddo) {

                    var defMustSeeAndDo = new $.Deferred();
                    promisesDestinations.push(defMustSeeAndDo);

                    self.processMustSeeAndDo(mustseeanddo, destination_id, defMustSeeAndDo);
                });
                deferred.resolve();
                return $.when.apply(undefined, promisesDestinations).promise();
            } 

            var mustseeanddos = destination.mustseeanddo;

            var query = "INSERT INTO destination (travel_id, state_id, location_id, date, description, deleted, unique_id) VALUES (?, ?, ? ,?, ?, ?, ?)";

            var location_id = null;
            if(destination.location_id > 0)
            {
                location_id = destination.location_id;
            }

            var destination_data = [travel_id, 
                                        destination.state_id, 
                                        location_id, 
                                        destination.date, 
                                        destination.description, 
                                        0, 
                                        destination.unique_id];
            $cordovaSQLite.execute(db, query, destination_data).then(insertDestinationSuccess);
        }

        // Method to insert data into travle's destination's mustseeanddo table
        self.processMustSeeAndDo = function(mustseeanddo, destination_id, deferred) {
            var query = "INSERT INTO travel_mustseeanddo (destination_id, mustseeanddo_id, deleted, unique_id) VALUES (?, ?, ?, ?)";

            var mustseeanddo_data = [destination_id,
                                        mustseeanddo.mustseeanddo_id, 
                                        0, 
                                        mustseeanddo.unique_id];
            $cordovaSQLite.execute(db, query, mustseeanddo_data).then(function() {
                deferred.resolve();
            });
        }

        //Method to get the user trackings on server.
        self.getUserTrackings = function(username, user_id, messages) {
            var processUserTrackingsJson = function (res) {
                var promises = [];
                $.each(res.data.trackings, function(i, tracking) {
                    var defTracking = new $.Deferred();
                    promises.push(defTracking);

                    self.processUserTracking(user_id, tracking, defTracking);
                });
                return $.when.apply(undefined, promises).promise();
            };
            //console.log('self.getUserTrackings:: init');
            return SyncronizationProxy.getUserTrackings(username).then(processUserTrackingsJson);
        };

        // Method to insert data into tracking table
        self.processUserTracking = function(user_id, tracking, deferred) {
            var insertCuepoints = function(result) {
                var promisesCuepoints = [];
                $.each(cuepoints, function(i, cuepoint) {

                    var defCuepoint = new $.Deferred();
                    promisesCuepoints.push(defCuepoint);

                    self.processUserTrackingCuepoint(result.insertId, cuepoint, defCuepoint);
                });

                deferred.resolve();
                return $.when.apply(undefined, promisesCuepoints).promise();
            }

            var cuepoints = tracking.cuepoints;

            // validates if tracking exists already
            self.existsTrackingByUniqueId(tracking.unique_id).then(function(result_unique) {
                if (result_unique.rows.length == 0) {
                    var query_insert_tracking = "INSERT INTO tracking (name, created_at, user_id, deleted, unique_id) VALUES (?,?,?,?,?)";
                    var tracking_data = [tracking.name, 
                                            tracking.created_at, 
                                            user_id, 
                                            0, 
                                            tracking.unique_id];
                    return $cordovaSQLite.execute(db, query_insert_tracking, tracking_data).then(insertCuepoints);
                }
                else {
                    deferred.resolve();
                }
            });
        }

        self.existsTrackingByUniqueId = function(uniqueId) {
            var query = 'SELECT * FROM tracking WHERE unique_id = "' + uniqueId + '"';
            return $cordovaSQLite.execute(db, query, []);
        }

        // Method to insert data into cuepoint table
        self.processUserTrackingCuepoint = function(tracking_id, cuepoint, deferred) {

            var query_tracking_cuepoint = "INSERT INTO cuepoint (name, description, geographic_position, date, tracking_id, deleted, unique_id) values (?, ?, ?, ?, ?, ?, ?)";
            var cuepoint_data = [cuepoint.name,
                                    cuepoint.description, 
                                    cuepoint.geographic_position, 
                                    cuepoint.date, 
                                    tracking_id, 
                                    0, 
                                    cuepoint.unique_id];
            $cordovaSQLite.execute(db, query_tracking_cuepoint, cuepoint_data).then(function() {
                deferred.resolve();
            });
        }

        // Method to insert data into location table
        self.processLocation = function(location, deferred) {
            var insertLocation = function(result) {
                if (result.rows.length == 0) {
                    var query_insert_location = "INSERT INTO location (id, name, state_id, geographic_position) VALUES (?,?,?,?)";
                    var location_data = [location.id, 
                                            location.name, 
                                            location.state_id, 
                                            location.geographic_position];
                    $cordovaSQLite.execute(db, query_insert_location, location_data).then(function(res_location) {
                        //alert('location::new::'+res_location.insertId);
                        deferred.resolve();
                    });
                }
                else {
                    var query_update_location = 'UPDATE location SET name = ?, state_id = ?, ' +
                                                ' geographic_position = ? WHERE id = ?';
                    var location_data = [location.name, 
                                            location.state_id, 
                                            location.geographic_position, 
                                            location.id];
                    $cordovaSQLite.execute(db, query_update_location, location_data).then(function() {
                        deferred.resolve();
                    });
                }
            }
            //console.log('self.processLocation::'+location.id);
            var query_select_location = 'SELECT * FROM location where id = ' + location.id;
            $cordovaSQLite.execute(db, query_select_location, []).then(insertLocation);
        }

        //Method to get the book must see and do.
        self.getBookMustSeeAndDo = function(bookid, messages) {
            var processBookMustSeeAndDoJson = function (res) {
                //console.log('processBookMustSeeAndDoJson::init::bookid::'+bookid);
                var promises = [];
                var defGetBookMustSeeAndDo = new $.Deferred();
                promises.push(defGetBookMustSeeAndDo);

                $.each(res.data.mustseeanddo, function(i, mustseeanddo) {

                    var defBookMustSeeAndDo = new $.Deferred();
                    promises.push(defBookMustSeeAndDo);

                    //console.log('processBookMustSeeAndDoJson::bookid::'+bookid+'::mustseeanddo.id::'+mustseeanddo.id+'::title::'+mustseeanddo.title+'::mustseeanddo.qr::'+mustseeanddo.qr+'::mustseeanddo.ar::'+mustseeanddo.ar);
                    self.processBookMustSeeAndDo(bookid, mustseeanddo, defBookMustSeeAndDo);
                });

                defGetBookMustSeeAndDo.resolve();

                return $.when.apply(undefined, promises).promise();
            };
            //console.log('self.getBookMustSeeAndDo::init::bookid::'+bookid);

            return SyncronizationProxy.getBookMustSeeAndDo(bookid).then(processBookMustSeeAndDoJson);
        }

        // Method to insert data into must see and do table
        self.processBookMustSeeAndDo = function(bookid, msad, deferred) {
            var insertMustSeeAndDo = function(result) {
                var promises = [];
                var defProcessInsertMustSeeAndDo = new $.Deferred();
                var defBookBookMustSeeAndDo = new $.Deferred();
                var defBookMustSeeAndDoAR = new $.Deferred();
                var defBookMustSeeAndDoQR = new $.Deferred();

                promises.push(defProcessInsertMustSeeAndDo);
                promises.push(defBookBookMustSeeAndDo);
                promises.push(defBookMustSeeAndDoAR);
                promises.push(defBookMustSeeAndDoQR);

                //console.log('insertMustSeeAndDo::result.rows.length::'+result.rows.length);

                self.processInsertMustSeeAndDo(result.rows.length, msad, defProcessInsertMustSeeAndDo).then(function() {
                    //console.log('insertMustSeeAndDo::bookid::'+bookid+'::msad.id::'+msad.id+'::self.processInsertMustSeeAndDo::done');
                    self.getBookBookMustSeeAndDo(bookid, msad, defBookBookMustSeeAndDo).then(function() {
                        //console.log('insertMustSeeAndDo::bookid::'+bookid+'::msad.id::'+msad.id+'::self.getBookBookMustSeeAndDo::done');
                        self.getBookMustSeeAndDoAR(msad, defBookMustSeeAndDoAR).then(function() {
                            //console.log('insertMustSeeAndDo::bookid::'+bookid+'::msad.id::'+msad.id+'::self.getBookMustSeeAndDoAR::done');
                            self.getBookMustSeeAndDoQR(msad, defBookMustSeeAndDoQR).then(function() {
                                //console.log('insertMustSeeAndDo::bookid::'+bookid+'::msad.id::'+msad.id+'::self.getBookMustSeeAndDoQR::done');
                                deferred.resolve();
                            });
                        });
                    });
                });

                return $.when.apply(undefined, promises).promise();
            }

            //console.log('self.processBookMustSeeAndDo::bookid::'+bookid+'::msad.id::'+msad.id);

            var query_select = 'SELECT * FROM mustseeanddo WHERE id = ' + msad.id;
            //console.log('self.processBookMustSeeAndDo::query_select::'+query_select);
            return $cordovaSQLite.execute(db, query_select, []).then(insertMustSeeAndDo);
        }

        self.processInsertMustSeeAndDo = function(insert, msad, deferred) {
            var promises = [];
            var defProcessInsertMustSeeAndDo = new $.Deferred();
            promises.push(defProcessInsertMustSeeAndDo);

            var image_ref = (msad.image_reference === undefined)?'':msad.image_reference;

            if (insert == 0) {
                var query_insert_msd_book = 'INSERT INTO mustseeanddo(id, title, summary, image_reference, geographic_position, location_id, qr, ar, price, catalog_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
                var mustseeanddo_data = [msad.id, msad.title, msad.summary, image_ref,
                                            msad.geographic_position, msad.location_id, 
                                            msad.qr, msad.ar, msad.price, msad.catalog_id];
                $cordovaSQLite.execute(db, query_insert_msd_book, mustseeanddo_data).then(function(res_mustseeando) {
                    deferred.resolve();
                    defProcessInsertMustSeeAndDo.resolve();
                });
            }
            else {
                var query_update_msd_book = 'UPDATE mustseeanddo SET title = ?, summary = ?' +
                                            ', image_reference = ? ' +
                                            ', geographic_position = ?, location_id = ?' +
                                            ', qr = ?, ar = ?, price = ?, catalog_id = ? WHERE id = ?';
                var mustseeanddo_data = [msad.title, msad.summary, image_ref,
                                            msad.geographic_position, msad.location_id, 
                                            msad.qr, msad.ar, msad.price, msad.catalog_id, msad.id];
                $cordovaSQLite.execute(db, query_update_msd_book, mustseeanddo_data).then(function() {
                    deferred.resolve();
                    defProcessInsertMustSeeAndDo.resolve();
                });
            }

            return $.when.apply(undefined, promises).promise();
        }

        self.getBookMustSeeAndDoQR = function(mustseeanddo, deferred) {
            //console.log('getBookMustSeeAndDoQR::mustseeanddo.id::' + mustseeanddo.id + '::mustseeanddo.qr::'+mustseeanddo.qr);
            var promises = [];
            var defMustSeeAndDoQR = new $.Deferred();
            promises.push(defMustSeeAndDoQR);

            var query_delete = 'DELETE FROM mustseeanddo_qr WHERE mustseeanddo_id = ' + mustseeanddo.id;
            $cordovaSQLite.execute(db, query_delete, []).then(function(res1) {
                if (mustseeanddo.qr == 1) {
                    var processBookMustSeeAndDoQRJson = function (res) {
                        //console.log(JSON.stringify('getBookMustSeeAndDoQR::mustseeanddo.id::' + mustseeanddo.id + '::res.data::' + res.data));
                        if (res.data.id > 0) {
                            var query_insert_msd_qr = 'INSERT INTO mustseeanddo_qr(id, mustseeanddo_id, title, summary) values (?, ?, ?, ?)';
                            var msd_qr_data = [res.data.id, res.data.mustseeanddo_id,
                                                res.data.title, res.data.summary];
                            $cordovaSQLite.execute(db, query_insert_msd_qr, msd_qr_data).then(function(res_mustseeando_qr) {
                                defMustSeeAndDoQR.resolve();
                                deferred.resolve();
                            });
                        }
                        else {
                            defMustSeeAndDoQR.resolve();
                            deferred.resolve();
                        }
                    };

                    // obtener la data del código QR
                    //console.log('getBookMustSeeAndDoQR::pre SyncronizationProxy.getBookMustSeeAndDoQR::'+mustseeanddo.id);
                    SyncronizationProxy.getBookMustSeeAndDoQR(mustseeanddo.id).then(processBookMustSeeAndDoQRJson);
                }
                else {
                    defMustSeeAndDoQR.resolve();
                    deferred.resolve();
                }
            });
            return $.when.apply(undefined, promises).promise();
        }

        self.getBookMustSeeAndDoAR = function(mustseeanddo, deferred) {
            //console.log('getBookMustSeeAndDoAR::mustseeanddo.id::' + mustseeanddo.id + '::mustseeanddo.ar::'+mustseeanddo.ar);

            var promises = [];
            var defMustSeeAndDoAR = new $.Deferred();
            promises.push(defMustSeeAndDoAR);

            var query_delete = 'DELETE FROM mustseeanddo_ar WHERE mustseeanddo_id = ' + mustseeanddo.id;
            $cordovaSQLite.execute(db, query_delete, []).then(function(res1) {
                if (mustseeanddo.ar == 1) {
                    var processBookMustSeeAndDoARJson = function (res) {
                        //console.log('processBookMustSeeAndDoARJson::mustseeanddo.id::'+mustseeanddo.id+'::res.data.id::'+res.data.id);
                        if (res.data.id > 0) {
                            var query_insert_msd_ar = 'INSERT INTO mustseeanddo_ar(id, mustseeanddo_id, image_preview, image_display, image_binary, is_360, image_360) values (?, ?, ?, ?, ?, ?, ?)';
                            var msd_ar_data = [res.data.id, res.data.mustseeanddo_id,
                                                res.data.image_preview, res.data.image_display, res.data.image_binary,
                                                res.data.is_360, res.data.image_360];
                            $cordovaSQLite.execute(db, query_insert_msd_ar, msd_ar_data).then(function(res_mustseeando_ar) {
                                //console.log('processBookMustSeeAndDoARJson::mustseeanddo.id::'+mustseeanddo.id+'::inserted::'+res_mustseeando_ar.insertId);
                                defMustSeeAndDoAR.resolve();
                                deferred.resolve();
                            });
                        }
                        else {
                            defMustSeeAndDoAR.resolve();
                            deferred.resolve();
                        }
                    };

                    // obtener la data del código QR
                    //console.log('getBookMustSeeAndDoAR::pre SyncronizationProxy.getBookMustSeeAndDoAR::'+mustseeanddo.id);
                    SyncronizationProxy.getBookMustSeeAndDoAR(mustseeanddo.id).then(processBookMustSeeAndDoARJson);
                }
                else {
                    defMustSeeAndDoAR.resolve();
                    deferred.resolve();
                }
            });
            return $.when.apply(undefined, promises).promise();
        }

        self.getBookBookMustSeeAndDo = function(book, mustseeanddo, deferred) {
            //console.log('getBookBookMustSeeAndDo::init::book::'+book+'::mustseeanddo.id::'+mustseeanddo.id);
            var promises = [];
            var defBookBookMustSeeAndDo = new $.Deferred();
            promises.push(defBookBookMustSeeAndDo);

            if(book != null) {
                self.existsBookMustSeeAndDoById(book, mustseeanddo.id).then(function(result_exists) {
                    if (result_exists.rows.length == 0) {
                        var query_insert_book_mustseeanddo = 'INSERT INTO book_mustseeanddo(mustseeanddo_id, book_id) values (?, ?)';
                        var book_mustseeanddo_obj = [mustseeanddo.id, book];
                        //console.log('getBookBookMustSeeAndDo::book_mustseeanddo_obj::'+JSON.stringify(book_mustseeanddo_obj));
                        $cordovaSQLite.execute(db, query_insert_book_mustseeanddo, book_mustseeanddo_obj).then(function() {
                            //console.log('getBookBookMustSeeAndDo::insert::done::book::'+book+'::mustseeanddo.id::'+mustseeanddo.id);
                            defBookBookMustSeeAndDo.resolve();
                            deferred.resolve();
                        });
                    }
                    else {
                        defBookBookMustSeeAndDo.resolve();
                        deferred.resolve();
                    }
                });
            }
            else {
                defBookBookMustSeeAndDo.resolve();
                deferred.resolve();
            }

            return $.when.apply(undefined, promises).promise();
        }

        self.existsBookMustSeeAndDoById = function(bookid, mustseeanddoid) {
            var query = 'SELECT * FROM book_mustseeanddo where book_id = ? and mustseeanddo_id = ?';
            //console.log('existsMustSeeAndDoById::query::'+query);
            return $cordovaSQLite.execute(db, query, [bookid, mustseeanddoid]);
        }

        //Method to get the book detail must see and do.
        self.getBookMustSeeAndDoDetail = function(bookid, messages) {
            var processBookMustSeeAndDoDetailJson = function (res) {
                var promises = [];

                var defProcessBookMustSeeAndDoDetailJson = new $.Deferred();
                promises.push(defProcessBookMustSeeAndDoDetailJson);

                //console.log('processBookMustSeeAndDoDetailJson::bookid::'+bookid+'::'+JSON.stringify(res.data));
                $.each(res.data.mustseeanddo_detail, function(i, mustseeanddo_detail) {

                    var defBookMustSeeAndDoDetail = new $.Deferred();
                    promises.push(defBookMustSeeAndDoDetail);

                    self.processBookMustSeeAndDoDetail(mustseeanddo_detail, defBookMustSeeAndDoDetail);
                });

                defProcessBookMustSeeAndDoDetailJson.resolve();
                return $.when.apply(undefined, promises).promise();
            };

            return SyncronizationProxy.getBookMustSeeAndDoDetail(bookid).then(processBookMustSeeAndDoDetailJson);
        };

        // Method to insert data into must see and do detail tablemsadd
        self.processBookMustSeeAndDoDetail = function(msadd, deferred) {

            var insertMustSeeAndDoDetail = function(result) {

                //console.log('insertMustSeeAndDoDetail::init::msadd.id::'+msadd.id);
                var promises = [];
                var defInsertMustSeeAndDoDetail = new $.Deferred();
                promises.push(defInsertMustSeeAndDoDetail);

                if (result.rows.length == 0) {
                    var query_insert_msddetail = "INSERT INTO mustseeanddo_detail (id, mustseeanddo_id, title, summary) VALUES (?,?,?,?)";
                    var mustseeanddo_detail_data = [msadd.id, msadd.mustseeanddo_id, msadd.title, msadd.summary];
                    $cordovaSQLite.execute(db, query_insert_msddetail, mustseeanddo_detail_data).then(function(res_mustseeanddo_detail) {
                        defInsertMustSeeAndDoDetail.resolve();
                        deferred.resolve();
                    });
                }
                else {
                    var query_update_msddetail = 'UPDATE mustseeanddo_detail SET mustseeanddo_id = ?, title = ?, summary = ? WHERE id = ?';
                    var mustseeanddo_detail_data = [msadd.mustseeanddo_id, msadd.title, msadd.summary, msadd.id];
                    $cordovaSQLite.execute(db, query_update_msddetail, mustseeanddo_detail_data).then(function() {
                        defInsertMustSeeAndDoDetail.resolve();
                        deferred.resolve();
                    });
                }

                return $.when.apply(undefined, promises).promise();
            }

            //console.log('self.processBookMustSeeAndDoDetail::'+msadd.id);
            var query_select = 'SELECT * FROM mustseeanddo_detail where id = ' + msadd.id;
            return $cordovaSQLite.execute(db, query_select, []).then(insertMustSeeAndDoDetail);
        }

        //Method to get the book state.
        self.getBookStates = function(bookid, messages) {
            var processStateJson = function (res) {
                var promises = [];

                var defProcessStateJson = new $.Deferred();
                promises.push(defProcessStateJson);

                $.each(res.data.states, function(i, state) {

                    var defBookStates = new $.Deferred();
                    promises.push(defBookStates);

                    self.processState(state, defBookStates);
                });

                defProcessStateJson.resolve();
                return $.when.apply(undefined, promises).promise();
            };

            return SyncronizationProxy.getBookStates(bookid).then(processStateJson);
        };

        // Method to insert data into states table
        self.processState = function(state, deferred) {
            var insertState = function(result) {
                //console.log('insertState::result.rows.length::'+result.rows.length);

                var promises = [];
                var defInsertState = new $.Deferred();
                promises.push(defInsertState);

                if (result.rows.length == 0) {
                    var query_insert_state = 'INSERT INTO state (id, name, country_id) VALUES (?, ?, ?);';
                    var state_data = [state.id,  state.name,  state.country_id];
                    $cordovaSQLite.execute(db, query_insert_state, state_data).then(function(res1) {
                        //console.log('insertState::res1::'+res1.insertId+'::done');
                        defInsertState.resolve();
                        deferred.resolve();
                    });
                }
                else {
                    var query_update_state = 'UPDATE state SET name = ?, country_id = ? WHERE id = ?';
                    var state_data = [state.name,  state.country_id,  state.id];
                    $cordovaSQLite.execute(db, query_update_state, state_data).then(function() {
                        //console.log('insertState::update::done');
                        defInsertState.resolve();
                        deferred.resolve();
                    });
                }

                return $.when.apply(undefined, promises).promise();
            }

            //console.log('self.processState::'+state.id);
            var query_select = 'SELECT * FROM state where id = ' + state.id;
            return $cordovaSQLite.execute(db, query_select, []).then(insertState);
        }

        self.getStateMsadRecommended = function() {

            var processAllState = function(result) {
                var promises = [];
                var defGetStateMsadRecommended = new $.Deferred();
                promises.push(defGetStateMsadRecommended);

                for (var i = 0; i < result.rows.length; i++) {
                    var defState = new $.Deferred();
                    promises.push(defState);
                    var state = result.rows.item(i);

                    $.queue('allState', self.processStateRecommended(state, defState));
                }
                $.dequeue('allState');

                defGetStateMsadRecommended.resolve();
                return $.when.apply(undefined, promises).promise();
            }

            var query_select = 'SELECT * FROM state';
            return $cordovaSQLite.execute(db, query_select, []).then(processAllState);
        }

        self.processStateRecommended = function(state, deferred) {
            //console.log('self.processStateRecommended::state.id::' + state.id);

            var promises = [];
            var defProcessStateRecommended = new $.Deferred();
            promises.push(defProcessStateRecommended);

            var query_delete = 'DELETE FROM state_recommended_mustseeanddo WHERE state_id = ?';
            $cordovaSQLite.execute(db, query_delete, [state.id]).then(function() {
                var processBookStateMsadRecommendeds = function (res) {
                    //console.log('processBookStateMsadRecommendeds::state.id::'+state.id);

                    var defEachRecommendedBookMustSeeAndDo = new $.Deferred();
                    promises.push(defEachRecommendedBookMustSeeAndDo);
                    self.eachRecommendedBookMustSeeAndDo(res.data).then(function() {
                        //console.log('processBookStateMsadRecommendeds::state.id::'+state.id+'::self.eachRecommendedBookMustSeeAndDo::done');
                        defEachRecommendedBookMustSeeAndDo.resolve();

                        var defEachRecommendedBookMustSeeAndDoDetail = new $.Deferred();
                        promises.push(defEachRecommendedBookMustSeeAndDoDetail);
                        self.eachRecommendedBookMustSeeAndDoDetail(res.data).then(function() {
                            //console.log('processBookStateMsadRecommendeds::state.id::'+state.id+'::self.eachRecommendedBookMustSeeAndDoDetail::done');
                            defEachRecommendedBookMustSeeAndDoDetail.resolve();

                            var defEachRecommendedStateMustSeeAndDo = new $.Deferred();
                            promises.push(defEachRecommendedStateMustSeeAndDo);
                            self.eachRecommendedStateMustSeeAndDo(res.data).then(function() {
                                //console.log('processBookStateMsadRecommendeds::state.id::'+state.id+'::self.eachRecommendedStateMustSeeAndDo::done');
                                defEachRecommendedStateMustSeeAndDo.resolve();
                                deferred.resolve();
                            });
                        });
                    });

                    defProcessStateRecommended.resolve();
                };

                SyncronizationProxy.getStatesMustSeeAndDoRecommended(state.id).then(processBookStateMsadRecommendeds,
                    function(error) {
                        defProcessStateRecommended.reject(error);
                        deferred.reject(error);
                    });
            });
            return $.when.apply(undefined, promises).promise();
        }

        self.eachRecommendedBookMustSeeAndDo = function(data) {
            var promises = [];
            var defEachRecommended = new $.Deferred();
            promises.push(defEachRecommended);

            $.each(data.mustSeeAndDoResponses, function(j, mustSeeAndDo) {
                var defRecommendedMustSeeAndDo = new $.Deferred();
                promises.push(defRecommendedMustSeeAndDo);

                self.processBookMustSeeAndDo(null, mustSeeAndDo, defRecommendedMustSeeAndDo);
            });

            defEachRecommended.resolve();
            return $.when.apply(undefined, promises).promise();
        }

        self.eachRecommendedBookMustSeeAndDoDetail = function(data) {
            var promises = [];
            var defEachRecommendedBookMustSeeAndDoDetail = new $.Deferred();
            promises.push(defEachRecommendedBookMustSeeAndDoDetail);

            $.each(data.mustSeeAndDoDetailResponses, function(i, mustseeanddo_detail) {
                var defBookMustSeeAndDoDetail = new $.Deferred();
                promises.push(defBookMustSeeAndDoDetail);

                self.processBookMustSeeAndDoDetail(mustseeanddo_detail, defBookMustSeeAndDoDetail);
            });
            defEachRecommendedBookMustSeeAndDoDetail.resolve();
            return $.when.apply(undefined, promises).promise();
        }

        self.eachRecommendedStateMustSeeAndDo = function(data) {
            var promises = [];
            var defEachRecommendedStateMustSeeAndDo = new $.Deferred();
            promises.push(defEachRecommendedStateMustSeeAndDo);

            $.each(data.stateMustSeeAndDoRecommendedResponses, function(i, destiny) {
                var defRecommendedDestiny = new $.Deferred();
                promises.push(defRecommendedDestiny);

                self.processRecommendedDestiny(destiny, defRecommendedDestiny);
            });

            defEachRecommendedStateMustSeeAndDo.resolve();
            return $.when.apply(undefined, promises).promise();
        }

        self.processRecommendedDestiny = function(destiny, deferred) {
            var query_insert_destiny = 'INSERT INTO state_recommended_mustseeanddo(id, mustseeanddo_id, state_id) values (?, ?, ?)';
            var destiny_data = [destiny.id, destiny.mustseeanddo_id, destiny.state_id];
            return $cordovaSQLite.execute(db, query_insert_destiny, destiny_data).then(function(res_mustseeando_qr) {
                deferred.resolve();
            });
        }

        //Method to get the book country.
        self.getBookCountries = function(bookid, messages) {
            var processCountryJson = function (res) {
                var promises = [];

                var defProcessCountryJson = new $.Deferred();
                promises.push(defProcessCountryJson);

                $.each(res.data.countries, function(i, country) {

                    var defBookCountries = new $.Deferred();
                    promises.push(defBookCountries);

                    self.processCountry(country, defBookCountries);
                });

                defProcessCountryJson.resolve();
                return $.when.apply(undefined, promises).promise();
            };

            return SyncronizationProxy.getBookCountries(bookid).then(processCountryJson);
        };

        // Method to insert data into country table
        self.processCountry = function(country, deferred) {
            var insertCountry = function(result) {
                var promises = [];
                var defInsertCountry = new $.Deferred();
                promises.push(defInsertCountry);

                if (result.rows.length == 0) {
                    var query_insert_country = 'INSERT INTO country (id, name) VALUES (?, ?)';
                    var country_data = [country.id, country.name];
                    $cordovaSQLite.execute(db, query_insert_country, country_data).then(function() {
                        defInsertCountry.resolve();
                        deferred.resolve();
                    });
                }
                else {
                    var query_update_country = 'UPDATE country SET name = ? WHERE id = ?';
                    var country_data = [country.name, country.id];
                    $cordovaSQLite.execute(db, query_update_country, country_data).then(function() {
                        defInsertCountry.resolve();
                        deferred.resolve();
                    });
                }
                return $.when.apply(undefined, promises).promise();
            }
            //console.log('self.processCountry::'+country.id);
            var query_select = 'SELECT * FROM country where id = ' + country.id;
            return $cordovaSQLite.execute(db, query_select, []).then(insertCountry);
        }


        self.updateStatusLogin = function (username) {
            //Actualizo el estatus de login
            var query_update = 'UPDATE user SET login_state = 1 where username = ?';
            return $cordovaSQLite.execute(db, query_update, [username]);
        };

        self.setUserLanguage = function (language)
        {
            //console.log("Asignado el lenguaje de la aplicacion " + language);
            SessionFactory.set("language", language);
            $translate.use(language);
        }

        //Method to send user password.
        self.recoveryPassword = function (username, messages) {
            var userdata = {};
            var success = function (res) {

                var promises = [];

                if (res.rows.length > 0) {
                    if ($rootScope.isOnline) {
                        var defRecoveryPassword = new $.Deferred();
                        promises.push(defRecoveryPassword);
            
                        userdata.username = res.rows.item(0).username;
                        userdata.password = res.rows.item(0).password;

                        UserProxy.recoveryPassword(userdata).then(function() {
                            messages.success.push($filter('translate')('messagessuccess-recoverypassword'));
                            defRecoveryPassword.resolve();
                        }, function() {
                            messages.error.push($filter('translate')('messageserror-recoverypassword'));
                            defRecoveryPassword.resolve();
                        });
                    }
                    else {
                        messages.error.push($filter('translate')('messageserror-recovery-no-internet'));
                    }
                }
                else {
                    messages.error.push($filter('translate')('messageserror-usernotregister'));
                }

                return $.when.apply(undefined, promises).promise();
            };
            var query_select = "SELECT username, password from user WHERE username = ?";
            return $cordovaSQLite.execute(db, query_select, [username]).then(success);
        }

        self.synchronizeData = function(userLogged, messages) {

            var promises = [];
            //console.log('self.synchronizeData::init');

            var username = userLogged.username;
            var userid = userLogged.id;

            var defGetBooks = new $.Deferred();
            var defGetUserBooks = new $.Deferred();
            var defGetStateMsadRecommended = new $.Deferred();
            var defGetUserTravels = new $.Deferred();
            var defGetUserTracking = new $.Deferred();
            var defGetUserData = new $.Deferred();

            promises.push(defGetBooks);
            promises.push(defGetUserBooks);
            promises.push(defGetStateMsadRecommended);
            promises.push(defGetUserTravels);
            promises.push(defGetUserTracking);
            promises.push(defGetUserData);

            //Get Books
            // buscando los libros turísticos registrados,
            // estos registros se guardan en la tabla book, si no existe se inserta, en caso de existir
            // entonces se procede a actualizar el registro
            self.getBooks(messages).then(function() {
                //console.log('self.getBooks::done');
                defGetBooks.resolve();

                //Get the user books
                // Se obtienen todos los libros que ha descargado el usuario, y se procede 
                // a procesarlos. Luego son insertados en la tabla user_book.
                // Para realizar esta inserción se hacen los siguientes procesamientos:
                // - obtener las localidades de cada libro descargado, si no existe en la
                // tabla location se procede a insertar el registro
                // - obtener los sitios turísticos (mustseeanddo) de cada libro descargado,
                // si no existe en la tabla mustseeanddo se procede a insertar el registro
                // - obtener el detalle de los sitios turísticos (mustseeanddo) de cada libro descargado,
                // si no existe en la tabla mustseeanddo_detail se procede a insertar el registro
                // - obtener el código qr de los sitios turísticos (mustseeanddo_qr) de cada libro descargado,
                // si no existe en la tabla mustseeanddo_qr se procede a insertar el registro
                // - obtener la imagen ar de los sitios turísticos (mustseeanddo_ar) de cada libro descargado,
                // si no existe en la tabla mustseeanddo_ar se procede a insertar el registro
                // - obtener los estados de cada libro descargado,
                // si no existe en la tabla state se procede a insertar el registro
                // - obtener los países de cada libro descargado,
                // si no existe en la tabla country se procede a insertar el registro
                self.getUserBooks(username, userid, messages).then(function() {
                    //console.log('self.getUserBooks::done');
                    defGetUserBooks.resolve();

                    // obtener los destinos recomendados de todos los estados
                    self.getStateMsadRecommended().then(function() {
                        //console.log('self.getStateMsadRecommended::done');
                        defGetStateMsadRecommended.resolve();

                        //Get the user travels
                        self.getUserTravels(userLogged.username, userLogged.id, messages).then(function() {
                            //console.log('self.getUserTravels::done');
                            defGetUserTravels.resolve();

                            //Get the user trackings
                            self.getUserTrackings(userLogged.username, userLogged.id, messages).then(function() {
                                //console.log('self.getUserTrackings::done');
                                defGetUserTracking.resolve();

                                //update status login
                                self.updateStatusLogin(userLogged.username).then(function() {
                                    //console.log('self.updateStatusLogin::done');
                                    //console.log('self.synchronizeData::end');
                                    defGetUserData.resolve();
                                }, function(error) {
                                    //console.log('self.updateStatusLogin::ERROR');
                                    defGetUserData.resolve();
                                });
                            }, function(error) {
                                //console.log('self.getUserTrackings::ERROR');
                                defGetUserTracking.resolve();
                                defGetUserData.resolve();
                            })
                        }, function(error) {
                            //console.log('self.getUserTravels::ERROR');
                            defGetUserTravels.resolve();
                            defGetUserTracking.resolve();
                            defGetUserData.resolve();
                        });
                    }, function(error) {
                        //console.log('self.getStateMsadRecommended::ERROR');
                        defGetStateMsadRecommended.resolve();
                        defGetUserTravels.resolve();
                        defGetUserTracking.resolve();
                        defGetUserData.resolve();
                    });
                }, function(error) {
                    //console.log('self.getUserBooks::ERROR');
                    defGetUserBooks.resolve();
                    defGetStateMsadRecommended.resolve();
                    defGetUserTravels.resolve();
                    defGetUserTracking.resolve();
                    defGetUserData.resolve();
                });
            }, function(error) {
                //console.log('self.getBooks::ERROR');
                defGetBooks.resolve();
                defGetUserBooks.resolve();
                defGetStateMsadRecommended.resolve();
                defGetUserTravels.resolve();
                defGetUserTracking.resolve();
                defGetUserData.resolve();
            });

            return $.when.apply(undefined, promises).promise();
        }

        self.updateImage = function (user) {
            return daouser.updateImage(user);
        };

        self.showPage = function (page) {
            //console.log('self.showPage::page::'+page);

            var user = SessionFactory.getObject("userLogged");

            // validate if user is logged
            if (self.isUserLogged()) {
                //console.log('self.showPage::user LOGGED');
                $ionicHistory.clearCache();
                $ionicHistory.clearHistory();
                $ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});

                var url = 'app.tabs.dashboard';
                if (page && page != '') {
                    url = 'app.tabs.' + page;
                }
                $state.go(url);
            }
            else {
                //console.log('self.showPage::user NOT LOGGED');
                $state.go('start', { page: page });
            }
        };

    }

})();
