'use strict';
(function ()
{
    angular.module('cym').service('DashboardService', ['$filter', 'UserProxy', DashboardService]);

    function DashboardService($filter, UserProxy)
    {
		var self = this;
        var friends = [];

        self.setFriends = function (data)
        {
            friends = data;
        };

        self.getFriends = function ()
        {
            return friends;
        };

        self.inviteFriends = function (userLogged, messages) {
            var promises = [];
            //console.log('self.inviteFriends::userLogged::'+JSON.stringify(userLogged)+'::friends::'+JSON.stringify(friends));
            if (friends.length > 0) {
                var defInviteFriends = new $.Deferred();

                promises.push(defInviteFriends);

                var inviteFriendsData = {};
                inviteFriendsData.user = {};
                inviteFriendsData.user.username = userLogged.username;
                inviteFriendsData.user.name = userLogged.name;
                inviteFriendsData.user.lastname = userLogged.lastname;
                inviteFriendsData.emailFriends = friends;
                //console.log('self.inviteFriends::inviteFriendsData::'+JSON.stringify(inviteFriendsData));

                UserProxy.inviteFriends(inviteFriendsData).then(function(res) {
                    //console.log('self.inviteFriends::post::UserProxy.inviteFriends::res::'+JSON.stringify(res));
                    messages.success.push($filter('translate')('messagessuccess-sentinvite'));
                    defInviteFriends.resolve();
                    friends = [];
                }, function() {
                    messages.error.push($filter('translate')('messageserror-noinvitesent'));
                    defInviteFriends.resolve();
                    friends = [];
                });

            }
            return $.when.apply(undefined, promises).promise();
        }
    }
})
();
