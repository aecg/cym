/**
 * Created by poncbi on 09/12/2015.
 */
'use strict';

(function ()
{
    angular.module('cym').service('TrackingService', ['$cordovaSQLite', '$filter', TrackingService]);

    function TrackingService($cordovaSQLite, $filter)
    {
        var self = this;

        var allTracks = {
            tracks: [],
            total: 0
        };

        var trackingTypes = [];

        this.allTracks = function ()
        {
            return allTracks;
        };

        this.initAllTrackingTypes = function() {
            trackingTypes = [];
            trackingTypes.push({id: 1, src: "img/maps/icon1.png"});
            trackingTypes.push({id: 2, src: "img/maps/icon2.png"});
            trackingTypes.push({id: 3, src: "img/maps/icon21.png"});
            trackingTypes.push({id: 4, src: "img/maps/icon23.png"});
            trackingTypes.push({id: 5, src: "img/maps/icon32.png"});
            trackingTypes.push({id: 6, src: "img/maps/icon39.png"});
            trackingTypes.push({id: 7, src: "img/maps/icon48.png"});
            trackingTypes.push({id: 8, src: "img/maps/icon5.png"});
            trackingTypes.push({id: 9, src: "img/maps/icon54.png"});
            trackingTypes.push({id: 10, src: "img/maps/icon19.png"});
            trackingTypes.push({id: 11, src: "img/maps/icon20.png"});
            trackingTypes.push({id: 12, src: "img/maps/icon22.png"});
            trackingTypes.push({id: 13, src: "img/maps/icon3.png"});
            trackingTypes.push({id: 14, src: "img/maps/icon55.png"});
            trackingTypes.push({id: 15, src: "img/maps/icon4.png"});
            trackingTypes.push({id: 16, src: "img/maps/icon49.png"});
            trackingTypes.push({id: 17, src: "img/maps/icon50.png"});
            trackingTypes.push({id: 18, src: "img/maps/icon0.png"});
            trackingTypes.push({id: 19, src: "img/maps/icon6.png"});
            trackingTypes.push({id: 20, src: "img/maps/icon18.png"});
        }

        self.getAllTrackingTypes = function ()
        {
            return trackingTypes;
        };

        self.getTrackingType = function(name) {
            //console.log('self.getTrackingType::name::'+name);
            return $.grep(trackingTypes, function(t) {
                //console.log('self.getTrackingType::t.id::'+t.id);
                return t.id === parseInt(name);
            });
        };

        this.clearAllTracks = function ()
        {
            allTracks = {
                tracks: [],
                total: 0
            };
        };

        var allCuepoints = {
            cuepoints: [],
            name: "",
            id: 0,
            total: 0,
            count: 0
        };

        this.setAllCuepoints = function (data) {
            allCuepoints = data;
        };
        this.getAllCuepoints = function () {
            return allCuepoints;
        };


        /**
         * Metodo que elimina de la memoria todos los datos del seguimiento
         */
        this.clearAllCuepoints = function ()
        {
            allCuepoints = {
                cuepoints: [],
                name: "",
                id: 0,
                total: 0,
                count: 0
            };
        };


        /**
         * Metodo que elimina la lista de locaciones al seguimiento en memoria
         */
        this.clearAllCuepointsList = function ()
        {
            allCuepoints.cuepoints = [];
            allCuepoints.total = 0;
            allCuepoints.count = 0;
        };


        /**
         * Metodo que almacena la informacion de una locacion de un seguimiento
         * @param id identificador del seguimiento
         * @param tracklocation objeto con los datos de la locacion
         * @param messages objeto que gestiona los mensajes de la aplicacion
         */
        this.addTrackLocation = function (id, tracklocation, messages)
        {
            var date = new Date();
            var dateTime = date.getTime();

            /**
             * Metodo que se ejecuta cuando el almacenamiento es exitoso
             * @param res respuesta de la funcion
             */
            var success = function (res)
            {
                //console.log('addTrackLocation::success::allCuepoints::'+JSON.stringify(allCuepoints));
                //console.log('localizacion guardado, con id: ' + res.insertId);
                var objeto = {};
                objeto.cuepointid = res.insertId;
                objeto.cuepointdesc = tracklocation.description;
                objeto.cuepointname = self.getTrackingType(parseInt(tracklocation.name))[0].src;
                objeto.cuepointdate = dateTime;
                allCuepoints.cuepoints.push(objeto);
                allCuepoints.total++;
                //console.log('addTrackLocation::success::allCuepoints::'+JSON.stringify(allCuepoints));
            };


            /**
             * Metodo que se ejecuta cuando el almacenamiento no es exitoso
             * @param res respuesta de la informacion
             */
            var error = function (res)
            {
                //console.log('Error guardando la localizacion ' + res.insertId);
                messages.error.push($filter('translate')('messageserror-savedocument'));
                //messages.error.push('Error al guardar el documento');
            };


            var insert_tracklocation = "INSERT INTO cuepoint (name, description, geographic_position, date, tracking_id, deleted)" +
                " VALUES (?, ?, ?, ?, ?, ? )";
            return $cordovaSQLite.execute(db, insert_tracklocation,
                [tracklocation.name, tracklocation.description, tracklocation.currentPosition, dateTime, id, 0]).then(success,
                error);
        };


        /**
         * Metodo que inserta el tracking en la base de datos
         * @param tracking datos del tracking
         * @param messages mensaje de respuesta
         */
        this.addTracking = function (tracking, messages)
        {

            var date = new Date();
            var dateTime = date.getTime();


            /**
             * Metodo que se ejecuta cuando el tracking se inserta de manera adecuada
             * @param res respuesta de la insersion
             */
            var success = function (res)
            {
                if (res.insertId !== 0)
                {
                    var objeto = {};
                    objeto.id = res.insertId;
                    objeto.name = tracking.trackingname;
                    objeto.created_at = dateTime;

                    if (tracking.locationname != null)
                    {

                        var insert_tracklocation = "INSERT INTO cuepoint (name, description, geographic_position, date, " +
                            "tracking_id, deleted) VALUES (?, ?, ?, ?, ?, ?)";
                        $cordovaSQLite.execute(db, insert_tracklocation,
                            [tracking.locationname, tracking.locationdes, tracking.currentPosition, dateTime, res.insertId,0]);
                    }
                }
            };


            /**
             * Metodo que se ejecuta cuando ocurre un error insertando el tracking
             * @param res respuesta de error del tracking
             */
            var error = function (res)
            {
                messages.error.push($filter('translate')('messageserror-savedocument'));
                //messages.error.push('Error al guardar el documento');
            };


            var strInsert = "INSERT INTO tracking (name, created_at, user_id, deleted) VALUES (?, ?, ?, ?)";
            $cordovaSQLite.execute(db, strInsert, [tracking.trackingname, dateTime, tracking.user_id, 0]).then(success,
                error);
        };


        /**
         * Metodo que obtiene los seguimientos de un usuario
         * @param id identificador del usuario
         * @param messages objeto para la gestion de mensajes
         */
        this.getTrackingByUser = function (id, messages)
        {

            /**
             * Metodo que se ejecuta cuando la consulta es exitosa
             * @param res obj con la respuesta del query
             */
            var success = function (res)
            {
                if (res.rows.length > 0)
                {
                    for (var i = 0; i < res.rows.length; i++)
                    {
                        allTracks.tracks.push(res.rows.item(i));
                    }
                    allTracks.total = res.rows.length;
                }
            };


            /**
             * Metodo que se ejecuta cuando la consulta es no exitosa
             * @param res obj con la respuesta del query
             */
            var error = function (res)
            {
                messages.error.push($filter('translate')('messageserror-gettracking'));
                //messages.error.push('Error al buscar seguimientos usuario');
            };


            var select_trackingbyuser = 'SELECT * FROM tracking WHERE user_id = ' + id + ' and deleted = 0';
            $cordovaSQLite.execute(db, select_trackingbyuser, []).then(success, error);

        };

        this.getTrackingsForMap = function (id, trackingTypes)
        {
            /**
             * Metodo que se ejecuta cuando la consulta es exitosa
             * @param res obj con la respuesta del query
             */
            var success = function (res) {
                //console.log('this.getTrackingsForMap::success::res.rows.length::'+res.rows.length);
                var promises = [];
                var defGetTrackingByUser = new $.Deferred();
                promises.push(defGetTrackingByUser);

                if (res.rows.length > 0) {

                    for (var i = 0; i < res.rows.length; i++) {
                        var defGetCuepointsByTracking = new $.Deferred();
                        promises.push(defGetCuepointsByTracking);
                        var track = res.rows.item(i);
                        $.queue('cuepoints', self.getCuepointsByTracking(track, trackingTypes, defGetCuepointsByTracking));
                    }
                    $.dequeue('cuepoints');
                    allTracks.total = res.rows.length;
                }
                defGetTrackingByUser.resolve();

                return $.when.apply(undefined, promises).promise();
            };

            this.initAllTrackingTypes();
            //console.log('this.getTrackingsForMap::id::'+id);
            var select_trackingbyuser = 'SELECT id, name FROM tracking WHERE user_id = ? and deleted = 0';
            return $cordovaSQLite.execute(db, select_trackingbyuser, [id]).then(success);

        };

        /**
         * Metodo que obtiene las locaciones de un seguimiento
         * @param id identificador del seguimiento
         * @param message objeto que gestiona los mensajes
         */
        self.getCuepointsByTracking = function (track, trackingTypes, deferred) {
            var success = function (res) {
                var promises = [];
                var defCuepointsByTracking = new $.Deferred();
                promises.push(defCuepointsByTracking);

                //console.log('self.getCuepointsByTracking::res.rows.length::'+res.rows.length);
                if (res.rows.length > 0) {
                    track.cuepoints = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        var cuepoint = res.rows.item(i);
                        //console.log('self.getCuepointsByTracking::cuepoint.name::'+cuepoint.name);
                        cuepoint.name = self.getTrackingType(parseInt(cuepoint.name))[0].src;
                        track.cuepoints.push(cuepoint);
                    }
                    allTracks.tracks.push(track);
                }
                defCuepointsByTracking.resolve();
                deferred.resolve();

                return $.when.apply(undefined, promises).promise();
            };

            var select_cuepointbytrack = 'SELECT id, name, geographic_position, description FROM cuepoint WHERE tracking_id = ? AND deleted = 0';
            //console.log('self.getCuepointsByTracking::track.id::'+track.id+'::select_cuepointbytrack::'+select_cuepointbytrack);
            return $cordovaSQLite.execute(db, select_cuepointbytrack, [track.id]).then(success);
        };

        /**
         * Metodo que obtiene las locaciones de un seguimiento
         * @param id identificador del seguimiento
         * @param message objeto que gestiona los mensajes
         */
        this.getCuePointByTrack = function (id, success, error)
        {
            var self = this;
            var select_cuepointbytrack = 'select id as cuepointid, name as cuepointname, geographic_position as position, date as cuepointdate, description as cuepointdesc from cuepoint where tracking_id = ' + id + ' and deleted = 0';
            //console.log('tracking.js::this.getCuePointByTrack::select_cuepointbytrack::'+select_cuepointbytrack);
            $cordovaSQLite.execute(db, select_cuepointbytrack, []).then(success, error);
        };

        //Method that update a user tracking
        this.updateTracking = function (data, userid, messages)
        {
            var self = this;

            var success = function (res)
            {
                self.clearAllTracks();
                self.getTrackingByUser(userid, messages);
            };


            var error = function (res)
            {
                messages.error.push($filter('translate')('messageserror-updatedocument'));
                //messages.error.push('Error al actualizar el documento');
            };

            var update_doc = 'UPDATE tracking SET name = ? WHERE id = ?';
            var data_tracking = [data.name, data.id];
            $cordovaSQLite.execute(db, update_doc, data_tracking).then(success, error);
        };


        /**
         * Metodo elimina una locacion
         * @param id identificador de la locacion
         * @param trackingid identificador del seguimiento
         * @param userid identificador del usuario
         * @param messages objeto para manipular los mensajes
         */
        this.deleteCuePoint = function (id,success, error)
        {

            var delete_cuepoint = 'UPDATE cuepoint SET deleted = 1 WHERE id = ' + id;
            $cordovaSQLite.execute(db, delete_cuepoint, []).then(success, error);

        };


        /**
         * Metodo que elimina un seguimiento
         * @param track objeto con la informacion del seguimiento
         * @param messages objeto para la gestion de mensajes
         */
        this.deleteTracking = function (track, messages)
        {
            var self = this;

            /**
             * Metodo que se ejecuta cuando la eliminacion es exitosa
             * @param res objeto con la respuesta del query
             */
            var success = function (res)
            {
                self.clearAllTracks();
                self.getTrackingByUser(track.user_id, messages);
            };


            /**
             * Metodo que se ejecuta cuando la eliminacion no es exitosa
             * @param res respuesta del manejador
             */
            var error = function (res)
            {
                messages.error.push($filter('translate')('messageserror-deletedocument'));
                //messages.error.push('Error al eliminar el documento');
            };


            var delete_tracking = 'UPDATE tracking SET deleted = 1 WHERE id = ' + track.id;
            $cordovaSQLite.execute(db, delete_tracking, []);


            var delete_cuepoint = 'UPDATE cuepoint SET deleted = 1  WHERE tracking_id = ' + track.id;
            $cordovaSQLite.execute(db, delete_cuepoint, []).then(success, error);
        };


        this.getTrackingInfo = function(id, success, error)
        {
            var select_tracking = 'select a.id as trackingid, a.name as trackingname from tracking a where a.id = ' + id + ' and a.deleted = 0';
            //console.log('tracking.js::this.getTrackingInfo::select_tracking::'+select_tracking);
            $cordovaSQLite.execute(db, select_tracking, []).then(success, error);
        }

    }
})();
