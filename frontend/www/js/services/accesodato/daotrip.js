/**
 * Created by Leonor Guzman on 12/8/2015.
 */
'use strict';
(function () {
    angular.module('cym').service('DaoTrip', ['$cordovaSQLite','$filter', DaoTrip]);

    function DaoTrip($cordovaSQLite, $filter) {


        /**
         * Nombre:                  selectTripId
         * Descripcion:              metodo que ubica por id
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/9/2015
         *
         */
        this.selectTripId = function (id) {
            var query = 'SELECT * FROM travel WHERE id = ' + id + ' AND deleted = 0';
            //console.log('selectTripId::query::'+query);
            return $cordovaSQLite.execute(db, query, []);
        };


        /**
         * Nombre: insertTrip
         * Descripcion:             metodo que sirve para la inserccion de destiny
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/8/2015
         *
         * @param data array con parametros a insertar
         * @returns {*}
         */
        this.insertTrip = function (data) {
            var query = 'INSERT INTO travel (name, budget, dateGoing, dateArrival, description, country_id, user_id, deleted) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?)';
            //console.log('insertTrip::query::'+query);
            return $cordovaSQLite.execute(db, query, [data.name, data.budget, data.dateGoing, data.dateArrival, data.description, data.country.id, data.user_id, 0]);
        };


        /**
         * Nombre:  selectAllTrip
         * Descripcion:             metodo que obtiene todos los destinos del usuario
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/8/2015
         *
         * @returns {*}
         */
        this.selectAllTrip = function (AllTravelsUser) {

            var success = function (res) {
                for (var i = 0; i < res.rows.length; i++) {
                    AllTravelsUser.travels.push(res.rows.item(i));
                }
            };

            AllTravelsUser.travels = [];
            var query = 'SELECT * FROM travel WHERE user_id = ' + AllTravelsUser.user_id + ' AND deleted = 0';
            //console.log('selectAllTrip::query::'+query);
            return $cordovaSQLite.execute(db, query, []).then(success);
        };


        /**
         * Nombre:                  deleteTrip
         * Descripcion:             metodo que realiza la eliminacion del destiny
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/8/2015
         *
         * @param data
         * @returns {*}
         */
        this.removeTrip = function (data) {

            var query_mustSeeAndDotravel = 'UPDATE travel set deleted = 1 WHERE id = '+data.id;
            //console.log('removeTrip::query_mustSeeAndDotravel::'+query_mustSeeAndDotravel);
            return $cordovaSQLite.execute(db, query_mustSeeAndDotravel, []).then(function() {
                var query_mustSeeAndDo = 'UPDATE travel_mustseeanddo set deleted = 1 WHERE travel_id = ' + data.id;
                //console.log('removeTrip::query_mustSeeAndDo::'+query_mustSeeAndDo);
                return $cordovaSQLite.execute(db, query_mustSeeAndDo, []);
            });
        };


        /**
         * Nombre:                  toUpdateTrip
         * Descripcion:             metodo que actualiza el viaje
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/9/2015
         *
         * @param data datos a actualizar
         * @returns {*}
         */
        this.toUpdateTrip = function (data) {
            var query = 'UPDATE travel SET name = ?,' +
                            ' budget = ?,'+
                            ' dateGoing = ?,'+
                            ' dateArrival = ?,'+
                            ' description = ?,'+
                            ' country_id  = ?'+
                            ' WHERE id = ?';
            var travel_data = [data.name, 
                                data.budget, 
                                data.dateGoing.getTime(),
                                data.dateArrival.getTime(), 
                                data.description, 
                                data.country.id,
                                data.id];
            //console.log('toUpdateTrip::query::'+query);
            return $cordovaSQLite.execute(db, query, travel_data);
        };
    }
})();
