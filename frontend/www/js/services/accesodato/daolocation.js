'use strict';
(function () {
  angular.module('cym').service('DaoLocation', ['$cordovaSQLite','$filter', DaoLocation]);

  function DaoLocation($cordovaSQLite, $filter) {

    /**
     * Nombre:
     * Descripcion:             metod que selecciona porId
     * @version                 1.0
     * @author                  Leonor Guzman
     * @since                   12/10/2015
     *
     * @param id
     */
    this.selecLocationId = function (id) {
      var query = "SELECT DISTINCT A.id, A.name " +
                  "FROM location A " +
                  "JOIN mustseeanddo B ON A.id = B.location_id " +
                  "JOIN catalog C ON C.id = B.catalog_id " +
                  "WHERE state_id = ? AND C.displayable = 1 " +
                  "ORDER BY A.id";
      //console.log('selecLocationId::query::'+query);
      return $cordovaSQLite.execute(db, query, [id]);
    };


    /**
     * Nombre:
     * Descripcion:             metodo que se trae las localidades segun el travel
     * @version                 1.0
     * @author                  Leonor Guzman
     * @since                   12/14/2015
     *
     * @param travelid
     * @returns {*}
     */
    this.selectLocationTravelId = function (travelid,date) {
      var query = 'SELECT A.*, B.date, B.order_position FROM location A JOIN destination B ON A.id = B.location_id WHERE B.travel_id = ' + travelid + ' AND B.date = ' + date + ' ORDER BY B.order_position';
      //console.log('selectLocationTravelId::query::'+query);
      return $cordovaSQLite.execute(db, query, []);
    };

    this.getLocationById = function (id) {
      var query = 'SELECT * FROM location WHERE id = ' + id;
      //console.log('getLocationById::query::'+query);
      return $cordovaSQLite.execute(db, query, []);
    };

  }
})();
