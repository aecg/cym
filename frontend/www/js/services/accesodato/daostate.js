/**
 * Created by Leonor Guzman on 12/8/2015.
 */
'use strict';
(function() {
  angular.module('cym').service('DaoState', ['$cordovaSQLite','$filter', DaoState]);

  function DaoState($cordovaSQLite, $filter) {

    /**
     * Nombre:
     * Descripcion:             metod que selecciona todos los estados
     * @version                 1.0
     * @author                  Leonor Guzman
     * @since                   12/10/2015
     *
     * @param id
     */
    this.selecAllStateId = function(id) {
      var query = 'SELECT * FROM state WHERE country_id = ' + id;
      //console.log('selecAllStateId::query::'+query);
      return $cordovaSQLite.execute(db,query,[]);
    };

    this.selecStateByCountryAndId = function(countryid, stateid) {
      var query = 'SELECT * FROM state WHERE country_id = ' + countryid + ' AND id = ' + stateid;
      //console.log('selecStateByCountryAndId::query::'+query);
      return $cordovaSQLite.execute(db,query,[]);
    };

    /**
     * Nombre:
     * Descripcion:             metodo que se trae las localidades segun el travel
     * @version                 1.0
     * @author                  Leonor Guzman
     * @since                   12/14/2015
     *
     * @param travelid
     * @returns {*}
     */
    this.selectStateTravelId = function(travelid) {
      var query = 'SELECT B.*, A.date, A.description FROM destination A JOIN state B ON A.state_id = B.id WHERE A.travel_id = ' + travelid;
      //console.log('selectStateTravelId::query::'+query);
      return $cordovaSQLite.execute(db, query, []);
    };


  }
})();
