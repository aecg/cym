/**
 * Created by Leonor Guzman on 12/8/2015.
 */
'use strict';
(function () {
  angular.module('cym').service('DaoCountry', ['$cordovaSQLite', '$filter', DaoCountry]);

  function DaoCountry($cordovaSQLite, $filter ) {


    /**
     * Nombre:  selectAllCountry
     * Descripcion:             metodo que obtiene todos los destinos del usuario
     * @version                 1.0
     * @author                  Leonor Guzman
     * @since                   12/8/2015
     *
     * @returns {*}
     */
    this.selectAllCountry = function () {
      var query = 'SELECT * FROM country';
      //console.log('selectAllCountry::query::'+query);
      return $cordovaSQLite.execute(db, query, []);
    };


    /**
     * Nombre:
     * Descripcion:             metodo que se trae las localidades segun el travel
     * @version                 1.0
     * @author                  Leonor Guzman
     * @since                   12/14/2015
     *
     * @param travelid
     * @returns {*}
     */
    this.selectCountryTravelId = function (travelid) {
      var query = 'SELECT country.* FROM travel INNER JOIN country ON travel.country_id = country.id WHERE travel.id = ' + travelid + ' group by country.id';
      //console.log('selectCountryTravelId::query::'+query);
      return $cordovaSQLite.execute(db, query, []);
    };


    //DaoTrip.$inject = ["DaoTrip"];
  }
})();
