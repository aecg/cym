/**
 * Created by Leonor Guzman on 2/1/2016.
 */
'use strict';
(function ()
{
    angular.module('cym').service('DaoUsers', ['$cordovaSQLite','$translate','$filter', DaoUsers]);

    function DaoUsers($cordovaSQLite, $translate, $filter)
    {

        var self = this;

        self.userValidate = function(email)
        {

        }

        /**
         * Descripcion:             metodo que sirve de dao de gestion de actualizacion de password
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   2/1/2016
         *
         * @returns {*}
         */
        self.updatePass = function (user, messages)
        {
            var success = function (res)
            {
                return self.insertUpdateDataUser(user, messages);
            };
            var error = function (res)
            {
                messages.success.push($filter('translate')('messagessuccess-updatecredential'));
                //message.error.push("Error actualizando las credenciales de seguridad.");
            };
            if(user.password != "")
            {
                var query_user = "UPDATE user SET password = ? WHERE id = ?";
                return $cordovaSQLite.execute(db, query_user, [user.password, user.id]).then(success, error);
            }
        };

        /**
         * Descripcion:             metodo que actaliza los datos del usuario
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   2/26/2016
         *
         */
        self.insertUpdateDataUser = function (data, messages)
        {
            var successUp = function (res)
            {
                messages.success.push($filter('translate')('messagessuccess-updateprofile'));
                //message.success.push("Actualizacion exitosa");
            };
            var errorUp = function (res)
            {
                messages.error.push($filter('translate')('messageserror-updateprofile'));
                //message.error.push("Ocurrio un error actualizando los detalles del perfil #2");
            };


            var success = function (res)
            {
                if (res.rows.length > 0)
                {
                    var query_datauser = "UPDATE datauser SET name = ? , lastname = ? , language = ? , image_profile = ? WHERE user_id = ?";
                    return $cordovaSQLite.execute(db, query_datauser, [data.name, data.lastname,  data.language, data.image_profile, data.id]).then(successUp, errorUp);
                }
                else
                {
                    var queryEr = "INSERT INTO datauser (name, lastname, user_id, language, image_profile) VALUES ( ?, ?, ?, ?, ?) ";
                    return $cordovaSQLite.execute(db, queryEr, [data.name, data.lastname, parseInt(data.id, 10), data.language, data.image_profile]).then(successUp, errorUp);
                }
            };

            var error = function (res)
            {
                messages.error.push($filter('translate')('messageserror-updateprofile'));
                //message.error.push("Ocurrio un error actualizando los detalles del perfil.");
            };
            var query = "select * from datauser where user_id = ?";
            return $cordovaSQLite.execute(db, query, [data.id]).then(success, error);

        }

        self.updateImage = function (user) {
            var query_datauser = 'UPDATE datauser SET image_profile = "' + user.image_profile + '" WHERE user_id = ' + user.id;
            //console.log('query_datauser::'+query_datauser);
            return $cordovaSQLite.execute(db, query_datauser, []);
        };

    }
})();
