'use strict';
(function () {
    angular.module('cym').service('DaoMustSeeAndDo', ['$cordovaSQLite','$filter', DaoMustSeeAndDo]);

    function DaoMustSeeAndDo($cordovaSQLite, $filter) {

        var self = this;

        /**
         * Nombre:
         * Descripcion:             metodo que ubica los must and por por id
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/15/2015
         *
         * @param id
         * @returns {*}
         */
        this.selectMustAndDoId = function (id) {
            var query = "SELECT A.id, A.title, A.geographic_position, A.price, A.ar, A.qr, A.image_reference, B.id as catalog_id, B.name " +
            //var query = "SELECT A.id, A.title, A.geographic_position, A.price, A.ar, A.qr, B.id as catalog_id, B.name " +
                        "FROM mustseeanddo A " +
                        "JOIN catalog B ON A.catalog_id = B.id " +
                        "WHERE A.location_id = ? AND B.displayable = 1 " +
                        "ORDER BY B.id, A.id";
            //console.log('selectMustAndDoId::query::'+query);
            return $cordovaSQLite.execute(db, query, [id]);
        };

        this.insertDestination = function(destinies, idTravel) {
            var promises = [];
            //console.log('insertDestination::init');

            var defInsertDestination = new $.Deferred();

            promises.push(defInsertDestination);

            //console.log('insertDestination::destinies.length::'+destinies.length);
            $.each(destinies, function(i, destiny) {
                //console.log('insertDestination::destiny::'+JSON.stringify(destiny));
                var defDestiny = new $.Deferred();
                promises.push(defDestiny);
                $.queue('destinies', self.processDestiny(idTravel, destiny, defDestiny));
            });
            $.dequeue('destinies');

            defInsertDestination.resolve();
            return $.when.apply(undefined, promises).promise();
        };

        self.processDestiny = function(idTravel, destination, deferred) {

            //console.log('processDestiny::init');
            var promises = [];

            var defInsertLocations = new $.Deferred();
            promises.push(defInsertLocations);

            var locations = destination.locations;
            //console.log('processDestiny::locations::'+JSON.stringify(locations));

            $.each(locations, function(i, locat) {
                //console.log('processDestiny::locat::'+JSON.stringify(locat));
                var defLocation = new $.Deferred();
                promises.push(defLocation);
                $.queue('locations', self.processLocation(idTravel, destination, locat, defLocation));
            });
            $.dequeue('locations');

            defInsertLocations.resolve();
            deferred.resolve();
            return $.when.apply(undefined, promises).promise();
        }

        self.processLocation = function(idTravel, destination, location, deferred) {

            var processMustSeeAnDo = function(reslocation) {
                var promises = [];
                //console.log('processMustSeeAnDo::init');

                var defProcessMustSeeAnDo = new $.Deferred();

                promises.push(defProcessMustSeeAnDo);

                if (location.mustSeeAndDo && location.mustSeeAndDo.length > 0) {
                    $.each(location.mustSeeAndDo, function(i, mustSeeAndDo) {
                        var defCurrentMustSeeAnDo = new $.Deferred();

                        promises.push(defCurrentMustSeeAnDo);
                        var query = 'INSERT INTO travel_mustseeanddo (destination_id, mustseeanddo_id, order_position) VALUES (?, ?, ?)';
                        //console.log('processMustSeeAnDo::query::'+query);
                        $cordovaSQLite.execute(db, query, [reslocation.insertId, mustSeeAndDo.id, mustSeeAndDo.order_position]).then(function() {
                            defCurrentMustSeeAnDo.resolve();
                        });
                    });
                    deferred.resolve();
                }
                else {
                    deferred.resolve();
                }
            }

            //console.log('self.processLocation::destination::'+JSON.stringify(destination));
            var query = 'INSERT INTO destination (travel_id, state_id, location_id, date, description, order_position) VALUES ( ?, ?, ? ,?, ?, ?)';
            var destinyObj = [idTravel, 
                                destination.id, 
                                location.id, 
                                destination.date, 
                                "",
                                location.order_position];
            //console.log('self.processLocation::query::'+query);
            //console.log('self.processLocation::destinyObj::'+JSON.stringify(destinyObj));
            return $cordovaSQLite.execute(db, query, destinyObj).then(processMustSeeAnDo);
        }

        /**
         * Nombre:
         * Descripcion:             metodo que remueve los mustSeeAndDo de un usuario por id  de travels
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/14/2015
         *
         */
        this.removeMustSeeAndDo = function (id_travels) {
            var query_mustSeeAndDo = 'DELETE FROM travel_mustseeanddo WHERE destination_id in '+
                            '(SELECT id FROM destination WHERE travel_id = '+id_travels+')';
            //console.log('removeMustSeeAndDo::query_mustSeeAndDo::'+query_mustSeeAndDo);
            return $cordovaSQLite.execute(db, query_mustSeeAndDo, []).then(function(res) {
                var query_destination = 'DELETE FROM destination WHERE travel_id = '+id_travels;
                //console.log('removeMustSeeAndDo::query_destination::'+query_destination);
                return $cordovaSQLite.execute(db, query_destination, []);
            });
        };


        /**
         * Nombre:
         * Descripcion:             metodo que se trae las localidades segun el travel
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/14/2015
         *
         * @param travelid
         * @param date
         * @returns {*}
         */
        this.selectMustSeeAndDoTravelId = function (travelid) {
            var query = 'SELECT A.*, C.date, B.order_position  FROM mustseeanddo A JOIN travel_mustseeanddo B ON B.mustseeanddo_id = A.id JOIN destination C ON B.destination_id = C.id WHERE C.travel_id = ' + travelid;
            //var query = 'SELECT A.id, A.title, A.geographic_position, A.qr, A.ar, A.location_id, A.price, C.date, B.order_position FROM mustseeanddo A JOIN travel_mustseeanddo B ON B.mustseeanddo_id = A.id JOIN destination C ON B.destination_id = C.id WHERE C.travel_id = ' + travelid;
            //console.log('selectMustSeeAndDoTravelId::query::'+query);
            return $cordovaSQLite.execute(db, query, []);
        };

        /**
         * Descripcion:             metodo que obtiene toda la informacion asociada a un mustseeando
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   2/2/2016
         *
         * @param data
         * @returns {*}
         */
        this.getInformation = function (data) {
            var query = 'SELECT * FROM mustseeanddo_detail WHERE mustseeanddo_id = '+data;
            //console.log('getInformation::query::'+query);
            return $cordovaSQLite.execute(db, query, []);
        };


        /**
         * Descripcion:             metodo que obtiene los mustseaanddo desde el estado
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   2/17/2016
         *
         * @param stateId
         * @returns {*}
         */
        this.getMustFromState = function (stateId, budget) {
            //console.log('this.getMustFromState::stateId::'+stateId+'::budget::'+budget);
            var query = "SELECT m.id, m.title, m.geographic_position, m.price, m.image_reference, loc.id AS locationId, loc.name AS location_name, m.catalog_id, cat.name AS catalog_name " +
            //var query = "SELECT m.id, m.title, m.geographic_position, m.price, loc.id AS locationId, loc.name AS location_name, m.catalog_id, cat.name AS catalog_name " +
                        "FROM mustseeanddo m " +
                        "JOIN location loc ON loc.id = m.location_id " +
                        "JOIN catalog cat ON m.catalog_id = cat.id " +
                        "WHERE m.price <= ?" +
                            " AND m.price > 0 " +
                            " AND loc.state_id = " + stateId +
                            " AND cat.displayable = 1 " +
                        "ORDER BY m.id ASC;";
            //console.log('this.getMustFromState::query::'+query);
            return $cordovaSQLite.execute(db, query, [budget]);
        };
    }


})();
