'use strict';

(function ()
{

    angular.module('cym').controller('DocumentEditCtrl', ['$stateParams', '$state', '$scope','$filter' ,'$cordovaCamera',
        '$cordovaFile', 'UserService', 'DocumentService', 'MessagesFactory', '$ionicPlatform', '$cordovaFileOpener2',
        '$ionicHistory', 'GeoLocation', 'GOOGLE_MAPS_APIKEY', 'Markers', '$ionicScrollDelegate', '$ionicModal', 'SessionFactory', 
        '$loading','MustSeeAndDoService', DocumentEditCtrl]);

    function DocumentEditCtrl($stateParams, $state, $scope, $filter, $cordovaCamera, 
                            $cordovaFile, userService, documentService, MessagesFactory, $ionicPlatform, $cordovaFileOpener2, 
                            $ionicHistory, GeoLocation, GOOGLE_MAPS_APIKEY, Markers, $ionicScrollDelegate, $ionicModal, SessionFactory, 
                            $loading, mustseeanddoService)
    {

        var self = this;
        self.item = {};
        self.docsDeleted = [];
        self.photosDeleted = [];
        self.photoSelected = {};
        self.showPosition = true;
        self.photoSelected.url_file = "";
        var bytesMax = 2000000;

        var urlimageattach = "";

        self.files = [];
        self.photos = [];

        self.data = {
            images: [],
            total: 0,
            count: 0
        };
        self.currentPosition = "";
        $scope.mapName = "EditarDoc";

        /**
         * Metodo que se ejecuta cuando se inicia la pagina
         */
        $scope.$on('$ionicView.enter', function (viewInfo, state)
        {
            $loading.start('loading');
            if (!mustseeanddoService.Latitude || !mustseeanddoService.Longitude)
            {
                mustseeanddoService.getMapLocation();
            }
            self.messages = MessagesFactory.createMessages();
            documentService.initIcons();
            //console.log('CTRL - $ionicView.loaded', viewInfo, state);
            self.loadDocument();
            //console.log('self.showPosition::'+self.showPosition);
        });

        $scope.setFileUpdate = function ()
        {
            var control = document.getElementById('fileEdit').files;
            console.log('setFileUpdate::control.length::'+control.length);
            //alert('setFileUpdate::control.length::'+control.length);
            if (control.length !== 0) {
                $loading.start('loading');
                var aux = 0;

                for (var i = 0; i < control.length; i++) {
                    var docFile = control[i];
                    var reader = new FileReader();

                    reader.onloadend = function (e) {
                        var data = e.target.result;
                        var obj = {};
                        obj.url_file = control[aux].name.toLowerCase();
                        //alert('setFileUpdate::obj.url_file::'+obj.url_file);
                        obj.byte = data;
                        obj.type = "doc";
                        var ext = "jpg,jpeg,png,doc,docx,pdf";
                        //var ext = "jpg,jpeg,png";
                        //console.log("CTRL extension " + obj.url_file.split('.').pop());
                        //console.log("CTRL extension valida " + ext.indexOf(obj.url_file.split('.').pop()));
                        //console.log("CTRL tamaño del archivo " + control[aux].size);

                        if(control[aux].size > bytesMax) {
                            navigator.notification.alert(
                                $filter('translate')('label-picturesizenotificatioconfirm1'),
                                function(){},
                                $filter('translate')('label-picturesizenotificatioconfirm2'),
                                $filter('translate')('label-picturesizenotificatioconfirm3')
                            );
                        }
                        else {
                            if(ext.indexOf(obj.url_file.split('.').pop().toLowerCase()) > -1) {
                                documentService.addAttachment($stateParams.id, obj, self.messages);
                                $cordovaFile.writeFile(cordova.file.dataDirectory, obj.url_file, obj.byte, true)
                                    .then(function (success) {
                                        console.log("setFileUpdate::writeFile::SUCCESS::obj.url_file::" + obj.url_file);
                                        //alert("setFileUpdate::writeFile::SUCCESS::obj.url_file::" + obj.url_file);
                                    },
                                    function (error) {
                                        console.log("setFileUpdate::writeFile::ERROR::obj.url_file::" + obj.url_file);
                                        //alert("setFileUpdate::writeFile::ERROR::obj.url_file::" + obj.url_file);
                                    });
                                self.loadDocument();
                                $loading.finish('loading');
                            }
                            else
                            {
                                $loading.finish('loading');
                                navigator.notification.alert(
                                    $filter('translate')('label-formatfilenotificatioconfirm1'),
                                    function(){},
                                    $filter('translate')('label-formatfilenotificatioconfirm2'),
                                    $filter('translate')('label-formatfilenotificatioconfirm3')
                                );
                            }
                        }

                        $scope.$apply();
                        document.getElementById('fileEdit').value = null;
                    };

                    reader.readAsArrayBuffer(docFile);
                }
            }
        };

        self.updateItem = function () {
            self.messages = MessagesFactory.createMessages();

            if (userService.isUserLogged() && $stateParams.id) {
                if (!self.item.name || self.item.name.trim() == '') {
                    self.messages.error.push($filter('translate')('messageserror-wrongnamedocument'));
                }

                if (!self.item.description || self.item.description.trim() == '') {
                    self.messages.error.push($filter('translate')('messageserror-wrongdescriptiondocument'));
                }

                if (self.messages.error.length == 0) {
                    self.item.user_id = userService.getUserLoggedId();
                    self.item.id = $stateParams.id;

                    //console.log('self.updateItem::self.item.geographic_position::'+self.item.geographic_position);
                    //console.log('self.updateItem::$scope.form.geoposicion.$dirty::'+$scope.form.geoposicion.$dirty);
                    if ($scope.form.geoposicion.$dirty)
                    {
                        //console.log('self.updateItem::self.geoposicion::'+self.geoposicion);
                        //console.log('self.updateItem::self.currentPosition::'+self.currentPosition);
                        if (self.geoposicion && (self.item.geographic_position != "")) {
                            self.item.geographic_position = self.currentPosition;
                        }
                        if(!self.geoposicion && self.item.geographic_position.length != 0) {
                            self.item.geographic_position = "";
                        }
                    }
                    //console.log('self.updateItem::self.item.geographic_position::'+self.item.geographic_position);

                    var item = angular.copy(self.item);
                    item.urlimageattach = urlimageattach;
                    documentService.editDocument(item, self.messages).then(function() {
                        self.docsDeleted = [];
                        self.photosDeleted = [];
                        self.photoSelected = {};
                        self.photoSelected.url_file = "";
                        self.item = {};

                        $ionicHistory.goBack();

                        var obj = {};
                        SessionFactory.setObject("documentsMap", obj);
                    });

                }
                else {
                    $ionicScrollDelegate.scrollTop(true);
                }
            }
        };

        self.positionClick = function()
        {
            //console.log('self.positionClick::self.geoposicion::'+self.geoposicion);
            //console.log('self.positionClick::self.item.geographic_position.length::'+self.item.geographic_position.length);
            if(self.geoposicion && self.item.geographic_position.length == 0) {
                //console.log();

                if (!mustseeanddoService.Latitude || !mustseeanddoService.Longitude)
                {
                    self.currentPosition = mustseeanddoService.Latitude + "," + mustseeanddoService.Longitude;
                    console.log('onMapSuccess::self.currentPosition::'+self.currentPosition);
                    self.showPosition = true; // Form object
                    self.item.geographic_position = self.currentPosition;

                    Markers.clearAllMarkers();
                    Markers.setMarkersDocument(self.item).then(function() {
                        GeoLocation.init("map" + $scope.mapName, GOOGLE_MAPS_APIKEY, false);
                        console.log('self.positionClick::self.item.geographic_position::'+self.item.geographic_position);
                    });
                }
                else
                {
                    self.currentPosition = "";
                    self.showPosition = false; // Form object                    
                }
            }
       }

        self.showDocument = function (name)
        {
            //console.log("showDocument::" + self.dataDirectory + "/" + name);
            var docType = documentService.getType(name)[0].type;
            //console.log("showDocument::docType::" + docType);
            $cordovaFileOpener2.open("/sdcard/cym/" + name, docType)
                .then(function () {
                    console.log("CTRL Se ha abierto exitosamente el archivo");
                }, function (err){ 
                    console.log("CTRL NO Se ha abierto exitosamente el archivo " + JSON.stringify(err));
                    navigator.notification.alert(

                        $filter('translate')('label-notvalidapptofileconfirm1'),
                        function(){},
                        $filter('translate')('label-notvalidapptofileconfirm2'),
                        $filter('translate')('label-notvalidapptofileconfirm3')
                   );
                });
        };

        self.deleteDocument = function (doc) {
            self.messages = MessagesFactory.createMessages();

            function confirmDelete(res) {
                if(res == 1) {
                    if (doc.id != null) {
                        var idx = self.files.indexOf(doc);
                        if (idx > -1) {
                            if(self.files[idx].id > 0) {
                                documentService.deleteAttachment(doc, self.messages);
                            }
                            self.files.splice(idx, 1);
                            //$scope.$apply();
                        }
                    }
                }
            }

            if(window.cordova) {
                navigator.notification.confirm(
                    $filter('translate')('label-deletedocumentnotificatioconfirm1') +  doc.url_file + "?",
                    confirmDelete,
                    $filter('translate')('label-deletedocumentnotificatioconfirm2'),
                    $filter('translate')('label-deletedocumentnotificatioconfirm3')
                );
            }
            else {
                if(confirm($filter('translate')('label-deletedocumentnotificatioconfirm1') + doc.url_file + "?")) {
                    confirmDelete(1);
                }
            }
        };

        self.removePhoto = function (photo) {
            function confirmDelete(res) {
                console.log("CTRL Se ha eliminado exitosamente la photo");
                if(res == 1) {
                    self.photoSelected = {};
                    var idx = self.photos.indexOf( photo );
                    if ( idx > -1 ) {
                        if(self.photos[idx].id > 0) {
                            documentService.deleteAttachment(photo, self.messages);
                        }
                        self.photos.splice( idx, 1 );
                        $scope.$apply();
                    }
                }
            }

            if(window.cordova) {
                navigator.notification.confirm(
                    $filter('translate')('label-deletepicturenotificatioconfirm1') + photo.nameForImage + "?",  // message
                    confirmDelete,
                    $filter('translate')('label-deletepicturenotificatioconfirm2'),
                    $filter('translate')('label-deletepicturenotificatioconfirm3')
                );
            }
            else {
                if(confirm($filter('translate')('label-deletepicturenotificatioconfirm1') + photo.nameForImage + "?")) {
                    confirmDelete(1);
                }
            }
        };

        self.loadDocument = function () {
            self.files = [];
            self.photos = [];
            self.item = {};
            self.itemattach = {};
            self.messages = MessagesFactory.createMessages();

            if (userService.isUserLogged() && $stateParams.id) {
                documentService.getDocument($stateParams.id).then(function (res) {
                    if (res.rows.length > 0) {
                        self.item = res.rows.item(0);
                        //console.log('self.loadDocument::self.item::'+JSON.stringify(self.item));
                        
                        documentService.getDocumentAttachment($stateParams.id).then(function (res_attachment) {
                            for (var i = 0; i < res_attachment.rows.length; i++) {
                                var obj = res_attachment.rows.item(i);
                                //console.log('self.loadDocument::obj::'+JSON.stringify(obj));
                                if (obj.url_file != "") {

                                    var mimeObj = documentService.getIcon(obj.url_file);
                                    if (mimeObj && mimeObj[0]) {
                                        obj.extension = mimeObj[0].src;
                                    }

                                    console.log('self.loadDocument::obj.url_file::'+obj.url_file);
                                    obj.urlForImage = self.urlForImage(obj.url_file);
                                    //alert('self.loadDocument::obj.urlForImage::'+obj.urlForImage);
                                    obj.nameForImage = self.nameForImage(obj.url_file);

                                    //alert('loadDocument::obj::'+JSON.stringify(obj));
                                    if (obj.type == "doc") {
                                        self.files.push(obj);
                                    }
                                    else {
                                        self.photos.push(obj);
                                    }
                                    console.log('loadDocument::obj::'+JSON.stringify(obj));
                                }

                                if(self.photos.length > 0) {
                                    self.photoSelected = self.photos[0];
                                }
                            }
                            $loading.finish('loading');
                        });

                        self.geoposicion = (self.item.geographic_position != "");
                        if(self.geoposicion) {
                            Markers.clearAllMarkers();
                            Markers.setMarkersDocument(self.item).then(function() {
                                GeoLocation.init("map" + $scope.mapName, GOOGLE_MAPS_APIKEY, false);
                                //console.log('self.positionClick::self.geoposicion::'+self.geoposicion);
                                //console.log('self.positionClick::self.item.geographic_position::'+self.item.geographic_position);
                            });
                        }
                    }
                    else {
                        self.messages.error.push($filter('translate')('label-notdocumentfound'))
                    }
                });

            }
        };


        self.addImage = function ()
        {
            // 2
            var options = {
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.CAMERA, // Camera.PictureSourceType.PHOTOLIBRARY
                allowEdit: false,
                encodingType: Camera.EncodingType.PNG,
                popoverOptions: CameraPopoverOptions,
            };

            // 3
            $cordovaCamera.getPicture(options).then(function (imageData) {

                //alert('self.addImage::options.destinationType::'+options.destinationType);      // 1
                $loading.start('loading');
                // 4
                onImageSuccess(imageData);

                function onImageSuccess(fileURI) {
                    //alert('self.addImage::onImageSuccess::fileURI::'+fileURI);  // ubicacion completa de la imagen tomada
                    urlimageattach = fileURI;
                    createFileEntry(fileURI);
                }

                function createFileEntry(fileURI) {
                    window.resolveLocalFileSystemURL(fileURI, copyFile, fail);
                }

                // 5
                function copyFile(fileEntry) {
                    //alert('self.addImage::copyFile::fileEntry.fullPath::'+fileEntry.fullPath); // nombre de la imagen 
                    var name = fileEntry.fullPath.substr(fileEntry.fullPath.lastIndexOf('/') + 1);
                    //alert('self.addImage::copyFile::name::'+name); // nombre de la imagen 

                    var directory = '';
                    if (window.cordova) {
                        directory = cordova.file.dataDirectory;
                    }

                    //alert('self.addImage::copyFile::directory::'+directory);

                    window.resolveLocalFileSystemURL(directory, function (fileSystem2) {
                        fileEntry.copyTo(
                            fileSystem2,
                            name,
                            onCopySuccess,
                            fail
                        );
                    },
                    fail);
                }

                // 6
                function onCopySuccess(entry) {
                    $scope.$apply(function () {
                        var photo = {};
                        //alert('self.addImage::onCopySuccess::entry.nativeURL::'+entry.nativeURL);  // ubicacion completa de la imagen tomada, luego de la copia
                        photo.url_file = entry.nativeURL;
                        photo.type = "img";
                        self.photoSelected = photo;
                        documentService.addAttachment($stateParams.id, photo, self.messages);
                        self.loadDocument();
                    });
                }

                function fail(error) {
                    $loading.finish('loading');
                    //alert("copy fail");
                    console.log("fail: " + error.code);
                }

                function makeid() {
                    var text = "";
                    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

                    for (var i = 0; i < 5; i++) {
                        text += possible.charAt(Math.floor(Math.random() * possible.length));
                    }
                    return text;
                }
            }, function (err) {
                console.log(err);
            });
        };


        self.urlForImage = function (imageName)
        {
            var directory = '';
            if (window.cordova)
            {
                directory = cordova.file.dataDirectory;
            }

            var name = imageName.substr(imageName.lastIndexOf('/') + 1);
            var trueOrigin = directory + name;
            urlimageattach = trueOrigin;
            return trueOrigin;
        };


        self.nameForImage = function (imageName)
        {
            //console.log("CTRL entrando a nameForImage" );
            var name = imageName.substr(imageName.lastIndexOf('/') + 1);
            //console.log('nameForImage::name::'+name);
            return name;
        };

        $ionicPlatform.ready(function ()
        {
            if (window.cordova)
            {
                self.dataDirectory = cordova.file.externalRootDirectory;
            }
            else
            {
                self.dataDirectory = '';
            }
        });

        $ionicModal.fromTemplateUrl('templates/modal.html', {
                scope: $scope
            }).then(function(modal) {
                $scope.modal = modal;
        });

        self.openModal = function(obj) {
            //alert('obj::'+JSON.stringify(obj));
            //alert('obj.url_file::'+obj.url_file);

            var objExt = "";
            var mimeObj = documentService.getIcon(obj.url_file);
            if (mimeObj && mimeObj[0]) {
                objExt = mimeObj[0].ext;
            }
            //alert('objExt::'+objExt);

            if (objExt == '.jpeg' || objExt == '.jpg' || objExt == '.png')
            {
                $scope.imgUrl = obj.urlForImage;
                $scope.modal.show();
            }
        }

    }

})();