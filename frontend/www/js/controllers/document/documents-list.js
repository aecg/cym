'use strict';

(function ()
{
    angular.module('cym').controller('DocumentListCtrl', ['UserService', 'DocumentService', 'MessagesFactory', '$filter', 
        '$scope', '$state', 'SessionFactory','MustSeeAndDoService', DocumentListCtrl]);

    function DocumentListCtrl(userService, documentService, MessagesFactory, $filter, 
        $scope, $state, SessionFactory, mustseeanddoService)
    {

        var self = this;
        self.item = {};
        self.docs = [];

        self.deleteItem = function ($id, name)
        {
            self.messages = MessagesFactory.createMessages();

            if (userService.isUserLogged() && $id)
            {
                self.item.user_id = userService.getUserLoggedId();
                self.item.id = $id;

                if(window.cordova)
                {
                    navigator.notification.confirm(
                        $filter('translate')('label-deletedocumentnotificatioconfirm1') + name + "?",
                        confirmDelete,
                        $filter('translate')('label-deletedocumentnotificatioconfirm2'),
                        $filter('translate')('label-deletedocumentnotificatioconfirm3')
                    );
                }
                else
                {
                    if(confirm($filter('translate')('label-deletedocumentnotificatioconfirm1') + name + "?")) {
                        confirmDelete(1);
                    }
                }
            }
        };

        function confirmDelete(res)
        {
            if (res === 1)
            {
                var item = angular.copy(self.item);
                documentService.deleteDocument(item, self.messages).then(function() {
                    var obj = {};
                    SessionFactory.setObject("documentsMap", obj);
                    self.docs = documentService.allDocuments();
                });
            }
        }

        self.loadDocuments = function ()
        {
            //console.log('self.loadDocuments');
            self.messages = MessagesFactory.createMessages();

            if (userService.isUserLogged())
            {
                //console.log('self.loadDocuments::userService.getUserLoggedId()::'+userService.getUserLoggedId());
                documentService.getDocuments(userService.getUserLoggedId(), self.messages).then(function() {
                    self.docs = documentService.allDocuments();
                    //console.log('self.loadDocuments::self.docs::'+JSON.stringify(self.docs));
                    //alert('self.loadDocuments::self.docs::'+JSON.stringify(self.docs));
                });
            }
        };


        self.navigateTo = function (id)
        {

            $state.go('app.tabs.document-edit', {id: id});
        };


        /**
         * Descripcion:             metodo que se ejecuta al cargar la pagina
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         */
        $scope.$on('$ionicView.enter', function ()
        {
            if (!mustseeanddoService.Latitude || !mustseeanddoService.Longitude)
            {
                mustseeanddoService.getMapLocation();
            }
            self.loadDocuments();
        });


        $scope.$on("$ionicView.afterLeave", function (event, data)
        {
            documentService.clearAllDocuments();
        });
    }

})();