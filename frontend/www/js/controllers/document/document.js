'use strict';

(function ()
{
    angular.module('cym').controller('DocumentCtrl', ['$state', '$scope','$filter', '$cordovaCamera', '$cordovaFile',
        'UserService', 'DocumentService', 'MessagesFactory', '$ionicPlatform','$ionicScrollDelegate', '$ionicModal', 
        'SessionFactory', '$loading', 'MustSeeAndDoService', DocumentCtrl]);

    function DocumentCtrl($state, $scope, $filter, $cordovaCamera, $cordovaFile, 
        userService, documentService, MessagesFactory, $ionicPlatform, $ionicScrollDelegate, 
        $ionicModal, SessionFactory, $loading, mustseeanddoService)
    {

        var self = this;
        self.item = {}; // Form object
        self.showPosition = true; // Form object
        self.files = []; // Form object
        self.photos = []; // Form object
        self.photoSelected = {}; // Form object
        self.photoSelected.name = ""; // Form object
        self.file = {}; // Form object
        self.currentPosition = "";

        var bytesMax = 2000000;

        var urlimageattach = "";

        $scope.images = [];

        /**
         * Metodo que se ejecuta cuando se inicia la pagina
         */
        $scope.$on('$ionicView.enter', function (viewInfo, state)
        {
            self.currentPosition = "";
            self.showPosition = false; // Form object
            if (!mustseeanddoService.Latitude || !mustseeanddoService.Longitude)
            {
                mustseeanddoService.getMapLocation();
            }
            else
            {
                self.currentPosition = mustseeanddoService.Latitude + "," + mustseeanddoService.Longitude;
                self.showPosition = true; // Form object
            }
            //console.log('CTRL - $ionicView.loaded', viewInfo, state);
            documentService.initIcons();
        });

        self.addItem = function ()
        {
            self.messages = MessagesFactory.createMessages();

            if (userService.isUserLogged())
            {

                if (!self.item.name || self.item.name.trim() == '')
                {
                    self.messages.error.push($filter('translate')('messageserror-wrongnamedocument'));
                    //self.messages.error.push('Nombre de documento, inv\u00e1lido');
                }

                if (!self.item.description || self.item.description.trim() == '')
                {
                    self.messages.error.push($filter('translate')('messageserror-wrongdescriptiondocument'));
                    //self.messages.error.push('Descripci\u00f3n del documento, inv\u00e1lido');
                }

                if (self.messages.error.length == 0)
                {
                    self.item.user_id = userService.getUserLoggedId();
                    var item = angular.copy(self.item);

                    //console.log("CTRL hay " + self.files.length + " archivos para guardar");
                    for(var i = 0; i < self.files.length; i++)
                    {
                        $cordovaFile.writeFile(cordova.file.dataDirectory, self.files[i].name, self.files[i].byte, true)
                            .then(function (success)
                            {
                                console.log("CTRL Se han leido y copiad estos archivos " + self.files[i].name);
                            },
                            function (error)
                            {
                                console.log("CTRL ERROR Se han leido estos archivos " + self.files[i].name);
                            });
                    }
                    item.urlimageattach = urlimageattach;
                    console.log('self.addItem::self.geoposicion::'+self.geoposicion);
                    console.log('self.addItem::self.currentPosition::'+self.currentPosition);
                    if (self.geoposicion === undefined || self.geoposicion === null) {
                        item.geographic_position = "";
                    }
                    else {
                        item.geographic_position = self.currentPosition;
                    }
                    console.log('self.addItem::item.geographic_position::'+item.geographic_position);
                    item.files = self.files;
                    item.images = self.photos;
                    documentService.addDocuments(item, self.messages);
                    self.item = {};
                    $scope.images =  [];
                    self.files = []; // Form objec
                    self.file = {}; // Form object
                    self.photos = []; // Form object
                    self.photoSelected = {};
                    self.photoSelected.name = "";

                    SessionFactory.setObject("documentsMap", null);
                    $state.go('app.tabs.documents', null);
                }
                else
                {
                    $ionicScrollDelegate.scrollTop(true);
                }
            }
        };

        self.openFileDialog = function ()
        {
            console.log('fire! $scope.openFileDialog()');
            ionic.trigger('click', {target: document.getElementById('file')});
        };

        angular.element('#file').on('change', function (event)
        {
            console.log('fire! angular#element change event');
            var file = event.target.files[0];
            console.log(file);
            $scope.fileName = file.name;
            console.log(file.name);
            $scope.$apply();
        });

        self.addImage = function ()
        {
            $loading.start('loading');
            // 2
            var options = {
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.CAMERA, // Camera.PictureSourceType.PHOTOLIBRARY
                allowEdit: false,
                encodingType: Camera.EncodingType.PNG,
                popoverOptions: CameraPopoverOptions,
            };

            // 3
            $cordovaCamera.getPicture(options).then(function (imageData)
            {

                // 4
                onImageSuccess(imageData);

                function onImageSuccess(fileURI)
                {
                    urlimageattach = fileURI;
                    createFileEntry(fileURI);
                }

                function createFileEntry(fileURI)
                {
                    window.resolveLocalFileSystemURL(fileURI, copyFile, fail);
                }

                // 5
                function copyFile(fileEntry)
                {
                    var name = fileEntry.fullPath.substr(fileEntry.fullPath.lastIndexOf('/') + 1);

                    var directory = '';
                    if(window.cordova)
                        directory = cordova.file.dataDirectory;

                    window.resolveLocalFileSystemURL(directory, function (fileSystem2)
                        {
                            fileEntry.copyTo(
                                fileSystem2,
                                name,
                                onCopySuccess,
                                fail
                            );
                        },
                        fail);
                }

                // 6
                function onCopySuccess(entry)
                {
                    $scope.$apply(function ()
                    {
                        var photo = {};
                        photo.name = entry.nativeURL;
                        photo.type = "img";
                        self.photoSelected = photo;

                        //alert('onCopySuccess::photo::'+JSON.stringify(photo));
                        photo.urlForImage = self.urlForImage(photo.name);
                        photo.nameForImage = self.nameForImage(photo.name);
                        self.photos.push(photo);
                        $loading.finish('loading');
                    });
                }

                function fail(error)
                {
                    aler("copy fail");
                    console.log("fail: " + error.code);
                }

                function makeid()
                {
                    var text = "";
                    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

                    for (var i = 0; i < 5; i++)
                    {
                        text += possible.charAt(Math.floor(Math.random() * possible.length));
                    }
                    return text;
                }

            }, function (err)
            {
                $loading.finish('loading');
                console.log(err);
            });
        };

        self.urlForImage = function (imageName)
        {
            var directory = '';
            if(window.cordova) {
                directory = cordova.file.dataDirectory;
            }
            console.log('self.urlForImage::imageName::'+imageName);
            //alert('self.urlForImage::imageName::'+imageName);
            var name = imageName.substr(imageName.lastIndexOf('/') + 1);
            var trueOrigin = directory + name;
            urlimageattach = trueOrigin;
            return trueOrigin;
        };

        self.nameForImage = function (imageName)
        {
            console.log('self.nameForImage::imageName::'+imageName);
            //alert('self.nameForImage::imageName::'+imageName);
            var name = imageName.substr(imageName.lastIndexOf('/') + 1);
            console.log('self.nameForImage::name::'+name);
            //alert('self.nameForImage::name::'+name);
            return name;
        };


        self.removeDocument = function (document)
        {
            function confirmDelete(res)
            {
                if(res == 1)
                {
                    var idx = self.files.indexOf( document );
                    if ( idx > -1 )
                    {
                        console.log("CTRL Se ha eliminado exitosamente el documento " + (res == 1));
                        self.files.splice( idx, 1 );
                        $scope.$apply();
                    }
                }
            }

            if(window.cordova) {
                navigator.notification.confirm(
                    $filter('translate')('label-deletedocumentnotificatioconfirm1') + document.name + "?",  // message
                    confirmDelete,
                    $filter('translate')('label-deletedocumentnotificatioconfirm2'),
                    $filter('translate')('label-deletedocumentnotificatioconfirm3')
                );
            }
            else {
                if(confirm($filter('translate')('label-deletedocumentnotificatioconfirm1') + document.name + "?")) {
                    confirmDelete(1);
                }
            }

        };


        self.removePhoto = function (photo)
        {
            function confirmDelete(res)
            {
                console.log("CTRL Se ha eliminado exitosamente la photo");
                if(res == 1)
                {
                    self.photoSelected = {};
                    self.photoSelected.name = "";
                    var idx = self.photos.indexOf( photo );
                    if ( idx > -1 )
                    {
                        self.photos.splice( idx, 1 );
                        $scope.$apply();
                    }
                }
            }

            if(window.cordova) {
                navigator.notification.confirm(

                    $filter('translate')('label-deletepicturenotificatioconfirm1') + photo.nameForImage + "?",  // message
                    confirmDelete,
                    $filter('translate')('label-deletepicturenotificatioconfirm2'),
                    $filter('translate')('label-deletepicturenotificatioconfirm3')
                );
            }
            else {
                if(confirm($filter('translate')('label-deletepicturenotificatioconfirm1') + photo.nameForImage + "?")) {
                    confirmDelete(1);
                }
            }
        };

        $scope.initHome = function ()
        {
            $cordovaFile.createDir(self.dataDirectory, "cym", false)
                .then(function (success) {
                    console.log("CTRL Se ha creado el directorio exitosamente el archivo");
                }, function (error) {
                    console.log("CTRL NO Se ha creado exitosamente el archivo " + JSON.stringify(error));
                });
        };


        $scope.setFiles = function ()
        {
            var file = document.getElementById('file').files;
            if (file.length !== 0)
            {
                $loading.start('loading');
                var aux = 0;

                for (var i = 0; i < file.length; i++)
                {
                    var docFile = file[i];
                    var reader = new FileReader();

                    reader.onloadend = function (e)
                    {
                        var data = e.target.result;
                        var obj = {};
                        //alert("setFiles::file[aux]::" + JSON.stringify(file[aux]));
                        obj.name = file[aux].name.toLowerCase();
                        obj.byte = data;
                        obj.type = "doc";
                        var ext = "jpg,jpeg,png,doc,docx,pdf";
                        //var ext = "jpg,jpeg,png";
                        console.log("CTRL extension " + obj.name.split('.').pop());
                        console.log("CTRL extension valida " + ext.indexOf(obj.name.split('.').pop()));
                        console.log("CTRL tamaño del archivo " + file[aux].size);

                        if(file[aux].size > bytesMax)
                        {
                            navigator.notification.alert(

                                $filter('translate')('label-picturesizenotificatioconfirm1'),
                                function(){},
                                $filter('translate')('label-picturesizenotificatioconfirm2'),
                                $filter('translate')('label-picturesizenotificatioconfirm3')

                                /*"Tamaño del archivo m\u00e1ximo permitido 1Mb",
                                function(){},
                                'Advertencia',
                                'Aceptar'*/
                            );
                        }
                        else
                        {
                            if(ext.indexOf(obj.name.split('.').pop().toLowerCase()) > -1) {
                                console.log("obj.name::" + obj.name);
                                obj.extension = documentService.getIcon(obj.name)[0].src;

                                //alert('setFiles::obj::'+JSON.stringify(obj));
                                //obj.urlForImage = self.urlForImage(obj.url_file);
                                //obj.nameForImage = self.nameForImage(obj.url_file);

                                self.files.push(obj);
                                $loading.finish('loading');
                            }
                            else
                            {
                                $loading.finish('loading');
                                navigator.notification.alert(

                                    $filter('translate')('label-formatfilenotificatioconfirm1'),
                                    function(){},
                                    $filter('translate')('label-formatfilenotificatioconfirm2'),
                                    $filter('translate')('label-formatfilenotificatioconfirm3')

                                   /*"El formato del archivo no corresponde. Formatos permitidos: .jpeg, .png, .doc. pdf",
                                    function(){},
                                    'Advertencia',
                                    'Aceptar'*/
                                );
                            }
                        }

                        $scope.$apply();
                        document.getElementById('file').value = null;

                    };

                    reader.readAsArrayBuffer(docFile);
                }
            }
        };

        //console.log("CTRL el cordova file " + $cordovaFile);
        $ionicPlatform.ready(function ()
        {
            if(window.cordova) {
                self.dataDirectory = cordova.file.externalRootDirectory;
            }
            else {
                self.dataDirectory = '';
            }
            $scope.initHome();
        });

        $ionicModal.fromTemplateUrl('templates/modal.html', {
                scope: $scope
            }).then(function(modal) {
                $scope.modal = modal;
        });

        self.openModal = function(obj) {
            var objExt = "";
            var mimeObj = documentService.getIcon(obj.name);
            if (mimeObj && mimeObj[0]) {
                objExt = mimeObj[0].ext;
            }

            if (objExt == '.jpeg' || objExt == '.jpg' || objExt == '.png')
            {
                $scope.imgUrl = self.urlForImage(obj.name);
                $scope.modal.show();
            }
        }
    }
})();

