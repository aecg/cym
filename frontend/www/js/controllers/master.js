'use strict';

(function ()
{
    angular.module('cym').controller('MasterCtrl', ['$scope', '$filter','$rootScope', '$location', 'UserService', 'DocumentService', 'TrackingService', 'TravelService', MasterCtrl]);

    function MasterCtrl($scope, $filter, $rootScope, $location, userService, documentService, trackingService, travelService)
    {

        $scope.isUserLogged = function ()
        {
            return userService.isUserLogged();
        };

        $scope.allDocuments = function ()
        {
            return documentService.allDocuments();
        };

        $scope.allTracks = function ()
        {
            return trackingService.allTracks();
        };

        $scope.travelDetail = function ()
        {
            return travelService.getInfoTravels();
        };

        $scope.getUser = function ()
        {
            return userService.getUserLogged();
        };


        $scope.viewGooglePlusLogin = function ()
        {
            return !!(window != null && window.plugins != null && window.plugins.googleplus != null);
        };

        $scope.showPage = function (page) {
            //console.log('$scope.showPage::page::'+page);
            return userService.showPage(page);
        };
    }
})();
