'use strict';

(function () {
    angular.module('cym').controller('MenuCtrl', ['$rootScope', '$scope', '$state', MenuCtrl]);

    function MenuCtrl($rootScope, $scope, $state) {
        var self = this;

        self.user = {};
        self.viewError = false;

        /**
         * Metodo que se ejecuta cuando se inicia la pagina
         */
        $scope.$on('$ionicView.enter', function (viewInfo, state) {
            console.log(userLogged.image_profile);
            $scope.apply();
        });
    }

})();
