/**
 * Created by poncbi on 11/02/2016.
 */
'use strict';

(function ()
{
    angular.module('cym').controller('SyncronizationCtrl', ['$scope','$filter','$rootScope','$ionicLoading', 'UserService', 'SyncronizationService','MessagesFactory', SyncronizationCtrl]);

    function SyncronizationCtrl($scope, $filter, $rootScope, $ionicLoading, userService, syncronizationService, MessagesFactory)
    {
        var self = this;
        self.security = {};

        self.viewError = false;


        self.resolveError = function(res) {
            self.messages.error.push($filter('translate')('messageserror-sync'));
            $ionicLoading.hide();
        }

        /**
         * Method to data syncronization
         */
        self.syncInfo = function () {
            //console.log('self.syncInfo::init');
            self.messages = MessagesFactory.createMessages();
            if ($rootScope.isOnline) {
                $ionicLoading.show({
                    template: $filter('translate')('messagetemplate-syncserver') 
                });
                var userLoggedId = userService.getUserLoggedId();
                var userNameLogged = userService.getUserNameLogged();

                //console.log('self.syncInfo::pre::syncronizationService.uploadUserData');
                syncronizationService.uploadUserData(userLoggedId, userNameLogged, self.messages).then(function(res) {
                    //console.log('self.syncInfo::syncronizationService.uploadUserData::done');
                    syncronizationService.deleteUserData(userLoggedId, userNameLogged).then(function() {
                        //console.log('self.syncInfo::syncronizationService.deleteUserData::done');
                        syncronizationService.downloadUserData(userLoggedId, userNameLogged, self.messages).then(function() {
                            //console.log('self.syncInfo::syncronizationService.downloadUserData::done');
                            syncronizationService.downloadMasterData(userLoggedId, userNameLogged, self.messages).then(function() {
                                //console.log('self.syncInfo::syncronizationService.downloadMasterData::done');
                                self.messages.success.push($filter('translate')('messagessuccess-sync'));
                                $ionicLoading.hide();
                            }, self.resolveError);
                        }, self.resolveError);
                    }, self.resolveError);
                }, self.resolveError);
            }
            else {
                self.messages.error.push($filter('translate')('messageserror-sync-no-internet'));
            }
        };

         $scope.$on('$ionicView.enter', function (viewInfo, state) {
            self.messages = MessagesFactory.createMessages();
         });

    }
})();

