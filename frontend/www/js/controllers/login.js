'use strict';

(function () {
    angular.module('cym').controller('LoginCtrl', ['$rootScope', '$filter','$state',
        'UserService', 'MessagesFactory',
        '$ionicPopup', '$scope',
        '$ionicLoading', '$ionicHistory', '$ionicSideMenuDelegate', 'DocumentService', 'TravelService', '$stateParams', LoginCtrl]);

    function LoginCtrl($rootScope, $filter, $state, userService, MessagesFactory, $ionicPopup, $scope, $ionicLoading, $ionicHistory, $ionicSideMenuDelegate,
        DocumentService, TravelService, $stateParams) {
        var self = this;

        self.user = {};
        self.viewError = false;
        self.page = '';

        $scope.$on('$ionicView.enter', function (viewInfo, state) {
            //console.log('$ionicView.enter::$stateParams.page::'+$stateParams.page);
            self.messages = MessagesFactory.createMessages();
            self.page = $stateParams.page;
            userService.setUserLogged(self.message);
        });

        /**
         * Descripcion:             funcion de login de usuario
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   1/4/2016
         *
         * @param isValid
         */
        self.submitForm = function (isValid) {
            self.messages = MessagesFactory.createMessages();

            if (isValid && self.isValidEmail(self.user.email)) {
                userService.login(self.user, self.messages, self.page);
                //$state.go($state.current, {}, {reload: true});
            }
            else {
                self.messages.error.push($filter('translate')('messageserror-invalidvalue'));
            }
            self.user = {};
        };

        /**
         * Descripcion:             metodo que valida el formato del email
         * @version                 1.0
         * @author                  Bismarck Ponce
         * @since                   12/04/2016
         *
         * @param isValid
         */
        self.isValidEmail = function demoShowMatchClick(email) {
            var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var re = new RegExp(pattern);
            var m = re.exec(email);
            if (m == null) {
                return false;
            } else {
                return true;
            }
        }

        /**
         * Descripcion:             metodo de close popup
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   1/4/2016
         *
         */
        $scope.sendOrder = function () {
            $scope.alert.close();
        };

        /**
         * Descripcion:             metodo de logout de usuario
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   1/4/2016
         *
         */
        $scope.logout = function () {
            self.messages = MessagesFactory.createMessages();
            $ionicLoading.hide();
            $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
            $ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
            userService.logout(self.messages);
            DocumentService.clearAllDocuments();
            TravelService.clearAllTravelsUser();
            if (window != null && window.plugins != null && window.plugins.googleplus != null) {
                window.plugins.googleplus.logout(
                    function (msg) {
                        console.log(msg);
                    },
                    function (fail) {
                        console.log(fail);
                    });
            }
            $state.go('app.tabs.dashboard');
        };

        /**
         * Descripcion:             metodo que cierra el menu de opciones
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   2/12/2016
         *
         */
        $scope.closeSideMenu = function () {
            $ionicSideMenuDelegate.toggleLeft();
            $scope.alert.close();
        };

        /**
         * Descripcion:             metodo que realiza login desde google
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   2/11/2016
         *
         */
        self.googleSignIn = function () {
            self.messages = MessagesFactory.createMessages();
            $ionicLoading.show({
                template: $filter('translate')('messagetemplate-starting')
            });

            if (window.plugins && window.plugins.googleplus.isAvailable) {
                window.plugins.googleplus.login(
                    {},
                    function (user_data) {
                        //console.log("datos de google "+ JSON.stringify(user_data));
                        if (user_data != null && user_data.email != null) {
                            //console.log("googleSignIn::calling userService.loginGoogle");
                            userService.loginGoogle(user_data, self.messages, self.page).then(function() {
                                $ionicLoading.hide();
                            });
                        }
                        else {
                            $ionicLoading.hide();
                        }
                    },
                    function (msg) {
                        //console.log('googleSignIn::error::'+JSON.stringify(msg));
                        self.messages.error.push($filter('translate')('messageserror-googleconnect'));
                        //self.messages.error.push("Error conectando con google");
                        $ionicLoading.hide();
                    }
                );
            }
            else {
                self.messages.error.push($filter('translate')('messageserror-googleconnectnotsupported'));
               $ionicLoading.hide(); 
            }
        };

        /**
         * Descripcion:             funcion de recuperar contraseña de usuario
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   1/4/2016
         *
         * @param isValid
         */
        self.recoveryPassword = function () {
            self.messages = MessagesFactory.createMessages();

            if (self.user.email != "" && self.user.email != null) {
                userService.recoveryPassword(self.user.email, self.messages);
            }
            else {
                self.messages.error.push($filter('translate')('messageserror-invaliduserrecovery'));
            }
            self.user = {};
        };

    }
})();
