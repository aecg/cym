/**
 * Created by poncbi on 11/02/2016.
 */
'use strict';

(function ()
{
    angular.module('cym').controller('SecurityCtrl', ['$scope', '$filter', '$state','$stateParams', '$rootScope', '$ionicHistory', '$location', 'UserService', 'MessagesFactory', 'SessionFactory', SecurityCtrl]);

    function SecurityCtrl($scope, $filter ,$state, $stateParams, $rootScope, $ionicHistory, $location, userService, MessagesFactory, SessionFactory)
    {
        var self = this;
        self.security = {};

        self.viewError = false;

        var imgCount = 4;
        var dir = 'img/';
        var images = [];
        images[1] = "bg-01.png";
        images[2] = "bg-02.png";
        images[3] = "bg-03.png";
        images[4] = "bg-04.png";
        
        $scope.$on('$ionicView.enter', function (viewInfo, state) {
            //console.log('$ionicView.enter::userService.isUserLogged()::'+userService.isUserLogged());
            if (userService.isUserLogged()) {
                $state.go('app.tabs.dashboard', {});
            }
            else {
                var randomCount = Math.round(Math.random() * (imgCount - 1)) + 1;
                $scope.startBackground = dir + images[randomCount];
                //console.log('$ionicView.enter::$scope.startBackground::'+$scope.startBackground);
                self.messages = MessagesFactory.createMessages();
            }
        });

        $scope.loginPage = function () {
            //console.log('$scope.loginPage::$stateParams.page::'+$stateParams.page);
            $state.go('app.login', { page: $stateParams.page });
        }

        $scope.skipPage = function () {
            $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
            $ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
            $state.go('app.tabs.dashboard', {});
        }
    }
})();

