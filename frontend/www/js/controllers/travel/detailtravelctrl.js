/**
 * Sistema:                 CyM-AR
 * Siglas:                  cym
 *
 * Nombre:                  detailtravelctrl.js
 * Descripcion:             controlador que mantiene el valor de las variables y metodos para la edicion del travel
 * @version                 1.0
 * @author                  Leonor Guzman
 * @since                   12/18/2015
 *
 */
'use strict';
(function ()
{

    angular.module('cym')
        .controller('DetailTravelCtrl', ['$timeout', '$scope', '$stateParams', '$state', 'UserService',
            'TravelService', 'MessagesFactory', '$ionicLoading', '$ionicHistory', '$filter', 'Markers', 
            'GeoLocation', 'GOOGLE_MAPS_APIKEY', '$ionicScrollDelegate', 'DEFAULT_COUNTRY', 'DEFAULT_STATE', 
            '$loading', 'MustSeeAndDoService', DetailTravelCtrl])
        .directive('detailDestiny', function ()
        {
            return {
                restricted: 'E',
                templateUrl: 'templates/travel/destiny.html'
            };
        });
    function DetailTravelCtrl($timeout, $scope, $stateParams, $state, userService, 
            travelService, MessagesFactory, $ionicLoading, $ionicHistory, $filter, Markers, 
            GeoLocation, GOOGLE_MAPS_APIKEY, $ionicScrollDelegate, DEFAULT_COUNTRY, DEFAULT_STATE, 
            $loading, mustseeanddoService)
    {

        var self = this;
        $scope.mapName = "EditarViaje";

        self.trip = travelService.getInfoTravels();
        $scope.countries = [];
        $scope.viewAcord = [];
        //$scope.viewAcord = false;

        $scope.$on('$ionicView.enter', function () {
            //console.log('$ionicView.enter');
            $loading.start('loading');
            if (!mustseeanddoService.Latitude || !mustseeanddoService.Longitude)
            {
                mustseeanddoService.getMapLocation();
            }
            self.trip = travelService.getInfoTravels();
            //console.log('self.trip::'+JSON.stringify(self.trip));
            self.loadTrip().then(function() {
                //console.log('self.loadTrip::done');
                //self.loadLocate();
                $loading.finish('loading');
            });
        });

        $scope.$watch(function () { 
          return mustseeanddoService.Latitude; 
        }, function(newVal, oldVal){
            if(oldVal!==newVal && newVal !== undefined){
                //console.log('calculating distances');
                self.calculateDistance();
            }
        });

        /**
         * Descripcion:             metodo que carga las localidades
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         */
        self.loadLocate = function () {

            self.messages = MessagesFactory.createMessages();
            var success = function (res) {
                for (var i = 0; i < res.rows.length; i++) {
                    $scope.countries.push(res.rows.item(i));
                }
                self.trip.country = $filter('filter')($scope.countries, {id: DEFAULT_COUNTRY})[0];;
            };

            var error = function (res) {
                self.messages.error.push($filter('translate')('messageserror-findmustseeanddo'));
            };

            return travelService.getAllLocationCountries().then(success, error);
        };

        self.redirectPreferred = function ()
        {
            travelService.setInfoTravels(self.trip);
            //console.log('self.redirectPreferred::self.trip::'+JSON.stringify(self.trip));
            //console.log('self.redirectPreferred::$scope.$parent.travelDetail().budget::'+$scope.$parent.travelDetail().budget);
            $state.go('app.tabs.preferred', {trip: self.trip.id, budget: $scope.$parent.travelDetail().budget, stateId: DEFAULT_STATE});
        }

        self.validateDestiniesDate = function() {
            for (var i = 0; i < $scope.$parent.travelDetail().destiny.length; i++) {
                if ($scope.$parent.travelDetail().destiny[i].date > $scope.$parent.travelDetail().dateArrival) {
                    return false;
                }
                if ($scope.$parent.travelDetail().destiny[i].date < $scope.$parent.travelDetail().dateGoing) {
                    return false;
                }
            }
            //console.log('self.validateDestiniesDate::return true');
            return true;
        }

        self.tripFormChange = function() {
            //console.log('self.tripFormChange::$scope.newUpdateTravel.name.$dirty::'+$scope.newUpdateTravel.name.$dirty);
            //console.log('self.tripFormChange::$scope.newUpdateTravel.budget.$dirty::'+$scope.newUpdateTravel.budget.$dirty);
            //console.log('self.tripFormChange::$scope.newUpdateTravel.dateGoing.$dirty::'+$scope.newUpdateTravel.dateGoing.$dirty);
            //console.log('self.tripFormChange::$scope.newUpdateTravel.dateArrival.$dirty::'+$scope.newUpdateTravel.dateArrival.$dirty);
            if ($scope.newUpdateTravel.name.$dirty || $scope.newUpdateTravel.budget.$dirty ||
                $scope.newUpdateTravel.dateGoing.$dirty || $scope.newUpdateTravel.dateArrival.$dirty)
            {
                return true;
            }
            return false;
        }

        self.autoUpdateTrip = function () {
            $loading.start('loading');
            //console.log('self.autoUpdateTrip');
            //console.log('self.autoUpdateTrip::$scope.$parent.travelDetail()::'+JSON.stringify($scope.$parent.travelDetail()));
            $scope.$parent.travelDetail().description = '';
            travelService.setInfoTravels(angular.copy($scope.$parent.travelDetail()));

            //console.log('self.autoUpdateTrip::');

            if (self.tripFormChange() && (userService.isUserLogged() && self.trip.id) && 
                ((!$scope.$parent.travelDetail().name || $scope.$parent.travelDetail().name.trim() == '') ||
                !$scope.$parent.travelDetail().budget ||
                !$scope.$parent.travelDetail().country ||
                !$scope.$parent.travelDetail().dateGoing ||
                !$scope.$parent.travelDetail().dateArrival ||
                ($scope.$parent.travelDetail().dateGoing && $scope.$parent.travelDetail().dateArrival
                    && ($scope.$parent.travelDetail().destiny != null) && ($scope.$parent.travelDetail().destiny.length > 0)
                    && self.validateDestiniesDate())))
            {
                travelService.autoUpdateTrip().then(function() {
                    $loading.finish('loading');
                    //console.log('self.autoUpdateTrip::travelService.autoUpdateTrip()::done');
                    //console.log('self.autoUpdateTrip::$scope.$parent.travelDetail()::'+JSON.stringify($scope.$parent.travelDetail()));
                });
            }
            else
            {
                $loading.finish('loading');
            }
        }

        self.updateTrip = function () {
            $loading.start('loading');
            //console.log('self.updateTrip::$scope.$parent.travelDetail()::'+JSON.stringify($scope.$parent.travelDetail()));
            $scope.$parent.travelDetail().description = '';
            travelService.setInfoTravels(angular.copy($scope.$parent.travelDetail()));
            self.messages = MessagesFactory.createMessages();

            if (userService.isUserLogged() && self.trip.id) {
                if (!$scope.$parent.travelDetail().name || $scope.$parent.travelDetail().name.trim() == '') {
                    self.messages.error.push($filter('translate')('messageserror-wrongnametrip'));
                }
                if (!$scope.$parent.travelDetail().budget) {
                    self.messages.error.push($filter('translate')('messageserror-wrongbugettrip'));
                }
                if (!$scope.$parent.travelDetail().country) {
                    self.messages.error.push($filter('translate')('messageserror-wrongcountrytrip'));
                }
                if (!$scope.$parent.travelDetail().dateGoing) {
                    self.messages.error.push($filter('translate')('messageserror-wrongdateout'));
                }
                if (!$scope.$parent.travelDetail().dateArrival) {
                    self.messages.error.push($filter('translate')('messageserror-wrongdatein'));
                }
                if ($scope.$parent.travelDetail().dateGoing && $scope.$parent.travelDetail().dateArrival
                    && $scope.$parent.travelDetail().destiny != null && $scope.$parent.travelDetail().destiny.length > 0) {
                    for (var i = 0; i < $scope.$parent.travelDetail().destiny.length; i++) {
                        if ($scope.$parent.travelDetail().destiny[i].date > $scope.$parent.travelDetail().dateArrival) {
                            self.messages.error.push($filter('translate')('messageserror-wrongdateinwithdestiny'));
                        }
                        if ($scope.$parent.travelDetail().destiny[i].date < $scope.$parent.travelDetail().dateGoing) {
                            self.messages.error.push($filter('translate')('messageserror-wrongdateoutwithdestiny'));
                        }
                    }
                }
                //console.log('self.updateTrip::self.messages.error.length::'+self.messages.error.length);
                if (self.messages.error.length == 0) {
                    //console.log('self.updateTrip:: calling travelService.updateTrip()')
                    travelService.updateTrip().then(function() {
                        //console.log('travelService.updateTrip::done');
                        self.trip = {};
                        travelService.setInfoTravels(self.trip);
                        //$ionicHistory.goBack();
                        $state.go('app.tabs.travel', {});
                    });
                }
                else {
                    $loading.finish('loading');
                    $ionicScrollDelegate.scrollTop(true);
                }
            }
            else {
                self.messages.error.push($filter('translate')('messageserror-usernotlogin'));
            }
        };

        /**
         * Descripcion:             metodo que carga el viaje que se desea actualizar segun el id
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         */
        self.loadTrip = function () {

            var promises = [];

            var defLoadTrip = new $.Deferred();
            promises.push(defLoadTrip);

            self.messages = MessagesFactory.createMessages();
            //console.log('self.loadTrip::$stateParams.id::'+$stateParams.id+'::self.trip.id::'+self.trip.id);

            if (($stateParams.id != null) && (self.trip.id != null) && ($stateParams.id != self.trip.id)) {
                self.trip = {};
                travelService.setInfoTravels(self.trip);
            }

            //console.log('self.loadTrip::1::self.trip.id::'+self.trip.id);
            if (self.trip.id == null) {

                if (userService.isUserLogged() && $stateParams.id > 0) {
                    var defGetTrip = new $.Deferred();
                    promises.push(defGetTrip);

                    self.trip.id = parseInt($stateParams.id, 10);
                    //console.log('self.loadTrip::self.trip.id::'+self.trip.id);

                    travelService.getTrip(self.trip).then(function() {
                        defGetTrip.resolve();

                        //console.log('travelService.getTrip::done');
                        //console.log('self.loadTrip::$stateParams.id::'+$stateParams.id);
                        if ($stateParams.id) {
                            self.trip = travelService.getInfoTravels();
                            //console.log('self.loadTrip::self.trip::'+JSON.stringify(self.trip));
                            if ((self.trip.destiny != null) && (self.trip.destiny.length > 0)) {

                                //console.log('self.loadTrip::self.trip::'+JSON.stringify(self.trip));
                                self.trip.destiny = self.trip.destiny.sort(function(a, b){
                                    return a.date > b.date;
                                });
                                //console.log('self.loadTrip::self.trip::'+JSON.stringify(self.trip));

                                if (self.trip.destiny[0].date != null) {
                                    $scope.dateDestiny = self.trip.destiny.date;
                                }
                                self.calculateDistance();
                            }
                        }
                    });
                }
                else {
                    self.messages.error.push($filter('translate')('messageserror-errorintravel'));
                }
            }
            defLoadTrip.resolve();
            //console.log('self.loadTrip::promises::'+promises.length);
            return $.when.apply(undefined, promises).promise();
        };

        self.calculateDistance = function()
        {
            //console.log('self.calculateDistance::self.trip::'+JSON.stringify(self.trip));
            //console.log('self.calculateDistance::self.trip.destiny.length::'+self.trip.destiny.length);
            for (var i = 0; i < self.trip.destiny.length; i++) {
                var destiny = self.trip.destiny[i];
                for (var j = 0; j < destiny.locations.length; j++) {
                    var location = destiny.locations[j];
                    for (var k = 0; k < location.mustSeeAndDo.length; k++) {
                        var msad = location.mustSeeAndDo[k];

                        var distance = null;
                        if (mustseeanddoService.Latitude && mustseeanddoService.Longitude)
                        {
                            var lat = null;
                            var lng = null;
                            try {
                                var pos = msad.geographic_position.split(",");
                                lat = pos[0].trim();
                                lng = pos[1].trim();
                            }
                            catch(e) {
                                lat = 0;
                                lng = 0;
                            }
                            distance = mustseeanddoService.getDistanceFromLatLonInKm(mustseeanddoService.Latitude, mustseeanddoService.Longitude, lat, lng);
                            //console.log('self.calculateDistance::distance::'+distance);
                        }

                        self.trip.destiny[i].locations[j].mustSeeAndDo[k].distance = distance;
                    }
                }
            }
        }

        /**
         * Descripcion:             metodo que elimina el destino
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   1/13/2016
         *
         * @param item
         */
        self.removeDestiny = function (itemParent, item, obj) {

            //console.log('self.removeDestiny::obj::'+JSON.stringify(obj));
            var name = obj.locations[item].name + " - " + $filter('date')(obj.date, "MM/dd/yyyy");
            //console.log('self.removeDestiny::itemParent::'+itemParent);
            //console.log('self.removeDestiny::item::'+item);
            //console.log('self.removeDestiny::name::'+name);
            if(window.cordova) {
                navigator.notification.confirm(
                    $filter('translate')('label-deletedestinynotificatioconfirm1') + name + "?",
                    confirmDelete,
                    $filter('translate')('label-deletedestinynotificatioconfirm2'),
                    $filter('translate')('label-deletedestinynotificatioconfirm3')
                );
            }
            else {
                if(confirm($filter('translate')('label-deletedestinynotificatioconfirm1') + name + "?")) {
                    confirmDelete(1);
                }
            }

            function confirmDelete(res) {
                if (res == 1) {
                    $loading.start('loading');

                    var trip = angular.copy(travelService.getInfoTravels());
                    //console.log('confirmDelete::trip::'+JSON.stringify(trip));

                    for (var i = 0; i < trip.destiny[itemParent].locations[item].mustSeeAndDo.length; i++)
                    {
                        var oldMust = trip.destiny[itemParent].locations[item].mustSeeAndDo[i];
                        //console.log('confirmDelete::oldMust::'+JSON.stringify(oldMust));
                        trip.budget += oldMust.price;
                    }

                    var order_position = trip.destiny[itemParent].locations[item].order_position;
                    var locations = trip.destiny[itemParent].locations.length;
                    //console.log('confirmDelete::order_position::'+order_position);
                    //console.log('confirmDelete::locations::'+locations);
                    //console.log('confirmDelete::trip.destiny[itemParent].locations::'+JSON.stringify(trip.destiny[itemParent].locations));
                    //console.log('confirmDelete::trip.destiny[itemParent].locations[item]::'+JSON.stringify(trip.destiny[itemParent].locations[item]));

                    for (var i = item + 1; i < locations; i++)
                    {
                        trip.destiny[itemParent].locations[i].order_position = order_position++;
                    }
                    //console.log('confirmDelete::trip.destiny::'+JSON.stringify(trip.destiny));
                    trip.destiny[itemParent].locations.splice(item, 1);
                    //console.log('confirmDelete::trip::'+JSON.stringify(trip));

                    // se verifica si hay destinos sin localidades
                    var destiniesNoLocations = _.filter(trip.destiny, function(destiny) { return destiny.locations == null || destiny.locations.length == 0; });
                    if(destiniesNoLocations.length > 0)
                    {
                        // existe destino sin localidades (se debe borrar)
                        //console.log('confirmDelete::destiniesNoLocations[0]::'+JSON.stringify(destiniesNoLocations[0]));
                        var destinyToDeleteIndex = _.indexOf(trip.destiny, destiniesNoLocations[0]);
                        //console.log('confirmDelete::destinyToDeleteIndex::'+destinyToDeleteIndex);
                        trip.destiny.splice(destinyToDeleteIndex, 1);
                    }
                    //console.log('self.confirmDelete::trip::'+JSON.stringify(trip));

                    travelService.setInfoTravels(trip);

                    travelService.autoUpdateDestiny().then(function() {
                        //console.log('confirmDelete::travelService.autoUpdateDestiny()::done');
                        self.trip = travelService.getInfoTravels();
                        $loading.finish('loading');
                        //console.log('self.autoUpdateTrip::$scope.$parent.travelDetail()::'+JSON.stringify($scope.$parent.travelDetail()));
                    });
                }
            }
        };

        self.editDestiny = function (item, obj, id) {
            //console.log('editDestiny::id::'+id+'::item::'+item+'::obj.date::'+obj.date);
            $state.go('app.tabs.edit-destiny', { trip: id, item: item, date: obj.date });
        }

        self.addDestiny = function (id) {
            //console.log('addDestiny::id::'+id);
            $state.go('app.tabs.destiny', { trip: id });
        }

        self.viewMap = function() {
            //console.log('self.viewMap::$scope.viewAcorde::'+$scope.viewAcorde);

            if (self.trip.destiny != null)
            {
                if ((typeof $scope.viewAcorde == "undefined") || !$scope.viewAcorde) {
                    //console.log('self.viewMap::mostrando mapa');

                    //console.log('self.viewMap::self.trip.destiny::'+JSON.stringify(self.trip.destiny));
                    var listDestiny = self.trip.destiny.sort(function(a, b){
                        return a.date - b.date || a.locations[0].order_position - b.locations[0].order_position;
                    });
                    //console.log('self.viewMap::listDestiny::'+JSON.stringify(listDestiny));

                    var listMsad = [];
                    for (var i = 0; i < listDestiny.length; i++)
                    {
                        var destiny_ = listDestiny[i];
                        var listLocation = destiny_.locations.sort(function(a, b){
                            return a.order_position > b.order_position;
                        });
                        for (var j = 0; j < listLocation.length; j++)
                        {
                            var location_ = listLocation[j];
                            if (location_.mustSeeAndDo.length > 0)
                            {
                                var listMustSeeAndDo = location_.mustSeeAndDo.sort(function(a, b){
                                    return a.order_position > b.order_position;
                                });
                                for (var k = 0; k < listMustSeeAndDo.length; k++)
                                {
                                    var mustSeeAndDo_ = listMustSeeAndDo[k];
                                    mustSeeAndDo_.date = destiny_.date;
                                    mustSeeAndDo_.location = location_.name;
                                    listMsad.push(mustSeeAndDo_);
                                }
                            }
                            else
                            {
                                var mustSeeAndDo_ = {};
                                mustSeeAndDo_.title = location_.name;
                                mustSeeAndDo_.geographic_position = location_.geographic_position;
                                mustSeeAndDo_.ar = 0;
                                mustSeeAndDo_.qr = 0;
                                mustSeeAndDo_.date = location_.date;
                                listMsad.push(mustSeeAndDo_);
                            }
                        }
                    }
                    //console.log('self.viewMap::listMsad::'+JSON.stringify(listMsad));
                    //console.log('self.viewMap::listMsad.length::'+listMsad.length);

                    Markers.clearAllMarkers();
                    Markers.setMarkersTravel(listMsad).then(function() {
                        GeoLocation.init("map" + $scope.mapName, GOOGLE_MAPS_APIKEY, false);
                        $ionicScrollDelegate.scrollBottom(true);
                    });
                }

                $scope.viewAcorde = !$scope.viewAcorde;
            }
        }

        self.showMustSeeAndDoDetail = function(msad) {
            $state.go('app.tabs.mustseeanddodetail-travel', {id: msad.id});
        }

        self.moveLocationUp = function (destinyPosition, locationPosition, id) {
            //console.log('self.moveLocationUp::destinyPosition::'+destinyPosition+'::locationPosition::'+locationPosition+'::id::'+id);
            self.moveLocation(destinyPosition, locationPosition, id, -1);
        };

        self.moveLocationDown = function (destinyPosition, locationPosition, id) {
            //console.log('self.moveLocationDown::destinyPosition::'+destinyPosition+'::locationPosition::'+locationPosition+'::id::'+id);
            self.moveLocation(destinyPosition, locationPosition, id, 1);
        };

        self.moveLocation = function (destinyPosition, locationPosition, id, direction) {
            $loading.start('loading');
            //console.log('self.moveLocation::destinyPosition::'+destinyPosition+'::locationPosition::'+locationPosition+'::id::'+id+'::direction::'+direction);
            var trip = angular.copy(travelService.getInfoTravels());
            //console.log('self.moveLocation::trip::'+JSON.stringify(trip));

            var destinyToEdit = trip.destiny[destinyPosition];
            //console.log('self.moveLocation::destinyToEdit::'+JSON.stringify(destinyToEdit));

            var locationToEdit = destinyToEdit.locations[locationPosition];
            //console.log('self.moveLocation::locationToEdit::'+JSON.stringify(locationToEdit));

            var itemToReplace = locationPosition + direction;
            //console.log('self.moveLocation::itemToReplace::'+itemToReplace);
            var locationToReplace = destinyToEdit.locations[itemToReplace];
            //console.log('self.moveLocation::locationToReplace::'+JSON.stringify(locationToReplace));

            if (locationToReplace != null)
            {
                var new_position = locationToReplace.order_position;
                locationToReplace.order_position = locationToEdit.order_position;
                locationToEdit.order_position = new_position;

                //console.log('self.moveLocation::locationToEdit::'+JSON.stringify(locationToEdit));
                trip.destiny[destinyPosition].locations[itemToReplace] = locationToEdit;
                //console.log('self.moveLocation::locationToReplace::'+JSON.stringify(locationToReplace));
                trip.destiny[destinyPosition].locations[locationPosition] = locationToReplace;

                trip.destiny = trip.destiny.sort(function (a, b) {   
                    return a.date - b.date || a.locations[0].order_position - b.locations[0].order_position;
                });
                //console.log('self.moveLocation::trip::'+JSON.stringify(trip));
                travelService.setInfoTravels(trip);

                travelService.autoUpdateDestiny().then(function() {
                    //console.log('self.moveLocation::travelService.autoUpdateDestiny()::done');
                    travelService.getTrip(self.trip).then(function() {
                        self.trip = travelService.getInfoTravels();
                        self.calculateDistance();
                        //console.log('self.moveLocation::self.trip::'+JSON.stringify(self.trip));
                    });
                });
            }
            $loading.finish('loading');
        };

        self.moveMustseeanddoUp = function (destinyPosition, locationPosition, msadPosition, mustseeanddo, destiny) {
            //console.log('self.moveMustseeanddoUp::destinyPosition::'+destinyPosition+'::locationPosition::'+locationPosition+'::msadPosition::'+msadPosition+'::mustseeanddo::'+JSON.stringify(mustseeanddo)+'::destiny::'+JSON.stringify(destiny));
            self.moveMustseeanddo(destinyPosition, locationPosition, msadPosition, -1);
        };

        self.moveMustseeanddoDown = function (destinyPosition, locationPosition, msadPosition, mustseeanddo, destiny) {
            //console.log('self.moveMustseeanddoDown::destinyPosition::'+destinyPosition+'::locationPosition::'+locationPosition+'::msadPosition::'+msadPosition+'::mustseeanddo::'+JSON.stringify(mustseeanddo)+'::destiny::'+JSON.stringify(destiny));
            self.moveMustseeanddo(destinyPosition, locationPosition, msadPosition, 1);
        };

        self.moveMustseeanddo = function (destinyPosition, locationPosition, msadPosition, direction) {
            $loading.start('loading');
            //console.log('self.moveMustseeanddo::destinyPosition::'+destinyPosition+'::locationPosition::'+locationPosition+'::msadPosition::'+msadPosition+'::direction::'+direction);

            var trip = angular.copy(travelService.getInfoTravels());
            //console.log('self.moveMustseeanddo::trip::'+JSON.stringify(trip));

            trip.destiny = trip.destiny.sort(function(a, b){
                return a.date > b.date;
            });

            var destinyOnReorder = trip.destiny[destinyPosition];
            //console.log('self.moveMustseeanddo::destinyOnReorder::'+JSON.stringify(destinyOnReorder));

            if (destinyOnReorder != null)
            {
                destinyOnReorder.locations = destinyOnReorder.locations.sort(function(a, b){
                    return a.order_position > b.order_position;
                });

                var locationOnReorder = destinyOnReorder.locations[locationPosition];
                //console.log('self.moveMustseeanddo::locationOnReorder::'+JSON.stringify(locationOnReorder));

                if (locationOnReorder != null)
                {
                    locationOnReorder.mustSeeAndDo = locationOnReorder.mustSeeAndDo.sort(function(a, b){
                        return a.order_position > b.order_position;
                    });

                    var mustseeanddoOnReorder = locationOnReorder.mustSeeAndDo[msadPosition];
                    //console.log('self.moveMustseeanddo::mustseeanddoOnReorder::'+JSON.stringify(mustseeanddoOnReorder));

                    if (mustseeanddoOnReorder != null)
                    {

                        var itemToReplace = msadPosition + direction;
                        //console.log('self.moveMustseeanddo::itemToReplace::'+itemToReplace);
                        var mustseeanddoToReplace = locationOnReorder.mustSeeAndDo[itemToReplace];
                        //console.log('self.moveMustseeanddo::mustseeanddoToReplace::'+JSON.stringify(mustseeanddoToReplace));

                        if (mustseeanddoToReplace != null)
                        {
                            var new_position = mustseeanddoToReplace.order_position;
                            mustseeanddoToReplace.order_position = mustseeanddoOnReorder.order_position;
                            mustseeanddoOnReorder.order_position = new_position;
                            //console.log('self.moveMustseeanddo::mustseeanddoOnReorder::'+JSON.stringify(mustseeanddoOnReorder));
                            //console.log('self.moveMustseeanddo::mustseeanddoToReplace::'+JSON.stringify(mustseeanddoToReplace));

                            trip.destiny[destinyPosition].locations[locationPosition].mustSeeAndDo[msadPosition] = mustseeanddoOnReorder;
                            trip.destiny[destinyPosition].locations[locationPosition].mustSeeAndDo[itemToReplace] = mustseeanddoToReplace;

                            travelService.setInfoTravels(trip);

                            travelService.autoUpdateDestiny().then(function() {
                                //console.log('self.moveMustseeanddo::travelService.autoUpdateDestiny()::done');
                                travelService.getTrip(self.trip).then(function() {
                                    self.trip = travelService.getInfoTravels();
                                    self.calculateDistance();
                                    //console.log('self.moveMustseeanddo::self.trip::'+JSON.stringify(self.trip));
                                });
                            });
                        }

                    }
                }

            }
            $loading.finish('loading');
        };
    }
})();
