/**
 * Sistema:                 CyM-AR
 * Siglas:                  cym
 *
 * Nombre:                  destinyctrl.js
 * Descripcion:             metodo que sirve de gestion de valores de variables y metodos para los destinos
 * @version                 1.0
 * @author                  Leonor Guzman
 * @since                   12/18/2015
 *
 */
'use strict';
(function ()
{

    angular.module('cym')
        .controller('DestinyCtrl', ['$stateParams', '$filter', '$state', '$scope', 'TravelService', 'MessagesFactory',
            '$ionicHistory', 'GeoLocation', 'GOOGLE_MAPS_APIKEY', 'Markers','$ionicScrollDelegate', 'DEFAULT_COUNTRY', 
            'DEFAULT_STATE', '$loading', 'MustSeeAndDoService', DestinyCtrl]);

    function DestinyCtrl($stateParams, $filter, $state, $scope, tripService, MessagesFactory, 
            $ionicHistory, GeoLocation, GOOGLE_MAPS_APIKEY, Markers, $ionicScrollDelegate, DEFAULT_COUNTRY, 
            DEFAULT_STATE, $loading, mustseeanddoService)
    {

        var self = this;
        $scope.viewAcord = false;
        $scope.viewMsadAcord = true;
        $scope.states = [];
        $scope.locations = [];
        $scope.catalog = [];
        $scope.mustseeandoCount = 0;
        $scope.locationsSelected = [];
        var redirect = false;
        $scope.marks = [];
        var edit = 0;
        $scope.mapName = "Destiny";
        $scope.search = {};
        $scope.search.title = "";

        self.trip = tripService.getInfoTravels();

        /**
         * Descripcion:             metodo que se ejecuta al cargar la pagina
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         */
        $scope.$on('$ionicView.enter', function ()
        {
            $loading.start('loading');

            if (!mustseeanddoService.Latitude || !mustseeanddoService.Longitude)
            {
                mustseeanddoService.getMapLocation();
            }

            //console.log('$stateParams.trip::'+$stateParams.trip);
            //console.log('$stateParams.item::'+$stateParams.item);
            //console.log('$stateParams.date::'+$stateParams.date);
            if($stateParams.date != "" && $stateParams.date != null)
            {
                self.edit = $stateParams.date.length;
            }
            else
            {
                self.edit = 0;
            }

            self.trip = tripService.getInfoTravels();
            //console.log('self.trip.id::'+self.trip.id);
            //console.log('ionicView.enter::self.trip::'+JSON.stringify(self.trip));
            if ((typeof self.trip.id === "undefined") && $stateParams.trip > 0) {
                //console.log('reload travel data');

                self.trip.id = parseInt($stateParams.trip, 10);
                //console.log('ionicView.enter::self.trip.id::'+self.trip.id);

                tripService.getTrip(self.trip).then(function() {
                    //console.log('tripService.getTrip::done');
                    self.trip = tripService.getInfoTravels();
                    //console.log('ionicView.enter::self.trip.name::'+self.trip.name);
                    //console.log('ionicView.enter::self.trip::'+JSON.stringify(self.trip));
                    var orderBy = $filter('orderBy');
                    self.trip.destiny = orderBy(self.trip.destiny, 'date', false);
                    //console.log('ionicView.enter::self.trip::'+JSON.stringify(self.trip));
                    self.initData();
                });
            }
            else
            {
                self.initData();
            }
        });

        self.initData = function() {
            if (redirect == false)
            {
                //console.log('self.initData::self.trip.name::'+self.trip.name);
                //console.log('self.initData::self.trip.id::'+self.trip.id);
                //console.log('self.initData::$stateParams.item::'+$stateParams.item);
                //console.log('$stateParams.date::'+$stateParams.date);
                if ($stateParams.date) {

                    if (self.trip.destiny != null) {
                        //for (var i = 0; i < self.trip.destiny.length; i++) {
                            //console.log('self.trip.destiny['+i+'].name::'+self.trip.destiny[i].name+'::date::'+self.trip.destiny[i].date+'::typeof date::'+(typeof self.trip.destiny[i].date));
                        //}

                        //console.log('self.initData::self.trip.destiny::'+JSON.stringify(self.trip.destiny));
                        var destinyOnDate = _.filter(self.trip.destiny, function(destiny) { return destiny.date === Number($stateParams.date); });
                        //console.log('self.initData::destinyOnDate::'+JSON.stringify(destinyOnDate));
                        //console.log('self.initData::destinyOnDate.length::'+destinyOnDate.length);

                        self.dateDestiny = new Date(destinyOnDate[0].date);
                        //console.log('self.initData::self.dateDestiny::'+self.dateDestiny+'::'+(typeof self.dateDestiny));
                        self.stateIdDestiny = destinyOnDate[0].id;
                        //console.log('self.initData::self.stateIdDestiny::'+self.stateIdDestiny);

                        var locationsOnDate = destinyOnDate[0].locations;
                        //console.log('self.initData::locationsOnDate::'+JSON.stringify(locationsOnDate));

                        var locationsOnDateSorted = locationsOnDate.sort(function(a, b){
                            return a.order_position > b.order_position;
                        });
                        //console.log('self.initData::locationsOnDateSorted::'+JSON.stringify(locationsOnDateSorted));

                        //console.log('self.initData::self.sortArray.length::'+self.sortArray.length);
                        if (locationsOnDateSorted.length > $stateParams.item)
                        {
                            var location = locationsOnDateSorted[$stateParams.item];
                            //console.log('self.initData::location::'+JSON.stringify(location));

                            self.location = location;
                        }
                        //console.log('self.initData::self.location::'+JSON.stringify(self.location));
                        self.description = "";
                    }
                }
                self.load();

            }

            redirect = false;
        }

        /**
         * Descripcion:             mmetodo que carga l pagin
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/15/2015
         *
         */
        self.load = function ()
        {
            //if (self.trip.country)
            //{
                self.loadState();
            //}
        };

        /**
         * Descripcion:             metodo que carga los estados disponibles segun el pais
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         */
        self.loadState = function ()
        {

            self.messages = MessagesFactory.createMessages();

            var countryId = DEFAULT_COUNTRY;
            if (self.trip.country)
            {
                countryId = parseInt(self.trip.country.id, 10);
            }
            //console.log('loadState::countryId::'+countryId);
            var success = function (res)
            {
                //console.log('loadState::success::res.rows.length::'+res.rows.length);
                if (res.rows.length > 0)
                {
                    $scope.states = [];
                    for (var i = 0; i < res.rows.length; i++)
                    {
                        $scope.states.push(res.rows.item(i));
                    }

                    //console.log('$scope.states::'+JSON.stringify($scope.states));
                }

                if ($stateParams.date) {
                    //console.log('loadState::$stateParams.date::'+$stateParams.date);
                    self.stateSelected = $filter('filter')($scope.states, {id: self.stateIdDestiny})[0];
                    //console.log('self.trip.states::'+JSON.stringify(self.stateSelected));

                    $scope.destiny = self.stateSelected;
                    $scope.destiny.locations = [];
                    $scope.destiny.locations.push(self.location);
                    //console.log('$scope.destiny::'+JSON.stringify($scope.destiny));

                    self.loadLocate();
                }
                else {
                    self.stateSelected = $filter('filter')($scope.states, {id: DEFAULT_STATE})[0];
                    //console.log('self.trip.states::'+JSON.stringify(self.stateSelected));
                    self.addStateSelect();
                }
            };

            var error = function (res)
            {
                self.messages.error.push($filter('translate')('messageserror-findlocation'));
                //self.messages.error.push('Error buscando localidad');
                //console.log('error ' + res)
            };

            tripService.selectAllLocationStatesId(countryId).then(success, error);
        };

        /**
         * Descripcion:             metodo que carga las localidades disponibles segun los stados escojidos
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         */
        self.loadLocate = function ()
        {
            self.messages = MessagesFactory.createMessages();
            //console.log('loadLocate::$scope.destiny::'+JSON.stringify($scope.destiny));
            if ($scope.destiny)
            {

                var success = function (res)
                {
                    //console.log('loadLocate::success::res.rows.length::'+res.rows.length);
                    if (res.rows.length > 0)
                    {
                        $scope.locations = [];
                        for (var i = 0; i < res.rows.length; i++)
                        {
                            $scope.locations.push(res.rows.item(i));
                        }
                        //console.log('loadLocate::success::$scope.locations::'+JSON.stringify($scope.locations));

                        if ($stateParams.date)
                        {
                            if (self.location != null)
                            {
                                self.locat = $filter('filter')($scope.locations, {id: self.location.id})[0];
                            }
                            else
                            {
                                //console.log('loadLocate::$scope.destiny::'+JSON.stringify($scope.destiny));
                                self.locat = $filter('filter')($scope.locations, {id: $scope.destiny.id})[0];
                            }
                            //console.log('loadLocate::self.locat::'+JSON.stringify(self.locat));
                        }
                    }
                    self.loadMustAndDo();
                };

                var error = function (res)
                {
                    self.messages.error.push($filter('translate')('messageserror-findlocation'));
                    //self.messages.error.push('Error buscando localidad');
                    //console.log('error ' + res)
                };

                //console.log('loadLocate::$scope.destiny.id::'+$scope.destiny.id);
                //console.log('loadLocate::$scope.destiny.state_id::'+$scope.destiny.state_id);
                var locationId = $scope.destiny.state_id;
                //console.log('loadLocate::locationId::'+locationId);
                if (typeof locationId === "undefined") {
                    locationId = DEFAULT_STATE;
                }
                //console.log('loadLocate::locationId::'+locationId);
                tripService.getLocationId(locationId).then(success, error);
            }

        };


        /**
         * Descripcion:             metodo que asignacion del check al mustseeando
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   1/7/2016
         *
         * @param item
         * @returns {*}
         */
        function convertCheck(item)
        {
            item.checked = false;
            if ((item != null) && (self.location != null))
            {
                var exist = _.filter(self.location.mustSeeAndDo, function(msad) { return msad.id === item.id; });
                //console.log('convertCheck::exist::'+JSON.stringify(exist));

                item.order_position = 10000000;
                if (exist && exist.length > 0)
                {
                    item.checked = true;
                    item.order_position = exist[0].order_position;
                }
            }
            return item;
        }

        /**
         * Descripcion:             metodo que carga los que ver y que hacer segun las localidades escojidas
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         */
        self.loadMustAndDo = function () {
            var locationId = null;
            if ( $scope.destiny.locations != null &&  $scope.destiny.locations[0] != null) {
                locationId = $scope.destiny.locations[0].id;
            }
            //console.log('self.loadMustAndDo::locationId::'+locationId);
            if ((locationId == null) && ($scope.destiny != null))
            {
                locationId = $scope.destiny.id;
            }
            //console.log('self.loadMustAndDo::locationId::'+locationId);
            self.messages = MessagesFactory.createMessages();
            if ( locationId != null ) {
                var exito = function (res) {
                    //console.log('self.loadMustAndDo::res.rows.length::'+res.rows.length);
                    //console.log('self.loadMustAndDo::self.location::'+JSON.stringify(self.location));

                    if (res.rows.length > 0) {
                        $scope.marks = [];
                        $scope.catalog = [];
                        $scope.mustseeandoCount = res.rows.length;

                        //console.log('loadMustAndDo::res.rows.length::'+res.rows.length);
                        //console.log('loadMustAndDo::$stateParams.date::'+$stateParams.date);
                        for (var i = 0; i < res.rows.length; i++) {
                            var obj = res.rows.item(i);
                            var must = convertCheck(obj);

                            must.distance = null;
                            if (mustseeanddoService.Latitude && mustseeanddoService.Longitude)
                            {
                                var lat = null;
                                var lng = null;
                                try {
                                    var pos = must.geographic_position.split(",");
                                    lat = pos[0].trim();
                                    lng = pos[1].trim();
                                }
                                catch(e) {
                                    lat = 0;
                                    lng = 0;
                                }
                                must.distance = mustseeanddoService.getDistanceFromLatLonInKm(mustseeanddoService.Latitude, mustseeanddoService.Longitude, lat, lng);
                            }

                            var catalogById = _.filter($scope.catalog, function(catalog) { return catalog.id === must.catalog_id; });
                            //console.log('self.loadMustAndDo::catalogById::'+JSON.stringify(catalogById));
                            if (catalogById.length > 0)
                            {
                                var catalogIndex = _.indexOf($scope.catalog, catalogById[0]);
                                //console.log('loadMustAndDo::catalogIndex::'+catalogIndex);
                                $scope.catalog[catalogIndex].mustSeeAndDo.push(must);
                            }
                            else
                            {
                                var catalogObj = {};
                                catalogObj.id = must.catalog_id;
                                catalogObj.name = must.name;
                                catalogObj.mustSeeAndDo = [];
                                catalogObj.mustSeeAndDo.push(must);
                                $scope.catalog.push(catalogObj);
                            }

                            var mark = {};
                            mark.title = obj.title;
                            mark.geographic_position = obj.geographic_position;
                            mark.qr = obj.qr;
                            mark.ar = obj.ar;
                            $scope.marks.push(mark);
                        }
                    }
                    //console.log('self.loadMustAndDo::$scope.catalog::'+JSON.stringify($scope.catalog));
                    //console.log('self.loadMustAndDo::self.trip::'+JSON.stringify(self.trip));
                    $loading.finish('loading');
                };

                var fail = function (res) {
                    self.messages.error.push($filter('translate')('messageserror-findmustseeanddo'));
                    //self.messages.error.push('Error buscando must and do');
                    //console.log('error ' + res)
                };

                //console.log('self.loadMustAndDo::locationId::'+locationId);
                tripService.getMustAndDoId(locationId).then(exito, fail);
            }
        };

        $scope.$watch(function () { 
          return mustseeanddoService.Latitude; 
        }, function(newVal, oldVal){
            if(oldVal!==newVal && newVal !== undefined){
                //console.log('calculating distances');
                self.loadMustAndDo();
            }
        });

        /**
         * Descripcion:             metodo que agregar a estados disponibles el estado seleccionado y lo elimina de los disponibles
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since
         */
        self.addStateSelect = function ()
        {
            $scope.destiny = self.stateSelected;
            //console.log('self.addStateSelect::$scope.destiny::'+JSON.stringify($scope.destiny));
            self.loadLocate();
            $scope.destiny.locations = [];
        };


        /**
         * Descripcion:             metodo que agrega a las localidades seleccionadas y las elimina de las disponibles
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         * @param item
         */
        self.addLocateSelect = function ()
        {
            $loading.start('loading');
            //console.log('addLocateSelect::self.trip.destiny::'+JSON.stringify(self.trip.destiny));
            //console.log('addLocateSelect::$scope.destiny::'+JSON.stringify($scope.destiny));
            $scope.destiny.locations = [];
            //console.log('addLocateSelect::self.locat::'+JSON.stringify(self.locat));
            self.pushToArray(self.locat, $scope.destiny.locations);
            //console.log('addLocateSelect::$scope.destiny::'+JSON.stringify($scope.destiny));

            $scope.catalog = [];
            $scope.mustseeandoCount = 0;
            self.loadMustAndDo();
        };

        /**
         * Descripcion:             metodo que remueve de las seleccionada y agrega a las disponibles
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         * @param item
         */
        self.removeLocateSelect = function (item)
        {
            self.messages = MessagesFactory.createMessages();
            if (item)
            {
                self.removeToArray(item, $scope.destiny.locations);
                self.pushToArray(item, $scope.locations);
            }
            $scope.catalog = [];
            $scope.mustseeandoCount = 0;
            
            self.loadLocate();
            self.loadMustAndDo();
        };

        /**
         * Descripcion:             metodo que agrega al objeto de viaje segun validaciones
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         */
        self.saveDestiny = function ()
        {
            self.messages = MessagesFactory.createMessages();
            //console.log('saveDestiny::$stateParams.date::'+$stateParams.date+'::$stateParams.item::'+$stateParams.item);

            //console.log('saveDestiny::$scope.destiny::'+JSON.stringify($scope.destiny));
            var destinyToAdd = {};
            destinyToAdd.id = $scope.destiny.id;
            destinyToAdd.name = $scope.destiny.name;
            destinyToAdd.country_id = $scope.destiny.country_id;
            destinyToAdd.locations = [];
            //console.log('saveDestiny::destinyToAdd::'+JSON.stringify(destinyToAdd));

            //console.log('saveDestiny::$scope.destiny.locations::'+JSON.stringify($scope.destiny.locations));
            var locationToAdd = $scope.destiny.locations[0];
            //if($scope.destiny.locations[0].mustSeeAndDo)
            //{
            //    $scope.destiny.locations[0].mustSeeAndDo = $scope.destiny.locations[0].mustSeeAndDo.sort(function(a, b){
            //        return a.order_position > b.order_position;
            //    });
            //}
            if(locationToAdd.mustSeeAndDo)
            {
                locationToAdd.mustSeeAndDo = locationToAdd.mustSeeAndDo.sort(function(a, b){
                    return a.order_position > b.order_position;
                });
            }
            //console.log('saveDestiny::locationToAdd::'+JSON.stringify(locationToAdd));
            //console.log('saveDestiny::$scope.destiny::'+JSON.stringify($scope.destiny));
            //console.log('saveDestiny::self.trip.destiny::'+JSON.stringify(self.trip.destiny));
            var destinyIndex = -1;
            if (validDate())
            {
                // ajusto el formato de fecha del destino
                //console.log('saveDestiny::self.dateDestiny::'+self.dateDestiny);
                if (self.dateDestiny)
                {
                    //$scope.destiny.date = self.dateDestiny.getTime();
                    destinyToAdd.date = self.dateDestiny.getTime();
                }
                locationToAdd.date = self.dateDestiny.getTime();
                locationToAdd.description = "";

                //console.log('saveDestiny::valid dates');
                //$scope.destiny.description = "";
                destinyToAdd.description = "";

                // filtro los mustseeandmustdo seleccionados
                var mustSelected = [];
                //console.log('self.saveDestiny::$scope.catalog.length::'+$scope.catalog.length);
                for (var i = 0; i < $scope.catalog.length; i++)
                {
                    var catalogMustseeanddo = $scope.catalog[i].mustSeeAndDo;
                    //console.log('self.saveDestiny::$scope.catalog[i].name::'+$scope.catalog[i].name);
                    //console.log('self.saveDestiny::catalogMustseeanddo.length::'+catalogMustseeanddo.length);
                    //console.log('self.saveDestiny::catalogMustseeanddo::'+JSON.stringify(catalogMustseeanddo));
                    var mustCatalogSelected = $filter('filter')(catalogMustseeanddo, {checked: 'true'});
                    //console.log('self.saveDestiny::mustCatalogSelected.length::'+mustCatalogSelected.length);
                    //console.log('self.saveDestiny::mustCatalogSelected::'+JSON.stringify(mustCatalogSelected));
                    if (mustCatalogSelected.length > 0)
                    {
                        mustSelected.push.apply(mustSelected, mustCatalogSelected);
                        //console.log('self.saveDestiny::for::mustSelected.length::'+mustSelected.length);
                        //console.log('self.saveDestiny::for::mustSelected::'+JSON.stringify(mustSelected));
                    }
                }
                //console.log('self.saveDestiny::mustSelected.length::'+mustSelected.length);
                //console.log('self.saveDestiny::mustSelected::'+JSON.stringify(mustSelected));
                mustSelected = mustSelected.sort(function(a, b){
                    return a.order_position > b.order_position;
                });
                //console.log('self.saveDestiny::mustSelected::'+JSON.stringify(mustSelected));
                var order_position = 0;
                //console.log('self.saveDestiny::order_position::'+order_position);
                for (var j = 0; j < mustSelected.length; j++)
                {
                    mustSelected[j].order_position = order_position++;
                }
                //console.log('self.saveDestiny::mustSelected::'+JSON.stringify(mustSelected));

                if ($stateParams.date && $stateParams.item)
                {
                    // obteniendo el destino donde está la localidad a editar
                    var destinyOnDate = _.filter(self.trip.destiny, function(destiny) { return destiny.date === Number($stateParams.date); });
                    //console.log('saveDestiny::destinyOnDate::'+JSON.stringify(destinyOnDate));
                    //console.log('saveDestiny::destinyOnDate[0]::'+JSON.stringify(destinyOnDate[0]));
                    destinyIndex = _.indexOf(self.trip.destiny, destinyOnDate[0]);
                    //console.log('saveDestiny::destinyIndex::'+destinyIndex);

                    //console.log('saveDestiny::self.trip::'+JSON.stringify(self.trip));

                    // antes de remover, incrementar el presupuesto con el monto de los sitios turísticos
                    // del destino a eliminar
                    //console.log('saveDestiny::destinyOnDate[0].locations::'+JSON.stringify(destinyOnDate[0].locations));
                    
                    var locationToEdit = destinyOnDate[0].locations[$stateParams.item];
                    //console.log('saveDestiny::locationToEdit::'+JSON.stringify(locationToEdit));

                    // se mantiene el destino en el orden que estaba
                    //$scope.destiny.locations[0].order_position = locationToEdit.order_position;
                    locationToAdd.order_position = locationToEdit.order_position;

                    for (var i = 0; i < destinyOnDate[0].locations[$stateParams.item].mustSeeAndDo.length; i++)
                    {
                        var oldMust = destinyOnDate[0].locations[$stateParams.item].mustSeeAndDo[i];
                        //console.log('saveDestiny::oldMust::'+JSON.stringify(oldMust));
                        self.trip.budget += oldMust.price;
                    }
                    //console.log('saveDestiny::self.trip::'+JSON.stringify(self.trip));

                    // se remueve el destino que se está editando, para agregar los cambios especificados
                    destinyOnDate[0].locations.splice($stateParams.item, 1);
                    for (var i = 0; i < destinyOnDate[0].locations.length; i++)
                    {
                        destinyOnDate[0].locations[i].order_position = i;
                    }
                    //console.log('saveDestiny::destinyOnDate[0].locations::'+JSON.stringify(destinyOnDate[0].locations));
                    self.trip.destiny[destinyIndex] = destinyOnDate[0];
                    //console.log('saveDestiny::self.trip::'+JSON.stringify(self.trip));
                }

                if (self.trip.budget == null)
                {
                    self.trip.budget = 0;
                }
                //console.log('self.saveDestiny::self.trip.budget::'+self.trip.budget);

                //console.log('self.saveDestiny::$scope.destiny::'+JSON.stringify($scope.destiny));
                //console.log('self.saveDestiny::$scope.destiny.locations[0]::'+JSON.stringify($scope.destiny.locations[0]));
                //$scope.destiny.locations[0].mustSeeAndDo = [];
                locationToAdd.mustSeeAndDo = [];
                if (mustSelected.length > 0) {
                    //$scope.destiny.locations[0].mustSeeAndDo = mustSelected;
                    locationToAdd.mustSeeAndDo = mustSelected;
                }
                //console.log('self.saveDestiny::locationToAdd::'+JSON.stringify(locationToAdd));

                //console.log('saveDestiny::self.trip.destiny::'+JSON.stringify(self.trip.destiny));

                // valido si estoy editando el destino
                if (self.trip.destiny == null)
                {
                    //console.log('saveDestiny::new travel');
                    self.trip.destiny = [];
                }

                //console.log('self.saveDestiny::$scope.destiny::'+JSON.stringify($scope.destiny));
                //console.log('self.saveDestiny::self.trip.destiny::'+JSON.stringify(self.trip.destiny));
                //console.log('self.saveDestiny::self.dateDestiny::'+self.dateDestiny);
                //console.log('self.saveDestiny::self.dateDestiny.getTime()::'+self.dateDestiny.getTime());

                // verificar que para la fecha establecida no se tenga este mismo destino, en caso afirmativo, debería editar ese destino
                // verificando destinos en la fecha especificada
                //var locationName = $scope.destiny.locations[0].name;
                var locationName = locationToAdd.name;
                //console.log('self.saveDestiny::locationName::'+locationName);
                var duplicatedDestiniesOnDate = _.filter(self.trip.destiny, function(destiny) { return destiny.date === locationToAdd.date; });
                var duplicatedDestiny = false;
                //console.log('self.saveDestiny::duplicatedDestiniesOnDate.length::'+duplicatedDestiniesOnDate.length);
                //console.log('self.saveDestiny::duplicatedDestiniesOnDate::'+JSON.stringify(duplicatedDestiniesOnDate));

                for (var i = 0; i < duplicatedDestiniesOnDate.length; i++) {
                    // hay destinos para la fecha especificada, se verifica si alguno tiene el municipio seleccionado

                    var locations = duplicatedDestiniesOnDate[i].locations;
                    //console.log('self.saveDestiny::locations::'+JSON.stringify(locations));
                    var duplicatedDestiniesOnLocation = _.filter(locations, function(location) { return location.name === locationName; });
                    if (duplicatedDestiniesOnLocation.length > 0)
                    {
                        duplicatedDestiny = true;
                        break;
                    }
                }
                //console.log('self.saveDestiny::duplicatedDestiny::'+duplicatedDestiny);

                if (duplicatedDestiny)
                {
                    self.messages.error.push($filter('translate')('messageserror-alreadydestinationonsamedate'));
                }
                else {

                    var order_position = 0;

                    if (($stateParams.date == null) || ($stateParams.item == null))
                    {
                        //$scope.destiny.locations[0].order_position = order_position;
                        locationToAdd.order_position = order_position;
                    }
                    //console.log('self.saveDestiny::$scope.destiny.locations[0].order_position::'+$scope.destiny.locations[0].order_position);

                    // se debe actualizar el presupuesto del viaje
                    //console.log('self.saveDestiny::$scope.destiny::'+JSON.stringify($scope.destiny));
                    //if ($scope.destiny.locations[0].mustSeeAndDo != null)
                    if (locationToAdd.mustSeeAndDo != null)
                    {
                        //for (var i = 0; i < $scope.destiny.locations[0].mustSeeAndDo.length; i++) {
                        for (var i = 0; i < locationToAdd.mustSeeAndDo.length; i++) {
                            //self.trip.budget -= $scope.destiny.locations[0].mustSeeAndDo[i].price;
                            self.trip.budget -= locationToAdd.mustSeeAndDo[i].price;
                            //console.log('self.saveDestiny::self.trip.budget::'+self.trip.budget);
                        }
                        //console.log('self.saveDestiny::self.trip.budget::'+self.trip.budget);
                    }

                    if (duplicatedDestiniesOnDate.length > 0)
                    {
                        // ya existe un destino
                        var duplicatedDestiniesOnDateIndex = _.indexOf(self.trip.destiny, duplicatedDestiniesOnDate[0]);
                        //console.log('self.saveDestiny::duplicatedDestiniesOnDateIndex::'+duplicatedDestiniesOnDateIndex);
                        locationToAdd.order_position = duplicatedDestiniesOnDate[0].locations.length;
                        self.trip.destiny[duplicatedDestiniesOnDateIndex].locations.push(locationToAdd);
                    }
                    else
                    {
                        //console.log('agregando destino');
                        locationToAdd.order_position = 0;
                        destinyToAdd.locations.push(locationToAdd);
                        //console.log('self.saveDestiny::destinyToAdd::'+JSON.stringify(destinyToAdd));
                        self.trip.destiny.push(destinyToAdd);
                    }

                    //console.log('self.saveDestiny::self.trip::'+JSON.stringify(self.trip));
                    tripService.setInfoTravels(self.trip);
                    //console.log('self.saveDestiny::self.trip::'+JSON.stringify(self.trip));
                    self.trip.destiny = self.trip.destiny.sort(function(a, b){
                        return a.date > b.date;
                    });

                    // se verifica si hay destinos sin localidades
                    var destiniesNoLocations = _.filter(self.trip.destiny, function(destiny) { return destiny.locations == null || destiny.locations.length == 0; });
                    if(destiniesNoLocations.length > 0)
                    {
                        // existe destino sin localidades (se debe borrar)
                        //console.log('saveDestiny::destiniesNoLocations[0]::'+JSON.stringify(destiniesNoLocations[0]));
                        var destinyToDeleteIndex = _.indexOf(self.trip.destiny, destiniesNoLocations[0]);
                        //console.log('saveDestiny::destinyToDeleteIndex::'+destinyToDeleteIndex);
                        self.trip.destiny.splice(destinyToDeleteIndex, 1);
                    }
                    //console.log('self.saveDestiny::self.trip::'+JSON.stringify(self.trip));

                    //console.log('self.saveDestiny::$stateParams.date::'+$stateParams.date);
                    //console.log('self.saveDestiny::self.trip.id::'+self.trip.id);

                    if (self.trip.id != null)
                    {
                        tripService.autoUpdateDestiny().then(function() {
                            //console.log('self.saveDestiny::tripService.autoUpdateDestiny()');
                            tripService.getTrip(self.trip).then(function() {
                                //console.log('self.saveDestiny::tripService.getTrip()');
                            });
                        });
                    }

                    if (self.trip.id != null) 
                    {
                        $state.go('app.tabs.detail-travel', {id: self.trip.id});
                    }
                    else if (typeof $ionicHistory.backTitle() !== "undefined")
                    {
                        $ionicHistory.goBack();
                    }
                    else
                    {
                        $state.go('app.tabs.newTravel', {cache: false});
                    }
                }
            }
        };

        /**
         * Descripcion:             metodo que valida las fechas que no sea inferior contra las de viaje
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   2/3/2016
         *
         */
        var validDate = function ()
        {
            var retorno = false;
            //console.log('validDate::self.dateDestiny::'+self.dateDestiny);
            //console.log('validDate::typeof self.dateDestiny::'+typeof self.dateDestiny);
            //console.log('validDate::self.dateDestiny.getTime()::'+self.dateDestiny.getTime());
            if (self.dateDestiny == null)
            {
                self.messages.error.push($filter('translate')('messageserror-datedetinyrequired'));
                //self.messages.error.push("El campo fecha de destino es obligatorio");
            }
            else
            {
                if ((self.dateDestiny > self.trip.dateArrival) || (self.dateDestiny < self.trip.dateGoing))
                {
                    self.messages.error.push($filter('translate')('messageserror-wrongdatedestinyouttrip', { dateGoing: self.trip.dateGoing.toLocaleDateString(), dateArrival: self.trip.dateArrival.toLocaleDateString() }));
                }
            }

            if (self.messages.error.length == 0)
            {
                retorno = true;
            }
            else
            {
                $ionicScrollDelegate.scrollTop(true);
            }

            return retorno;

        };

        /**
         * Descripcion:             metodo que limpia todas las variables scope que se usan en la ventana
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         */
        self.clearScope = function ()
        {
            $scope.states = [];
            $scope.destiny = {};
            $scope.locations = [];

        };

        /**
         * Descripcion:             metodo que compara un iten con un array segun parametro
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   1/6/2016
         *
         * @param element
         * @param array
         */
        self.elemenExist = function (element, array)
        {

            if (array != null && element != null)
            {
                for (var x = 0; x < array.length; x++)
                {
                    if (array[x].id == element.id)
                    {
                        return true;
                    }
                }
            }
            return false;
        };

        /**
         * Descripcion:             metodo que  agregar a un array
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   1/6/2016
         *
         * @param element
         * @param array
         */
        self.pushToArray = function (element, array)
        {
            if (array != null && element != null)
            {
                if (!self.elemenExist(element, array))
                //{
                //    self.removeToArray(element, array);
                //    array.push(element);
                //}
                //else
                {
                    array.push(element);
                }
            }
        };

        /**
         * Descripcion:             metodo que elimina de un determinado array
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   1/6/2016
         *
         * @param element
         * @param array
         */
        self.removeToArray = function (element, array)
        {
            if (array != null && element != null)
            {
                for (var i = 0; i < array.length; i++)
                {
                    if (array[i].id == element.id)
                    {
                        array.splice(i, 1);
                        break;
                    }
                }
            }
        };

        /**
         * Descripcion:             metodo que redirecciona a los destinos recomendados
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   2/15/2016
         *
         */
        self.redirectPreferred = function ()
        {
            redirect = true;
            $state.go('app.tabs.preferred', {stateId: self.stateSelected.id});
        }

        self.viewMap = function()
        {
            //console.log('self.viewMap::$scope.viewAcorde::'+$scope.viewAcorde);

            if ((typeof $scope.viewAcorde == "undefined") || !$scope.viewAcorde) {
                //console.log('self.viewMap::mostrando mapa');
                Markers.clearAllMarkers();
                //console.log('self.viewMap::$scope.marks::'+JSON.stringify($scope.marks));
                Markers.setMarkersMustSeeAndDoTravel($scope.marks).then(function() {
                    GeoLocation.init("map" + $scope.mapName, GOOGLE_MAPS_APIKEY, false);
                    $ionicScrollDelegate.scrollBottom(true);
                });
            }
            $scope.viewAcorde = !$scope.viewAcorde;
        }

        self.clearButton = function () {
            $scope.search.title = "";
        };
    }
})
();
