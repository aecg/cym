/**
 * Sistema:                 CyM-AR
 * Siglas:                  cym
 *
 * Nombre:                  travelsctrl.js
 * Descripcion:             controlado que mantiene el valor de las varibales y la ejecucion de metodos de la vista de consulta de todos los viajes
 * @version                 1.0
 * @author                  Leonor Guzman
 * @since                   12/18/2015
 *
 */
'use strict';
(function ()
{

    angular.module('cym').controller('TravelsCtrl', ['$scope', '$filter', '$rootScope', '$state', 'UserService', 
            'TravelService', 'MessagesFactory', 'MustSeeAndDoService', TravelsCtrl]);

    function TravelsCtrl($scope, $filter, $rootScope, $state, userService, 
            travelsService, MessagesFactory, mustseeanddoService)
    {

        var self = this;

        $scope.viewOption = false;

        self.data = travelsService.getAllTravelsUser();


        /**
         * Descripcion:             metodo que se ejecuta al cargar la pagina
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         */
        $scope.$on('$ionicView.enter', function ()
        {
            if (!mustseeanddoService.Latitude || !mustseeanddoService.Longitude)
            {
                mustseeanddoService.getMapLocation();
            }
            self.loadTravels();
        });


        $scope.$on("$ionicView.afterLeave", function (event, data)
        {
            self.data = {
                travels: {
                    destiny: []
                },
                user_id: 0
            };
            //console.log("State Params: ", data.stateParams);
        });

        /**
         * Descripcion:             metodo que carga todos los travels asociados al usuario
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         */
        self.loadTravels = function ()
        {
            self.messages = MessagesFactory.createMessages();
            if (userService.isUserLogged())
            {
                self.data.user_id = userService.getUserLoggedId();
                travelsService.setAllTravelsUser(self.data);
                travelsService.getTravels(self.data);
            }
            else
            {
                self.messages.error.push($filter('translate')('messageserror-usernotlogin'));
                //self.messages.error.push('Usuario no logeado');
            }
        };


        /**
         * Descripcion:             metodo que elimina el viaje seleccionado
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         * @param $id
         */
        self.deleteTrip = function ($id, name)
        {
            self.messages = MessagesFactory.createMessages();

            if (userService.isUserLogged() && $id)
            {
                if(window.cordova)
                {
                    navigator.notification.confirm(
                        $filter('translate')('label-deletetravelnotificatioconfirm1') + name + "?",
                        confirmDelete,
                        $filter('translate')('label-deletetravelnotificatioconfirm2'),
                        $filter('translate')('label-deletetravelnotificatioconfirm3')
                    );
                }
                else
                {
                    if(confirm($filter('translate')('label-deletetravelnotificatioconfirm1') + name + "?")) {
                        confirmDelete(1);
                    }
                }
            }
            else
            {
                self.messages.error.push($filter('translate')('messageserror-deletetrip'));
                //self.messages.error.push('Ocurrio un error al elminar el viaje');
            }

            function confirmDelete(res)
            {
                if (res == 1)
                {
                    var trip = {};
                    trip.id = $id;
                    travelsService.deleteTravels(trip);

                    self.loadTravels();
                }
            }

        };


        /**
         * Descripcion:             metodo que limipa las variables de travels asociadas al usuario
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         */
        self.clearTrip = function ()
        {
            travelsService.infoClear();
            $state.go('app.tabs.newTravel', {cache: false});
        };


        self.navigateTo = function (id)
        {
            $state.go('app.tabs.detail-travel', {id: id});
        }
    }
})();

