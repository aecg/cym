/**
 * Sistema:                 CyM-AR
 * Siglas:                  cym
 *
 * Nombre:                  destinyctrl.js
 * Descripcion:             metodo que sirve de controlador a la pagina de destino recomendado
 * @version                 1.0
 * @author                  Leonor Guzman
 * @since                   2/15/2016
 *
 */
'use strict';
(function ()
{

    angular.module('cym')
        .controller('DestinyPreferredCtrl', ['$stateParams', '$filter', '$state', '$scope', 'TravelService', 'MessagesFactory',
            '$ionicHistory', 'GeoLocation', 'GOOGLE_MAPS_APIKEY', 'Markers', 'DEFAULT_COUNTRY', 'DEFAULT_STATE', '$loading',
            '$ionicScrollDelegate', 'MustSeeAndDoService', DestinyPreferredCtrl]);

    function DestinyPreferredCtrl($stateParams, $filter, $state, $scope, tripService, MessagesFactory, 
            $ionicHistory, GeoLocation, GOOGLE_MAPS_APIKEY, Markers, DEFAULT_COUNTRY, DEFAULT_STATE, $loading, 
            $ionicScrollDelegate, mustseeanddoService)
    {

        var self = this;
        self.arreglo = {};
        self.budget = 0;
        var redirect = false;
        self.selectedMustSeeAndDo = [];

        /**
         * Descripcion:             metodo que se inicia al cargar la pagina
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   2/15/2016
         *
         */
        $scope.$on('$ionicView.enter', function () {
            //console.log('redirect::'+redirect);
            //console.log('$stateParams.trip::'+$stateParams.trip);
            //console.log('$stateParams.stateId::'+$stateParams.stateId);
            //console.log('$stateParams.budget::'+$stateParams.budget);
            if (!mustseeanddoService.Latitude || !mustseeanddoService.Longitude)
            {
                mustseeanddoService.getMapLocation();
            }
            if (redirect == false) {
                $loading.start('loading');
                self.trip = tripService.getInfoTravels();

                if ((typeof self.trip.id === "undefined") && $stateParams.trip > 0) {
                    //console.log('reload travel data');

                    self.trip.id = parseInt($stateParams.trip, 10);
                    //console.log('self.trip.id::'+self.trip.id);

                    tripService.getTrip(self.trip).then(function() {
                        //console.log('tripService.getTrip::done');
                        self.trip = tripService.getInfoTravels();
                        //console.log('self.trip.name::'+self.trip.name);
                        self.loadState();
                        //console.log('$ionicView.enter::self.trip::'+JSON.stringify(self.trip));
                    });
                }
                else
                {
                    //console.log('self.trip.name::'+self.trip.name);
                    self.loadState();
                    //console.log('$ionicView.enter::self.trip::'+JSON.stringify(self.trip));
                }
            }
            redirect = false;
        });

        $scope.referred = function () {
            return self.arreglo;
        };

        /**
         * Descripcion:             metodo que carga los estados disponibles segun el pais
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         */
        self.loadState = function () {

            //self.recommendedDate = new Date();
            //if (self.trip.dateGoing != null)
            //{
            //    self.recommendedDate = self.trip.dateGoing;
            //}
            //self.recommendedDate.setHours(0,0,0);
            //console.log('self.loadState::self.recommendedDate::'+self.recommendedDate);

            self.messages = MessagesFactory.createMessages();
            var id = DEFAULT_COUNTRY;
            if (self.trip.country)
            {
                id = parseInt(self.trip.country.id, 10);
            }
            //console.log('self.loadState::id::'+id);
            var success = function (res) {
                //console.log('self.loadState::res.rows.length::'+res.rows.length);
                if (res.rows.length > 0) {
                    self.arreglo.state = res.rows.item(0);
                    //console.log('self.loadState::self.arreglo.state::'+JSON.stringify(self.arreglo.state));
                    $scope.destiny = self.arreglo.state;
                    //console.log('self.loadState::$scope.destiny::'+JSON.stringify($scope.destiny));
                    self.loadMustAndDo();
                }
            };

            var error = function (res) {
                self.messages.error.push($filter('translate')('messageserror-findlocation'));
                //console.log('error::' + res)
            };

            var stateId = $stateParams.stateId;
            if (stateId == null || stateId == '') {
                stateId = DEFAULT_STATE;
            }

            tripService.selectStatesByCountryAndId(id, stateId).then(success, error);
        };

        /**
         * Descripcion:             metodo que carga los que ver y que hacer segun las localidades escojidas
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         */
        self.loadMustAndDo = function () {

            self.messages = MessagesFactory.createMessages();
            var exito = function (res) {
                //console.log('loadMustAndDo::exito::res.rows.length::'+res.rows.length);
                self.arreglo.price = 0;
                self.arreglo.mustSeeAndDo = [];
                for (var i = 0; i < res.rows.length; i++) {
                    var msad = res.rows.item(i);

                    msad.distance = null;
                    if (mustseeanddoService.Latitude && mustseeanddoService.Longitude)
                    {
                        var lat = null;
                        var lng = null;
                        try {
                            var pos = msad.geographic_position.split(",");
                            lat = pos[0].trim();
                            lng = pos[1].trim();
                        }
                        catch(e) {
                            lat = 0;
                            lng = 0;
                        }
                        msad.distance = mustseeanddoService.getDistanceFromLatLonInKm(mustseeanddoService.Latitude, mustseeanddoService.Longitude, lat, lng);
                    }

                    self.arreglo.mustSeeAndDo.push(msad);
                }
                self.arreglo.mustSeeAndDo = self.arreglo.mustSeeAndDo.sort(function(a, b) {
                    return a.distance - b.distance || a.price - b.price;
                });
                //console.log('loadMustAndDo::exito::self.arreglo.mustSeeAndDo::'+JSON.stringify(self.arreglo.mustSeeAndDo));
                $loading.finish('loading');
            };

            var fail = function (res) {
                self.messages.error.push($filter('translate')('messageserror-findmustseeanddo'));
                //console.log('error ' + res)
            };

            var stateId = $stateParams.stateId;
            if (stateId == null || stateId == '') {
                stateId = DEFAULT_STATE;
            }
            //console.log('loadMustAndDo::$stateParams.budget::'+$stateParams.budget);
            self.budget = parseInt($stateParams.budget, 10);
            self.tempBudget = self.budget;

            //console.log('loadMustAndDo::stateId::'+stateId+'::self.budget::'+self.budget);
            tripService.getMustFromState(stateId, self.budget).then(exito, fail);
        };

        $scope.$watch(function () { 
          return mustseeanddoService.Latitude; 
        }, function(newVal, oldVal){
            if(oldVal!==newVal && newVal !== undefined){
                //console.log('calculating distances');
                self.loadMustAndDo();
            }
        });

        self.showMustSeeAndDoDetail = function(msad) {
            $state.go('app.tabs.mustseeanddodetail-travel', {id: msad.id});
        }

        self.updateBudget = function(item)
        {
            //console.log('self.updateBudget::item::'+JSON.stringify(item));
            if (item.checked)
            {
                self.tempBudget -= item.price;
            }
            else
            {
                self.tempBudget += item.price;
            }
            //console.log('self.updateBudget::self.tempBudget::'+self.tempBudget);
        }

        var validDate = function ()
        {
            var retorno = false;
            //console.log('validDate::self.recommendedDate::'+self.recommendedDate);
            //console.log('validDate::typeof self.recommendedDate::'+typeof self.recommendedDate);
            if (self.recommendedDate == null)
            {
                self.messages.error.push($filter('translate')('messageserror-datepreferredrequired'));
                //self.messages.error.push("El campo fecha de destino es obligatorio");
            }
            else
            {
                //console.log('validDate::self.recommendedDate.getTime()::'+self.recommendedDate.getTime());
                if ((self.trip.dateArrival != null) && (self.trip.dateGoing != null))
                {
                    if ((self.recommendedDate > self.trip.dateArrival) || (self.recommendedDate < self.trip.dateGoing))
                    {
                        self.messages.error.push($filter('translate')('messageserror-wrongdatedestinyouttrip', { dateGoing: self.trip.dateGoing.toLocaleDateString(), dateArrival: self.trip.dateArrival.toLocaleDateString() }));
                    }
                }
            }

            if (self.messages.error.length == 0)
            {
                retorno = true;
            }
            else
            {
                $ionicScrollDelegate.scrollTop(true);
            }

            return retorno;

        };

        self.addPreferred = function()
        {
            self.messages = MessagesFactory.createMessages();
            if (validDate())
            {
                $loading.start('loading');

                self.messages = MessagesFactory.createMessages();
                //console.log('self.loadState::self.recommendedDate::'+self.recommendedDate);
                $scope.destiny.date = self.recommendedDate.getTime();
                $scope.destiny.description = "";
                $scope.destiny.locations = [];
                //console.log('self.loadState::$scope.destiny::'+JSON.stringify($scope.destiny));
                //console.log('self.loadState::$scope.destiny.date::'+$scope.destiny.date);

                //console.log('self.addPreferred::self.trip::'+JSON.stringify(self.trip));
                if (self.trip.destiny == null)
                {
                    self.trip.destiny = [];
                }

                //console.log('self.addPreferred::self.messages.error.length::'+self.messages.error.length);
                if (self.messages.error.length == 0) {
                    // obteniendo los destinos para la fecha recomendada
                    self.duplicatedDestiniesOnDate = _.filter(self.trip.destiny, function(destiny) { return destiny.date === self.recommendedDate.getTime(); });
                    //console.log('self.addPreferred::self.duplicatedDestiniesOnDate::'+JSON.stringify(self.duplicatedDestiniesOnDate));

                    //console.log('self.addPreferred::self.trip.destiny.length::'+self.trip.destiny.length);
                    //console.log('self.addPreferred::$scope.destiny::'+JSON.stringify($scope.destiny));
                    self.processRecommendedMustSeeAndDo().then(function() {
                        //console.log('self.addPreferred::self.processRecommendedMustSeeAndDo::done');
                        //console.log('self.addPreferred::$scope.destiny::'+JSON.stringify($scope.destiny));

                        if ($scope.destiny.locations.length > 0)
                        {
                            //console.log('self.addPreferred::self.duplicatedDestiniesOnDate::'+JSON.stringify(self.duplicatedDestiniesOnDate));
                            //console.log('self.addPreferred::self.duplicatedDestiniesOnDate.length::'+self.duplicatedDestiniesOnDate.length);
                            if (self.duplicatedDestiniesOnDate.length == 0)
                            {
                                // ordenar los destinos
                                //console.log('self.addPreferred::$scope.destiny.locations.length::'+$scope.destiny.locations.length);
                                for (var i = 0; i < $scope.destiny.locations.length; i++) {
                                    $scope.destiny.locations[i].order_position = i;

                                    //console.log('self.addPreferred::$scope.destiny.locations['+i+'].mustSeeAndDo.length::'+$scope.destiny.locations[i].mustSeeAndDo.length);
                                    for (var j = 0; j < $scope.destiny.locations[i].mustSeeAndDo.length; j++) {
                                        $scope.destiny.locations[i].mustSeeAndDo[j].order_position = j;
                                    }
                                }
                                //console.log('self.addPreferred::$scope.destiny::'+JSON.stringify($scope.destiny));
                                // no se tienen destinos duplicados para la fecha recomendada, nada que validar
                                self.trip.destiny.push($scope.destiny);
                            }
                            else
                            {
                                var duplicatedDestiniesOnDateIndex = _.indexOf(self.trip.destiny, self.duplicatedDestiniesOnDate[0]);
                                //console.log('self.addPreferred::duplicatedDestiniesOnDateIndex::'+duplicatedDestiniesOnDateIndex);

                                for (var i = 0; i < $scope.destiny.locations.length; i++) {
                                    var location_ = $scope.destiny.locations[i];
                                    //console.log('self.addPreferred::location_::'+JSON.stringify(location_));

                                    var duplicatedLocation_ = _.filter(self.duplicatedDestiniesOnDate[0].locations, function(location) { return location.id === location_.id; });
                                    //console.log('self.addPreferred::duplicatedLocation_.length::'+JSON.stringify(duplicatedLocation_.length));
                                    //console.log('self.addPreferred::duplicatedLocation_::'+JSON.stringify(duplicatedLocation_));
                                    
                                    if (duplicatedLocation_.length > 0)
                                    {
                                        // ya se tiene una location para esta fecha
                                        var duplicatedDestinyIndex = _.indexOf(self.trip.destiny, self.duplicatedDestiniesOnDate[0]);
                                        //console.log('addPreferred::duplicatedDestinyIndex::'+duplicatedDestinyIndex);

                                        var duplicatedLocationIndex = _.indexOf(self.trip.destiny[duplicatedDestinyIndex].locations, duplicatedLocation_[0]);
                                        //console.log('addPreferred::duplicatedLocationIndex::'+duplicatedLocationIndex);

                                        var mustseeandos_ = location_.mustSeeAndDo;
                                        var order_position = self.trip.destiny[duplicatedDestinyIndex].locations[duplicatedLocationIndex].mustSeeAndDo.length;
                                        for (var j = 0; j < mustseeandos_.length; j++) {
                                            var msad_ = mustseeandos_[j];
                                            msad_.order_position = order_position++;
                                            self.trip.destiny[duplicatedDestinyIndex].locations[duplicatedLocationIndex].mustSeeAndDo.push(msad_);
                                        }
                                    }
                                    else
                                    {
                                        // ordenando el destino
                                        location_.order_position = self.trip.destiny[duplicatedDestiniesOnDateIndex].locations.length;
                                        //console.log('self.addPreferred::location_.mustSeeAndDo.length::'+location_.mustSeeAndDo.length);
                                        for (var j = 0; j < location_.mustSeeAndDo.length; j++) {
                                            location_.mustSeeAndDo[j].order_position = j;
                                        }
                                        self.trip.destiny[duplicatedDestiniesOnDateIndex].locations.push(location_);
                                    }
                                }
                            }

                            //console.log('addPreferred::self.trip::'+JSON.stringify(self.trip));
                            tripService.setInfoTravels(self.trip);

                            if (self.trip.id != null)
                            {
                                tripService.autoUpdateDestiny().then(function() {
                                    //console.log('addPreferred::tripService.autoUpdateDestiny()');
                                    tripService.getTrip(self.trip).then(function() {
                                        //console.log('addPreferred::tripService.getTrip()');
                                    });
                                });
                            }

                            if (self.trip.id != null) 
                            {
                                $state.go('app.tabs.detail-travel', {id: self.trip.id});
                            }
                            else if (typeof $ionicHistory.backTitle() !== "undefined")
                            {
                                $ionicHistory.goBack();
                            }
                            else
                            {
                                $state.go('app.tabs.newTravel', {cache: false});
                            }
                        }
                        else
                        {
                            self.messages.error.push($filter('translate')('messageserror-recommended-duplicated'));
                            //console.log('addPreferred::No hay destinos recomendados que agregar');
                        }

                        $loading.finish('loading');
                    });
                }
                else {
                    $loading.finish('loading');
                    $ionicScrollDelegate.scrollTop(true);
                }
            }
        };

        self.processRecommendedMustSeeAndDo = function() {

            var promises = [];
            var defAddPreferred = new $.Deferred();

            promises.push(defAddPreferred);

            //console.log('self.processRecommendedMustSeeAndDo::$scope.destiny::'+JSON.stringify($scope.destiny));
            self.selectedMustSeeAndDo = $filter('filter')(self.arreglo.mustSeeAndDo, {checked: 'true'});
            //console.log('self.processRecommendedMustSeeAndDo::self.selectedMustSeeAndDo::'+JSON.stringify(self.selectedMustSeeAndDo));
            $.each(self.selectedMustSeeAndDo, function(i, mustSeeAndDo) {
                var defMustSeeAndDo = new $.Deferred();
                //console.log('self.processRecommendedMustSeeAndDo::mustSeeAndDo::'+JSON.stringify(mustSeeAndDo));
                promises.push(defMustSeeAndDo);
                $.queue('mustSeeAndDos', self.processMustSeeAndDo(mustSeeAndDo, defMustSeeAndDo));
            });
            $.dequeue('mustSeeAndDo');

            defAddPreferred.resolve();
            return $.when.apply(undefined, promises).promise();
        }

        self.processMustSeeAndDo = function(msad, deferred) {

            var processLocation = function(reslocation) {
                var promises = [];

                var defProcessLocation = new $.Deferred();
                promises.push(defProcessLocation);

                var location = reslocation.rows.item(0);
                //console.log('processLocation::location::'+JSON.stringify(location));

                // antes de proceder con el procesamiento del destino recomendado, verificamos si este destino ya
                // fue seleccionado manualmente, para la fecha recomendada
                var duplicatedMsad = false;
                //console.log('processLocation::self.duplicatedDestiniesOnDate::'+JSON.stringify(self.duplicatedDestiniesOnDate));
                //console.log('processLocation::self.duplicatedDestiniesOnDate.length::'+self.duplicatedDestiniesOnDate.length);

                //for (var i = 0; i < self.duplicatedDestiniesOnDate.length; i++) {
                if (self.duplicatedDestiniesOnDate.length > 0)
                {
                    // hay destinos para la fecha recomendada, se verifica si alguno tiene el municipio seleccionado
                    //var locations = self.duplicatedDestiniesOnDate[i].locations;
                    var locations = self.duplicatedDestiniesOnDate[0].locations;

                    for (var j = 0; j < locations.length; j++) {

                        var mustseeandos = locations[j].mustSeeAndDo;
                        //console.log('processLocation::locations::'+JSON.stringify(locations));
                        var duplicatedMsadOnLocation = _.filter(mustseeandos, function(must) { return must.id === msad.id; });
                        if (duplicatedMsadOnLocation.length > 0)
                        {
                            duplicatedMsad = true;
                            break;
                        }
                    }
                }
                //console.log('self.processLocation::duplicatedMsad::'+duplicatedMsad);

                if (!duplicatedMsad)
                {
                    //console.log('self.processLocation::$scope.destiny::'+JSON.stringify($scope.destiny));
                    var duplicatedLocation = _.filter($scope.destiny.locations, function(locat) { return locat.id === location.id; });
                    //console.log('self.processLocation::duplicatedLocation::'+JSON.stringify(duplicatedLocation));
                    if (duplicatedLocation.length > 0)
                    {
                        //console.log('processLocation::duplicatedLocation::'+JSON.stringify(duplicatedLocation));
                        //console.log('processLocation::duplicatedLocation.length::'+duplicatedLocation.length);
                        // si está duplicado, es porque ya se ha seleccionado un destino recomendado de este municipio
                        // obteniendo el indice del location para agregar el destino recomendad
                        var duplicatedLocationIndex = _.indexOf($scope.destiny.locations, duplicatedLocation[0]);
                        //console.log('addPreferred::duplicatedLocationIndex::'+duplicatedLocationIndex);
                        $scope.destiny.locations[duplicatedLocationIndex].mustSeeAndDo.push(msad);
                    }
                    else
                    {
                        location.date = $scope.destiny.date;
                        //console.log('addPreferred::location.date::'+location.date);
                        location.description = "";
                        location.mustSeeAndDo = [];
                        location.mustSeeAndDo.push(msad);
                        //console.log('processLocation::location::'+JSON.stringify(location));
                        $scope.destiny.locations.push(location);
                    }
                    self.trip.budget -= msad.price;
                    //console.log('processLocation::$scope.destiny::'+JSON.stringify($scope.destiny));
                }

                defProcessLocation.resolve();
                deferred.resolve();
                return $.when.apply(undefined, promises).promise();
            }

            return tripService.getLocationById(msad.locationId).then(processLocation);
        }
    }
})();
