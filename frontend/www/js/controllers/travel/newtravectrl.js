/**
 * Sistema:                 CyM-AR
 * Siglas:                  cym
 *
 * Nombre:                  newtravectrl.js
 * Descripcion:             controlador que maneja los meodos y eventos y vlor de las variables al crear un nuevo viaje
 * @version                 1.0
 * @author                  Leonor Guzman
 * @since                   12/18/2015
 *
 */
'use strict';
(function ()
{

    angular.module('cym').controller('NewTravelCtrl', ['$scope', '$state', 'UserService', 'TravelService',
        'MessagesFactory', '$filter','Markers', 'GeoLocation', 'GOOGLE_MAPS_APIKEY', 'DEFAULT_COUNTRY', 
        'DEFAULT_STATE', '$ionicScrollDelegate', '$loading', 'MustSeeAndDoService', NewTravelCtrl])
        .directive('travelDestiny', function ()
        {
            return {
                restricted: 'E',
                templateUrl: 'templates/travel/destiny.html'
            };
        });

    function NewTravelCtrl($scope, $state, userService, tripService, 
        MessagesFactory, $filter, Markers, GeoLocation, GOOGLE_MAPS_APIKEY, DEFAULT_COUNTRY, 
        DEFAULT_STATE, $ionicScrollDelegate, $loading, mustseeanddoService)
    {
        var self = this;
        self.trip = tripService.getInfoTravels();
        $scope.mapName = "AgregarViaje";

        $scope.countries = [];
        $scope.viewAcord = [];
        //$scope.viewAcord = false;


        /**
         * Descripcion:             metodo que se ejecuta al cargar la pagina
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         */
        $scope.$on('$ionicView.enter', function ()
        {
            //   self.clearTravels();
            //console.log('$ionicView.enter::init');
            if (!mustseeanddoService.Latitude || !mustseeanddoService.Longitude)
            {
                mustseeanddoService.getMapLocation();
            }
            self.loadLocate();
            self.trip = tripService.getInfoTravels();

            //console.log('$ionicView.enter::self.trip::'+JSON.stringify(self.trip));
            //console.log('$ionicView.enter::$scope.travelDetail()::'+JSON.stringify($scope.travelDetail()));
            if($scope.travelDetail().destiny != null)
            {
                //var orderBy = $filter('orderBy');
                //var listDestiny = orderBy($scope.travelDetail().destiny, 'date', false);

                //Markers.clearAllMarkers();
                //Markers.setMarkersTravel(listDestiny).then(function() {
                //    GeoLocation.init("map" + $scope.mapName, GOOGLE_MAPS_APIKEY, false);
                //    $ionicScrollDelegate.scrollBottom(true);
                //});

                self.trip.destiny = self.trip.destiny.sort(function (a, b) {   
                    return a.date - b.date || a.locations[0].order_position - b.locations[0].order_position;
                });
                self.calculateDistance();
            }

        });

        $scope.$watch(function () { 
          return mustseeanddoService.Latitude; 
        }, function(newVal, oldVal){
            if(oldVal!==newVal && newVal !== undefined){
                //console.log('calculating distances');
                self.calculateDistance();
            }
        });

        self.calculateDistance = function()
        {
            if (self.trip.destiny)
            {
                for (var i = 0; i < self.trip.destiny.length; i++) {
                    var destiny = self.trip.destiny[i];
                    for (var j = 0; j < destiny.locations.length; j++) {
                        var location = destiny.locations[j];
                        for (var k = 0; k < location.mustSeeAndDo.length; k++) {
                            var msad = location.mustSeeAndDo[k];

                            var distance = null;
                            if (mustseeanddoService.Latitude && mustseeanddoService.Longitude)
                            {
                                var lat = null;
                                var lng = null;
                                try {
                                    var pos = msad.geographic_position.split(",");
                                    lat = pos[0].trim();
                                    lng = pos[1].trim();
                                }
                                catch(e) {
                                    lat = 0;
                                    lng = 0;
                                }
                                distance = mustseeanddoService.getDistanceFromLatLonInKm(mustseeanddoService.Latitude, mustseeanddoService.Longitude, lat, lng);
                                //console.log('self.calculateDistance::distance::'+JSON.stringify(distance));
                            }

                            self.trip.destiny[i].locations[j].mustSeeAndDo[k].distance = distance;
                        }
                    }
                }
            }
        }

        self.redirectPreferred = function ()
        {
            tripService.setInfoTravels(self.trip);
            //console.log('self.redirectPreferred::self.trip::'+JSON.stringify(self.trip));
            $state.go('app.tabs.preferred', {trip: 0, budget: self.trip.budget, stateId: DEFAULT_STATE});
        }

        /**
         * Descripcion:             metodo que agrega el viaje
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         * @param form
         */
        this.addTrip = function (form)
        {

            self.messages = MessagesFactory.createMessages();

            if (userService.isUserLogged())
            {
                if (!self.trip.name || self.trip.name.trim() == '')
                {
                    self.messages.error.push($filter('translate')('messageserror-wrongnametrip'));
                    //self.messages.error.push('Nombre de viaje, inv\u00e1lido');
                }
                if (!self.trip.budget)
                {
                    self.messages.error.push($filter('translate')('messageserror-wrongbugettrip'));
                    //self.messages.error.push('Presupuesto  del viaje, inv\u00e1lido');
                }
                //if (!self.trip.country)
                //{
                //    self.messages.error.push($filter('translate')('messageserror-wrongcountrytrip'));
                    //self.messages.error.push('Debe seleccionar el Pa\u00eds');
                //}
                if (!self.trip.dateGoing)
                {
                    self.messages.error.push($filter('translate')('messageserror-wrongdateout'));
                    //self.messages.error.push('Fecha de salida inv\u00e1lido');
                }
                if (!self.trip.dateArrival)
                {
                    self.messages.error.push($filter('translate')('messageserror-wrongdatein'));
                    //self.messages.error.push('Fecha de llegada inv\u00e1lido');
                }
                if (self.trip.dateGoing > self.trip.dateArrival)
                {
                    self.messages.error.push($filter('translate')('messageserror-wrongdateout'));
                    //self.messages.error.push('Fecha de salida inv\u00e1lido');
                }
                //console.log('self.trip.destiny::'+self.trip.destiny);
                if (self.trip.dateGoing && self.trip.dateArrival && (self.trip.destiny && self.trip.destiny.length > 0))
                {
                    for (var i = 0; i < self.trip.destiny.length; i++)
                    {
                        if (self.trip.destiny[i].date > self.trip.dateArrival)
                        {
                            self.messages.error.push($filter('translate')('messageserror-wrongdateinwithdestiny'));
                            //self.messages.error.push('Fecha de llegada inv\u00e1lido con relacion a los destinos seleccionados');
                        }
                        if (self.trip.destiny[i].date < self.trip.dateGoing)
                        {
                            self.messages.error.push($filter('translate')('messageserror-wrongdateoutwithdestiny'));
                            //self.messages.error.push('Fecha de salida inv\u00e1lido con relacion a los destinos seleccionados');

                        }
                    }

                }
                if (self.messages.error.length == 0)
                {
                    this.trip.user_id = userService.getUserLoggedId();
                    this.trip.description = '';
                    if (this.trip.dateArrival)
                    {
                        this.trip.dateArrival = this.trip.dateArrival.getTime();
                    }
                    else
                    {
                        var date = new Date();
                        this.trip.dateArrival = date.getTime();
                    }
                    if (this.trip.dateGoing)
                    {
                        this.trip.dateGoing = this.trip.dateGoing.getTime();
                    }
                    else
                    {
                        var date = new Date();
                        this.trip.dateGoing = date.getTime();
                    }
                    tripService.setInfoTravels(this.trip);
                    tripService.addTravel().then(function() {
                        //console.log('travelService.addTravel::done');
                        $state.go('app.tabs.travel', null);
                        this.trip = {};
                        form.$setPristine();
                        form.$setUntouched();
                    });
                }
                $ionicScrollDelegate.scrollTop(true);
            }

        };

        /**
         * Descripcion:             metodo que carga las localidades
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         */
        this.loadLocate = function ()
        {
            //console.log('this.loadLocate');
            self.messages = MessagesFactory.createMessages();

            //tripService.infoClear();
            this.trip = tripService.getInfoTravels();
            //console.log('this.loadLocate::this.trip::'+JSON.stringify(this.trip));

            if (userService.isUserLogged())
            {

                var success = function (res)
                {
                    if (res.rows.length > 0)
                    {
                        $scope.countries = [];
                        for (var i = 0; i < res.rows.length; i++)
                        {
                            $scope.countries.push(res.rows.item(i));
                        }
                        //console.log('$scope.countries::'+JSON.stringify($scope.countries));

                        self.trip.country = $filter('filter')($scope.countries, {id: DEFAULT_COUNTRY})[0];;
                        //console.log('self.trip.country::'+JSON.stringify(self.trip.country));
                    }
                };

                var error = function (res)
                {
                    self.messages.error.push($filter('translate')('messageserror-laodbookcountrys'));
                    //self.messages.error.push('Error cargando libros disponibles (Paises)');
                };

                tripService.getAllLocationCountries().then(success, error);
            }
        };

        /**
         * Descripcion:             metodo que limpia las variables
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         */
        this.clearTravels = function ()
        {
            tripService.infoClear();
            tripService.destinyClear();
        };

        /**
         * Descripcion:             metodo que limpia la variable de destiny
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         */
        this.destinyClear = function ()
        {
            tripService.destinyClear();
        };

        /**
         * Descripcion:             metodo que  agregar a un array
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   1/6/2016
         *
         * @param element
         * @param array
         */
        self.pushToArray = function (element, array)
        {
            if (array != null && element != null)
            {
                if (self.elemenExist(element, array))
                {
                    self.removeToArray(element, array);
                    array.push(element);
                }
                else
                {
                    array.push(element);
                }
            }
        };

        /**
         * Descripcion:             metodo que elimina de un determinado array
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   1/6/2016
         *
         * @param element
         * @param array
         */
        self.removeToArray = function (element, array)
        {
            if (array != null && element != null)
            {
                for (var i = 0; i < array.length; i++)
                {
                    if (array[i].id == element.id)
                    {
                        array.splice(i, 1);
                    }
                }
            }
        };

        /**
         * Descripcion:             metodo que compara un iten con un array segun parametro
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   1/6/2016
         *
         * @param element
         * @param array
         */
        self.elemenExist = function (element, array)
        {
            if (array != null && element != null)
            {
                for (var x = 0; x < array.length; x++)
                {
                    if (array[x].id == element.id)
                    {
                        return true;
                    }
                }
            }
            return false;
        };

        /**
         * Descripcion:             metodo que elimina el destino
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   1/13/2016
         *
         * @param item
         * @param obj
         */
        self.removeDestiny = function (itemParent, item, obj) {

            //console.log('self.removeDestiny::itemParent::'+itemParent);
            //console.log('self.removeDestiny::item::'+item);
            //console.log('self.removeDestiny::obj::'+JSON.stringify(obj));
            var name = obj.locations[item].name + " - " + $filter('date')(obj.date, "MM/dd/yyyy");
            //console.log('self.removeDestiny::name::'+name);
            if(window.cordova) {
                navigator.notification.confirm(
                    $filter('translate')('label-deletedestinynotificatioconfirm1') + name + "?",
                    confirmDelete,
                    $filter('translate')('label-deletedestinynotificatioconfirm2'),
                    $filter('translate')('label-deletedestinynotificatioconfirm3')
                );
            }
            else {
                if(confirm($filter('translate')('label-deletedestinynotificatioconfirm1') + name + "?")) {
                    confirmDelete(1);
                }
            }

            function confirmDelete(res) {
                if (res == 1) {
                    var trip = angular.copy(tripService.getInfoTravels());
                    trip.destiny = trip.destiny.sort(function (a, b) {   
                        return a.date - b.date || a.locations[0].order_position - b.locations[0].order_position;
                    });
                    //console.log('confirmDelete::trip::'+JSON.stringify(trip));

                    for (var i = 0; i < trip.destiny[itemParent].locations[item].mustSeeAndDo.length; i++)
                    {
                        var oldMust = trip.destiny[itemParent].locations[item].mustSeeAndDo[i];
                        //console.log('confirmDelete::oldMust::'+JSON.stringify(oldMust));
                        trip.budget += oldMust.price;
                    }

                    var order_position = trip.destiny[itemParent].locations[item].order_position;
                    //console.log('confirmDelete::order_position::'+order_position);
                    var destinies = trip.destiny.length;
                    //console.log('confirmDelete::destinies::'+destinies);
                    var locations = trip.destiny[itemParent].locations.length;
                    //console.log('confirmDelete::locations::'+locations);
                    //console.log('confirmDelete::trip.destiny[itemParent].locations::'+JSON.stringify(trip.destiny[itemParent].locations));
                    //console.log('confirmDelete::trip.destiny[itemParent].locations[item]::'+JSON.stringify(trip.destiny[itemParent].locations[item]));

                    //console.log('confirmDelete::trip.destiny[itemParent].date::'+trip.destiny[itemParent].date);
                    for (var i = itemParent + 1; i < destinies; i++)
                    {
                        if (trip.destiny[itemParent].date == trip.destiny[i].date)
                        {
                            trip.destiny[i].locations[0].order_position = order_position++;
                        }
                        else
                        {
                            break;
                        }
                    }

                    //console.log('confirmDelete::trip.destiny::'+JSON.stringify(trip.destiny));
                    trip.destiny[itemParent].locations.splice(item, 1);
                    //console.log('confirmDelete::trip::'+JSON.stringify(trip));

                    // se verifica si hay destinos sin localidades
                    var destiniesNoLocations = _.filter(trip.destiny, function(destiny) { return destiny.locations == null || destiny.locations.length == 0; });
                    if(destiniesNoLocations.length > 0)
                    {
                        // existe destino sin localidades (se debe borrar)
                        //console.log('confirmDelete::destiniesNoLocations[0]::'+JSON.stringify(destiniesNoLocations[0]));
                        var destinyToDeleteIndex = _.indexOf(trip.destiny, destiniesNoLocations[0]);
                        //console.log('confirmDelete::destinyToDeleteIndex::'+destinyToDeleteIndex);
                        trip.destiny.splice(destinyToDeleteIndex, 1);
                    }
                    //console.log('self.confirmDelete::trip::'+JSON.stringify(trip));

                    tripService.setInfoTravels(trip);
                    self.trip = tripService.getInfoTravels();
                    self.calculateDistance();
                }
            }
        };

        self.addDestiny = function () {
            //console.log('addDestiny');
            $state.go('app.tabs.destiny', {});
        }

        self.editDestiny = function (item, obj, id) {
            //console.log('editDestiny::id::'+id+'::item::'+item+'::obj.date::'+obj.date);
            $state.go('app.tabs.edit-destiny', { trip: id, item: item, date: obj.date });
        }

        self.viewMap = function() {
            //console.log('self.viewMap::$scope.viewAcorde::'+$scope.viewAcorde);

            if (self.trip.destiny != null)
            {
                if ((typeof $scope.viewAcorde == "undefined") || !$scope.viewAcorde) {
                    //console.log('self.viewMap::mostrando mapa');

                    //console.log('self.viewMap::self.trip.destiny::'+JSON.stringify(self.trip.destiny));
                    var listDestiny = self.trip.destiny.sort(function(a, b){
                        return a.date - b.date || a.locations[0].order_position - b.locations[0].order_position;
                    });
                    //console.log('self.viewMap::listDestiny::'+JSON.stringify(listDestiny));

                    var listMsad = [];
                    for (var i = 0; i < listDestiny.length; i++)
                    {
                        var destiny_ = listDestiny[i];
                        var listLocation = destiny_.locations.sort(function(a, b){
                            return a.order_position > b.order_position;
                        });
                        for (var j = 0; j < listLocation.length; j++)
                        {
                            var location_ = listLocation[j];
                            if (location_.mustSeeAndDo.length > 0)
                            {
                                var listMustSeeAndDo = location_.mustSeeAndDo.sort(function(a, b){
                                    return a.order_position > b.order_position;
                                });
                                for (var k = 0; k < listMustSeeAndDo.length; k++)
                                {
                                    var mustSeeAndDo_ = listMustSeeAndDo[k];
                                    mustSeeAndDo_.date = destiny_.date;
                                    mustSeeAndDo_.location = location_.name;
                                    listMsad.push(mustSeeAndDo_);
                                }
                            }
                            else
                            {
                                var mustSeeAndDo_ = {};
                                mustSeeAndDo_.title = location_.name;
                                mustSeeAndDo_.geographic_position = location_.geographic_position;
                                mustSeeAndDo_.ar = 0;
                                mustSeeAndDo_.qr = 0;
                                mustSeeAndDo_.date = location_.date;
                                listMsad.push(mustSeeAndDo_);
                            }
                        }
                    }
                    //console.log('self.viewMap::listMsad::'+JSON.stringify(listMsad));
                    //console.log('self.viewMap::listMsad.length::'+listMsad.length);

                    Markers.clearAllMarkers();
                    Markers.setMarkersTravel(listMsad).then(function() {
                        GeoLocation.init("map" + $scope.mapName, GOOGLE_MAPS_APIKEY, false);
                        $ionicScrollDelegate.scrollBottom(true);
                    });
                }

                $scope.viewAcorde = !$scope.viewAcorde;
            }
        }

        self.showMustSeeAndDoDetail = function(msad) {
            $state.go('app.tabs.mustseeanddodetail-travel', {id: msad.id});
        }

        self.moveLocationUp = function (destinyPosition, locationPosition, id) {
            //console.log('self.moveLocationUp::destinyPosition::'+destinyPosition+'::locationPosition::'+locationPosition+'::id::'+id);
            self.moveLocation(destinyPosition, locationPosition, id, -1);
        };

        self.moveLocationDown = function (destinyPosition, locationPosition, id) {
            //console.log('self.moveLocationDown::destinyPosition::'+destinyPosition+'::locationPosition::'+locationPosition+'::id::'+id);
            self.moveLocation(destinyPosition, locationPosition, id, 1);
        };

        self.moveLocation = function (destinyPosition, locationPosition, id, direction) {
            $loading.start('loading');
            //console.log('self.moveLocation::destinyPosition::'+destinyPosition+'::locationPosition::'+locationPosition+'::id::'+id+'::direction::'+direction);
            var trip = angular.copy(tripService.getInfoTravels());
            //console.log('self.moveLocation::trip::'+JSON.stringify(trip));

            var destinyToEdit = trip.destiny[destinyPosition];
            //console.log('self.moveLocation::destinyToEdit::'+JSON.stringify(destinyToEdit));

            var locationToEdit = destinyToEdit.locations[locationPosition];
            //console.log('self.moveLocation::locationToEdit::'+JSON.stringify(locationToEdit));

            var itemToReplace = locationPosition + direction;
            //console.log('self.moveLocation::itemToReplace::'+itemToReplace);
            var locationToReplace = destinyToEdit.locations[itemToReplace];

            if (locationToReplace != null)
            {
                var new_position = locationToReplace.order_position;
                locationToReplace.order_position = locationToEdit.order_position;
                locationToEdit.order_position = new_position;

                //var itemViewAcord = false;
                //var itemToReplaceViewAcord = false;
                //if ($scope.viewAcord[destinyToEditIndex])
                //{
                //    if ($scope.viewAcord[destinyToEditIndex][item])
                //    {
                //        itemViewAcord = $scope.viewAcord[destinyToEditIndex][item];
                //    }
                //    if ($scope.viewAcord[destinyToEditIndex][itemToReplace])
                //    {
                //        itemToReplaceViewAcord = $scope.viewAcord[destinyToEditIndex][itemToReplace];
                //    }
                //}
                //console.log('self.moveLocation::destinyToEditIndex::'+destinyToEditIndex);
                //console.log('self.moveLocation::item::'+item);
                //console.log('self.moveLocation::itemToReplace::'+itemToReplace);
                //console.log('self.moveLocation::itemViewAcord::'+itemViewAcord);
                //console.log('self.moveLocation::itemToReplaceViewAcord::'+itemToReplaceViewAcord);

                //console.log('self.moveLocation::locationToEdit::'+JSON.stringify(locationToEdit));
                trip.destiny[destinyPosition].locations[itemToReplace] = locationToEdit;
                //console.log('self.moveLocation::locationToReplace::'+JSON.stringify(locationToReplace));
                trip.destiny[destinyPosition].locations[locationPosition] = locationToReplace;

                //trip.destiny[destinyToEditIndex] = destinyToEdit;
                //trip.destiny[itemToReplace] = destinyToReplace;
                trip.destiny = trip.destiny.sort(function (a, b) {   
                    return a.date - b.date || a.locations[0].order_position - b.locations[0].order_position;
                });
                //console.log('self.moveLocation::trip::'+JSON.stringify(trip));
                tripService.setInfoTravels(trip);
                self.trip = tripService.getInfoTravels();
                self.calculateDistance();

                //if ($scope.viewAcord[destinyToEditIndex])
                //{
                //    $scope.viewAcord[destinyToEditIndex][item] = itemToReplaceViewAcord;
                //    $scope.viewAcord[destinyToEditIndex][itemToReplace] = itemViewAcord;
                //}

            }

            $loading.finish('loading');
        };

        self.moveMustseeanddoUp = function (destinyPosition, locationPosition, msadPosition, mustseeanddo, destiny) {
            //console.log('self.moveMustseeanddoUp::destinyPosition::'+destinyPosition+'::locationPosition::'+locationPosition+'::msadPosition::'+msadPosition+'::mustseeanddo::'+JSON.stringify(mustseeanddo)+'::destiny::'+JSON.stringify(destiny));
            self.moveMustseeanddo(destinyPosition, locationPosition, msadPosition, -1);
        };

        self.moveMustseeanddoDown = function (destinyPosition, locationPosition, msadPosition, mustseeanddo, destiny) {
            //console.log('self.moveMustseeanddoDown::destinyPosition::'+destinyPosition+'::locationPosition::'+locationPosition+'::msadPosition::'+msadPosition+'::mustseeanddo::'+JSON.stringify(mustseeanddo)+'::destiny::'+JSON.stringify(destiny));
            self.moveMustseeanddo(destinyPosition, locationPosition, msadPosition, 1);
        };

        self.moveMustseeanddo = function (destinyPosition, locationPosition, msadPosition, direction) {
            $loading.start('loading');
            //console.log('self.moveMustseeanddo::destinyPosition::'+destinyPosition+'::locationPosition::'+locationPosition+'::msadPosition::'+msadPosition+'::direction::'+direction);

            var trip = angular.copy(tripService.getInfoTravels());
            //console.log('self.moveMustseeanddo::trip::'+JSON.stringify(trip));

            trip.destiny = trip.destiny.sort(function(a, b){
                return a.date > b.date;
            });

            var destinyOnReorder = trip.destiny[destinyPosition];
            //console.log('self.moveMustseeanddo::destinyOnReorder::'+JSON.stringify(destinyOnReorder));

            if (destinyOnReorder != null)
            {
                destinyOnReorder.locations = destinyOnReorder.locations.sort(function(a, b){
                    return a.order_position > b.order_position;
                });

                var locationOnReorder = destinyOnReorder.locations[locationPosition];
                //console.log('self.moveMustseeanddo::locationOnReorder::'+JSON.stringify(locationOnReorder));

                if (locationOnReorder != null)
                {
                    locationOnReorder.mustSeeAndDo = locationOnReorder.mustSeeAndDo.sort(function(a, b){
                        return a.order_position > b.order_position;
                    });

                    var mustseeanddoOnReorder = locationOnReorder.mustSeeAndDo[msadPosition];
                    //console.log('self.moveMustseeanddo::mustseeanddoOnReorder::'+JSON.stringify(mustseeanddoOnReorder));

                    if (mustseeanddoOnReorder != null)
                    {
                        var itemToReplace = msadPosition + direction;
                        //console.log('self.moveMustseeanddo::itemToReplace::'+itemToReplace);
                        var mustseeanddoToReplace = locationOnReorder.mustSeeAndDo[itemToReplace];
                        //console.log('self.moveMustseeanddo::mustseeanddoToReplace::'+JSON.stringify(mustseeanddoToReplace));

                        if (mustseeanddoToReplace != null)
                        {
                            var new_position = mustseeanddoToReplace.order_position;
                            mustseeanddoToReplace.order_position = mustseeanddoOnReorder.order_position;
                            mustseeanddoOnReorder.order_position = new_position;
                            //console.log('self.moveMustseeanddo::mustseeanddoOnReorder::'+JSON.stringify(mustseeanddoOnReorder));
                            //console.log('self.moveMustseeanddo::mustseeanddoToReplace::'+JSON.stringify(mustseeanddoToReplace));

                            trip.destiny[destinyPosition].locations[locationPosition].mustSeeAndDo[msadPosition] = mustseeanddoOnReorder;
                            trip.destiny[destinyPosition].locations[locationPosition].mustSeeAndDo[itemToReplace] = mustseeanddoToReplace;

                            tripService.setInfoTravels(trip);
                            self.trip = tripService.getInfoTravels();
                            self.calculateDistance();
                        }
                    }
                }

            }
            $loading.finish('loading');
        };
    }
})();
