/**
 * Created by Leonor Guzman on 1/28/2016.
 */
/**
 * Sistema:                 CyM-AR
 * Siglas:                  cym
 *
 * Nombre:                  destinyctrl.js
 * Descripcion:             metodo que sirve de gestion de valores de variables y metodos para los destinos
 * @version                 1.0
 * @author                  Leonor Guzman
 * @since                   12/18/2015
 *
 */
'use strict';
(function ()
{

    angular.module('cym')
        .controller('DetailProfileCtrl', ['$scope', '$translate', '$filter','MessagesFactory', '$ionicSideMenuDelegate', '$ionicScrollDelegate','$ionicHistory', 'UserService', 'SessionFactory', DetailProfileCtrl]);

    function DetailProfileCtrl($scope, $translate, $filter,  MessagesFactory, $ionicSideMenuDelegate, $ionicScrollDelegate,$ionicHistory, userService, SessionFactory)
    {

        var self = this;

        self.languageList = [
            { text: $filter('translate')('labeloption-spanish'), valu: "es_VE" },
            { text: $filter('translate')('labeloption-english'), valu: "en_EN" }
        ];

        self.files = [];
        self.file = {};
        self.hours = [];
        self.durationSetted = {};
        self.durationSelected = {};
        self.languageSelected = {};

        self.changePassword = false;
        self.current_password = null;
        self.new_password = null;
        self.confirm_new_password = null;

        var imagebase64 = "";

        //var bytesMax = 1048576;

        $scope.data = {
            default_language: 'es_VE'
        };

        $scope.changeLanguage = function()
        {
            $translate.use(self.languageSelected.valu);
            self.user = userService.getUserLogged();
            //self.user.confirNewPass = self.user.pass = self.user.password;
        };


        self.loadUser = function() {

            self.files = [];
            self.file = {};
            self.languageSelected = {};

            self.changePassword = false;
            self.current_password = null;
            self.new_password = null;
            self.confirm_new_password = null;

            userService.setUserLogged().then(function() {
                self.user = userService.getUserLogged();
                //console.log('self.loadUser::self.user::'+JSON.stringify(self.user));

                self.user.flag = ((self.user.password == null) || (self.user.password == "")) ? "" : "usuario_normal";
                //console.log('self.loadUser::self.user::'+JSON.stringify(self.user));

                if (self.user.language == "" || self.user.language == null)
                {
                    self.languageSelected = self.languageList[0];
                    self.user.language = self.languageSelected.valu;
                }
                else
                {
                    for(var j = 0; j<self.languageList.length; j++)
                    {
                        if(self.languageList[j].valu == self.user.language)
                        {
                            self.languageSelected = self.languageList[j];
                            self.user.language =  self.languageSelected.valu;
                        }
                    }
                }

                $translate.use(self.languageSelected.valu);
                //$ionicSideMenuDelegate.toggleLeft();
            });
        }


        /**
         * Descripcion:
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   metodo que se ejecuta al iniciar la pagina
         *
         */
        $scope.$on('$ionicView.enter', function ()
        {
            self.messages = MessagesFactory.createMessages();
            self.loadUser();
        });

        /**
         * Descripcion:             metodo que se ejecuta con el evento del clik del boton de actualizar desde la pantalla de detail profile
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   2/1/2016
         *
         */
        self.updateProfile = function () {
            self.messages = MessagesFactory.createMessages();

            self.user.language =  self.languageSelected.valu;
            //SessionFactory.setObject("userLogged", self.user );

            if (self.changePassword) {

                //console.log('self.updateProfile::self.current_password::'+self.current_password);
                //console.log('self.updateProfile::self.user.password::'+self.user.password);
                //console.log('self.updateProfile::self.new_password::'+self.new_password);
                //console.log('self.updateProfile::self.confirm_new_password::'+self.confirm_new_password);

                if ((self.current_password == null) || (self.current_password.trim() == "")) {
                    self.messages.error.push($filter('translate')('messageserror-current-password'));
                }
                else if (self.new_password == null || (self.new_password.trim() == "")) {
                    self.messages.error.push($filter('translate')('messageserror-new-password'));
                }
                else if (self.confirm_new_password == null || (self.confirm_new_password.trim() == "")) {
                    self.messages.error.push($filter('translate')('messageserror-confirm-new-password'));
                }
                else if (self.new_password.trim() != self.confirm_new_password.trim()) {
                    self.current_password = "";
                    self.new_password = "";
                    self.confirm_new_password = "";
                    self.messages.error.push($filter('translate')('messageserror-new-and-confirm-password-not-equal'));
                }
                else if (self.current_password.trim() != self.user.password) {
                    self.current_password = "";
                    self.new_password = "";
                    self.confirm_new_password = "";
                    self.messages.error.push($filter('translate')('messageserror-wrong-current-password'));
                }
            }

            if (self.messages.error.length == 0) {
                //alert('imagebase64.length::'+imagebase64.length);
                if(imagebase64.length > 0) {
                    self.user.image_profile = imagebase64;
                    userService.updateImage(self.user);
                }
                if (self.changePassword) {
                    self.user.password = self.new_password.trim();
                }
                //console.log('self.updateProfile::self.user::'+JSON.stringify(self.user));
                userService.updatePass(self.user, self.messages).then(function() {
                    self.loadUser();
                });
            }
            $ionicScrollDelegate.scrollTop(true);
        }

        /**
         * Descripcion:             metodo que obtiene la imagen del perfil para ser guardada en base de datos.
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   2/1/2016
         *
         */
        $scope.setFiles = function() {
            var files = document.getElementById('file').files;
            var file = files[0];

            if (files && file) {
                var reader = new FileReader();

                reader.onload = function(readerEvt) {
                    var binaryString = readerEvt.target.result;
                    var resultado = btoa(binaryString);
                    imagebase64 = resultado;
                };

                reader.readAsBinaryString(file);
            }
        };

        self.toChangePassword = function() {
            self.changePassword = !self.changePassword;
        }
    }
})();
