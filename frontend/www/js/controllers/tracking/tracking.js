/**
 * Created by poncbi on 07/12/2015.
 */
'use strict';

(function ()
{
    angular.module('cym').controller('TrackingCtrl', ['$stateParams', '$scope','$filter', 'UserService', 'TrackingService',
        'MessagesFactory', '$state', 'SessionFactory', TrackingCtrl]);

    /**
     * Controlador para la vista de listar seguimiento por gps
     * @param $stateParams variable para el manejo del estado
     * @param $scope variable para el manejo de los objetos de la vista
     * @param userService objeto para las operaciones sobre los datos del usuario
     * @param trackingService objeto para las operaciones sobre el seguimiento
     * @param MessagesFactory objeto para la gestion de mensajes
     * @constructor
     */
    function TrackingCtrl($stateParams, $scope, $filter, userService, trackingService, MessagesFactory, $state, SessionFactory)
    {
        var self = this;

        self.item = {};

        /**
         * Metodo que se ejecuta cuando se inicia la pagina
         */
        $scope.$on('$ionicView.enter', function (viewInfo, state)
        {
            trackingService.clearAllTracks();
            //console.log('CTRL - $ionicView.loaded', viewInfo, state);
            self.loadTracking();
        });

        /**
         * Metodo que carga los tracking del usuario
         */
        self.loadTracking = function ()
        {
            self.messages = MessagesFactory.createMessages();
            if (userService.isUserLogged())
            {
                $scope.user_id = parseInt(userService.getUserLoggedId(), 10);
                trackingService.getTrackingByUser($scope.user_id, self.messages);
            }
        };

        $scope.$on("$ionicView.afterLeave", function(event, data)
        {
            trackingService.clearAllTracks();
        });

        /**
         * Metodo que elimina el seguimiento
         * @param $id identificador del seguimiento
         */
        self.deleteTracking = function ($id, name)
        {
            self.messages = MessagesFactory.createMessages();
            if (userService.isUserLogged() && $id)
            {
                if(window.cordova)
                {
                    navigator.notification.confirm(
                        $filter('translate')('label-deletetrackingnotificatioconfirm1') + name + "?",
                        confirmDelete,
                        $filter('translate')('label-deletetrackingnotificatioconfirm2'),
                        $filter('translate')('label-deletetrackingnotificatioconfirm3')
                    );
                }
                else
                {
                    if(confirm($filter('translate')('label-deletetrackingnotificatioconfirm1') + name + "?")) {
                        confirmDelete(1);
                    }
                }
            }

            function confirmDelete(res)
            {
                if(res == 1)
                {
                    self.item.user_id = userService.getUserLoggedId();
                    self.item.id = $id;
                    var item = angular.copy(self.item);
                    trackingService.deleteTracking(item, self.messages);

                    var obj = {};
                    SessionFactory.setObject("trackingsMap", obj);
                }
            }
        };

        self.navigateTo = function (id)
        {
            $state.go('app.tabs.edittracking', {id: id});
        }
    }
})();
