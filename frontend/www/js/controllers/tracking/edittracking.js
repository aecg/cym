/**
 * Created by poncbi on 11/12/2015.
 */

'use strict';

(function ()
{
    angular.module('cym').controller('EditTrackingCtrl', ['$state', '$scope','$filter', '$stateParams', 'UserService',
        'TrackingService', 'MessagesFactory', 'GeoLocation','GOOGLE_MAPS_APIKEY', '$ionicHistory', 'Markers','$ionicScrollDelegate', 'SessionFactory', EditTrackingCtrl]);


    /**
     * Controlador para la vista que se encarga de modificar los datos de un seguimieto
     * @param $state variable para el manejo del estado
     * @param $stateParams variable para el manejo del estado
     * @param $scope variable para el manejo de los objetos de la vista
     * @param userService objeto para las operaciones sobre los datos del usuario
     * @param trackingService objeto para las operaciones sobre el seguimiento
     * @param MessagesFactory objeto para la gestion de mensajes
     * @param $ionicHistory objeto para regresarse en la vista
     * @constructor
     */
    function EditTrackingCtrl($state, $scope, $filter, $stateParams, userService, trackingService, MessagesFactory, 
                            GeoLocation, GOOGLE_MAPS_APIKEY,
                              $ionicHistory,Markers, $ionicScrollDelegate, SessionFactory)
    {
        var self = this;
        self.item = {};
        self.item.cuepoints = [];
        $scope.mapName = "EditarSeguimiento";

        /**
         * Metodo que se ejecuta cuando se inicia la pagina
         */
        $scope.$on('$ionicView.enter', function (viewInfo, state) {
            //console.log('CTRL - $ionicView.loaded', viewInfo, state);
            trackingService.initAllTrackingTypes();
            self.getCuePointByTrack();
        });

        /**
         * Metodo que se ejecuta cuando la consulta no es exitosa
         * @param res objeto con la informacion del resultado
         */
        var error = function (res) {
            //console.log('ERROR::'+JSON.stringify(res));
            self.messages.error.push($filter('translate')('messageserror-gettracking'));
        };

        /**
         * Metodo que obtiene las locaciones del seguimiento
         */
        self.getCuePointByTrack = function () {
            //console.log('getCuePointByTrack::init');
            if (userService.isUserLogged() && $stateParams.id) {
                $scope.tracking_id = parseInt($stateParams.id, 10);

                self.item = trackingService.getAllCuepoints();
                //console.log('getCuePointByTrack::self.item::'+JSON.stringify(self.item));
                //console.log('getCuePointByTrack::self.item.id::'+self.item.id);
                //console.log('getCuePointByTrack::$scope.tracking_id::'+$scope.tracking_id);
                if (!self.item.id || (self.item.id != $scope.tracking_id)) {
                    self.item = {};
                    self.item.cuepoints = [];
                    self.messages = MessagesFactory.createMessages();
                    //console.log('getCuePointByTrack::$scope.tracking_id::'+$scope.tracking_id);
                    trackingService.getCuePointByTrack($scope.tracking_id, successGetLocations, error);
                }
            }
        };

        function successGetTrackingInfo(res) {
            //console.log('successGetTrackingInfo::'+JSON.stringify(res));
            if (res.rows.length > 0) {
                self.item.name = res.rows.item(0).trackingname;
                self.item.id = res.rows.item(0).trackingid;
                //console.log('successGetTrackingInfo::self.item'+JSON.stringify(self.item));
                trackingService.setAllCuepoints(self.item);
            }
        }

        function successGetLocations(res) {
            //console.log('edittracking.js::successGetLocations::res::'+JSON.stringify(res));
            for (var i = 0; i < res.rows.length; i++) {
                var cuepoint = res.rows.item(i);
                //console.log('successGetLocations::cuepoint.cuepointname::'+cuepoint.cuepointname);
                cuepoint.cuepointname = trackingService.getTrackingType(cuepoint.cuepointname)[0].src;
                self.item.cuepoints.push(cuepoint);
            }
            self.item.total = res.rows.length;

            trackingService.getTrackingInfo($scope.tracking_id, successGetTrackingInfo, error);
        }

        /**
         * Metodo que modifica los datos de un seguimiento
         */
        self.updateTracking = function () {
            self.messages = MessagesFactory.createMessages();
            if (userService.isUserLogged() && $stateParams.id) {
                self.item = trackingService.getAllCuepoints();
                if (!self.item.name || self.item.name.trim() == '') {
                    self.messages.error.push($filter('translate')('messageserror-wrongnametracking'));
                    //self.messages.error.push('Nombre de seguimiento, inv\u00e1lido');
                }
                if (self.messages.error.length == 0) {
                    trackingService.updateTracking(self.item, userService.getUserLoggedId(), self.messages);
                    self.item = {};
                    $ionicHistory.goBack();
                    var obj = {};
                    SessionFactory.setObject("trackingsMap", obj);
                }
                else {
                    $ionicScrollDelegate.scrollTop(true);
                }
            }
        };


        /**
         * Metodo que elimina la locacion de un seguimiento
         * @param cuepoint locacion
         */
        self.deleteCuePoint = function (cuepoint) {
            //console.log('deleteCuePoint::cuepoint::'+JSON.stringify(cuepoint));
            self.messages = MessagesFactory.createMessages();
            if (userService.isUserLogged() && cuepoint) {

                //console.log('deleteCuePoint::userService.isUserLogged()::'+JSON.stringify(userService.isUserLogged()));
                if(window.cordova) {
                    navigator.notification.confirm(
                        $filter('translate')('label-deletelocationnotificatioconfirm1') + cuepoint.cuepointdesc + "?",
                        confirmDelete,
                        $filter('translate')('label-deletelocationnotificatioconfirm2'),
                        $filter('translate')('label-deletelocationnotificatioconfirm3')
                    );
                }
                else {
                    if(confirm($filter('translate')('label-deletelocationnotificatioconfirm1') + cuepoint.cuepointdesc + "?")) {
                        confirmDelete(1);
                    }
                }
            }

            function confirmDelete(res) {
                //console.log("CTRL Se ha eliminado exitosamente la photo");
                if(res == 1) {
                    $scope.user_id = parseInt(userService.getUserLoggedId(), 10);
                    self.item.cuepoints = self.item.cuepoints.filter(function(el) {
                        return el.cuepointid !== cuepoint.cuepointid;
                    });
                    trackingService.deleteCuePoint(cuepoint.cuepointid, successDeleteCuePoint, error);
                }
            }
        };

        /**
         * Metodo que se ejecuta cuando la eliminacion es exitosa
         * @param res respuesta de la consulta
         */
        var successDeleteCuePoint = function (res) {
            self.getCuePointByTrack();
        };


        $scope.showAddLocation = function (id) {
            $state.go('app.tabs.addtracklocation', {id: id});
        };

        self.viewMap = function() {
            //console.log('self.viewMap::$scope.viewAcorde::'+$scope.viewAcorde);

            if ((typeof $scope.viewAcorde == "undefined") || !$scope.viewAcorde) {
                Markers.clearAllMarkers();
                Markers.setMarkersTrackingGPS(self.item.cuepoints).then(function() {
                    GeoLocation.init("map" + $scope.mapName, GOOGLE_MAPS_APIKEY, false);
                    $ionicScrollDelegate.scrollBottom(true);
                });
            }

            $scope.viewAcorde = !$scope.viewAcorde;
        }

    }

})();
