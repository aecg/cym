/**
 * Created by poncbi on 10/12/2015.
 */
'use strict';

(function ()
{
    angular.module('cym').controller('AddTrackLocationCtrl', ['$scope', '$filter','$stateParams', '$state', 'UserService',
        'TrackingService', 'MessagesFactory', 'Markers', 'GeoLocation', 'GOOGLE_MAPS_APIKEY','$ionicHistory', 
        '$ionicScrollDelegate', 'MustSeeAndDoService', AddTrackLocationCtrl]);

    /**
     * Controlador para la vista que se encarga de agregar una locacion
     * @param $state variable para el manejo del estado
     * @param $stateParams variable para el manejo del estado
     * @param $scope variable para el manejo de los objetos de la vista
     * @param userService objeto para las opeaciones sobre usuario
     * @param trackingService objeto para las operaciones sobre el seguimiento
     * @param MessagesFactory objeto para la manipulacion de los mensajes
     * @param $ionicHistory objeto para poder regresarse luego de realizar la operacion
     * @constructor
     */
    function AddTrackLocationCtrl($scope, $filter,$stateParams, $state, userService, 
        trackingService, MessagesFactory, Markers, GeoLocation, GOOGLE_MAPS_APIKEY, $ionicHistory, 
        $ionicScrollDelegate, mustseeanddoService)
    {
        var self = this;

        self.item = {};
        self.currentPosition = "";

        $scope.mapName = "AgregarLocation";
        self.trackingTypes = [];

        /**
         * Metodo que se ejecuta cuando se inicia la pagina
         */
        $scope.$on('$ionicView.enter', function (viewInfo, state) {
            self.currentPosition = "";
            if (!mustseeanddoService.Latitude || !mustseeanddoService.Longitude)
            {
                mustseeanddoService.getMapLocation();
            }
            else
            {
                self.currentPosition = mustseeanddoService.Latitude + "," + mustseeanddoService.Longitude;
            }
            trackingService.initAllTrackingTypes();
            self.trackingTypes = trackingService.getAllTrackingTypes();

            //console.log('CTRL - $ionicView.loaded', viewInfo, state);
        });


        /**
         * Metodo que se ejecuta cuando el usuario le da click al boton de agregar
         */
        self.setType = function (type) {
            self.item.name = type.id;
        }

        /**
         * Metodo que agrega la locacion de un seguimiento
         */
        self.addTrackLocation = function () {

            self.messages = MessagesFactory.createMessages();
            if (userService.isUserLogged()) {
                if (!self.item.name) {
                    self.messages.error.push($filter('translate')('messageserror-namelocationrequired'));
                }
                if (!self.item.description || self.item.description.trim() == '') {
                    self.messages.error.push($filter('translate')('messageserror-descriptionlocationrequired'));
                }
                if (self.messages.error.length == 0) {
                    self.item.user_id = userService.getUserLoggedId();
                    var item = angular.copy(self.item);
                    $scope.idtracking = parseInt($stateParams.id, 10);
                    item.currentPosition = self.currentPosition;
                    trackingService.addTrackLocation($scope.idtracking, item, self.messages).then(function() {
                        self.item = {};
                        $ionicHistory.goBack();
                    });
                }
                else {
                    $ionicScrollDelegate.scrollTop(true);
                }
            }
        };

        self.viewMap = function() {
            //console.log('self.viewMap::$scope.viewAcorde::'+$scope.viewAcorde);

            if ((typeof $scope.viewAcorde == "undefined") || !$scope.viewAcorde) {
                Markers.clearAllMarkers();
                GeoLocation.init("map" + $scope.mapName, GOOGLE_MAPS_APIKEY, true);
                $ionicScrollDelegate.scrollBottom(true);
            }

            $scope.viewAcorde = !$scope.viewAcorde;
        }
    }
})();
