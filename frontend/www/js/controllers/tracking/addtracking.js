/**
 * Created by poncbi on 10/12/2015.
 */
'use strict';

(function ()
{
    angular.module('cym').controller('AddTrackingCtrl', ['$scope','$filter' ,'$state', 'UserService', 'TrackingService', 'MessagesFactory'
        , '$ionicHistory','$ionicScrollDelegate', 'SessionFactory', 'MustSeeAndDoService', AddTrackingCtrl]);


    /**
     * Controlador para la vista que se encarga de guardar un seguimiento por gps
     * @param $scope
     * @param $state objeto para la manipulacion del estado de la vista
     * @param userService objeto para las opeaciones sobre usuario
     * @param trackingService objeto para las operaciones sobre el seguimiento
     * @param MessagesFactory objeto para la manipulacion de los mensajes
     * @param $ionicHistory objeto para poder regresarse luego de realizar la operacion
     * @constructor
     */
    function AddTrackingCtrl($scope, $filter, $state, userService, trackingService, MessagesFactory, 
        $ionicHistory, $ionicScrollDelegate, SessionFactory, mustseeanddoService)
    {

        var self = this;
        self.item = {};
        self.currentPosition = "";

        self.trackingTypes = [];

        /**
         * Metodo que se ejecuta cuando se inicia la pagina
         */
        $scope.$on('$ionicView.enter', function (viewInfo, state)
        {
            self.currentPosition = "";
            if (!mustseeanddoService.Latitude || !mustseeanddoService.Longitude)
            {
                mustseeanddoService.getMapLocation();
            }
            else
            {
                self.currentPosition = mustseeanddoService.Latitude + "," + mustseeanddoService.Longitude;
            }

            self.item = {};

            trackingService.initAllTrackingTypes();
            self.trackingTypes = trackingService.getAllTrackingTypes();

            //console.log('CTRL - $ionicView.loaded', viewInfo, state);
        });

        /**
         * Metodo que se ejecuta cuando el usuario le da click al boton de agregar
         */
        self.setType = function (type)
        {
            self.item.locationname = type.id;
        }

        /**
         * Metodo que se ejecuta cuando el usuario le da click al boton de agregar
         */
        self.addTracking = function ()
        {
            self.messages = MessagesFactory.createMessages();
            if (userService.isUserLogged())
            {
                console.log('addTracking::self.item::'+JSON.stringify(self.item));
                if (!self.item.trackingname || self.item.trackingname.trim() == '')
                {
                    self.messages.error.push($filter('translate')('messageserror-wrongnametracking'));
                    //self.messages.error.push('Nombre de seguimiento inv\u00e1lido.');
                }

                //var locationNameFill = !self.item.locationname || self.item.locationname.trim() == '';
                var locationNameFill = !self.item.locationname;
                var locatioDescriptionFill = !self.item.locationdes || self.item.locationdes.trim() == '';

                if (!locatioDescriptionFill || !locationNameFill)
                //if (!locatioDescriptionFill)
                {
                    if (locationNameFill)
                        self.messages.error.push($filter('translate')('messageserror-wrongnamelocation'));
                        //self.messages.error.push('Nombre de localizaci\u00f3n inv\u00e1lido.');

                    if (locatioDescriptionFill)
                        self.messages.error.push($filter('translate')('messageserror-wrongdescriptionlocation'));
                        //self.messages.error.push('Descripci\u00f3n de localizaci\u00f3n inv\u00e1lido.');
                }

                 console.log('addTracking::self.messages.error.length::'+self.messages.error.length);
               if (self.messages.error.length == 0)
                {
                    self.item.user_id = userService.getUserLoggedId();
                    var item = angular.copy(self.item);
                    item.currentPosition = self.currentPosition;
                    trackingService.addTracking(item, self.messages);
                    self.item = {};
                    $ionicHistory.goBack();
                    var obj = {};
                    SessionFactory.setObject("trackingsMap", obj);
                }
                else
                {
                    $ionicScrollDelegate.scrollTop(true);
                }
            }
        };
    }
})();
