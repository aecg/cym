'use strict';

(function () {
    angular.module('cym').controller('DashboardCtrl', ['$scope', '$filter', 'SessionFactory', 'MustSeeAndDoService', DashboardCtrl]);

    function DashboardCtrl( $scope, $filter, SessionFactory, mustseeanddoService ) {
        var imgCount = 4;
        var dir = 'img/';
        var images = [];
        images[1] = "bg-01.png";
        images[2] = "bg-02.png";
        images[3] = "bg-03.png";
        images[4] = "bg-04.png";

        /**
         * Metodo que se ejecuta cuando se inicia la pagina
         */
        $scope.$on('$ionicView.enter', function (viewInfo, state) {
            if (!mustseeanddoService.Latitude || !mustseeanddoService.Longitude)
            {
                mustseeanddoService.getMapLocation();
            }
            var randomCount = Math.round(Math.random() * (imgCount - 1)) + 1;
            $scope.dashboardBackground = dir + images[randomCount];
        });

    }
})();

