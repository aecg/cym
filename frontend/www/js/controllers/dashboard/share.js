'use strict';

(function () {
    angular.module('cym').controller('SharingCtrl', ['$scope', '$filter', 'SessionFactory', 'DashboardService', 'UserService', '$loading', 'MessagesFactory', SharingCtrl])   
        .directive('inviteFriend', function ()
        {
            return {
                restricted: 'E',
                templateUrl: 'templates/share/friends.html'
            };
        });

    function SharingCtrl( $scope, $filter, SessionFactory, DashboardService, userService, $loading, MessagesFactory ) {

        var self = this;

        /**
         * Metodo que se ejecuta cuando se inicia la pagina
         */
        $scope.$on('$ionicView.enter', function (viewInfo, state) {
            self.friend = "";
            self.friends = [];
        });

        self.addButton = function () {
            //console.log('addButton::self.friend::'+self.friend);
            self.messages = MessagesFactory.createMessages();
            if ((typeof self.friend != "undefined") && (self.friend.trim() != "")) {
                self.friends.push(self.friend);
                self.friend = "";
                DashboardService.setFriends(self.friends);
            }
            else
            {
                console.log('addButton::no friend');
                self.messages.error.push($filter('translate')('messageserror-no-friend'));
            }
            //console.log('addButton::self.friends::'+JSON.stringify(self.friends));
        };

        self.sendInvites = function ()
        {
            $loading.start('loading');
            var userLogged = userService.getUserLogged();
            DashboardService.inviteFriends(userLogged, self.messages).then(function() {
                $scope.friend = "";
                $scope.friends = [];
                $loading.finish('loading');
            })
        }

    }
})();

