'use strict';

(function () {
    angular.module('cym').controller('Image360Ctrl',
        ['$stateParams', '$state', '$scope', '$cordovaSQLite', Image360Ctrl]);

    function Image360Ctrl($stateParams, $state, $scope, $cordovaSQLite) {
        var self = this;

        self.locationid = 0;
        self.bookid = 0;
        self.msadid = 0;
        self.image360 = "";

        $scope.$on('$ionicView.enter', function (viewInfo, state) {

            var success = function (res) {
                //console.log('Image360Ctrl::success::res.rows.length::'+res.rows.length);
                if (res.rows.length > 0) {
                    var mustseeanddoar = res.rows.item(0);

                    self.image360 = mustseeanddoar.image_360;
                }
            };

            self.locationid = parseInt($stateParams.locationid, 10);
            self.bookid = parseInt($stateParams.bookid, 10);
            self.msadid = parseInt($stateParams.msadid, 10);

            //console.log('Image360Ctrl::self.bookid::'+self.bookid);
            //console.log('Image360Ctrl::self.msadid::'+self.msadid);

            var select_msad_ar = 'SELECT image_360 FROM mustseeanddo_ar where mustseeanddo_id = ?';
            //console.log('Image360Ctrl::select_msad_ar::'+select_msad_ar+'::self.msadid::'+self.msadid);
            $cordovaSQLite.execute(db, select_msad_ar, [self.msadid]).then(success);
        });

        self.showBook = function(id, locationid) {
            $state.go('app.tabs.mustseeanddolist', {id: id, location: locationid, download: true});
            //window.location.href = "#/app/tabs/mustseeanddolist/" + id + "/true";
        }

    }
})();
