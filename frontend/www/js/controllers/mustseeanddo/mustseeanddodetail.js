/**
 * Created by poncbi on 04/12/2015.
 */
'use strict';

(function ()
{
    angular.module('cym').controller('MustSeeAndDoDetailCtrl', ['$stateParams', '$scope', '$filter' ,'UserService',
        'MustSeeAndDoService', 'MessagesFactory', 'GeoLocation', 'GOOGLE_MAPS_APIKEY', 'Markers', '$ionicScrollDelegate', 
        '$loading', MustSeeAndDoDetailCtrl]);

    /**
     * Controlador para la vista del detalle de que ver y que hacer
     * @param $stateParams objeto para la manipulacion del estado
     * @param $scope objeto para la manipulacion de los objetos de la vista
     * @param userService objeto para las operaciones sobre usuario
     * @param mustseeanddoService objeto para la manipulación de mustSeeAndDo
     * @param MessagesFactory objeto para la manipulacion de los mensajes
     * @constructor
     */
    function MustSeeAndDoDetailCtrl($stateParams, $scope, $filter, userService, 
        mustseeanddoService, MessagesFactory, GeoLocation, GOOGLE_MAPS_APIKEY, Markers, $ionicScrollDelegate, 
        $loading)
    {

        var self = this;

        self.item = {};
        $scope.viewAcord = false;
        $scope.viewAcorde = false;
        self.information = [];
        $scope.mapName = "DetailMustAndDo";


        /**
         * Descripcion:             metodo que se ejecuta al cargar la pagina
         * @version                 1.0
         * @author                  Leonor Guzman
         * @since                   12/18/2015
         *
         */
        $scope.$on('$ionicView.enter', function ()
        {
            $loading.start('loading');
            self.loadLocationDetail();
        });

        /**
         * Metodo que consulta el detalle del MustAndDo
         */
        self.loadLocationDetail = function ()
        {
            if (!mustseeanddoService.Latitude || !mustseeanddoService.Longitude)
            {
                mustseeanddoService.getMapLocation();
            }
            self.messages = MessagesFactory.createMessages();
            $scope.mustseeid = parseInt($stateParams.id, 10);
            mustseeanddoService.getLocationDetail($scope.mustseeid).then(function (res)
            {
                if (res.rows.length > 0)
                {
                    self.item = res.rows.item(0);

                    var distance = null;
                    if (mustseeanddoService.Latitude && mustseeanddoService.Longitude)
                    {
                        var lat = null;
                        var lng = null;
                        try {
                            var pos = self.item.geographic_position.split(",");
                            lat = pos[0].trim();
                            lng = pos[1].trim();
                        }
                        catch(e) {
                            lat = 0;
                            lng = 0;
                        }
                        distance = mustseeanddoService.getDistanceFromLatLonInKm(mustseeanddoService.Latitude, mustseeanddoService.Longitude, lat, lng);
                    }
                    self.item.distance = distance;

                    self.getInfo();
                }
                else
                {
                    $loading.finish('loading');
                    messages.error.push($filter('translate')('messageserror-mustseeanddonotfound'));
                }
            });
        };

        $scope.$watch(function () { 
          return mustseeanddoService.Latitude; 
        }, function(newVal, oldVal){
            if(oldVal!==newVal && newVal !== undefined){
                console.log('calculating distances');
                self.loadLocationDetail();
            }
        });

        self.getInfo = function ()
        {
            var success = function (res)
            {
                if (res.rows.length > 0)
                {
                    for (var i = 0; i < res.rows.length; i++)
                    {
                        self.information.push(res.rows.item(i));
                    }
                }
                $loading.finish('loading');
            };

            var error = function (res)
            {
                $loading.finish('loading');
            };
            mustseeanddoService.getInfor($scope.mustseeid).then(success, error);
        };

        self.viewMap = function() {
            console.log('self.viewMap::$scope.viewAcorde::'+$scope.viewAcorde);

            if ((typeof $scope.viewAcorde == "undefined") || !$scope.viewAcorde) {
                var itemForMap = {};
                itemForMap.name = self.item.title;
                itemForMap.latlong = self.item.geographic_position;
                itemForMap.qr = self.item.qr;
                itemForMap.ar = self.item.ar;
                Markers.setMarkersMustSeeAndDo(itemForMap).then(function() {
                    GeoLocation.init("map" + $scope.mapName, GOOGLE_MAPS_APIKEY, false);

                    /*
                    var div_map_outer = $('#map_outer');
                    var offset = div_map_outer.offset();
                    var top = offset.top;
                    var bottom = top + div_map_outer.outerHeight();
                    console.log('self.viewMap::bottom::'+bottom);
                    $ionicScrollDelegate.scrollTo(0, bottom, true);
                    */
                    $ionicScrollDelegate.scrollBottom(true);
                });
            }
            $scope.viewAcorde = !$scope.viewAcorde;
        };

    }

})();
