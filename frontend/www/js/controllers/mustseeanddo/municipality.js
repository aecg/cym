'use strict';

(function ()
{
    angular.module('cym').controller('MunicipalityCtrl', ['UserService', '$scope', '$state', '$filter', 'MustSeeAndDoService', 
                                'MessagesFactory', '$rootScope', '$loading', 'DEFAULT_STATE', MunicipalityCtrl]);

    function MunicipalityCtrl(userService, $scope, $state, $filter, mustseeanddoService, 
                            MessagesFactory, $rootScopeg, $loading, DEFAULT_STATE)
    {
        var self = this;
        self.item = {};

        self.data = {
            municipality: [],
            state: 0,
            total: 0,
            count: 0
        };

        var clearData = function ()
        {
            self.data.municipality = [];
            self.data.total = 0;
            self.data.count = 0;
        };

        $scope.$on('$ionicView.enter', function (viewInfo, state)
        {
            $loading.start('loading');
            if (!mustseeanddoService.Latitude || !mustseeanddoService.Longitude)
            {
                mustseeanddoService.getMapLocation();
            }
            self.data.state = DEFAULT_STATE;
            //console.log('$ionicView.enter');
            self.messages = MessagesFactory.createMessages();
            mustseeanddoService.getMunicipalities(self.data, self.messages).then(function() {
                //console.log('$ionicView.enter::self.data::'+JSON.stringify(self.data));
                $loading.finish('loading');
            });
        });

        self.showMunicipalityDetail = function(municipality) {
            $state.go('app.tabs.book', {location: municipality.id});
        };
    }

})();