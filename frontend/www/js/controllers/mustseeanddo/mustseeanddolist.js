/**
 * Created by poncbi on 04/12/2015.
 */
'use strict';

(function ()
{
    angular.module('cym').controller('MustSeeAndDoListCtrl',
        ['$stateParams', '$state', '$scope', 'PREFIJO', '$cordovaSQLite', '$filter', 'UserService', 'MustSeeAndDoService', 
        'MessagesFactory', '$cordovaBarcodeScanner','SyncronizationProxy', '$loading', '$ionicModal', '$sce', MustSeeAndDoListCtrl]);

    /**
     * controlador de la vista
     * @param $stateParams parametros para el manejo del estado
     * @param $scope variable para el manejo del contexto
     * @param userService servicio del usuario
     * @param mustseeanddoService servicio mustseendo1
     * @param MessagesFactory Fabrica para los mensajes
     * @param $cordovaBarcodeScanner Fabrica para los mensajes
     * @constructor
     */
    function MustSeeAndDoListCtrl($stateParams, $state, $scope, PREFIJO, $cordovaSQLite, $filter, userService, mustseeanddoService, 
        MessagesFactory, $cordovaBarcodeScanner, SyncronizationProxy, $loading, $ionicModal, $sce)
    {
        var self = this;

        self.item = 0;

        self.data = {
            location: [],
            bookid: 0,
            locationid: 0,
            total: 0,
            count: 0
        };

        var clearData = function ()
        {
            self.data.location = [];
            self.data.bookid = 0;
            self.data.locationid = 0;
            self.data.total = 0;
            self.data.count = 0;
        };

        $scope.$on('$ionicView.enter', function (viewInfo, state) {
            $loading.start('loading');
            if (!mustseeanddoService.Latitude || !mustseeanddoService.Longitude)
            {
                mustseeanddoService.getMapLocation();
            }

            self.messages = MessagesFactory.createMessages();
            self.data.locationid = parseInt($stateParams.location, 10);
            self.data.bookid = parseInt($stateParams.id, 10);

            if ($stateParams.download == "true") {
                mustseeanddoService.getMustSeeAndDos(self.data, self.messages).then(function() {
                    $loading.finish('loading');
                });
            }
            else {
                $state.go('app.tabs.book', {location: $scope.locationid});
            }
        });

        $scope.$watch(function () { 
          return mustseeanddoService.Latitude; 
        }, function(newVal, oldVal){
            if(oldVal!==newVal && newVal !== undefined){
                console.log('calculating distances');
                self.messages = MessagesFactory.createMessages();
                self.data.locationid = parseInt($stateParams.location, 10);
                self.data.bookid = parseInt($stateParams.id, 10);

                if ($stateParams.download == "true") {
                    mustseeanddoService.getMustSeeAndDos(self.data, self.messages);
                }
            }
        });

        /**
         * Metodo que limpia los datos de la interfaz
         */
        var clearData = function () {
            self.data.location = [];
            self.data.total = 0;
            self.data.count = 0;
        };

        /**
         * Metodo que procesa la informacion obtenida por el lector del qr
         */
        var processInfoQr = function (image) {
            //alert('image.text::'+image.text);
            if (image.text != "") {
                var indexOfPrefix = image.text.indexOf(PREFIJO);
                if(indexOfPrefix == 0) {

                    var success = function (res) {
                        if (res.rows.length > 0) {
                            navigator.notification.alert(
                                res.rows.item(0).summary,
                                function(){},
                                $filter('translate')('label-qrcodenotificatioconfirm2'),
                                $filter('translate')('label-picturesizenotificatioconfirm3')
                            );
                        }
                        else {
                            navigator.notification.alert(
                                $filter('translate')('label-qrinvalid'),
                                function(){},
                                $filter('translate')('label-qrcodenotificatioconfirm2'),
                                $filter('translate')('label-picturesizenotificatioconfirm3')
                            );
                        }
                    };

                    // corresponde a un QR de la app
                    var select_msad_qr = 'SELECT * FROM mustseeanddo_qr where mustseeanddo_id = ' + self.item + ' AND title = "' + image.text + '"';
                    $cordovaSQLite.execute(db, select_msad_qr, []).then(success);
                }
                else {
                    navigator.notification.alert(
                        //$filter('translate')('label-qrinvalid'),
                        image.text,
                        function(){},
                        $filter('translate')('label-qrcodenotificatioconfirm2'),
                        $filter('translate')('label-picturesizenotificatioconfirm3')
                    );
                }
            }
            self.item = 0;
        };

        /**
         * Metodo que procesa la informacion obtenida por el lector del qr
         */
        var processErroroQr = function (error) {
            console.log('Ha ocurrido un error procesando la imagen obtenida del lector qr');
            //alert("Ha ocurrido error : " + error);
        };

        /**
         * Metodo que abre el lector del qr
         */
        $scope.readCode = function (msad_id) {
            //alert('readCode::'+msad_id);
            self.item = msad_id;
            $cordovaBarcodeScanner.scan().then(processInfoQr, processErroroQr);
        };

        /**
         * Metodo que abre el lector del qr
         */
        $scope.loadWikitude = function (msad_id) {
            var success = function (res) {
                //alert('loadWikitude::res.rows.length::'+res.rows.length);
                //console.log('loadWikitude::res.rows.length::'+res.rows.length);
                if (res.rows.length > 0) {
                    var mustseeanddoar = res.rows.item(0);

                    //console.log('loadWikitude::mustseeanddoar.is_360::'+mustseeanddoar.is_360);
                    if (mustseeanddoar.is_360 == 0) {
                        var image_preview_base64 = mustseeanddoar.image_preview;
                        var image_display_base64 = mustseeanddoar.image_display;
                        var image_binary_base64 = mustseeanddoar.image_binary;

                        // obtener los archivos de la imagen AR y bajarlos a disco, remplazando los archivos fisicos
                        self.writeToFile('display.png', image_display_base64, 'image/JPEG').then(function() {
                            //alert('self.writeToFile::display::done');

                            self.writeToFile('preview.png', image_preview_base64, 'image/JPEG').then(function() {
                                //alert('self.writeToFile::preview::done');

                                self.writeToFile('binary.wtc', image_binary_base64, 'application/octet-stream').then(function() {
                                    //alert('self.writeToFile::binary::done');

                                    //alert('loading var app');
                                    app.urlLauncher('www/ar.html');
                                });
                            });
                        });
                    }
                    else {
                        var stateObj = {locationid: self.data.locationid, bookid: self.data.bookid, msadid: msad_id};
                        // console.log('loadWikitude::stateObj::'+JSON.stringify(stateObj));
                        //$state.go('app.tabs.image360', stateObj);
                        self.openModal(stateObj);
                    }
                }
                else {
                    navigator.notification.alert(
                        $filter('translate')('label-arinvalid'),
                        function(){},
                        $filter('translate')('label-arcodenotificatioconfirm2'),
                        $filter('translate')('label-picturesizenotificatioconfirm3')
                    );
                }
            };

            //alert('loadWikitude::'+msad_id);
            //console.log('loadWikitude::'+msad_id);
            // corresponde a un AR de la app
            var select_msad_ar = 'SELECT is_360, image_preview, image_display, image_binary FROM mustseeanddo_ar where mustseeanddo_id = ?';
            //alert('loadWikitude::select_msad_ar::'+select_msad_ar+'::msad_id::'+msad_id);
            $cordovaSQLite.execute(db, select_msad_ar, [msad_id]).then(success);
        };

        self.writeToFile = function (fileName, dataObj, typeObj) {
            var promises = [];
            var defWriteToFile = new $.Deferred();
            promises.push(defWriteToFile);

            window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function (dirEntry) {
                //console.log('writeToFile::cordova.file.dataDirectory::'+cordova.file.dataDirectory);
                self.saveFile(dirEntry, dataObj, typeObj, fileName).then(function() {
                    defWriteToFile.resolve();
                });

            }, function(error) {
                console.log('error::window.resolveLocalFileSystemURL::'+JSON.stringify(error));
                self.errorHandler(error);
            });

            return $.when.apply(undefined, promises).promise();
        }

        self.saveFile = function (dirEntry, fileData, typeData, fileName) {
            var promises = [];
            var defSaveFile = new $.Deferred();
            promises.push(defSaveFile);

            dirEntry.getFile(fileName, { create: true, exclusive: false }, function (fileEntry) {
                //console.log('saveFile::fileName::'+fileName);
                self.writeFileBinary(fileEntry, fileData, typeData).then(function() {
                    defSaveFile.resolve();
                });
            }, function(error) {
                console.log('error::dirEntry.getFile::'+JSON.stringify(error));
                self.errorHandler(error);
            });

            return $.when.apply(undefined, promises).promise();
        }

        self.writeFileBinary = function(fileEntry, dataObj, typeObj) {

            var promises = [];
            var defWriteFileBinary = new $.Deferred();
            promises.push(defWriteFileBinary);

            // Create a FileWriter object for our FileEntry (log.txt).
            fileEntry.createWriter(function (fileWriter) {

                fileWriter.onwriteend = function() {
                    //console.log('writeFileBinary::file wrote');
                    defWriteFileBinary.resolve();
                };

                fileWriter.onerror = function(e) {
                    console.log("Failed file write: " + e.toString());
                };

                var byteCharacters = atob(dataObj);
                var byteNumbers = new Array(byteCharacters.length);
                for (var i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }
                var byteArray = new Uint8Array(byteNumbers);
                var blob = self.b64toBlob(dataObj, typeObj, 512);
                fileWriter.write(blob);
            });

            return $.when.apply(undefined, promises).promise();
        }

        self.b64toBlob = function(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, {type: contentType});
            return blob;
        }

        self.errorHandler = function (e) {
            var msg = '';

            switch (e.code) {
                case FileError.QUOTA_EXCEEDED_ERR:
                msg = 'QUOTA_EXCEEDED_ERR';
                break;
            case FileError.NOT_FOUND_ERR:
                msg = 'NOT_FOUND_ERR';
                break;
            case FileError.SECURITY_ERR:
                msg = 'SECURITY_ERR';
                break;
            case FileError.INVALID_MODIFICATION_ERR:
                msg = 'INVALID_MODIFICATION_ERR';
                break;
            case FileError.INVALID_STATE_ERR:
                msg = 'INVALID_STATE_ERR';
                break;
            case FileError.ABORT_ERR:
                msg = 'ABORT_ERR';
                break;
            default:
                msg = 'Unknown Error';
                break;
            };

            console.log('Error: ' + msg);
        }

        self.showMustSeeAndDoDetail = function(msad) {
            $state.go('app.tabs.mustseeanddodetail', {id: msad.id});
            //window.location.href = "#/app/mustseeanddodetail/" + msad.id;
        }

        $ionicModal.fromTemplateUrl('templates/modal.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.modal = modal;
        });

        $scope.$on('modal.shown', function() {
            //alert('Modal is shown!');
            //console.log('$scope.initPanorama::inicializando obj panorama');
        //    pannellum.viewer('panorama', {
        //        "type": "equirectangular",
                //"panorama": "360.jpg",
        //        "panorama": $scope.imgUrl,
        //        "autoLoad" : true,
        //        "showFullscreenCtrl" : false
                //"showFullscreenCtrl" : false,
                //"basePath" : cordova.file.dataDirectory
        //   });
        });

        self.openModal = function(obj) {
            //console.log('self.openModal::obj::'+JSON.stringify(obj));
            var success = function (res) {
                //console.log('self.openModal::success::res.rows.length::'+res.rows.length);
                if (res.rows.length > 0) {
                    var mustseeanddoar = res.rows.item(0);
                    var image_360_base64 = mustseeanddoar.image_360;

                    // pasando la imagen como String Base64
                    //$scope.imgUrl = "data:image/JPEG;base64," + image_360_base64;
                    //console.log('self.openModal::success::1::$scope.imgUrl::'+$scope.imgUrl);
                    //alert($scope.imgUrl);
                    //$scope.modal.show();

                    // pasando la imagen como Blob
                    //var blob = self.b64toBlob(image_360_base64, 'image/JPEG', 512);
                    //window.URL = window.URL || window.webkitURL;
                    //$scope.imgUrl = window.URL.createObjectURL(blob);
                    //console.log('self.openModal::success::2::$scope.imgUrl::'+$scope.imgUrl);
                    //alert($scope.imgUrl);
                    //$scope.modal.show();

                    // pasando la url de la imagen, a partir del Blob usando FileReader
                    //var reader = new FileReader();
                    //var blob = self.b64toBlob(mustseeanddoar.image_360_base64, 'image/JPEG', 512);
                    //reader.onload = function(e){
                    //    $scope.imgUrl = reader.result;
                    //    console.log('self.openModal::success::3::$scope.imgUrl::'+$scope.imgUrl);
                        //alert($scope.imgUrl);
                    //    $scope.modal.show();
                    //}
                    //reader.readAsDataURL(blob);

                    // escribiendo la imagen en un archivo físico
                    //self.writeToFile('360.jpg', image_360_base64, 'image/JPEG').then(function() {
                        //var imgUrl = cordova.file.dataDirectory + "360.jpg";
                    //    var imgUrl = "assets/" + "360.jpg";

                    //    self.explicitlyTrustedHtml = $sce.trustAsHtml(
                    //        '<iframe width="100%" height="100%" allowfullscreen style="border-style:none;" '+
                    //        'src="#/app/tabs/pannellum?panorama="' +
                    //        'assets/360.jpg&amp;title=Jordan%20Pond"></iframe>');

                    //    $scope.modal.show();
                    //});

                    self.explicitlyTrustedHtml = $sce.trustAsHtml(
                        '<script type="text/javascript" src="js/360.js"></script>'
                    );
                    self.image360 = image_360_base64;

                    //self.explicitlyTrustedHtml = $sce.trustAsHtml(
                    //    '<link rel="stylesheet" href="css/360.css">' +
                    //    '<div class="panorama">' +
                    //        '<img ng-src="data:image/JPEG;base64,' + image_360_base64 + '">' +
                    //    '</div>' +
                    //    '<script type="text/javascript" src="js/360.js"></script>'
                    //);
                    $scope.modal.show();
                }
            };

            var select_msad_ar = 'SELECT image_360 FROM mustseeanddo_ar where mustseeanddo_id = ?';
            //console.log('self.openModal::select_msad_ar::'+select_msad_ar+'::obj.msadid::'+obj.msadid);
            $cordovaSQLite.execute(db, select_msad_ar, [obj.msadid]).then(success);
        }
    }
})();
