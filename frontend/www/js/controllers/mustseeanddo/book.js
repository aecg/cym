/**
 * Created by poncbi on 12/03/2016.
 */
/**
 * Created by poncbi on 07/12/2015.
 */
'use strict';

(function ()
{
    angular.module('cym').controller('BookCtrl', ['UserService', '$scope', '$state', '$filter','MustSeeAndDoService', 
            'MessagesFactory', '$rootScope', '$ionicLoading', '$loading', '$stateParams', BookCtrl]);

    function BookCtrl(userService, $scope, $state, $filter, mustseeanddoService, 
                MessagesFactory, $rootScope, $ionicLoading, $loading, $stateParams)
    {

        var self = this;

        self.item = {};

        var back = false;

        self.data = {
            book: [],
            total: 0,
            count: 0
        };

        self.listLocations = {
            location: [],
            locationid: 0,
            total: 0,
            count: 0
        };

        var clearData = function ()
        {
            self.data.book = [];
            self.data.total = 0;
            self.data.count = 0;
        };

        $scope.$on('$ionicView.enter', function (viewInfo, state)
        {
            $loading.start('loading');
            if (!mustseeanddoService.Latitude || !mustseeanddoService.Longitude)
            {
                mustseeanddoService.getMapLocation();
            }
            //console.log('$ionicView.enter');
            self.messages = MessagesFactory.createMessages();
            $scope.locationid = parseInt($stateParams.location, 10);
            self.data.locationid = $scope.locationid;
            mustseeanddoService.getLocalBooks(self.data, self.messages).then(function() {
                $loading.finish('loading');
            });
        });

        var bookaux = {};

        //Get the countrys
        self.showBookDetail = function(book) {

            $state.go('app.tabs.mustseeanddolist', {id: book.id, location: self.data.locationid, download: true});

            /*
            //console.log('book::'+JSON.stringify(book));
            if(!book.pertenece) {
                bookaux = book;
                if(window.cordova) {
                    navigator.notification.confirm(
                        $filter('translate')('messageconfirm-downloadbook1') + book.name + $filter('translate')('messageconfirm-downloadbook2'),
                        confirmDownload,
                        $filter('translate')('label-deletetrackingnotificatioconfirm2'),
                        $filter('translate')('label-deletetrackingnotificatioconfirm3')
                    );
                }
                else {
                    if(confirm( $filter('translate')('messageconfirm-downloadbook1') + book.name + $filter('translate')('messageconfirm-downloadbook2'))) {
                        confirmDownload();
                    }
                }
            }
            else {
                window.location.href = "#/app/mustseeanddolist/" + book.id + "/true";
            }
            */
        };


        function confirmDownload(res) {
            var errorDownload = function(res) {
                //console.log(JSON.stringify(res));
                self.messages.error.push($filter('translate')('messageserror-sync'));
                if (res.data != null) {
                    //messages.error.push(res.data.message);
                    console.log(res.data.message)
                }
                // error, resolver las promesas faltantes
                $.each(promises, function(i, promise) {
                    promise.resolve(i);
                });
                $ionicLoading.hide();
            }

            //console.log('confirmDownload::init');
            var promises = [];

            var defCreateUserBook = new $.Deferred();
            var defGetBookMustSeeAndDo = new $.Deferred();
            var defGetBookMustSeeAndDoDetail = new $.Deferred();
            var defGetBookStates = new $.Deferred();
            var defGetBookCountries = new $.Deferred();

            promises.push(defCreateUserBook);
            promises.push(defGetBookMustSeeAndDo);
            promises.push(defGetBookMustSeeAndDoDetail);
            promises.push(defGetBookStates);
            promises.push(defGetBookCountries);

            $ionicLoading.show({
                template: $filter('translate')('messagetemplate-downloadbook')
            });
            var usernameLogged = userService.getUserNameLogged();
            var useridLogged = userService.getUserLoggedId();
            var bookid = bookaux.id;

            self.messages = MessagesFactory.createMessages();

            // estableciendo que el usuario va a descargar un libro, se guarda la relación
            // en la tabla user_book en el backend. Si la respuesta es 200, se inserta la
            // relación en la tabla user_book local del móvil
            //console.log('confirmDownload::bookaux::'+JSON.stringify(bookaux));
            mustseeanddoService.createUserBook(bookid, usernameLogged, useridLogged, self.messages).then(function() {
                //console.log("createUserBook::done");
                defCreateUserBook.resolve();

                // buscando los sitios turísticos de un libro, y se procede a verificar la existencia de
                // tales registros en la tabla mustseeanddo, si no existe se inserta, en caso contrario
                // se actualiza
                mustseeanddoService.getBookMustSeeAndDo(bookid, self.messages).then(function() {
                    //console.log("getBookMustSeeAndDo::done");
                    defGetBookMustSeeAndDo.resolve();

                    // buscando los detalles de los sitios turísticos de un libro, y se procede a verificar la existencia de
                    // tales registros en la tabla mustseeanddo_detail, si no existe se inserta, en caso contrario
                    // se actualiza
                    mustseeanddoService.getBookMustSeeAndDoDetail(bookid, self.messages).then(function() {
                        //console.log("getBookMustSeeAndDoDetail::done");
                        defGetBookMustSeeAndDoDetail.resolve();

                        // buscando los estados geográficos de un libro, y se procede a verificar la existencia de
                        // tales registros en la tabla state, si no existe se inserta
                        mustseeanddoService.getBookStates(bookid, self.messages).then(function() {
                            //console.log("getBookStates::done");
                            defGetBookStates.resolve();

                            // buscando los países geográficos de un libro, y se procede a verificar la existencia de
                            // tales registros en la tabla country, si no existe se inserta
                            mustseeanddoService.getBookCountries(bookid, self.messages).then(function() {
                                //console.log("getBookCountries::done");
                                defGetBookCountries.resolve();

                                //console.log('confirmDownload::done');
                                $ionicLoading.hide();
                                bookaux.pertenece = true;
                                //console.log('bookaux::'+bookaux);
                                $state.go('app.tabs.mustseeanddolist', {id: bookid, download: true});
                            }, errorDownload);
                        }, errorDownload);
                    }, errorDownload);
                }, errorDownload);
            }, errorDownload);
            return $.when.apply(undefined, promises).promise();
        }
    }

})();