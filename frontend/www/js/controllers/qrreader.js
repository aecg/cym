'use strict';

(function ()
{
    angular.module('cym').controller('QRReaderCtrl',
        ['$stateParams', '$state', '$scope', 'PREFIJO', '$cordovaSQLite', '$filter', 'UserService', 'MustSeeAndDoService', 'MessagesFactory', '$cordovaBarcodeScanner', QRReaderCtrl]);

    function QRReaderCtrl($stateParams, $state, $scope, PREFIJO, $cordovaSQLite, $filter, userService, mustseeanddoService, MessagesFactory, $cordovaBarcodeScanner)
    {
        /**
         * Metodo que procesa la informacion obtenida por el lector del qr
         */
        var processInfoQr = function (image) {
            //alert('image.text::'+image.text);
            if (image.text != "") {
                navigator.notification.alert(
                    image.text,
                    function(){},
                    $filter('translate')('label-qrcodenotificatioconfirm2'),
                    $filter('translate')('label-picturesizenotificatioconfirm3')
                );
            }
        };

        /**
         * Metodo que procesa la informacion obtenida por el lector del qr
         */
        var processErrorQr = function (error) {
            console.log('Ha ocurrido un error procesando la imagen obtenida del lector qr');
            //alert("Ha ocurrido error : " + error);
        };

        /**
         * Metodo que abre el lector del qr
         */
        $scope.readCode = function () {
            console.log('readCode::');
            $cordovaBarcodeScanner.scan().then(processInfoQr, processErrorQr);
        };
    }
})();
