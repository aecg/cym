'use strict';

(function () {
    angular.module('cym').controller('MapCtrl', ['$scope', '$filter', 'GeoLocation', 'GOOGLE_MAPS_APIKEY', 'Markers', 'SyncronizationService', 'SessionFactory', 
        'UserService', 'TrackingService', 'DocumentService', 'MessagesFactory', MapCtrl]);

    function MapCtrl( $scope, $filter, GeoLocation, GOOGLE_MAPS_APIKEY, Markers, syncronizationService, SessionFactory, 
        userService, trackingService, documentService, MessagesFactory ) {

        var self = this;
        self.mustseeanddoCoordinates = [];
        self.trackings = [];
        self.documents = [];

        /**
         * Metodo que se ejecuta cuando se inicia la pagina
         */
        $scope.$on('$ionicView.enter', function (viewInfo, state) {
            var promises = [];

            var defVerifyMsadCoordinates = new $.Deferred();
            var defGetTrackings =  new $.Deferred();
            var defGetDocuments =  new $.Deferred();

            var defSetAllMarkersMap =  new $.Deferred();

            promises.push(defVerifyMsadCoordinates);
            promises.push(defGetTrackings);
            promises.push(defGetDocuments);
            promises.push(defSetAllMarkersMap);

            var mustseeanddoObj = SessionFactory.getObject("mustseeanddoMap");
            if ((mustseeanddoObj != null) && (mustseeanddoObj.length != null)) {
                self.mustseeanddoCoordinates = mustseeanddoObj;
            }
            else {
                self.mustseeanddoCoordinates = [];
            }

            var trackingsObj = SessionFactory.getObject("trackingsMap");
            if ((trackingsObj != null) && (trackingsObj.length != null)) {
                self.trackings = trackingsObj;
            }
            else {
                self.trackings = [];
            }

            var documentsObj = SessionFactory.getObject("documentsMap");
            if ((documentsObj != null) && (documentsObj.length != null)) {
                self.documents = documentsObj;
            }
            else {
                self.documents = [];
            }

            //console.log('$ionicView.enter::pre::self.mustseeanddoCoordinates.length::'+self.mustseeanddoCoordinates.length);
            self.verifyMsadCoordinates().then(function() {
                //console.log('$ionicView.enter::post::self.verifyMsadCoordinates()');
                defVerifyMsadCoordinates.resolve();
                //alert('$ionicView.enter::init::self.mustseeanddoCoordinates.length::'+self.mustseeanddoCoordinates.length);
                //console.log('$ionicView.enter::post::self.mustseeanddoCoordinates.length::'+self.mustseeanddoCoordinates.length);
                //console.log('$ionicView.enter::pre::self.trackings.length::'+self.trackings.length);

                self.getTrackings().then(function() {
                    //console.log('$ionicView.enter::post::self.getTrackings()');
                    defGetTrackings.resolve();

                    //console.log('$ionicView.enter::post::self.trackings.length::'+self.trackings.length);
                    //console.log('$ionicView.enter::self.trackings::'+JSON.stringify(self.trackings));
                    //console.log('$ionicView.enter::pre::self.documents.length::'+self.documents.length);

                    self.getDocuments().then(function() {
                        //console.log('$ionicView.enter::post::self.getDocuments()');
                        defGetDocuments.resolve();

                        //console.log('$ionicView.enter::post::self.documents.length::'+self.documents.length);
                        //console.log('$ionicView.enter::self.documents::'+JSON.stringify(self.documents));

                        Markers.clearAllMarkers();

                        var pointsMap = {};
                        pointsMap.mustSeeAndDos = self.mustseeanddoCoordinates;
                        pointsMap.trackingGPS = self.trackings;
                        pointsMap.documents = self.documents;

                        Markers.setAllMarkersMap(pointsMap).then(function() {
                            defSetAllMarkersMap.resolve();
                            GeoLocation.init("map_canvas", GOOGLE_MAPS_APIKEY, true);
                        });
                    });
                });
            });
            return $.when.apply(undefined, promises).promise();
        });

        self.verifyMsadCoordinates = function() {
            //console.log('self.verifyMsadCoordinates::init');
            var promises = [];
            var defInitializeCoordinates = new $.Deferred();
            promises.push(defInitializeCoordinates);

            //console.log('self.verifyMsadCoordinates::self.mustseeanddoCoordinates.length::'+self.mustseeanddoCoordinates.length);

            if (self.mustseeanddoCoordinates.length == 0) {
                syncronizationService.initializeMsadCoords().then(function() {
                    self.mustseeanddoCoordinates = syncronizationService.getMustseeanddoCoordinates();
                    SessionFactory.setObject("mustseeanddoMap", self.mustseeanddoCoordinates);

                    //alert('$ionicView.enter::reload::self.mustseeanddoCoordinates.length::'+self.mustseeanddoCoordinates.length);
                    //console.log('self.verifyMsadCoordinates::self.mustseeanddoCoordinates.length::'+self.mustseeanddoCoordinates.length);
                    defInitializeCoordinates.resolve();
                });
            }
            else {
                defInitializeCoordinates.resolve();
            }
            return $.when.apply(undefined, promises).promise();
        }

        self.getTrackings = function () {
            //console.log('self.getTrackings::init');
            var promises = [];

            var defGetTrackings = new $.Deferred();
            var defGetTrackingsForMap = new $.Deferred();

            promises.push(defGetTrackings);

            //console.log('self.getTrackings::self.trackings.length::'+self.trackings.length);

            if (self.trackings.length == 0) {
                promises.push(defGetTrackingsForMap);
                if (userService.isUserLogged()) {

                    var userId = parseInt(userService.getUserLoggedId(), 10);
                    //console.log('self.getTrackings::userId::'+userId);

                    trackingService.getTrackingsForMap(userId, self.trackingTypes).then(function() {
                        //console.log('self.getTrackings::post::trackingService.getTrackingsForMap');

                        self.trackings = trackingService.allTracks().tracks;
                        SessionFactory.setObject("trackingsMap", self.trackings);
                        //console.log('self.getTrackings::self.trackings::'+JSON.stringify(self.trackings));

                        defGetTrackingsForMap.resolve();
                    });
                }
                else {
                    defGetTrackingsForMap.resolve();
                }
            }
            defGetTrackings.resolve();
            return $.when.apply(undefined, promises).promise();
        };

        self.getDocuments = function() {
            //console.log('self.getDocumentsgetDocuments::init');
            var promises = [];
            var defGetDocuments = new $.Deferred();
            promises.push(defGetDocuments);

            //console.log('self.getDocuments::self.documents.length::'+self.documents.length);

            if (self.documents.length == 0) {
                if (userService.isUserLogged()) {

                    var userId = parseInt(userService.getUserLoggedId(), 10);
                    //console.log('self.getTrackings::userId::'+userId);

                    documentService.getDocumentsWithGPS(userId).then(function() {
                        //console.log('self.getDocuments::post::trackingService.getTrackingsForMap');

                        self.documents = documentService.allDocuments().documents;
                        SessionFactory.setObject("documentsMap", self.documents);
                        //console.log('self.getDocuments::self.documents::'+JSON.stringify(self.documents));

                        defGetDocuments.resolve();
                    });
                }
                else {
                    defGetDocuments.resolve();
                }
            }
            else {
                defGetDocuments.resolve();
            }
            return $.when.apply(undefined, promises).promise();
        }
    }
})();

