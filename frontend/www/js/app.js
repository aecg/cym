// database global var
var db = null;

// wikitude app global var
var app = null;

var mustseeanddoCoordinates = [];

ionic.Platform.ready(function() {

    /* DATABASE INTEGRATION */
    var olddbname = ['cymar.db', 'cymardb.db', 'itrip.db', 'itrip_db.db', 'itripdb.db', 'dbitrip.db', 'itrip00.db'];
    var dbname = 'itrip103.db';
    var dblocationsqlite = 'default';
    var dblocationcopy = 2;

    var initializedb = function () {
        db = window.sqlitePlugin.openDatabase({name: dbname, location: dblocationsqlite},
            function (db) {
                angular.bootstrap(document.body, ['cym']);
            });
    };

    if (ionic.Platform.isWebView()) {

        if (window.sqlitePlugin) {

            for (var i = 0; i < olddbname.length; i++) {
                window.plugins.sqlDB.remove(olddbname[i], dblocationcopy);
            }

            window.plugins.sqlDB.copy(dbname, dblocationcopy, 
                function () {
                    // nueva instalación
                    initializedb();
                },
                function(e) {
                    initializedb();
                }
            );
        }
    }
    else {
        if (window.cordova) {
            db = $cordovaSQLite.openDB(dbname);
            angular.bootstrap(document.body, ['cym']);
        }
        else {
            db = window.openDatabase(dbname, "1.0", "iTrip", -1);
            angular.bootstrap(document.body, ['cym']);
        }
    }

    if (window.cordova) {
        if (window.navigator.network.connection.type == 'none') {
            $rootScope.isOnline = false;
        }

        if (cordova.file !== undefined) {
            $rootScope.dataDirectory = cordova.file.dataDirectory;
        }
    }

    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
    }

    if (window.StatusBar) {
        StatusBar.styleDefault();
    }
});

angular.module('cym', ['ionic', 'ui.router', 'ngCordova', 'pascalprecht.translate', 'darthwade.dwLoading'])

    .run(function ($ionicPlatform, $cordovaSQLite, $cordovaNetwork, $rootScope) {
        ionic.Platform.isFullScreen = true;
        $rootScope.isOnline = true;

        app = {
            // represents the device capability of launching ARchitect Worlds
            // with specific features
            isDeviceSupported: false,

            // Application Constructor
            initialize: function () {
                this.bindEvents();
            },
            // Bind Event Listeners
            //
            // Bind any events that are required on startup. Common events are:
            // 'load', 'deviceready', 'offline', and 'online'.
            bindEvents: function () {
                document.addEventListener('deviceready', this.onDeviceReady, false);
            },
            // deviceready Event Handler
            onDeviceReady: function () {
                app.wikitudePlugin = cordova.require("com.wikitude.phonegap.WikitudePlugin.WikitudePlugin");
            },
            // --- Wikitude Plugin ---
            // Use this method to load a specific ARchitect World from either the local file
            // system or a remote server
            loadARchitectWorld: function (example) {
                // check if the current device is able to launch ARchitect Worlds
                app.wikitudePlugin.isDeviceSupported(function () {
                    app.wikitudePlugin.setOnUrlInvokeCallback(app.onUrlInvoke);

                    app.wikitudePlugin.loadARchitectWorld(function successFn(loadedURL) {
                        app.wikitudePlugin.callJavaScript('passData(\''+cordova.file.dataDirectory+'\');');
                        /* Respond to successful world loading if you need to */
                    }, function errorFn(error) {
                        alert('Loading AR web view failed: ' + error);
                    },
                    example.path, example.requiredFeatures, example.startupConfiguration);
                }, function (errorMessage) {
                    alert('wikitude error:'+errorMessage);
                },
                example.requiredFeatures);
            },
            urlLauncher: function (url) {
                //alert('urlLauncher::'+url);
                var world = {
                    "path": url,
                    "requiredFeatures": [
                        "2d_tracking"
                    ],
                    "startupConfiguration": {
                        "camera_position": "back"
                    }
                };
                app.loadARchitectWorld(world);
            },
            // This function gets called if you call "document.location = architectsdk://"
            // in your ARchitect World
            onUrlInvoke: function (url) {
                if (url.indexOf('captureScreen') > -1) {
                    app.wikitudePlugin.captureScreen(function (absoluteFilePath) {
                        alert("snapshot stored at:\n" + absoluteFilePath);
                    },
                    function (errorMessage) {
                        alert('wikitude invoke:'+errorMessage);
                    },
                    true, null);
                }
                else {
                    alert(url + "not handled");
                }
            },
            // --- End Wikitude Plugin ---
        };
        app.initialize();
    })

    .config(['$translateProvider', function ($translateProvider) {
        $translateProvider.useStaticFilesLoader({
            prefix: 'resources/locale-',
            suffix: '.json'
        });
        $translateProvider.preferredLanguage('es_VE');
        $translateProvider.useSanitizeValueStrategy('escaped');
    }])

    .filter('customCurrency', ["$filter", function ($filter) {       
        return function(amount, currencySymbol){
            var currency = $filter('currency');         

            if(amount < 0){
                return currency(amount, currencySymbol).replace("(", "-").replace(")", ""); 
            }

            return currency(amount, currencySymbol);
        };
    }])

    .constant("DEFAULT_COUNTRY",154)
    .constant("DEFAULT_STATE",2454)
    .constant("PREFIJO","CYM_")
    .constant("GOOGLE_MAPS_APIKEY", "AIzaSyAnZev4iiAWB8Q4J1ErXGn7L2Vs-xHFPKc")

    .constant("BASE_URL","http://cym.softclear.net/backend");
    //.constant("BASE_URL","http://training.cym.softclear.net/backend");
    //.constant("BASE_URL","http://localhost:8080/cym");
    //.constant("BASE_URL","http://localhost:8080/backend");
