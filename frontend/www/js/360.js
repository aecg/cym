// Use $(window).load() on live site instead of document ready. This is for the purpose of running locally only
//$(window).load(function() {
$(document).ready(function() {
	//console.log('pre call panorama_viewer');
	$(".panorama").panorama_viewer({
		overlay: false,
		repeat: true,
		animationTime: 1000,
		easing: "ease-out"
	});
});
