'use strict';

/**
 * Route configuration
 */

angular.module('cym').config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider)
{
    $stateProvider

        .state('start', {
            url: '/start?/:page',
            templateUrl: 'templates/start.html',
            controller: 'SecurityCtrl as ctrl'
        })
        .state('app', {
            url: '/app',
            abstract: true,
            templateUrl: 'templates/menu.html'
        })
        .state('app.tabs', {
            url: '/tabs',
            views: {
                'menuContent': {
                    templateUrl: 'templates/tabs.html'
                }
            }
        })
        .state('app.login', {
            url: '/login/:page',
            views: {
                'menuContent': {
                    templateUrl: 'templates/login.html',
                    controller: 'LoginCtrl as ctrl'
                }
            }
        })
        .state('app.tabs.dashboard', {
            url: '/dashboard',
            views: {
                'dashboard-tab': {
                    templateUrl: 'templates/dashboard/dashboard.html',
                    controller: 'DashboardCtrl as ctrl'
                }
            }
        })
        .state('app.tabs.map', {
            url: '/map',
            views: {
                'map-tab': {
                    templateUrl: 'templates/map/map.html',
                    controller: 'MapCtrl as ctrl'
                }
            }
        })
        .state('app.tabs.documents', {
            url: '/documents-list',
            views: {
                'document-tab': {
                    templateUrl: 'templates/documents/list.html',
                    controller: 'DocumentListCtrl as ctrl'
                }
            }
        })
        .state('app.tabs.document-edit', {
            url: '/documents/:id',
            views: {
                'document-tab': {
                    templateUrl: 'templates/documents/edit.html',
                    controller: 'DocumentEditCtrl as ctrl'
                }
            }
        })
        .state('app.tabs.document-new', {
            url: '/documents',
            views: {
                'document-tab': {
                    templateUrl: 'templates/documents/add.html',
                    controller: 'DocumentCtrl as ctrl'
                }
            }
        })
        .state('app.tabs.travel', {
            url: '/travel',
            views: {
                'travel-tab': {
                    templateUrl: 'templates/travel/travel.html',
                    controller: 'TravelsCtrl as ctrl'
                }
            }
        })
        .state('app.tabs.detail-travel', {
            url: '/detail-travel/:id',
            views: {
                'travel-tab': {
                    templateUrl: 'templates/travel/detail-travel.html',
                    controller: 'DetailTravelCtrl as ctrl'
                }
            }
        })
        .state('app.tabs.newTravel', {
            url: '/newTravel',
            views: {
                'travel-tab': {
                    templateUrl: 'templates/travel/new-travel.html',
                    controller: 'NewTravelCtrl as ctrl'
                }
            }
        })
        .state('app.tabs.destiny', {
            url: '/destiny/:trip',
            views: {
                'travel-tab': {
                    templateUrl: 'templates/travel/new-destiny.html',
                    controller: 'DestinyCtrl as dCtrl'
                }
            }
        })
        .state('app.tabs.edit-destiny', {
            url: '/edit-destiny/:trip/:item/:date',
            views: {
                'travel-tab': {
                    templateUrl: 'templates/travel/new-destiny.html',
                    controller: 'DestinyCtrl as dCtrl'
                }
            }
        })
        .state('app.tabs.preferred', {
            url: '/destiny-preferred/:trip/:stateId/:budget',
            views: {
                'travel-tab': {
                    templateUrl: 'templates/travel/destiny-preferred.html',
                    controller: 'DestinyPreferredCtrl as ctrl'
                }
            }
        })
        .state('app.tabs.tracking', {
            url: '/tracking',
            views: {
                'tracking-tab': {
                    templateUrl: 'templates/tracking/tracking.html',
                    controller: 'TrackingCtrl as ctrl'
                }
            }
        })
        .state('app.tabs.edittracking', {
            url: '/edittracking/:id',
            views: {
                'tracking-tab': {
                    templateUrl: 'templates/tracking/edittracking.html',
                    controller: 'EditTrackingCtrl as ctrl'
                }
            }
        })
        .state('app.tabs.addtracklocation', {
            url: '/addtracklocation/:id',
            views: {
                'tracking-tab': {
                    templateUrl: 'templates/tracking/addtracklocation.html',
                    controller: 'AddTrackLocationCtrl as ctrl'
                }
            }
        })
        .state('app.tabs.addtracking', {
            url: '/addtracking',
            views: {
                'tracking-tab': {
                    templateUrl: 'templates/tracking/addtracking.html',
                    controller: 'AddTrackingCtrl as ctrl'
                }
            }
        })
        .state('app.tabs.municipality', {
            url: '/municipality',
            views: {
                'book-tab': {
                    templateUrl: 'templates/mustseeanddo/municipality.html',
                    controller: 'MunicipalityCtrl as ctrl'
                }
            }
        })
        .state('app.tabs.book', {
            url: '/book/:location',
            views: {
                'book-tab': {
                    templateUrl: 'templates/mustseeanddo/book.html',
                    controller: 'BookCtrl as ctrl'
                }
            }
        })
        .state('app.tabs.mustseeanddolist', {
            url: '/mustseeanddolist/:id/:location/:download',
            views: {
                'book-tab': {
                    templateUrl: 'templates/mustseeanddo/mustseeanddolist.html',
                    controller: 'MustSeeAndDoListCtrl as ctrl'
                }
            }
        })

        .state('app.tabs.image360', {
            url: '/image360/:locationid/:bookid/:msadid',
            views: {
                'book-tab': {
                    templateUrl: 'templates/mustseeanddo/modal360.html',
                    controller: 'Image360Ctrl as ctrl'
                }
            }
        })

        .state('app.tabs.mustseeanddodetail', {
            url: '/mustseeanddodetail/:id',
            views: {
                'book-tab': {
                    templateUrl: 'templates/mustseeanddo/mustseeanddodetail.html',
                    controller: 'MustSeeAndDoDetailCtrl as ctrl'
                }
            }
        })
        .state('app.tabs.mustseeanddodetail-travel', {
            url: '/mustseeanddodetailtravel/:id',
            views: {
                'travel-tab': {
                    templateUrl: 'templates/mustseeanddo/mustseeanddodetail.html',
                    controller: 'MustSeeAndDoDetailCtrl as ctrl'
                }
            }
        })
        .state('app.tabs.syncronization', {
            url: '/syncronization',
            views: {
                'dashboard-tab': {
                    templateUrl: 'templates/syncronization.html',
                    controller: 'SyncronizationCtrl as ctrl'
                }
            }
        })
        .state('app.tabs.share', {
            url: '/share',
            views: {
                'dashboard-tab': {
                    templateUrl: 'templates/dashboard/share.html',
                    controller: 'SharingCtrl as ctrl'
                }
            }
        })
        .state('app.tabs.profile', {
            url: '/profile',
            views: {
                'dashboard-tab': {
                    templateUrl: 'templates/user/detailprofile.html',
                    controller: 'DetailProfileCtrl as ctrl'
                }
            }
        });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('start', {page: ''});

    //$ionicConfigProvider.navBar.alignTitle('left');
    // ubicación de la barra de botones, en el fondo de la pantalla, para android
    $ionicConfigProvider.tabs.position('bottom');
});
