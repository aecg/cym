
'use strict';

(function() {
    angular.module('cym').factory('SessionFactory', ['$window', SessionFactory]);

    function SessionFactory($window) {
        return {
            set: function (key, value)
            {
                $window.localStorage[key] = value;
            },
            get: function (key, defaultValue)
            {
                return $window.localStorage[key] || defaultValue;
            },
            setObject: function (key, value)
            {
                $window.localStorage[key] = JSON.stringify(value);
            },
            getObject: function (key)
            {
                var retorno = {};
                try
                {
                    retorno = JSON.parse($window.localStorage[key] || '{}');
                }catch(err)
                {
                    console.log("ocurrio un error parseando el objeto " + key + " " + err);
                }
                return retorno;
            }
        }
    }

})();

