'use strict';

(function ()
{
    angular.module('cym')
        .factory('ConnectivityMonitor', ConnectivityMonitor);

    function ConnectivityMonitor($cordovaNetwork, $rootScope)
    {


        return {
            isOnline: function() {
                if(ionic.Platform.isWebView()) {
                    //console.log('ConnectivityMonitor::isOnline::isWebView');
                    //alert('ConnectivityMonitor::isOnline::isWebView');
                    var cordovaNetworkIsOnline = $cordovaNetwork.isOnline();
                    //console.log('ConnectivityMonitor::isOnline::cordovaNetworkIsOnline::'+cordovaNetworkIsOnline);
                    //alert('ConnectivityMonitor::isOnline::cordovaNetworkIsOnline::'+cordovaNetworkIsOnline);
                    return cordovaNetworkIsOnline;    
                } else {
                    //console.log('ConnectivityMonitor::isOnline::NOT isWebView');
                    //alert('ConnectivityMonitor::isOnline::NOT isWebView');
                    var navigatorOnLine = navigator.onLine;
                    //console.log('ConnectivityMonitor::isOnline::navigatorOnLine::'+navigatorOnLine);
                    //alert('ConnectivityMonitor::isOnline::navigatorOnLine::'+navigatorOnLine);
                    return navigatorOnLine;
                }
            },
            ifOffline: function() {

                if(ionic.Platform.isWebView()) {
                    //console.log('ConnectivityMonitor::ifOffline::isWebView');
                    //alert('ConnectivityMonitor::ifOffline::isWebView');
                    var cordovaNetworkIsOnline = $cordovaNetwork.isOnline();
                    //console.log('ConnectivityMonitor::ifOffline::cordovaNetworkIsOnline::'+cordovaNetworkIsOnline);
                    //alert('ConnectivityMonitor::ifOffline::cordovaNetworkIsOnline::'+cordovaNetworkIsOnline);
                    return !cordovaNetworkIsOnline;    
                } else {
                    //console.log('ConnectivityMonitor::ifOffline::NOT isWebView');
                    //alert('ConnectivityMonitor::ifOffline::NOT isWebView');
                    var navigatorOnLine = navigator.onLine;
                    //console.log('ConnectivityMonitor::ifOffline::navigatorOnLine::'+navigatorOnLine);
                    //alert('ConnectivityMonitor::ifOffline::navigatorOnLine::'+navigatorOnLine);
                    return !navigatorOnLine;
                }
            }
        }
/*
        return {
            isOnline: function ()
            {
                var response = $rootScope.isOnline;
                if (ionic.Platform.isWebView())
                {
                    response = $rootScope.isOnline;
                }

                //console.log("Is Online " + response );
                return response;

            },
            ifOffline: function ()
            {

                if (ionic.Platform.isWebView())
                {
                    return !$rootScope.isOnline;
                }
                else
                {
                    return !navigator.onLine;
                }

            }
        }
*/
    }

})();