'use strict';

(function () {
	angular.module('cym')
		.factory('GeoLocation', ['Markers', '$filter', 'ConnectivityMonitor', '$cordovaNetwork', '$rootScope', GeoLocation] );

	function GeoLocation(Markers, $filter, ConnectivityMonitor, $cordovaNetwork, $rootScope) {

		var apiKey = null;
		var map = null;
		var idMap = "map";
		var Latitude = undefined;
		var Longitude = undefined;
		var centerOnCurrentPosition = true;
		var centeredMarker = null;
		var noError = true;

		function initMap() {
			//console.log('initMap');
			navigator.geolocation.getCurrentPosition(onMapSuccess, onMapError, { enableHighAccuracy: true });
		}

		function enableMap() {
			//console.log('enableMap');
			$rootScope.view_map_no_internet = false;
			//console.log('enableMap::$rootScope.view_map_no_internet::'+$rootScope.view_map_no_internet);
		}

		function disableMap() {
			//console.log('disableMap');
			$rootScope.view_map_no_internet = true;
			//console.log('disableMap::$rootScope.view_map_no_internet::'+$rootScope.view_map_no_internet);
		}

		var onMapSuccess = function (position) {
			//console.log('onMapSuccess');
			Latitude = position.coords.latitude;
			Longitude = position.coords.longitude;
			//console.log('onMapSuccess::Latitude::'+Latitude+'::Longitude::'+Longitude);

			getMap(Latitude, Longitude);
		}

		function getMap(latitude, longitude) {
			var mapOptions = {
				center: new google.maps.LatLng(0, 0),
				zoom: 1,
				mapTypeId: google.maps.MapTypeId.HYBRID
			};

			map = new google.maps.Map(document.getElementById(idMap), mapOptions);
			map.setZoom(8);

			var latLong = new google.maps.LatLng(Latitude, Longitude);
			var marker = new google.maps.Marker({
				animation: google.maps.Animation.DROP,
				map: map,
				title: $filter('translate')('labelmessage-current-position'),
				position: latLong
			});

			var contenido = '<h3>' + $filter('translate')('labelmessage-current-position') + '</h3>';
			var infowindow = new google.maps.InfoWindow({
				content: contenido
			});
			(function(marker, contenido) {
				google.maps.event.addListener(marker, 'click', function() {
					infowindow.open(map, marker);
				});
			})(marker, contenido);

			// Create the DIV to hold the control and call the CenterControl()
			// constructor passing in this DIV.
			var centerControlDiv = document.createElement('div');
			var centerControl = new CenterControl(centerControlDiv);

			centerControlDiv.index = 1;
			map.controls[google.maps.ControlPosition.TOP_RIGHT].push(centerControlDiv);

			google.maps.event.addListenerOnce(map, 'idle', function () {
				//console.log('loadMap::map LOADED');
				loadMarkers();

				if (centerOnCurrentPosition) {
					centeredMarker = marker;
					map.setCenter(centeredMarker.getPosition());
				}

			});
		}

		function CenterControl(controlDiv) {

			// Set CSS for the control border.
			var controlUI = document.createElement('div');
			controlUI.style.backgroundColor = '#fff';
			controlUI.style.border = '1px solid #fff';
			controlUI.style.borderRadius = '3px';
			controlUI.style.boxShadow = 'rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px';
			controlUI.style.cursor = 'pointer';
			controlUI.style.margin = '12px';
			controlUI.style.textAlign = 'center';
			controlUI.title = $filter('translate')('labelmessage-position');
			controlDiv.appendChild(controlUI);

			// Set CSS for the control interior.
			var controlText = document.createElement('div');
			controlText.style.color = 'rgb(0,0,0)';
			controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
			controlText.style.fontSize = '12px';
			controlText.style.lineHeight = '20px';
			controlText.style.padding = '6px';
			controlText.innerHTML = $filter('translate')('labelmessage-position');
			controlText.style.fontWeight = '500';
			
			controlUI.appendChild(controlText);

			// Setup the click event listeners: simply set the map to Chicago.
			controlUI.addEventListener('click', function() {

				if (centeredMarker != null) {
					centeredMarker.setMap(null);
				}

				var latLong = new google.maps.LatLng(Latitude, Longitude);
				centeredMarker = new google.maps.Marker({
					animation: google.maps.Animation.DROP,
					map: map,
					title: $filter('translate')('labelmessage-current-position'),
					position: latLong
				});
				var contenido = '<h3>' + $filter('translate')('labelmessage-current-position') + '</h3>';
				var infowindow = new google.maps.InfoWindow({
					content: contenido
				});
				(function(centeredMarker, contenido) {
					google.maps.event.addListener(centeredMarker, 'click', function() {
						infowindow.open(map, centeredMarker);
					});
				})(centeredMarker, contenido);


				map.setCenter(centeredMarker.getPosition());
			});
		}

		function onMapError(error) {
			//console.log('onMapError::code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
			//alert('onMapError::code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
			disableMap();
		}

		var onMapWatchSuccess = function (position) {
			var updatedLatitude = position.coords.latitude;
			var updatedLongitude = position.coords.longitude;

			if (updatedLatitude != Latitude && updatedLongitude != Longitude) {

				Latitude = updatedLatitude;
				Longitude = updatedLongitude;

				getMap(updatedLatitude, updatedLongitude);
			}
		}

		function watchMapPosition() {
			return navigator.geolocation.watchPosition(onMapWatchSuccess, onMapError, { enableHighAccuracy: true });
		}

		function addConnectivityListeners() {
			//console.log("addConnectivityListeners::init");
			//alert("addConnectivityListeners::init");
			if (ionic.Platform.isWebView()) {
				// Check if the map is already loaded when the user comes online, 
				//if not, load it
				//console.log("addConnectivityListeners::isOnline::isWebView\nchecking if map is already loaded");
				//alert("addConnectivityListeners::isOnline::isWebView\nchecking if map is already loaded");

				$rootScope.$on('$cordovaNetwork:online', function(event, networkState) {
					//console.log("addConnectivityListeners:: $rootScope.$on('$cordovaNetwork:online')");
					//alert("addConnectivityListeners:: $rootScope.$on('$cordovaNetwork:online')");
					checkLoaded();
				});

				// Disable the map when the user goes offline
				$rootScope.$on('$cordovaNetwork:offline', function(event, networkState) {
					//console.log("addConnectivityListeners:: $rootScope.$on('$cordovaNetwork:offline')");
					//alert("addConnectivityListeners:: $rootScope.$on('$cordovaNetwork:offline')");
					disableMap();
				});

			} else {
				//Same as above but for when we are not running on a device
				//console.log("addConnectivityListeners::checking if map is already loaded (not running on device)");
				window.addEventListener("online", function(e) {
					//console.log("addConnectivityListeners::window.addEventListener(\"online\")");
					//alert("addConnectivityListeners::window.addEventListener(\"online\")");
					checkLoaded();
				}, false);

				window.addEventListener("offline", function(e) {
					//console.log("addConnectivityListeners::window.addEventListener(\"offline\")");
					//alert("addConnectivityListeners::window.addEventListener(\"offline\")");
					disableMap();
				}, false);
			}
		}

		function checkLoaded(){
			//console.log("checkLoaded::init");
			//alert("checkLoaded::init");
			//if(typeof google == "undefined" || typeof google.maps == "undefined"){
				//console.log("checkLoaded::calling loadGoogleMaps");
				//alert("checkLoaded::calling loadGoogleMaps");
				enableMap();
				loadGoogleMaps();
			//} else {
			//	console.log("checkLoaded::calling enableMap");
			//	alert("checkLoaded::calling enableMap");
			//	enableMap();
			//}
		}

		function loadGoogleMaps() {
			window.mapInit = function () {
				//console.log('loadGoogleMaps::window.initMapLocation');
				initMap();
			};

			//Creamos un script para insertar en la página
			var script = document.createElement("script");
			script.type = "text/javascript";
			script.id = "map_google_apis";
			script.src = 'https://maps.googleapis.com/maps/api/js?callback=mapInit&key=' + apiKey;
			document.body.appendChild(script);
		}

		function addMarkerWithTimeout(position, msad, bounds, infowindow, centerMap, timeout) {
			//window.setTimeout(function() {

				//console.log('addMarkerWithTimeout::position::'+JSON.stringify(position)+'::msad::'+JSON.stringify(msad)+'::centerMap::'+centerMap);
				var pinColor = "img/maps/blue.png";
				if (!msad.pin) {
					if(msad.ar && msad.qr) {
						pinColor = "img/maps/purple.png";
					}
					else {
						if(msad.ar) {
							pinColor = "img/maps/yellow.png";
						}
						else if(msad.qr) {
							pinColor = "img/maps/green.png";
						}
					}
				}
				else {
					pinColor = msad.pin;
				}

				var contenido = '<h3>' + msad.name + '</h3>';
				if(msad.date) {
					contenido += '<h4>' + $filter('date')(msad.date, 'MM/dd/yyyy') + '</h4>';
				}
				if(msad.ar) {
					contenido += '<img src="img/maps/ar.png" width="64px">';
				}
				if(msad.qr) {
					contenido += '<img src="img/maps/qr.png" width="64px">';
				}

				var pinImage = new google.maps.MarkerImage(pinColor, new google.maps.Size(30, 30));

				var labelMark = "";
				if ((msad.label != null) && msad.label != "")
				{
					labelMark = msad.label;
				}
				var marker = new google.maps.Marker({
					position: position,
					map: map,
					title: msad.name,
					icon: pinImage,
					label: labelMark,
					animation: google.maps.Animation.DROP
				});
				bounds.extend(marker.getPosition());

				(function(marker, contenido) {
					google.maps.event.addListener(marker, 'click', function() {
						infowindow.setContent(contenido);
						infowindow.open(map, marker);
					});
				})(marker, contenido);

				//console.log('msad.name::'+msad.name+'::centerMap::'+centerMap);
				if (centerMap) {
					centeredMarker = marker;
					map.setCenter(position);
				}

			//}, timeout);
		}

		function loadMarkers() {
			//console.log("loadMarkers::init");

			var bounds = map.getBounds();
			var allMarkers = Markers.getAllMarkers();
			//console.log("loadMarkers::allMarkers::"+JSON.stringify(allMarkers));
			//console.log("loadMarkers::allMarkers.length::"+allMarkers.length);
			//console.log('loadMarkers::allMarkers[0].directions::'+allMarkers[0].directions);
			if (allMarkers[0].directions && noError)
			{
				var markerArray = [];

				// Instantiate a directions service.
				var directionsService = new google.maps.DirectionsService;

				// Create a renderer for directions and bind it to the map.
				var directionsDisplay = new google.maps.DirectionsRenderer({map: map});

				// Instantiate an info window to hold step text.
				var stepDisplay = new google.maps.InfoWindow;

				// Display the route between the initial start and end selections.

				var point = allMarkers[0].markers[0];
				//console.log('loadMarkers::point::'+JSON.stringify(point));
				var latLng = new google.maps.LatLng(point.lat, point.lng);
				//console.log('loadMarkers::latLng::'+JSON.stringify(latLng));

				calculateAndDisplayRoute(latLng, directionsDisplay, directionsService, markerArray, stepDisplay, map);
			}
			else
			{
				for (var i = 0; i < allMarkers.length; i++) {
					var markersObj = allMarkers[i];
					//console.log("loadMarkers::markersObj::"+JSON.stringify(markersObj));

					var infowindow = new google.maps.InfoWindow({
						content: ''
					});
					var coordinates = [];

					var markers = markersObj.markers;
					//console.log("loadMarkers::markers.length::"+markers.length);
					//console.log("loadMarkers::markers::"+JSON.stringify(markers));

					for (var j = 0; j < markers.length; j++) {
						//console.log('loadMarkers::markers['+j+']::'+JSON.stringify(markers[j]));
						var point = markers[j];
						//console.log('loadMarkers::point::'+j+'::'+JSON.stringify(point));
						var latLng = new google.maps.LatLng(point.lat, point.lng);
						coordinates.push(latLng);
						addMarkerWithTimeout(latLng, point, bounds, infowindow, (!centerOnCurrentPosition && (j == (markers.length - 1))), j * 25);
					}

					if(markersObj.markLine) {
						var flightPath = new google.maps.Polyline({
							path: coordinates,
							geodesic: true,
							strokeColor: '#000000',
							strokeOpacity: 1.0,
							strokeWeight: 2
						});
						flightPath.setMap(map);
					}
				}
			}

			enableMap();
		}

		function calculateAndDisplayRoute(destinationPoint, directionsDisplay, directionsService, markerArray, stepDisplay, map) {
			// First, remove any existing markers from the map.
			for (var i = 0; i < markerArray.length; i++) {
				markerArray[i].setMap(null);
			}

			//Latitude = 19.334414;
			//Longitude = -98.1703687;

			var originPoint = new google.maps.LatLng(Latitude, Longitude);
			//console.log('calculateAndDisplayRoute::originPoint::'+JSON.stringify(originPoint));

			// Retrieve the start and end locations and create a DirectionsRequest using
			// WALKING directions.
			directionsService.route({
				origin: originPoint,
				destination: destinationPoint,
				travelMode: google.maps.TravelMode.WALKING
			}, function(response, status) {
				// Route the directions and pass the response to a function to create
				// markers for each step.
				if (status === google.maps.DirectionsStatus.OK) {
					directionsDisplay.setDirections(response);
					showSteps(response, markerArray, stepDisplay, map);
				} else {
					//window.alert('Directions request failed due to ' + status);
					//console.log('calculateAndDisplayRoute::Directions request failed due to::'+status);
					noError = false;
					loadMarkers();
				}
			});
		}

		function showSteps(directionResult, markerArray, stepDisplay, map) {
			// For each step, place a marker, and add the text to the marker's infowindow.
			// Also attach the marker to an array so we can keep track of it and remove it
			// when calculating new routes.
			var myRoute = directionResult.routes[0].legs[0];
			for (var i = 0; i < myRoute.steps.length; i++) {
				var marker = markerArray[i] = markerArray[i] || new google.maps.Marker;
				marker.setMap(map);
				marker.setPosition(myRoute.steps[i].start_location);
				attachInstructionText(stepDisplay, marker, myRoute.steps[i].instructions, map);
			}
		}

		function attachInstructionText(stepDisplay, marker, text, map) {
			google.maps.event.addListener(marker, 'click', function() {
				// Open an info window when the marker is clicked on, containing the text
				// of the step.
				stepDisplay.setContent(text);
				stepDisplay.open(map, marker);
			});
		}

		return {
		   init: function (idControl, key, currentPosition) {
				//console.log('init::idControl::'+idControl+'::key::'+key+'::currentPosition::'+currentPosition);

				if (typeof idControl != "undefined") {
					idMap = idControl;
				}
				if (typeof key != "undefined") {
					apiKey = key;
				}
				if (typeof currentPosition != "undefined") {
					centerOnCurrentPosition = currentPosition;
				}

				if (typeof google == "undefined" || typeof google.maps == "undefined") {
					//console.warn("init::Google Maps SDK needs to be loaded");
					//alert("init::Google Maps SDK needs to be loaded");
					if (ConnectivityMonitor.isOnline()) {
						//console.log("init::is online, calling loadGoogleMaps");
						loadGoogleMaps();
					} else {
						disableMap();
					}
				} else {
					//console.log("init::Google_Maps_SDK loaded");
					if (ConnectivityMonitor.isOnline()) {
						initMap();
						//enableMap();
					} else {
						disableMap();
					}
				}
				addConnectivityListeners();
			}
		} 
	}
})();