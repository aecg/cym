'use strict';

(function() {
    angular.module('cym')
        .factory('Markers', Markers);

    function Markers ($http)
    {
        var allMarkers = [];

        var processMustSeeAndDo = function (mustSeeAndDo) {
            //console.log('processMustSeeAndDo::mustSeeAndDo::'+JSON.stringify(mustSeeAndDo));
            var marker = {};
            marker.name = mustSeeAndDo.name;
            try {
                var pos = mustSeeAndDo.latlong.split(",");
                marker.lat = pos[0].trim();
                marker.lng = pos[1].trim();
            }
            catch(e) {
                marker.lat = 0;
                marker.lng = 0;
            }
            marker.qr = mustSeeAndDo.qr;
            marker.ar = mustSeeAndDo.ar;
            return marker;
        };

        return {
            getAllMarkers: function ()
            {
                return allMarkers;
            },
            clearAllMarkers: function ()
            {
                allMarkers = [];
            },

            setAllMarkersMap: function (params) {
                var promises = [];

                var defSetAllMarkersMap = new $.Deferred();
                promises.push(defSetAllMarkersMap);
                //console.log('setAllMarkersMap::params::'+JSON.stringify(params));

                var mustSeeAndDos = params.mustSeeAndDos;
                if (mustSeeAndDos) {
                    var markersMustSeeAndDo = [];
                    for(var i = 0; i < mustSeeAndDos.length; i++) {
                        var marker = processMustSeeAndDo(mustSeeAndDos[i]);
                        markersMustSeeAndDo.push(marker);
                    }

                    var markersMustSeeAndDoObj = {};
                    markersMustSeeAndDoObj.markers = markersMustSeeAndDo;
                    markersMustSeeAndDoObj.markLine = false;
                    //console.log('setAllMarkersMap::markersMustSeeAndDoObj::'+JSON.stringify(markersMustSeeAndDoObj));

                    allMarkers = [];
                    allMarkers.push(markersMustSeeAndDoObj);
                    //console.log('setAllMarkersMap::allMarkers::'+JSON.stringify(allMarkers));                    
                }

                var trackings = params.trackingGPS;
                if (trackings) {
                    //console.log('setAllMarkersMap::trackings::'+JSON.stringify(trackings));
                    for(var i = 0; i < trackings.length; i++) {

                        var markersTrackingGPS = [];
                        var tracking = trackings[i];
                        //console.log('setAllMarkersMap::tracking::'+JSON.stringify(tracking));
                        var trackingName = tracking.name;
                        var cuepoints = tracking.cuepoints;
        
                        for(var j = 0; j < cuepoints.length; j++) {
                            var cuepoint = cuepoints[j];

                            var marker = {};
                            marker.name = trackingName + ' - ' + cuepoint.description;
                            marker.pin = cuepoint.name;
                            try {
                                var pos = cuepoint.geographic_position.split(",");
                                marker.lat = pos[0].trim();
                                marker.lng = pos[1].trim();
                            }
                            catch(e) {
                                marker.lat = 0;
                                marker.lng = 0;
                            }
                            marker.qr = false;
                            marker.ar = false;

                            markersTrackingGPS.push(marker);
                        }
                        var markersTrackingGPSObj = {};
                        markersTrackingGPSObj.markers = markersTrackingGPS;
                        markersTrackingGPSObj.markLine = true;
                        //console.log('setAllMarkersMap::markersTrackingGPSObj::'+JSON.stringify(markersTrackingGPSObj));

                        allMarkers.push(markersTrackingGPSObj);
                        //console.log('setAllMarkersMap::allMarkers::'+JSON.stringify(allMarkers));
                    }
                }

                var documents = params.documents;
                if (documents) {
                    var markersDocuments = [];
                    //console.log('setAllMarkersMap::documents::'+JSON.stringify(documents));
                    for(var i = 0; i < documents.length; i++) {

                        var doc = documents[i];
                        //console.log('setAllMarkersMap::doc::'+JSON.stringify(doc));

                        var marker = {};
                        marker.name = doc.name;
                        marker.pin = "img/maps/icon8.png";
                        try {
                            var pos = doc.geographic_position.split(",");
                            marker.lat = pos[0].trim();
                            marker.lng = pos[1].trim();
                        }
                        catch(e) {
                            marker.lat = 0;
                            marker.lng = 0;
                        }
                        marker.qr = false;
                        marker.ar = false;

                        markersDocuments.push(marker);
                    }
                    var markersDocumentsObj = {};
                    markersDocumentsObj.markers = markersDocuments;
                    markersDocumentsObj.markLine = false;
                    //console.log('setAllMarkersMap::markersDocumentsObj::'+JSON.stringify(markersDocumentsObj));

                    allMarkers.push(markersDocumentsObj);
                    //console.log('setAllMarkersMap::allMarkers::'+JSON.stringify(allMarkers));
                }

                defSetAllMarkersMap.resolve();

                return $.when.apply(undefined, promises).promise();
            },

            setMarkersMustSeeAndDo: function (params) {
                var promises = [];

                //console.log('setMarkersMustSeeAndDo::params::'+JSON.stringify(params));
                var defSetMarkersMustSeeAndDo = new $.Deferred();
                promises.push(defSetMarkersMustSeeAndDo);

                var markersMustSeeAndDo = [];

                var marker = processMustSeeAndDo(params);
                markersMustSeeAndDo.push(marker);

                var markersObj = {};
                markersObj.markers = markersMustSeeAndDo;
                markersObj.markLine = false;
                markersObj.directions = true;
                //console.log('setMarkersMustSeeAndDo::markersObj::'+JSON.stringify(markersObj));

                allMarkers = [];
                allMarkers.push(markersObj);
                //console.log('setMarkersMustSeeAndDo::allMarkers::'+JSON.stringify(allMarkers));

                defSetMarkersMustSeeAndDo.resolve();

                return $.when.apply(undefined, promises).promise();
            },

            setMarkersDocument: function (params) {
                var promises = [];

                var defSetMarkersDocument = new $.Deferred();
                promises.push(defSetMarkersDocument);
                var markersDocument = [];

                var marker = {};
                marker.name = params.name;
                try {
                    var pos = params.geographic_position.split(",");
                    marker.lat = pos[0].trim();
                    marker.lng = pos[1].trim();
                }
                catch(e) {
                    marker.lat = 0;
                    marker.lng = 0;
                }
                marker.qr = false;
                marker.ar = false;
                markersDocument.push(marker);

                var markersObj = {};
                markersObj.markers = markersDocument;
                markersObj.markLine = false;
                //console.log('setMarkersDocument::markersObj::'+JSON.stringify(markersObj));

                allMarkers = [];
                allMarkers.push(markersObj);
                //console.log('setMarkersDocument::allMarkers::'+JSON.stringify(allMarkers));

                defSetMarkersDocument.resolve();

                return $.when.apply(undefined, promises).promise();
            },

            setMarkersTrackingGPS: function (params) {
                //console.log('setMarkersTrackingGPS::init');
                var promises = [];

                var defSetMarkers = new $.Deferred();
                promises.push(defSetMarkers);
                var markersTrackingGPS = [];

                for(var i = 0; i < params.length; i++)
                {
                    var marker = {};
                    marker.id = params[i].cuepointid;
                    marker.name = params[i].cuepointdesc;
                    marker.pin = params[i].cuepointname;
                    try {
                        var pos = params[i].position.split(",");
                        marker.lat = pos[0].trim();
                        marker.lng = pos[1].trim();
                    }
                    catch(e) {
                        marker.lat = 0;
                        marker.lng = 0;
                    }
                    marker.qr = false;
                    marker.ar = false;
                    //console.log('setMarkersTrackingGPS::marker::'+JSON.stringify(marker));
                    markersTrackingGPS.push(marker);
                    //console.log('setMarkersTrackingGPS::markersTrackingGPS::'+JSON.stringify(markersTrackingGPS));
                }

                var markersObj = {};
                markersObj.markers = markersTrackingGPS;
                markersObj.markLine = true;
                //console.log('setMarkersTrackingGPS::markersObj::'+JSON.stringify(markersObj));

                allMarkers = [];
                allMarkers.push(markersObj);
                //console.log('setMarkersTrackingGPS::allMarkers::'+JSON.stringify(allMarkers));

                defSetMarkers.resolve();

                return $.when.apply(undefined, promises).promise();
            },

            setMarkersTravel: function (params) {
                var promises = [];

                var defSetMarkersTravel = new $.Deferred();
                promises.push(defSetMarkersTravel);
                var markersTravel = [];
                //console.log('setMarkersTravel::params.length::'+params.length);
                if (params) {
                    for(var i = 0; i < params.length; i++) {
                        var msad = params[i];

                        var marker = {};
                        marker.name = msad.title;
                        try {
                            var pos = msad.geographic_position.split(",");
                            marker.lat = pos[0].trim();
                            marker.lng = pos[1].trim();
                        }
                        catch(e) {
                            marker.lat = 0;
                            marker.lng = 0;
                        }
                        marker.qr = msad.qr;
                        marker.ar = msad.ar;
                        marker.label = "" + (i + 1);
                        marker.date = msad.date;

                        markersTravel.push(marker);
                    }

                    /*
                    for(var i = 0; i < params.length; i++) {
                        var destiny = params[i];
                        //console.log('setMarkersTravel::destiny::'+JSON.stringify(destiny));
                        //console.log('setMarkersTravel::destiny.locations.length::'+destiny.locations.length);
                        for(var j = 0; j < destiny.locations.length; j++) {
                            var location = destiny.locations[j];
                            //console.log('setMarkersTravel::location::'+JSON.stringify(location));
                            
                            var marker = {};
                            marker.name = location.name;
                            try {
                                var pos = location.geographic_position.split(",");
                                marker.lat = pos[0].trim();
                                marker.lng = pos[1].trim();
                            }
                            catch(e) {
                                marker.lat = 0;
                                marker.lng = 0;
                            }
                            marker.qr = false;
                            marker.ar = false;

                            //console.log('setMarkersTravel::marker::'+JSON.stringify(marker));
                            markersTravel.push(marker);

                            //console.log('setMarkersTravel::location.mustSeeAndDo.length::'+location.mustSeeAndDo.length);
                            //for(var k = 0; k < location.mustSeeAndDo.length; k++) {
                            //    var mustSeeAndDo = location.mustSeeAndDo[k];
                            //    if (mustSeeAndDo.geographic_position != "") {
                            //    }
                            //}
                        }
                    }
                    */
                    var markersObj = {};
                    markersObj.markers = markersTravel;
                    markersObj.markLine = true;
                    //console.log('setMarkersTravel::markersObj::'+JSON.stringify(markersObj));

                    allMarkers = [];
                    allMarkers.push(markersObj);
                }
                defSetMarkersTravel.resolve();

                return $.when.apply(undefined, promises).promise();
            },

            setMarkersMustSeeAndDoTravel: function (params)
            {
                var promises = [];

                var defSetMarkersMustSeeAndDoTravel = new $.Deferred();
                promises.push(defSetMarkersMustSeeAndDoTravel);
                var markersMustSeeAndDoTravel = [];
                for(var i = 0; i < params.length; i++)
                {
                    //console.log("setMarkersMustSeeAndDoTravel::params.item(i)::" + JSON.stringify(params.item(i)));
                    var marker = {};
                    var item = params[i];
                    marker.name = item.title;
                    try {
                        var pos = item.geographic_position.split(",");
                        marker.lat = pos[0].trim();
                        marker.lng = pos[1].trim();
                    }
                    catch(e) {
                        marker.lat = 0;
                        marker.lng = 0;
                    }
                    marker.qr = item.qr;
                    marker.ar = item.ar;
                    markersMustSeeAndDoTravel.push(marker);

                }

                var markersObj = {};
                markersObj.markers = markersMustSeeAndDoTravel;
                markersObj.markLine = false;
                //console.log('setMarkersMustSeeAndDoTravel::markersObj::'+JSON.stringify(markersObj));

                allMarkers = [];
                allMarkers.push(markersObj);

                defSetMarkersMustSeeAndDoTravel.resolve();

                return $.when.apply(undefined, promises).promise();
            }
        }
    }

})();