'use strict';

(function() {
    angular.module('cym').factory('MessagesFactory', [MessagesFactory]);

    function MessagesFactory() {
        return {
            createMessages: function() {
                return {
                    success: [],
                    error: []
                };            	
            }
        };
    }

})();