/**
 * Created by poncbi on 15/03/2016.
 */
'use strict';

angular.module('cym').service('SyncronizationProxy', ['$http', 'BASE_URL', SyncronizationProxy]);

function SyncronizationProxy($http, BASE_URL)
{
    /**
     * Metodo que obtiene los libros del sistema.
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.getBooks = function()
    {
        return $http
                .get(BASE_URL + '/listbooks' );
    };

    /**
     * Metodo que obtiene la información de los estados de un libro.
     * @param book_id contiene el identificador del libro a descargar
     */
    this.getBookStates = function(book_id) {
        return $http
                    .get(BASE_URL + '/bookstates/'+ book_id + '/');
    };

    /**
     * Metodo que obtiene los destinos recomendados de un estado.
     * @param book_id contiene el identificador del estdo a descargar
     */
    this.getStatesMustSeeAndDoRecommended = function(state_id) {
        return $http
                    .get(BASE_URL + '/msdrecommended/'+ state_id + '/');
    };

    /**
     * Metodo que obtiene la información de los paises de un libro.
     * @param book_id contiene el identificador del libro a descargar
     */
    this.getBookCountries = function(book_id) {
        return $http
                    .get(BASE_URL + '/bookcountries/'+ book_id + '/');
    };

    /**
     * Metodo que obtiene la información de que ver y que hacer de un libro.
     * @param book_id contiene el identificador del libro a descargar
     */
    this.getBookMustSeeAndDo = function(book_id) {
        return $http
                    .get(BASE_URL + '/mustseeanddo/'+ book_id + '/' );
    };

    /**
     * Metodo que obtiene la información del detalle de que ver y que hacer de un libro.
     * @param book_id contiene el identificador del libro a descargar
     */
    this.getBookMustSeeAndDoDetail = function(book_id)
    {
        return $http
                    .get(BASE_URL + '/msddetail/book/'+ book_id + '/' );
    };

    /**
     * Metodo que obtiene la información del código qr que ver y que hacer de un libro.
     * @param book_id contiene el identificador del libro a descargar
     */
    this.getBookMustSeeAndDoQR = function(msamd_id)
    {
        return $http
                    .get(BASE_URL + '/msdqr/'+ msamd_id + '/' );
    };

    /**
     * Metodo que obtiene la información de la imagen ar que ver y que hacer de un libro.
     * @param book_id contiene el identificador del libro a descargar
     */
    this.getBookMustSeeAndDoAR = function(msamd_id)
    {
        return $http
                    .get(BASE_URL + '/msdar/'+ msamd_id + '/' );
    };

    /**
     * Metodo que especifica que un usuario va a descargar un libro, o ya lo ha descargado
     * anteriormente.
     * @param book_id contiene el identificador del libro a descargar
     * @param user_name contiene el nombre de usuario del usuario que descarga el libro
     */
    this.createUserBook = function(book_id, user_name)
    {
        var data = {};
        data.book_id = book_id;
        data.username = user_name;
        return $http
                    .post(BASE_URL + '/createuserbook', data);
    };

    /**
     * Metodo que obtiene la información del detalle de que ver y que hacer de un libro.
     * @param user_name contiene el nombre de usuario del usuario que descarga el libro
     */
    this.getUserBooks = function(user_name)
    {
        return $http
                    .get(BASE_URL + '/books/' + user_name + '/' );
    };

    /**
     * Metodo que obtiene la información de los seguimientos GPS del usuario.
     * @param user_name contiene el nombre de usuario.
     */
    this.getUserTrackings = function(user_name)
    {
        return $http
                .get(BASE_URL + '/trackings/' + user_name + '/' );
    };

    /**
     * Metodo que obtiene la información de los viajes del usuario.
     * @param user_name contiene el nombre de usuario.
     */
    this.getUserTravels = function(user_name)
    {
        return $http
                .get(BASE_URL + '/travels/' + user_name + '/' );
    };

    /**
     * Metodo que actualiza la información de los seguimiento del usuario en el servidor.
     * @param trackings contiene el nombre de usuario.
     */
    this.updateTrackingUser = function(trackings)
    {
        return $http
                .put(BASE_URL + '/trackings', trackings);
    };

    /**
     * Metodo que actualiza la información de los viajes del usuario en el servidor.
     * @param travels contiene el nombre de usuario.
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.updateTravelsUser = function(travels)
    {
        return $http
                .put(BASE_URL + '/travels', travels);
    };

    /**
     * Metodo que actualiza la información del usuario en el servidor.
     * @param user contiene los datos del  usuario.
     */
    this.updateUser = function(user)
    {
        return $http
                .put(BASE_URL + '/user', user);
    };
}

