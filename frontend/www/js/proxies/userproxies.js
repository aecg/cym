'use strict';

angular.module('cym').service('UserProxy', ['$http', 'BASE_URL', UserProxy]);

function UserProxy($http, BASE_URL)
{
    /**
     * Metodo que crea al usuario en el servidor
     * @param data objeto que contiene los datos del usuario a crear.
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.createUser = function(data)
    {
        return $http
            .post(BASE_URL + '/users', data );
    };


    /**
     * Metodo que realiza la invocacion del servicio que realiza para validar el registro  del usuario
     * @param username nombre del usuario a validar
     */
    this.getUserValidate = function(username)
    {
        return $http
            .get(BASE_URL + '/users/' + username + '/' );
    };

    /**
     * Metodo que envia el password al usuario
     * @param user objeto que contiene los datos del usuario
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.recoveryPassword = function(data) {
        return $http
            .post(BASE_URL + '/user/retrieve', data);
    };

    /**
     * Metodo que permite invitar a amigod a descargar y utilizar la app
     * @param data objeto que contiene los datos del usuario que invita y de los amigos a invitar.
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.inviteFriends = function(data)
    {
        return $http
            .post(BASE_URL + '/invite', data );
    };
}