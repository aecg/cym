'use strict';

module.exports = function (grunt)
{

    require('load-grunt-tasks')(grunt);
    require('time-grunt')(grunt);

    grunt.initConfig({

        myApp: {
            app: require('./bower.json').appPath || 'app',
            dist: 'dist'
        },

        watch: {

            js: {
                files: ['<%= myApp.app %>/resources/*.js','<%= myApp.app %>/views/**/*.js','<%= myApp.app %>/services/*.js'],
                tasks: ['newer:jshint:all'],
                options: {
                    livereload: true
                }
            },
            html: {
                files: ['<%= myApp.app %>/views/**/*.html'],
                options: {
                    livereload: true
                }
            },
            jsTest: {
                files: ['test/unit/{,*/}*.js'],
                tasks: ['newer:jshint:test', 'karma']
            },
            styles: {
                files: ['<%= myApp.app %>/css/{,*/}*.css'],
                tasks: ['newer:copy:styles', 'autoprefixer']
            },
            gruntfile: {
                files: ['Gruntfile.js']
            },
            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },
                files: [
                    '<%= myApp.app %>/{,*/}*.html',
                    '.tmp/css/{,*/}*.css',
                    '<%= myApp.app %>/img/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
                    '<%= myApp.app %>/views/**/*.html'
                ]
            }
        },

        connect: {
            options: {
                port: 9000,
                hostname: 'localhost',
                livereload: 35729
            },
            livereload: {
                options: {
                    open: true,
                    base: [
                        '.tmp',
                        '<%= myApp.app %>'
                    ]
                }
            },
            test: {
                options: {
                    port: 9001,
                    base: [
                        '.tmp',
                        'test',
                        '<%= myApp.app %>'
                    ]
                }
            },
            dist: {
                options: {
                    base: '<%= myApp.dist %>'
                }
            }
        },

        'bower-install': {
            app: {
                html: '<%= myApp.app %>/index.html',
                ignorePath: '<%= myApp.app %>/'
            }
        },

        karma: {
            unit: {
                configFile: './test/karma.conf.js',
                singleRun: true
            }
        }
    });

    grunt.registerTask('serve', function (target)
    {
        if (target === 'dist')
        {
            return grunt.task.run(['build', 'connect:dist:keepalive']);
        }

        grunt.task.run([
            'bower-install',
            'connect:livereload',
            'watch'
        ]);
    });

    grunt.registerTask('server', function ()
    {
        grunt.log.warn(
            'The `server` task has been deprecated. Use `grunt serve` to start a server.'
        );
        grunt.task.run(['serve']);
    });

    grunt.registerTask('build', [
        'bower-install',
        'rev'
    ]);

    grunt.registerTask('default', [
        'test',
        'build'
    ]);
};
