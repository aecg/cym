'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('myApp', [
        'ngRoute',
        'ngAnimate',
        'ngCookies',
        'ngMessages',
        'ui.load',
        'ui.jq',
        'vcRecaptcha',
        'ui.utils',
        'pascalprecht.translate',
        'myApp.homeUser',
        'myApp.home',
        'myApp.login',
        'myApp.listBooks',
        'myApp.listMustSeeAndDo',
        'myApp.addBook',
        'myApp.updateBook',
        'myApp.addMustSeeAnddo',
        'myApp.updateMustSeeAnddo',
        'monospaced.qrcode',
        'myApp.forgotPassword',
        'myApp.changePassword'

    ]).
        config(['$routeProvider', function ($routeProvider)
        {
            $routeProvider.otherwise({redirectTo: '/login'});
        }]).
    /**
     * Metodo donde se declaran las variables globales de la aplicación
     */
        run(function ($rootScope)
        {
            $rootScope.showHeader = true;
            $rootScope.isLogged = false;
            $rootScope.claseContent = "app-content-full";
            $rootScope.userName = "";
        }).
        config(['$translateProvider', function ($translateProvider)
        {
            $translateProvider.useStaticFilesLoader({
                prefix: 'resources/locale-',
                suffix: '.json'
            });
            $translateProvider.preferredLanguage('es_VE');
            $translateProvider.useSanitizeValueStrategy('escaped');
        }])
        .controller('IndexCtrl', ['$scope', '$translate', '$location', 'AuthProxy', function ($scope, $translate, $location, AuthProxy)
        {
            $scope.changeLanguaje = function (languaje)
            {
                $translate.use(languaje);
            };

            $scope.logout = function ()
            {
                AuthProxy.logout(function ()
                {
                    $location.path('/login');
                });
            };
        }])
        .controller('TemplateCtrl', ['$scope', '$translate', function ($scope, $translate)
        {

            $scope.headerTemplate = {name: 'header.html', url: 'header/header.html'};
            $scope.menuTemplate = {name: 'template1.html', url: 'template1.html'};

        }])
        .controller('FooterCtrl', ['$scope', '$translate', function ($scope, $translate)
        {


        }])
        // development
        //.constant('BASE_URL', 'http://localhost:8080/cym')
        //.constant('IMAGE_BOOKS_URL', 'http://images.martin.cym.softclear.net/books/')
        //.constant('IMAGE_MSADS_URL', 'http://images.martin.cym.softclear.net/msads/')

        // AWS training
        //.constant("BASE_URL","http://training.cym.softclear.net/backend")
        //.constant('IMAGE_BOOKS_URL', 'http://training.cym.softclear.net/books/')
        //.constant('IMAGE_MSADS_URL', 'http://training.cym.softclear.net/msads/')

        // AWS production
        .constant("BASE_URL","http://cym.softclear.net/backend")
        .constant('IMAGE_BOOKS_URL', 'http://cym.softclear.net/books/')
        .constant('IMAGE_MSADS_URL', 'http://cym.softclear.net/msads/')

        .constant("PREFIJO","CYM_")
        /**
         * jQuery plugin config use ui-jq directive , config the js and css files that required
         * key: function name of the jQuery plugin
         * value: array of the css js file located
         */
        .constant('JQ_CONFIG', {
            easyPieChart: ['components/jquery/jquery.easy-pie-chart/dist/jquery.easypiechart.fill.js'],
            sparkline: ['components/jquery/jquery.sparkline/dist/jquery.sparkline.retina.js'],
            plot: ['components/jquery/flot/jquery.flot.js',
                'components/jquery/flot/jquery.flot.pie.js',
                'components/jquery/flot/jquery.flot.resize.js',
                'components/jquery/flot.tooltip/js/jquery.flot.tooltip.min.js',
                'components/jquery/flot.orderbars/js/jquery.flot.orderBars.js',
                'components/jquery/flot-spline/js/jquery.flot.spline.min.js'],
            moment: ['components/jquery/moment/moment.js'],
            screenfull: ['components/jquery/screenfull/dist/screenfull.min.js'],
            slimScroll: ['components/jquery/slimscroll/jquery.slimscroll.min.js'],
            sortable: ['components/jquery/html5sortable/jquery.sortable.js'],
            nestable: ['components/jquery/nestable/jquery.nestable.js',
                'components/jquery/nestable/jquery.nestable.css'],
            filestyle: ['components/jquery/bootstrap-filestyle/src/bootstrap-filestyle.js'],
            slider: ['components/jquery/bootstrap-slider/bootstrap-slider.js',
                'components/jquery/bootstrap-slider/bootstrap-slider.css'],
            chosen: ['components/jquery/chosen/chosen.jquery.min.js',
                'components/jquery/chosen/bootstrap-chosen.css'],
            TouchSpin: ['components/jquery/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js',
                'components/jquery/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css'],
            wysiwyg: ['components/jquery/bootstrap-wysiwyg/bootstrap-wysiwyg.js',
                'components/jquery/bootstrap-wysiwyg/external/jquery.hotkeys.js'],
            dataTable: ['components/jquery/datatables/media/js/jquery.dataTables.min.js',
                'components/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.js',
                'components/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.css'],
            vectorMap: ['components/jquery/bower-jvectormap/jquery-jvectormap-1.2.2.min.js',
                'components/jquery/bower-jvectormap/jquery-jvectormap-world-mill-en.js',
                'components/jquery/bower-jvectormap/jquery-jvectormap-us-aea-en.js',
                'components/jquery/bower-jvectormap/jquery-jvectormap.css'],
            footable: ['components/jquery/footable/v3/js/footable.min.js',
                'components/jquery/footable/v3/css/footable.bootstrap.min.css'],
            fullcalendar: ['components/jquery/moment/moment.js',
                'components/jquery/fullcalendar/dist/fullcalendar.min.js',
                'components/jquery/fullcalendar/dist/fullcalendar.css',
                'components/jquery/fullcalendar/dist/fullcalendar.theme.css'],
            daterangepicker: ['components/jquery/moment/moment.js',
                'components/jquery/bootstrap-daterangepicker/daterangepicker.js',
                'components/jquery/bootstrap-daterangepicker/daterangepicker-bs3.css'],
            tagsinput: ['components/jquery/bootstrap-tagsinput/dist/bootstrap-tagsinput.js',
                'components/jquery/bootstrap-tagsinput/dist/bootstrap-tagsinput.css']

        }
    )
        .config(
        ['$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
            function ($controllerProvider, $compileProvider, $filterProvider, $provide)
            {

                // lazy controller, directive and service
                app.controller = $controllerProvider.register;
                app.directive = $compileProvider.directive;
                app.filter = $filterProvider.register;
                app.factory = $provide.factory;
                app.service = $provide.service;
                app.constant = $provide.constant;
                app.value = $provide.value;
            }
        ])
    ;
