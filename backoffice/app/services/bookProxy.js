angular.module('myApp').service('BookProxy', ['$http', 'BASE_URL', BookProxy]);

/**
 * Servicio que hace la vez de Proxy para los servicios rest de la aplicacion
 * @param $http objeto requerido para la invocaciones http de los servicios
 * @param BASE_URL Constante que posee la ruta base de los servicios
 * @constructor
 */
function BookProxy($http, BASE_URL)
{

    /**
     * Metodo que realiza la invocacion del servicio que realiza el login
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.getBooks = function (success, error)
    {
        $http
        .get(BASE_URL + '/listbooks')
        .success(success)
        .error(error);
    };

    this.getBooksSimple = function (success, error)
    {
        $http
        .get(BASE_URL + '/listbookssimple')
        .success(success)
        .error(error);
    };


    /**
     * Metodo que realiza la invocacion del servicio que realiza el login
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.addBook = function (book, success, error)
    {
        $http
            .post(BASE_URL + '/book', book)
            .success(success)
            .error(error);
    };




    /**
     * Metodo que realiza la invocacion del servicio que realiza el login
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.getLocationBook = function (book, success, error)
    {
        $http
            .get(BASE_URL + '/booklocations/' + book.id)
            .success(success)
            .error(error);
    };

    this.getBookMustseeanddos = function (book, success, error)
    {
        $http
            .get(BASE_URL + '/bookmustseeanddos/' + book.id)
            .success(success)
            .error(error);
    };


    this.getBook = function (book, success, error)
    {
        $http
        .get(BASE_URL + '/book/' + book.id)
        .success(success)
        .error(error);
    };
}
