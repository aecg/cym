angular.module('myApp').service('LocationProxy', ['$http', 'BASE_URL', LocationProxy]);

/**
 * Servicio que hace la vez de Proxy para los servicios rest de la aplicacion
 * @param $http objeto requerido para la invocaciones http de los servicios
 * @param BASE_URL Constante que posee la ruta base de los servicios
 * @constructor
 */
function LocationProxy($http, BASE_URL)
{

    /**
     * Metodo que realiza la invocacion del servicio que realiza el login
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.getCountries = function (success, error)
    {
        $http
            .get(BASE_URL + '/countries')
            .success(success)
            .error(error);
    };

    /**
     * Metodo que realiza la invocacion del servicio que realiza el login
     * @param country datos del pais
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.getStates = function (country, success, error)
    {
        $http
            .get(BASE_URL + '/country/' + country.id + '/states')
            .success(success)
            .error(error);
    };

    /**
     * Metodo que realiza la invocacion del servicio que realiza el login
     * @param state datos del estado
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.getLocations = function (state, success, error)
    {
        $http
            .get(BASE_URL + '/state/' + state.id + '/locations')
            .success(success)
            .error(error);
    };

}
