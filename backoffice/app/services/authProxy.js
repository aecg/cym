angular.module('myApp').service('AuthProxy', ['$http', 'BASE_URL', AuthProxy]);

/**
 * Servicio que hace la vez de Proxy para los servicios rest de la aplicacion
 * @param $http objeto requerido para la invocaciones http de los servicios
 * @param BASE_URL Constante que posee la ruta base de los servicios
 * @constructor
 */
function AuthProxy($http, BASE_URL)
{

    /**
     * Metodo que realiza la invocacion del servicio que realiza el login
     * @param user datos del usuario
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.login = function (user, success, error)
    {
        $http
        ({
            method: 'post',
            url: BASE_URL + '/oauth/token',
            headers: {
                'Authorization': 'Basic ZnJvbnRlbmQ6', // Basic frontend:
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            data: {
                'grant_type': 'password',
                'username': user.username,
                'password': user.password
            },
            transformRequest: function (data)
            {
                return $.param(data);
            }
        }).then(success, error);
    };


    /**
     * Metodo qe realiza la invocacion del servicio que realiza el logout de la aplicacion
     * @param success metodo que se ejecuta cuando termina la ejecucion del servicio
     */
    this.logout = function (success)
    {
        $http.get(BASE_URL + '/oauth/revoke-token')
            .success(function ()
            {
                userLogged = false;
                delete $http.defaults.headers.common['Authorization'];
                success();
            });
    };


    /**
     * Metodo que realiza la invocacion del servicio que realiza el registro del usuario
     * @param user datos del usuario a registrar
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.userRegister = function (user, success, error)
    {
        $http
            .post(BASE_URL + '/oauth/register', user)
            .success(success)
            .error(error);
    };


    /**
     * Metodo que invoca el servicio que permite el envio de la contraseña por correo
     * @param user datos del usuario a registrar
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.retrieveUser = function (user, success, error)
    {

        $http
            .post(BASE_URL + '/user/recover_pass', user)
            .success(success)
            .error(error);
    };


    /**
     * Metodo que invoca el servicio que permite el cambio de contraseña
     * @param user datos del usuario a registrar
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.changePassword = function (user, success, error)
    {
        $http
            .put(BASE_URL + '/user/change-password', user)
            .success(success)
            .error(error);
    };
}
