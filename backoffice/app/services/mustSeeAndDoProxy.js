angular.module('myApp').service('MustSeeAndDoProxy', ['$http', 'BASE_URL', MustSeeAndDoProxy]);

/**
 * Servicio que hace la vez de Proxy para los servicios rest de la aplicacion
 * @param $http objeto requerido para la invocaciones http de los servicios
 * @param BASE_URL Constante que posee la ruta base de los servicios
 * @constructor
 */
function MustSeeAndDoProxy($http, BASE_URL)
{

    /**
     * Metodo que realiza la invocacion del servicio que realiza el login
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.getMustSeeAndDoList = function (location, success, error)
    {
        var id = 0;
        if(location.id > 0) {
            id = location.id;
        }
        $http
            .get(BASE_URL + '/location/' + id + '/mustseeanddos')
            .success(success)
            .error(error);
    };

    this.getMustSeeAndDoSimpleList = function (location, success, error)
    {
        var id = 0;
        if(location.id > 0) {
            id = location.id;
        }
        $http
            .get(BASE_URL + '/location/' + id + '/mustseeanddossimple')
            .success(success)
            .error(error);
    };


    /**
     * Metodo que realiza la invocacion del servicio que realiza el login
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.getCatalogs = function ( success, error)
    {
        $http
            .get(BASE_URL + '/catalogs/')
            .success(success)
            .error(error);
    };


    /**
     * Metodo que realiza la invocacion del servicio que realiza el login
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.getDetail = function ( must, success, error)
    {
        $http
            .get(BASE_URL + '/msddetail/msad/' + must.id )
            .success(success)
            .error(error);
    };


    /**
     * Metodo que realiza la invocacion del servicio que realiza el login
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.addMust = function (must, success, error)
    {
        $http
            .post(BASE_URL + '/mustseeanddo', must)
            .success(success)
            .error(error);
    };

    this.getMustSeeAndDo = function ( id, success, error)
    {
        $http
            .get(BASE_URL + '/msad/' + id )
            .success(success)
            .error(error);
    };

}
