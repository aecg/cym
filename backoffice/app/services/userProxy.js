angular.module('myApp').service('UserProxy', ['$http', 'BASE_URL', UserProxy]);

/**
 * Servicio que hace la vez de Proxy para los servicios rest de la aplicacion
 * @param $http objeto requerido para la invocaciones http de los servicios
 * @param BASE_URL Constante que posee la ruta base de los servicios
 * @constructor
 */
function UserProxy($http, BASE_URL)
{

    /**
     * Metodo que realiza la invocacion del servicio que realiza el login
     * @param user datos del usuario
     * @param success metodo que se ejecuta cuando es exitosa la respuesta
     * @param error metodo que se ejecuta cuando da error la repuesta
     */
    this.getInfo = function (user, success, error)
    {
        $http
        .get(BASE_URL + '/users/' + user)
        .success(success)
        .error(error);
    };

}
