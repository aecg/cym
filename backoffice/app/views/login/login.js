'use strict';

angular.module('myApp.login', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider)
    {
        $routeProvider.when('/login', {
            templateUrl: 'views/login/login.html',
            controller: 'LoginCtrl'
        });
    }])

    .controller('LoginCtrl', ['$scope', '$http', '$location', '$rootScope', 'AuthProxy', 'UserProxy', 'MessagesFactory',
        function ($scope, $http, $location, $rootScope, AuthProxy, UserProxy, MessagesFactory)
    {

        $scope.user = {};

        /**
         * Funcion que se ejecuta cuando se da click al boton de login
         */
        $scope.login = function (messages)
        {
            AuthProxy.login($scope.user, success, error);
        };


        /**
         * Metodo que se ejecuta cuando la autenticacion es exitosa
         * @param res respuesta del servicio
         */
        var successGetData = function (res)
        {
            var data = {};
            var user = {};

            data.authError = null;
            data.showHeader = true;
            data.isLogged = true;
            data.claseContent = "app-content";

            user.username = $scope.user.username;
            user.id = res.id;
            user.name = res.name;
            user.email = res.email;
            user.lastname = res.lastname;
            user.image = res.image;

            $rootScope.user = user;
            $rootScope.data = data;

            $location.path('/homeUser');

        };


        /**
         * Metodo que se ejecuta cuando la autenticacion es exitosa
         * @param res respuesta del servicio
         */
        var success = function (res)
        {
            $http.defaults.headers.common['Authorization'] = 'Bearer ' + res.data.access_token;
            UserProxy.getInfo($scope.user.username, successGetData, error);
        };


        /**
         * Metodo que se ejecuta cuando la autenticacion es exitosa
         * @param res respuesta del servicio
         */
        var error = function (res)
        {
            $scope.messages = MessagesFactory.createMessages();
            $scope.messages.error.push('Usuario o contraseña inválido');
        };
    }]);






