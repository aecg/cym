'use strict';

angular.module('myApp.changePassword', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/changePassword/:username', {
        templateUrl: '/views/changePassword/changePassword.html',
        controller: 'ChangePasswordCtrl'
    });
}])

.controller('ChangePasswordCtrl', ['$scope', '$http', '$location', '$rootScope','$routeParams', 'AuthProxy', 'MessagesFactory', function($scope, $http, $location, $rootScope,$routeParams, AuthProxy, MessagesFactory) {

        $rootScope.oldUrl = "changePassword.html";

        $scope.user = {};
        $scope.user.username = $routeParams.username;


        /**
         * Funcion que se ejecuta cuando se da click al boton de login
         */
        $scope.recover = function() {
          AuthProxy.changePassword($scope.user, success, error);
         /// $('#myloader').show();
        };



        /**
         * Metodo que se ejecuta cuando la autenticacion es exitosa
         * @param res respuesta del servicio
         */
        var success = function (res)
        {
           $('#myloader').hide();
            $location.path('/login/');
        };


        /**
         * Metodo que se ejecuta cuando la autenticacion es exitosa
         * @param res respuesta del servicio
         */
        var error = function (res)
        {
          $('#myloader').hide();
          if(res.error == "unauthorized")
          {
            $location.path('/login/');
          }
          else
          {
            console.log('Failed recover pass');
            $scope.messages = MessagesFactory.createMessages();
            $scope.messages.error.push(res.message);
          }

        };
}]);
