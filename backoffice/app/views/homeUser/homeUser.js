'use strict';

angular.module('myApp.homeUser', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider)
    {
        $routeProvider.when('/homeUser', {
            templateUrl: 'views/homeUser/homeUser.html',
            controller: 'HomeUserCtrl'
        });
    }])

    .controller('HomeUserCtrl', ['$scope', '$http', '$location', '$rootScope', 'MessagesFactory', '$filter', function ($scope, $http, $location, $rootScope, MessagesFactory,$filter)
    {

        //console.log($filter('translate')('label-menu-mustSeeAndDo') );
        if($rootScope.data == null || !$rootScope.data.isLogged)
        {
            $location.path('/login');
        }

    }]);

