'use strict';

angular.module('myApp.listBooks', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider)
    {
        $routeProvider.when('/list_books', {
            templateUrl: 'views/listBooks/listBooks.html',
            controller: 'ListBooksCtrl'
        });
    }])

    .controller('ListBooksCtrl', ['$scope', '$http', '$location', '$rootScope', 'MessagesFactory', 'BookProxy', 'IMAGE_BOOKS_URL',
        function ($scope, $http, $location, $rootScope, MessagesFactory, BookProxy, IMAGE_BOOKS_URL)
    {
        $('#myloader').hide();
        $scope.books = [];
        $scope.imageBooksURL = IMAGE_BOOKS_URL;
        $scope.randomImg = '?q='+Math.random();

        function success( res )
        {
            $scope.books = res.books;
            $('#myloader').hide();
        }

        function error( res )
        {
            console.log("Ocurrio un error en la consulta");
            $('#myloader').hide();
        }

        $scope.showBook = function (book)
        {
            $('#myloader').hide();
            $rootScope.book = book;
            $location.path('/update_book');
        };


        function init()
        {
            $('#myloader').show();

            if($rootScope.data != null && $rootScope.data.isLogged)
            {
                BookProxy.getBooksSimple(success, error);
            }
            else
            {

                $location.path('/login');
            }
        }

        init();
    }]);

