'use strict';

angular.module('myApp.home', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider)
    {
        $routeProvider.when('/home', {
            templateUrl: '/views/home/home.html',
            controller: 'HomeCtrl'
        });
    }])

    .controller('HomeCtrl', ['$rootScope', '$location', function ( $rootScope, $location )
    {

        $rootScope.claseContent = "app-content-full";

        $rootScope.oldUrl = "home.html";

        if($rootScope.data == null || !$rootScope.data.isLogged)
        {
            $location.path('/login');
        }
    }]);
