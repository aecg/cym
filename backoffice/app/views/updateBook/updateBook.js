'use strict';

angular.module('myApp.updateBook', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/update_book', {
            templateUrl: 'views/updateBook/updateBook.html',
            controller: 'UpdateBookCtrl'
        });
    }])

    .controller('UpdateBookCtrl', ['PREFIJO', '$scope', '$http', '$location', '$rootScope', 'MessagesFactory', 'LocationProxy', 'BookProxy', 'MustSeeAndDoProxy', function (PREFIJO, $scope, $http, $location, $rootScope, MessagesFactory, LocationProxy, BookProxy, MustSeeAndDoProxy) {

        $('#myloader').hide();
        $scope.states = [];
        $scope.countries = [];
        $scope.locations = [];
        $scope.bookMustseeanddo = [];

        $scope.countrySelected = {};
        $scope.stateSelected = {};
        $scope.citySelected = {};
        $scope.book = $rootScope.book;

        $scope.getStates = function () {
            LocationProxy.getStates($scope.countrySelected, function (res) {
                $scope.states = res.states;
                $scope.citySelected = {};
            }, error)
        };

        $scope.getLocation = function () {
            LocationProxy.getLocations($scope.stateSelected, function (res) {
                $scope.locations = res.locations;
            }, error)
        };

        $scope.toggleSelectionBookMustseeanddo = function toggleSelectionMustseeanddo(mustseeanddo) {
            var idx = $scope.bookMustseeanddo.indexOf(mustseeanddo);

            if (idx > -1) {
                $scope.bookMustseeanddo.splice(idx, 1);
            }
            else {
                $scope.bookMustseeanddo.push(mustseeanddo);
            }
        };

        // toggle selection for a given fruit by name
        $scope.toggleSelectionMustSeeAndDo = function toggleSelectionMustSeeAndDo(mustseeanddo) {
            var idx = $scope.isMustSeeAndDoSelected(mustseeanddo);

            if (idx > -1) {
                $scope.bookMustseeanddo.splice(idx, 1);
            }
            else {
                $scope.bookMustseeanddo.push(mustseeanddo);
            }
        };

        $scope.isMustSeeAndDoSelected = function isMustSeeAndDoSelected(mustseeanddo) {
            var idx = -1;
            for( var i = 0; i < $scope.bookMustseeanddo.length; i++) {
                if($scope.bookMustseeanddo[i].id == mustseeanddo.id) {
                    idx = i;
                    break;
                }
            }

            return idx;
        };

        $scope.removeImage = function removeImage() {
            document.getElementById('imageBook').value = '';
            $scope.book.image = null;
        };

        function error(res) {
            console.log("Error:"+res.message);
            $scope.messages = MessagesFactory.createMessages();
            $scope.messages.error.push("Ha ocurrido un error");
            $('#myloader').hide();
            $("html, body").animate({ scrollTop: 0 }, "slow");
        }

        $scope.getMustseeanddoByLocationId = function () {
            if($scope.citySelected.id != null) {
                MustSeeAndDoProxy.getMustSeeAndDoSimpleList($scope.citySelected, function (res) {
                    $scope.mustSeeAndDoSimpleList = res.mustseeanddo;
                }, error);
            }
        };

        $scope.addBook = function () {
            $('#myloader').show();
            $scope.messages = MessagesFactory.createMessages();

            var obj = {};
            obj.book = $scope.book;
            obj.mustseeanddos = $scope.bookMustseeanddo;

            if (obj.book.name === '' || obj.book.name.length > 255) {
                $scope.messages.error.push('El nombre del libro no puede exceder los 255 caracteres');
            }
            if (obj.book.description === '' || obj.book.description.length > 1000) {
                $scope.messages.error.push('La descripción del libro no puede exceder los 1000 caracteres');
            }
            if (obj.book.image === undefined || obj.book.image === ''  || obj.book.image === null ) {
                $scope.messages.error.push('Debe especificar una imagen para el libro');
            }

            if ($scope.messages.error.length === 0) {
                BookProxy.addBook(obj, function (res) {
                    $scope.mustseeanddos = [];
                    $scope.states = [];

                    $scope.stateSelected = {};
                    $scope.citySelected = {};

                    $scope.messages = MessagesFactory.createMessages();
                    $scope.messages.success.push("Se ha guardado la información");
                    $( "#tab_1" ).addClass( "active" );
                    $( "#tab_2" ).removeClass( "active" );
                    $( "#list_1" ).addClass( "active" );
                    $( "#list_2" ).removeClass( "active" );

                    $scope.form.$setPristine();
                    $('#myloader').hide();
                    document.getElementById('imageBook').value = '';
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }, error);
            }
            else {
                $('#myloader').hide();
                $("html, body").animate({ scrollTop: 0 }, "slow");
            }

        };

        function init() {
            $('#myloader').show();
            if($rootScope.data != null && $rootScope.data.isLogged) {
                LocationProxy.getCountries(function (res) {
                    $scope.countries = res.countries;
                    $scope.stateSelected = {};
                    $scope.citySelected = {};

                    BookProxy.getBook($scope.book, function (res2) {
                        $scope.book = res2;

                        BookProxy.getBookMustseeanddos($scope.book, function (res3) {
                            $scope.bookMustseeanddo = res3.mustseeanddo;
                            $('#myloader').hide();
                    	    $("html, body").animate({ scrollTop: 0 }, "slow");
                        }, error);
                    }, error);
                }, error);
            }
            else {
                $location.path('/login');
            }
        }
        
        $scope.getQR = function (word) {
            $scope.must.url_qr = PREFIJO + word;
        };

        $scope.setImage = function () {
            var image = null;

            image = document.getElementById('imageBook').files[0];

            if (image != null) {

                if (image.size <= 1024000) {
                    var reader = new FileReader();

                    reader.onloadend = function (e) {

                        var extension = image.name.split(".")[1];

                        if (( extension == 'jpg') || (extension == 'jpeg') || (extension == 'png')) {
                            var data = e.target.result;
                            $scope.book.image = window.btoa(data);
                            $scope.messages = null;
                            $scope.$apply();
                        }
                        else {
                            console.log('Error loading image');
                            $scope.messages = MessagesFactory.createMessages();
                            $scope.messages.error.push("Los formatos permitidos para la imagen son: jpg, jpeg y png");
                            $scope.$apply();
                            document.getElementById('imageBook').value = '';
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        }

                    };

                    reader.readAsBinaryString(image);
                }
                else {
                    console.log('Error loading image');
                    $scope.messages = MessagesFactory.createMessages();
                    $scope.messages.error.push("Tamaño máximo permitido del archivo 1Mb");
                    $scope.$apply();
                    document.getElementById('imageBook').value = '';
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }

            }
        };

        init();

    }]);

