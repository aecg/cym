angular.module('myApp').
  directive('ngHeader', function() {
    return {
      restrict: 'E',
      templateUrl: 'views/header/header.html'
    };
  })
.directive('ngHeaderLoggeado', function() {
  return {
    restrict: 'E',
    templateUrl: 'views/header/header_loggeado.html'
  };
});
