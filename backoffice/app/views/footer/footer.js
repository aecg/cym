angular.module('myApp').
  directive('ngFooter', function() {
    return {
      restrict: 'E',
      templateUrl: 'views/footer/footer.html'
    };
  });
