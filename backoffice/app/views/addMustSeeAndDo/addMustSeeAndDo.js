'use strict';

angular.module('myApp.addMustSeeAnddo', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/add_mustseeanddos', {
            templateUrl: 'views/addMustSeeAndDo/addMustSeeAndDo.html',
            controller: 'AddMustSeeAndDo'
        });
    }])

    .controller('AddMustSeeAndDo', ['PREFIJO', '$scope', '$http', '$location', '$rootScope', 'MessagesFactory', 'LocationProxy', 'MustSeeAndDoProxy', function (PREFIJO, $scope, $http, $location, $rootScope, MessagesFactory, LocationProxy, MustSeeAndDoProxy) {

        $('#myloader').hide();
        $scope.states = [];
        $scope.countries = [];
        $scope.locations = [];
        $scope.details = [];
        $scope.catalogs = [];

        $scope.countrySelected = {};
        $scope.stateSelected = {};
        $scope.citySelected = {};
        $scope.catalogSelected = {};
        $scope.prefijo = PREFIJO;

        $scope.must = {};
        $scope.must.ar_code = {};
        $scope.must.qr_code = {};

        $scope.getStates = function () {
            LocationProxy.getStates($scope.countrySelected, function (res) {
                $scope.states = res.states;
                $scope.citySelected = {};
            }, error)
        };

        $scope.getLocation = function () {
            LocationProxy.getLocations($scope.stateSelected, function (res) {
                $scope.locations = res.locations;
            }, error)
        };

        // toggle selection for a given fruit by name
        $scope.toggleSelectionDetail = function toggleSelectionDetail(detail) {
            var idx = $scope.details.indexOf(detail);

            // is currently selected
            if (idx > -1) {
                $scope.details.splice(idx, 1);
            }
            else {
                $scope.details.push(detail);
            }
        };

        // toggle selection for a given fruit by name
        $scope.removeMustARImageAugmented = function removeMustARImageAugmented() {
            document.getElementById('mustARImageAugmented').value = '';
            $scope.must.ar_code.image_display = null;
        };

        $scope.removeMustARBinaryWTC = function removeMustARBinaryWTC() {
            document.getElementById('mustARBinaryWTC').value = '';
            $scope.must.ar_code.image_binary = null;
        };

        $scope.removeMustARImageReference = function removeMustARImageReference() {
            document.getElementById('mustARImageReference').value = '';
            $scope.must.ar_code.image_preview = null;
        };

        $scope.removeMustARImage360 = function removeMustARImage360() {
            document.getElementById('mustARImage360').value = '';
            $scope.must.ar_code.image_360 = null;
        };

        $scope.removeMustSeeAndDoImage = function removeMustSeeAndDoImage() {
            document.getElementById('imageMustSeeAndDo').value = '';
            $scope.must.image_reference = null;
        };

        function error(res) {
            console.log("Error:"+res.message);
            $scope.messages = MessagesFactory.createMessages();
            $scope.messages.error.push("Ha ocurrido un error");
            $('#myloader').hide();
            $("html, body").animate({ scrollTop: 0 }, "slow");
        }

        $scope.addDetail = function () {
            $scope.messages = MessagesFactory.createMessages();
            var existe = false;

            var obj = angular.copy($scope.detail);
            if (obj.title === '') {
                $scope.messages.error.push('El título del detalle es obligatorio');
            }
            if (obj.title.length > 255) {
                $scope.messages.error.push('El título del detalle no puede exceder los 255 caracteres');
            }
            if (obj.summary === '') {
                $scope.messages.error.push('La descripción del detalle es obligatoria');
            }
            if (obj.summary.length > 1000) {
                $scope.messages.error.push('La descripción del detalle no puede exceder los 1000 caracteres');
            }

            $scope.detail = {};
            $scope.details.push( obj );
            if ($scope.messages.error.length > 0) {
                $("html, body").animate({ scrollTop: 0 }, "slow");
            }
        };

        $scope.addMust = function () {
            $('#myloader').show();
            $scope.messages = MessagesFactory.createMessages();

            var obj = {};

            $scope.must.location_id = $scope.citySelected.id;
            $scope.must.catalog_id = $scope.catalogSelected.id;
            obj.must = $scope.must;
            obj.must.realities = $scope.imagesAR;
            obj.detail = $scope.details;

            if (obj.must.title === '') {
                $scope.messages.error.push('El título del Qué ver y qué hacer es obligatorio');
            }
            if (obj.must.title.length > 255) {
                $scope.messages.error.push('El título del Qué ver y qué hacer no puede exceder los 255 caracteres');
            }
            if (obj.must.summary === '') {
                $scope.messages.error.push('La descripción del Qué ver y qué hacer es obligatoria');
            }
            if (obj.must.summary.length > 1000) {
                $scope.messages.error.push('La descripción del Qué ver y qué hacer no puede exceder los 1000 caracteres');
            }

            if (angular.isUndefined(obj.must.qr) || (obj.must.qr == 0)) {
                obj.must.qr = 0;
                obj.must.qr_code = null;
            }
            else {
                obj.must.qr_code.title = PREFIJO + obj.must.title;
                if (obj.must.qr_code.summary.length == 0) {
                    $scope.messages.error.push('Debe introducir el texto del Código QR');
                }
                if (obj.must.qr_code.summary.length > 1000) {
                    $scope.messages.error.push('El texto del Código QR no puede exceder los 1000 caracteres');
                }
            }

            if (angular.isUndefined(obj.must.ar) || (obj.must.ar == 0)) {
                obj.must.ar = 0;
                obj.must.ar_code.image_display = null;
                obj.must.ar_code.image_binary = null;
                obj.must.ar_code.image_preview = null;
            }
            else {
                console.log('obj.must.ar_code.is_360::'+obj.must.ar_code.is_360);
                if ((obj.must.ar_code.is_360 == undefined) || (obj.must.ar_code.is_360 == 0)) {
                    if (obj.must.ar_code.image_display == null) {
                        $scope.messages.error.push('Debe especificar la imagen aumentada de la imagen AR');
                    }
                    if (obj.must.ar_code.image_binary == null) {
                        $scope.messages.error.push('Debe especificar el archivo WTC binario de la imagen AR');
                    }
                    if (obj.must.ar_code.image_preview == null) {
                        $scope.messages.error.push('Debe especificar la imagen de referencia de la imagen AR');
                    }
                }
                else {
                    if (obj.must.ar_code.image_360 == null) {
                        $scope.messages.error.push('Debe especificar la imagen panorámica (360º) de la imagen AR');
                    }
                }
            }

            if (angular.isUndefined(obj.must.image_reference) || (obj.must.image_reference == null)) {
                $scope.messages.error.push('Debe especificar la imagen fija o de referencia del sitio turístico');
            }

            if ($scope.messages.error.length === 0) {
                MustSeeAndDoProxy.addMust(obj, function (res) {
                    $scope.locations = [];
                    $scope.details = [];
                    $scope.states = [];

                    $scope.countrySelected = {};
                    $scope.stateSelected = {};
                    $scope.citySelected = {};
                    $scope.catalogSelected = {};
                    $scope.must = {};

                    $scope.messages = MessagesFactory.createMessages();
                    $scope.messages.success.push("Se ha guardado la información");
                    $("#tab_1").addClass("active");
                    $("#tab_2").removeClass("active");
                    $("#list_1").addClass("active");
                    $("#list_2").removeClass("active");

                    $scope.form.$setPristine();
                    $('#myloader').hide();
                    //console.log("Se ha guardado el Qué ver y qué hacer con exito")
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }, error);
            }
            else {
                $('#myloader').hide();
                $("html, body").animate({ scrollTop: 0 }, "slow");
            }
        };

        /**
         * Metodo que valida el tamaño de la imagen del proyecto y su extension
         */
        $scope.setImage = function () {
            var image = null;

            image = document.getElementById('imageMustSeeAndDo').files[0];

            if (image != null) {

                if (image.size <= 1024000) {
                    var reader = new FileReader();

                    reader.onloadend = function (e) {

                        var extension = image.name.split('.').pop();

                        if (( extension == 'jpg') || (extension == 'jpeg') || (extension == 'png')) {
                            var data = e.target.result;
                            $scope.must.image_reference = window.btoa(data);
                            $scope.messages = null;
                            $scope.$apply();
                        }
                        else {
                            console.log('Error loading image');
                            $scope.messages = MessagesFactory.createMessages();
                            $scope.messages.error.push("Los formatos permitidos para la imagen son: jpg, jpeg y png");
                            $scope.$apply();
                            document.getElementById('imageMustSeeAndDo').value = '';
    	                    $("html, body").animate({ scrollTop: 0 }, "slow");
                        }
                    };
                    reader.readAsBinaryString(image);
                }
                else {
                    console.log('Error loading image');
                    $scope.messages = MessagesFactory.createMessages();
                    $scope.messages.error.push("Tamaño máximo permitido del archivo 1Mb");
                    $scope.$apply();
                    document.getElementById('imageMustSeeAndDo').value = '';
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }
            }
        };

        /**
         * Metodo que valida el tamaño de la imagen del proyecto y su extension
         */
        $scope.setARImageAugmented = function () {
            var image = null;

            image = document.getElementById('mustARImageAugmented').files[0];

            if (image != null) {

                if (image.size <= 1024000) {
                    var reader = new FileReader();

                    reader.onloadend = function (e) {

                        var extension = image.name.split('.').pop();

                        if (( extension == 'jpg') || (extension == 'jpeg') || (extension == 'png')) {
                            var data = e.target.result;
                            var obj = {};
                            obj.image = window.btoa(data);
                            $scope.must.ar_code.image_display = window.btoa(data);
                            $scope.messages = null;
                            $scope.$apply();
                        }
                        else {
                            console.log('Error loading image');
                            $scope.messages = MessagesFactory.createMessages();
                            $scope.messages.error.push("Los formatos permitidos para la imagen son: jpg, jpeg y png");
                            $scope.$apply();
                            document.getElementById('mustARImageAugmented').value = '';
                    	    $("html, body").animate({ scrollTop: 0 }, "slow");
                        }
                    };

                    reader.readAsBinaryString(image);
                }
                else {
                    console.log('Error loading image');
                    $scope.messages = MessagesFactory.createMessages();
                    $scope.messages.error.push("Tamaño máximo permitido del archivo 1Mb");
                    $scope.$apply();
                    document.getElementById('mustARImageAugmented').value = '';
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }
            }
        };

        $scope.setARBinaryWTC = function () {
            var image = null;

            image = document.getElementById('mustARBinaryWTC').files[0];

            if (image != null) {

                if (image.size <= 1024000) {
                    var reader = new FileReader();

                    reader.onloadend = function (e) {

                        var extension = image.name.split('.').pop();

                        if (( extension == 'wtc')) {
                            var data = e.target.result;
                            var obj = {};
                            obj.image = window.btoa(data);
                            $scope.must.ar_code.image_binary = window.btoa(data);
                            $scope.messages = null;
                            $scope.$apply();
                        }
                        else {
                            console.log('Error loading binary');
                            $scope.messages = MessagesFactory.createMessages();
                            $scope.messages.error.push("Los formatos permitidos para el archivo es: wtc");
                            $scope.$apply();
                            document.getElementById('mustARBinaryWTC').value = '';
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        }

                    };

                    reader.readAsBinaryString(image);
                }
                else {
                    console.log('Error loading binary');
                    $scope.messages = MessagesFactory.createMessages();
                    $scope.messages.error.push("Tamaño máximo permitido del archivo 1Mb");
                    $scope.$apply();
                    document.getElementById('mustARBinaryWTC').value = '';
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }

            }
        };

        $scope.setARImageReference = function () {
            var image = null;

            image = document.getElementById('mustARImageReference').files[0];

            if (image != null) {

                if (image.size <= 1024000) {
                    var reader = new FileReader();

                    reader.onloadend = function (e) {

                        var extension = image.name.split('.').pop();

                        if (( extension == 'jpg') || (extension == 'jpeg') || (extension == 'png')) {
                            var data = e.target.result;
                            var obj = {};
                            obj.image = window.btoa(data);
                            $scope.must.ar_code.image_preview = window.btoa(data);
                            $scope.messages = null;
                            $scope.$apply();
                        }
                        else {
                            console.log('Error loading image');
                            $scope.messages = MessagesFactory.createMessages();
                            $scope.messages.error.push("Los formatos permitidos para la imagen son: jpg, jpeg y png");
                            $scope.$apply();
                            document.getElementById('mustARImageReference').value = '';
                    	    $("html, body").animate({ scrollTop: 0 }, "slow");
                        }

                    };

                    reader.readAsBinaryString(image);
                }
                else
                {
                    console.log('Error loading image');
                    $scope.messages = MessagesFactory.createMessages();
                    $scope.messages.error.push("Tamaño máximo permitido del archivo 1Mb");
                    $scope.$apply();
                    document.getElementById('mustARImageReference').value = '';
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }

            }
        };

        $scope.setARImage360 = function () {
            var image = null;

            image = document.getElementById('mustARImage360').files[0];

            if (image != null) {

                console.log('setARImage360::image.size::'+image.size);

                if (image.size <= 1024000) {
                    var reader = new FileReader();

                    reader.onloadend = function (e) {

                        var extension = image.name.split('.').pop();

                        if (( extension == 'jpg') || (extension == 'jpeg') || (extension == 'png')) {
                            var data = e.target.result;
                            var obj = {};
                            obj.image = window.btoa(data);
                            $scope.must.ar_code.image_360 = window.btoa(data);
                            $scope.messages = null;
                            $scope.$apply();
                        }
                        else {
                            console.log('Error loading image');
                            $scope.messages = MessagesFactory.createMessages();
                            $scope.messages.error.push("Los formatos permitidos para la imagen son: jpg, jpeg y png");
                            $scope.$apply();
                            document.getElementById('mustARImage360').value = '';
                    	    $("html, body").animate({ scrollTop: 0 }, "slow");
                        }
                    };

                    reader.readAsBinaryString(image);
                }
                else
                {
                    console.log('Error loading image');
                    $scope.messages = MessagesFactory.createMessages();
                    $scope.messages.error.push("Tamaño máximo permitido del archivo 1Mb");
                    $scope.$apply();
                    document.getElementById('mustARImage360').value = '';
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }
            }
        };

        $scope.changeMustQR = function () {
            if (angular.isUndefined($scope.must.qr) || ($scope.must.qr == 0)) {
                $scope.must.qr_code.summary = '';
            }
        }

        function init() {

            $('#myloader').show();
            if ($rootScope.data != null && $rootScope.data.isLogged) {
                LocationProxy.getCountries(function (res) {
                    $scope.countries = res.countries;
                    $scope.stateSelected = {};
                    $scope.citySelected = {};
                    $scope.catalogSelected = {};
                    MustSeeAndDoProxy.getCatalogs(function (res) {
                        $scope.catalogs = res.catalogs;
                        $('#myloader').hide();
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                    }, error);
                }, error);


            }
            else {
                $location.path('/login');
            }
        }

        init();
    }]);

