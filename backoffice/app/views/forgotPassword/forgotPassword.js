'use strict';

angular.module('myApp.forgotPassword', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/forgotPassword', {
        templateUrl: '/views/forgotPassword/forgotPassword.html',
        controller: 'ForgotPassword'
    });
}])

.controller('ForgotPassword', ['$scope', '$http', '$location', '$rootScope', 'AuthProxy', 'MessagesFactory',
    function($scope, $http, $location, $rootScope, AuthProxy, MessagesFactory) {

        $rootScope.oldUrl = "forgotPassword.html";

        $scope.user = {};
        $rootScope.isLogged = false;
        $scope.authError = null;

        /**
         * Funcion que se ejecuta cuando se da click al boton de login
         */
        $scope.recover = function() {

          AuthProxy.retrieveUser($scope.user, success, error);
        };



        /**
         * Metodo que se ejecuta cuando la autenticacion es exitosa
         * @param res respuesta del servicio
         */
        var success = function (res)
        {
            $scope.messages = MessagesFactory.createMessages();
            $scope.messages.success.push("Se ha enviado la informacion a su correo");
        };


        /**
         * Metodo que se ejecuta cuando la autenticacion es exitosa
         * @param res respuesta del servicio
         */
        var error = function (res)
        {
            console.log('Failed recover pass');
            $scope.messages = MessagesFactory.createMessages();
            $scope.messages.error.push(res.message);

        };
}]);
