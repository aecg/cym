'use strict';

angular.module('myApp.listMustSeeAndDo', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider)
    {
        $routeProvider.when('/list_mustseeanddo', {
            templateUrl: 'views/listMustSeeAndDo/listMustSeeAndDo.html',
            controller: 'ListMustSeeAndDoCtrl'
        });
    }])

    .controller('ListMustSeeAndDoCtrl', ['$scope', '$http', '$location', '$rootScope', 'MessagesFactory', 'LocationProxy', 'MustSeeAndDoProxy', 'IMAGE_MSADS_URL', 
            function ($scope, $http, $location, $rootScope, MessagesFactory, LocationProxy, MustSeeAndDoProxy, IMAGE_MSADS_URL)
    {
        $('#myloader').hide();
        $scope.openedSearch = true;
        $scope.states = [];
        $scope.countries = [];
        $scope.locations = [];
        $scope.locationsBook = [];
        $scope.mustSeeAndDoList = [];

        $scope.countrySelected = {};
        $scope.stateSelected = {};
        $scope.citySelected = {};
        $scope.imageMsadsURL = IMAGE_MSADS_URL;
        $scope.randomImg = '?q='+Math.random();

        $scope.getStates = function ()
        {
            LocationProxy.getStates($scope.countrySelected, function (res)
            {
                $scope.states = res.states;
                $scope.citySelected = {};
            }, error)
        };


        $scope.getLocation = function ()
        {
            LocationProxy.getLocations($scope.stateSelected, function (res)
            {
                $scope.locations = res.locations;
            }, error)
        };


        function error(res)
        {
            console.log("Error consultando los paises");
            $scope.messages = MessagesFactory.createMessages();
            $scope.messages.error.push("Ha ocurrido un error");
            $('#myloader').hide();
        }


        /**
         * Funcion que se ejecuta cuando se da click al boton de login
         */
        $scope.openSearch = function ()
        {
            $scope.openedSearch = !$scope.openedSearch;
        };


        /**
         * Funcion que se ejecuta cuando se da click al boton de login
         */
        $scope.findMust = function ()
        {
            MustSeeAndDoProxy.getMustSeeAndDoSimpleList($scope.citySelected, function (res)
            {
                $scope.mustSeeAndDoList = res.mustseeanddo;
                $('#myloader').hide();

            }, error);
        };



        /**
         * Funcion que se ejecuta cuando se da click al boton de login
         */
        $scope.showMust = function (must) {
            MustSeeAndDoProxy.getMustSeeAndDo(must.id, function (res) {
                $rootScope.must = res;
                $('#myloader').hide();
                $location.path('/update_mustseeanddo');
            }, error);
        };

        function init()
        {
            $('#myloader').show();

            if($rootScope.data != null && $rootScope.data.isLogged)
            {
                LocationProxy.getCountries(function (res)
                {
                    $scope.countries = res.countries;
                    $scope.stateSelected = {};
                    $scope.citySelected = {};
                    $scope.findMust();
                }, error);
            }
            else
            {

                $location.path('/login');
            }
        }

        init();

    }]);

